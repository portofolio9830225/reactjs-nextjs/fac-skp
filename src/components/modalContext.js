import React from 'react';
import PropTypes from 'prop-types';
import useModal from './useModal';
import Modal from './PortalModal'; // eslint-disable-line import/no-cycle

const ModalContext = React.createContext();
const { Provider } = ModalContext;

const ModalProvider = ({ children }) => {
	const { modal, handleModal, modalContent } = useModal();
	return (
		<Provider value={{ modal, handleModal, modalContent }}>
			<Modal />
			{children}
		</Provider>
	);
};

ModalProvider.propTypes = {
	children: PropTypes.node,
};
ModalProvider.defaultProps = {
	children: null,
};
export { ModalContext, ModalProvider };
