import React from 'react';
import DataTable, { createTheme } from 'react-data-table-component';

createTheme(
	'custom_dark',
	{
		background: {
			default: '#242731',
		},
	},
	'dark',
);

const DarkDataTable = (rest) => {
	return <DataTable {...rest} />;
};

export default DarkDataTable;
