import React, { useRef, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import useDarkMode from '../../hooks/useDarkMode';
import Button from '../bootstrap/Button';
import FormGroup from '../bootstrap/forms/FormGroup';
import InputGroup, { InputGroupText } from '../bootstrap/forms/InputGroup';
import showNotification from '../extras/showNotification';
import CustomSelect from './CustomSelect';
import { getaActiveRoles } from '../../helpers/helpers';
import AreaModule from '../../modules/AreaModule';

const NewParameterCity = ({
	initialValues,
	isReset,
	setReset,
	isReadOnly,
	onChange,
	area,
	index_area,
}) => {
	const { darkModeStatus } = useDarkMode();
	const { default_segment_code } = getaActiveRoles();

	const [data, setData] = useState([]);
	const count = useRef(0);

	const [list_city, setListCity] = useState([]);

	const handleCustomAdd = () => {
		if (!area) {
			showNotification(
				'Information',
				"can't add an area if the area is not selected",
				'danger',
			);
			return;
		}

		if (list_city.length === 0) {
			showNotification('Information', "city list does not exist. can't add city", 'danger');
			return;
		}

		if (data.length === list_city.length) {
			showNotification(
				'Information',
				'has reached the maximum city limit that can be added',
				'danger',
			);
			return;
		}

		const new_data = [...data];
		new_data.push({ key: count.current, city: null });
		setData(new_data);

		count.current += 1;
	};

	const handleCustomRemove = (index) => {
		const new_data = [...data];
		new_data.splice(index, 1);
		setData(new_data);

		if (onChange) {
			const result = getFormatValue(new_data);
			onChange(result, index_area);
		}
	};

	const onChangeCity = (e, index) => {
		const new_data = [...data];

		// find index
		const find_index = new_data.findIndex((find) => find.city?.value === e.value);

		// if it is the same as the previously selected data, the process will not continue
		if (find_index === index) {
			return;
		}

		// if the selected data already exists, then the process will not continue
		if (find_index !== -1) {
			showNotification(
				'Information',
				`${e.label}, the city has been selected, try choosing another one`,
				'danger',
			);
			return;
		}

		// set new data
		new_data[index] = {
			...new_data.at(index),
			city: e,
		};
		setData(new_data);

		if (onChange) {
			const result = getFormatValue(new_data);
			onChange(result, index_area);
		}
	};

	useEffect(() => {
		if (area !== null) {
			fetchDataArea();
		}

		return <div>{/* */}</div>;
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [area]);

	const fetchDataArea = async () => {
		const query = `segment_code=${default_segment_code}&area_code=${area}`;
		return AreaModule.readSelectCity(query)
			.then((response) => {
				if (Array.isArray(response)) {
					setListCity(response);
				}
			})
			.catch(() => {})
			.finally(() => {});
	};

	useEffect(() => {
		if (isReset) {
			setData([]);
			count.current = 0;

			setReset(false);
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [isReset]);

	useEffect(() => {
		if (initialValues.length !== 0 && data.length === 0) {
			const { value_data, value_count } = getFormatSelect(initialValues);

			setData(value_data);
			count.current = value_count;
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [initialValues]);

	const getFormatSelect = (params) => {
		const _data = [];

		params.forEach((item, index) => {
			let new_data = { key: index, area: null };

			if (item.city_code) {
				new_data = {
					...new_data,
					city: {
						value: item.city_code,
						label: item.city_name,
						detail: {
							city_code: item.city_code,
							city_name: item.city_name,
						},
					},
				};
			}

			_data.push(new_data);
		});

		return {
			value_data: _data,
			value_count: _data.length,
		};
	};

	const getFormatValue = (params) => {
		const result = params
			.map((item) => {
				let new_value = {};

				if (item.city) {
					new_value = { ...item.city?.detail };
				}

				return new_value;
			})
			.filter((item) => Object.keys(item).length !== 0);
		return result;
	};

	return (
		<div>
			{!isReadOnly && area && (
				<div className='row mb-1'>
					<div className='col-md-6'>
						<Button
							icon='Add'
							color='info'
							type='button'
							isLight={darkModeStatus}
							onClick={handleCustomAdd}
							style={{ minHeight: '2.95rem' }}>
							Add City
						</Button>
					</div>
				</div>
			)}
			<div className='row'>
				<div className='col-md-12'>
					{data.map((item, index) => (
						<div key={'div-'.concat(item.key)}>
							<InputGroup key={'input-group-'.concat(item.key)} className='mb-1'>
								<Button
									key={'button-remove-'.concat(item.key)}
									icon='Clear'
									color='danger'
									type='button'
									isLight={darkModeStatus}
									style={{ minHeight: '2.95rem' }}
									onClick={() => handleCustomRemove(index)}
									isDisable={isReadOnly}
								/>
								<InputGroupText key={'igt-'.concat(item.key)}>
									City {index + 1}
								</InputGroupText>
								<FormGroup key={'fg'.concat(item.key)} className='col-md-6'>
									<CustomSelect
										key={'cs'.concat(item.key)}
										options={list_city}
										onChange={(e) => onChangeCity(e, index)}
										value={item.city}
										isDisable={isReadOnly}
									/>
								</FormGroup>
							</InputGroup>
						</div>
					))}
				</div>
			</div>
		</div>
	);
};

NewParameterCity.propTypes = {
	onChange: PropTypes.func,
	initialValues: PropTypes.instanceOf(Array),
	isReset: PropTypes.bool,
	setReset: PropTypes.func,
	isReadOnly: PropTypes.bool,
	area: PropTypes.string,
	index_area: PropTypes.number,
};
NewParameterCity.defaultProps = {
	onChange: null,
	initialValues: [],
	isReset: false,
	setReset: null,
	isReadOnly: false,
	area: null,
	index_area: 0,
};

export default NewParameterCity;
