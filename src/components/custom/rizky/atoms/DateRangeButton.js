import moment from 'moment/moment';
import { DateRangePicker } from 'react-date-range';
import React, { useReducer, useCallback, useState, useRef } from 'react';
import useEventOutside from '@omtanke/react-use-event-outside';
import Button from '../../../bootstrap/Button';

export default (props) => {
	// eslint-disable-next-line react/prop-types
	const { label, onChange, onReset } = props;

	const buttonRef = useRef(null);
	const today = new Date();
	const tomorrow = new Date();

	// Add 1 Day
	tomorrow.setDate(today.getDate() + 1);

	const initialState = {
		hasFilled: false,
		dateRange: [
			{
				startDate: '',
				endDate: '',
				key: 'selection',
				enabled: false,
			},
		],
	};

	const [state, updateState] = useReducer(
		(states, updates) => ({ ...states, ...updates }),
		initialState,
	);

	const onDateRangeChange = (item) => {
		updateState({
			dateRange: [item.selection],
		});
	};

	const ref = useRef(null);
	const [isOpen, setIsOpen] = useState(false);

	const closeMenu = useCallback(() => {
		const { dateRange } = state;
		if (!dateRange[0].enabled) {
			//
		} else {
			setTimeout(() => {
				setIsOpen(false);
				dateRange[0].enabled = false;
				updateState({
					dateRange,
				});
				if (
					onChange &&
					typeof onChange === 'function' &&
					state.dateRange[0].startDate &&
					state.dateRange[0].endDate
				) {
					onChange(state.dateRange[0]);
				}
			}, 150);
		}
	}, [state, onChange]);

	useEventOutside(ref, 'mousedown', closeMenu);

	const onToggleDateRange = () => {
		const { dateRange } = state;
		if (dateRange[0].enabled === false) {
			setIsOpen(true);
			dateRange[0].enabled = true;
			updateState({
				dateRange,
				hasFilled: true,
			});
		} else {
			//
		}
	};

	const onResetValue = () => {
		if (
			onReset &&
			typeof onReset === 'function' &&
			state.dateRange[0].startDate &&
			state.dateRange[0].endDate
		) {
			const dateRange = [
				{
					startDate: today,
					endDate: tomorrow,
					key: 'selection',
					enabled: false,
				},
			];

			updateState({
				dateRange,
				hasFilled: false,
			});

			onReset({
				startDate: null,
				endDate: null,
				key: 'selection',
				enabled: false,
			});
		}
	};

	return (
		// eslint-disable-next-line react/jsx-props-no-spreading
		<div {...props} className='d-flex align-items-center' style={{ position: 'relative' }}>
			<div style={{ marginRight: '10px' }}>{label || 'Select date'}</div>
			<div>
				<Button color='light' onClick={() => onToggleDateRange()} ref={buttonRef}>
					{state.dateRange[0].startDate && state.dateRange[0].endDate
						? `${moment(state.dateRange[0].startDate).format('DD MMM YYYY')} - ${moment(
								state.dateRange[0].endDate,
						  ).format('DD MMM YYYY')}`
						: 'Select date'}
				</Button>
				{state.dateRange[0].enabled && isOpen ? (
					<div
						style={{
							position: 'absolute',
							marginTop: '5px',
							right: 0,
							zIndex: 999,
							backgroundColor: '#333',
							border: '1px solid gray',
						}}
						ref={ref}>
						<DateRangePicker
							direction='horizontal'
							showSelectionPreview
							onChange={(item) => onDateRangeChange(item)}
							moveRangeOnFirstSelection={false}
							months={2}
							ranges={state.dateRange}
							rangeColors={['#7C6DD7']}
						/>
					</div>
				) : null}
			</div>
			{state.hasFilled ? (
				<div style={{ marginLeft: '10px' }}>
					<Button
						icon='close'
						isOutline
						type='button'
						color='danger'
						onClick={() => onResetValue()}>
						Reset
					</Button>
				</div>
			) : null}
		</div>
	);
};
