

const FormSearch = (dt) => {
	const { handleSearch } = dt;
	const [listAccount, setListAccount] = useState([]);
	const [listYear, setListYear] = useState([]);

	const onSubmit = (values, { setSubmitting, setStatus, setErrors }) => {
		try {
			handleSearch(values);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	const initialValues = {
		datee:'',
		store_code:''
	}
	const fetchAccount = async () => {
		return masterNkaModule.list().then((res) => {
			setListAccount(res.foundData);
		});
	}

	const fetchYear = async () => {
		trading_term_pointModule.listYear().then((res) => {
			setListYear(res.foundData)
		})
	}

	useEffect(() => {
		fetchAccount()
		fetchYear()
	}, []);

	return (
		<Formik
			enableReinitialize
			initialValues={{ ...initialValues }}
			onSubmit={onSubmit}>
			{(formikField) => {
				return (
					<Form className='mb-4'>
						<div className='row'>
							<div className='col-md-2'>
								<Select
									id='store_code'
									placeholder='Select Account'
									onChange={formikField.handleChange}
									value={formikField.values.store_code}
									list={listAccount}
								/>
							</div>
							<div className='col-md-2'>
								<Select
									id='datee'
									placeholder='Select Year'
									onChange={formikField.handleChange}
									value={formikField.values.datee}
									list={listYear}
								/>
							</div>
							<div className='col-md-2'>
								<div className='row'>
										<Button
										className='col-md-5'
											icon='search'
											type='submit'
											color='success'>
											Filter
										</Button>
									<div className='col-md-6'>
										{
											(formikField.values.date || formikField.values.store_code) ?
												<Button
													type='reset'
													icon='close'
													onClick={() => {
														formikField.resetForm({ values: initialValues })
														handleSearch()
													}}
													color='primary'>
													Clear
												</Button>
												: <div />}
									</div>
								</div>

							</div>

						</div>
					</Form>
				);
			}}
		</Formik >
	);
}