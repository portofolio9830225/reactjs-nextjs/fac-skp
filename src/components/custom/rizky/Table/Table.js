/* eslint-disable eslint-comments/disable-enable-pair */
/* eslint-disable prettier/prettier */
/* eslint-disable react/react-in-jsx-scope */
import './table.css';
import { convertToRupiah } from '../../../../helpers/helpers';

const Table = (dt) => {
	const { data, column, title, type } = dt;
	return (
		<>
			{' '}
			<div className='form-label mt-5'>
				<b className='pt-3'>{title}</b>
			</div>
			<div className='table-responsive'>
				<table className='table table-bordered'>
					<thead>
						<tr style={{ backgroundColor: '#e9ecef' }}>
							{column.map((item, i) => (
								<TableHeadItem index={i} item={item} />
							))}
						</tr>
					</thead>
					<tbody>
						{data.map((item, i) => (
							<TableRow
								item={item}
								column={column}
								type={type}
								index={i}
								totalRow={data.length}
							/>
						))}
					</tbody>
				</table>
			</div>
		</>
	);
};

const TableHeadItem = (dt) => {
	const { item, index } = dt;
	if (index === 0) {
		return <th width='18%'>{item.heading}</th>;
	}
	return <th className='text-end'>{item.heading}</th>;
};
const TableRow = (dt) => {
	const { item, column, type, index, totalRow } = dt;

	return (
		<tr style={{ backgroundColor: totalRow - 1 === index && '#e9ecef' }}>
			{column.map((columnItem) => {
				if (columnItem.sum) {
					const result = item[`${columnItem.value}`].reduce(sum, 0);
					if (type === 'nominal') {
						return <th>{convertToRupiah(result)}</th>;
					}
					return <th>{result} %</th>;
				}
				if (columnItem.index) {
					if (type === 'nominal') {
						return (
							<td className='text-end'>
								{item[`${columnItem.value}`][columnItem.index] == 0
									? '-'
									: convertToRupiah(
											item[`${columnItem.value}`][columnItem.index],
									  )}
							</td>
						);
					}
					return (
						<td className='text-end'>
							{item[`${columnItem.value}`][columnItem.index] == 0
								? '0.00 '
								: item[`${columnItem.value}`][columnItem.index]}{' '}
							%
						</td>
					);
				}
				return <th>{item[`${columnItem.value}`]}</th>;
			})}
		</tr>
	);
};

const sum = (acc, a) => {
	return acc + a;
};

export default Table;
