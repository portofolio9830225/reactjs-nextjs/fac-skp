import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import useDarkMode from '../../hooks/useDarkMode';
import Button from '../bootstrap/Button';
import FormGroup from '../bootstrap/forms/FormGroup';
import InputGroup, { InputGroupText } from '../bootstrap/forms/InputGroup';
import CustomSelect from './CustomSelect';
import { getActiveRoles } from '../../helpers/helpers';
import AreaModule from '../../modules/AreaModule';
import showNotification from '../extras/showNotification';
import Spinner from '../bootstrap/Spinner';
import CustomParameterCity from './CustomParameterCity';

const CustomParameterArea = ({ valueData, regional, isReadOnly, onChange, indexRegional }) => {
	const { darkModeStatus } = useDarkMode();
	const { default_segment_code } = getActiveRoles();

	const [data, setData] = useState(valueData);
	const count = useRef(valueData.length);

	const [tempRegional, setTempRegional] = useState(null);
	const [listArea, setListArea] = useState([]);

	const [loading, setLoading] = useState(false);

	const handleAdd = () => {
		if (listArea.length === 0) {
			showNotification('Information', "area list does not exist. can't add area", 'danger');
			return;
		}

		if (data.length === listArea.length) {
			showNotification(
				'Information',
				'has reached the maximum area limit that can be added',
				'danger',
			);
			return;
		}

		const new_data = [...data];
		new_data.push({ key: count.current });
		setData(new_data);

		const new_values = new_data;
		onChange(new_values, indexRegional);

		count.current += 1;
	};

	const handleRemove = (index) => {
		const new_data = [...data];
		new_data.splice(index, 1);
		setData(new_data);

		const new_values = new_data;
		onChange(new_values);
	};

	const onChangeArea = (e, index) => {
		const new_data = [...data];

		const find_index = new_data.findIndex((find) => find?.area?.value === e.value);

		if (find_index === index) {
			return;
		}

		if (find_index !== -1) {
			showNotification(
				'Information',
				`${e.label}, the region has been selected, try choosing another one`,
				'danger',
			);
			return;
		}

		new_data[index] = { ...new_data[index], area: e };
		setData(new_data);

		const new_values = new_data;
		onChange(new_values, indexRegional);
	};

	const onChangeCity = () => {};

	// const getFormatSelectCity = () => {
	// 	return [];
	// };

	// const getFormatValueCity = () => {};

	useEffect(() => {
		if (regional !== tempRegional) {
			fetchDataArea();
			setTempRegional(regional);

			setData([]);
			count.current = 0;
		}
		fetchDataArea();

		return <div>{/* */}</div>;
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [regional]);

	const fetchDataArea = async () => {
		setLoading(true);

		const query = `segment_code=${default_segment_code}&regional_code=${regional}`;
		return AreaModule.readSelect(query)
			.then((response) => {
				if (Array.isArray(response)) {
					setListArea(response);
				}
			})
			.catch(() => {
				setListArea([]);
			})
			.finally(() => {
				setLoading(false);
			});
	};

	return (
		<div className='my-1 ps-5'>
			<div className='row'>
				<div className='col-md-6'>
					{loading ? (
						<Button color='info' isLight={darkModeStatus}>
							<Spinner isSmall inButton='onlyIcon' /> Loading
						</Button>
					) : (
						<Button
							icon='Add'
							color='info'
							type='button'
							isLight={darkModeStatus}
							onClick={handleAdd}
							style={{ minHeight: '2.95rem' }}>
							Add Area
						</Button>
					)}
				</div>
			</div>
			<div className='row'>
				<div className='col-md-12'>
					{data.map((item, index) => (
						<div key={'div-'.concat(item.key)}>
							<div className='col-md-12 border-bottom my-1' />
							<InputGroup key={'ig-'.concat(item.key)}>
								<Button
									key={'btn-remove-'.concat(item.key)}
									icon='Clear'
									color='danger'
									type='button'
									isLight={darkModeStatus}
									style={{ minHeight: '2.95rem' }}
									onClick={() => handleRemove(index)}
								/>
								<InputGroupText key={'igt-'.concat(item.keys)}>
									Area {index + 1}
								</InputGroupText>
								<FormGroup
									key={'fg-'.concat(item.key)}
									className='col-md-4 col-sm-8'>
									<CustomSelect
										options={listArea}
										isDisable={isReadOnly}
										value={item?.area}
										onChange={(e) => onChangeArea(e, index)}
									/>
								</FormGroup>
							</InputGroup>
							{item?.area && (
								<div>
									<CustomParameterCity
										key={'param-city-'.concat(item.key)}
										area={item?.area?.value}
										indexArea={index}
										onChange={onChangeCity}
									/>
								</div>
							)}
						</div>
					))}
				</div>
			</div>
		</div>
	);
};

CustomParameterArea.propTypes = {
	valueData: PropTypes.instanceOf(Array),
	regional: PropTypes.string,
	isReadOnly: PropTypes.bool,
	onChange: PropTypes.func,
	indexRegional: PropTypes.number,
};
CustomParameterArea.defaultProps = {
	valueData: [],
	regional: null,
	isReadOnly: false,
	onChange: () => {},
	indexRegional: -1,
};

export default CustomParameterArea;
