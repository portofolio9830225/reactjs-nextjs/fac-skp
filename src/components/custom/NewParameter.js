import React, { useRef, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import useDarkMode from '../../hooks/useDarkMode';
import Button from '../bootstrap/Button';
import FormGroup from '../bootstrap/forms/FormGroup';
import InputGroup, { InputGroupText } from '../bootstrap/forms/InputGroup';
import showNotification from '../extras/showNotification';
import CustomSelect from './CustomSelect';
import NewParameterRegional from './NewParameterRegional';
import UserModule from '../../modules/UserModule';
import { getaActiveRoles } from '../../helpers/helpers';

const NewParameter = ({
	initialValues,
	list_role,
	list_regional,
	isReset,
	setReset,
	isReadOnly,
	onChange,
	user_value,
	init,
	setInit,
}) => {
	const { darkModeStatus } = useDarkMode();
	const { default_segment_code } = getaActiveRoles();

	const [data, setData] = useState([]);
	const count = useRef(0);

	const [data_main, setDataMain] = useState([]);
	const [select_main, setSelectMain] = useState();

	const [list_parent, setListParent] = useState([]);

	const handleCustomAdd = () => {
		if (list_role.length === 0) {
			showNotification('Information', "role list does not exist. can't add role", 'danger');
			return;
		}

		if (data.length === list_role.length) {
			showNotification(
				'Information',
				'has reached the maximum role limit that can be added',
				'danger',
			);
			return;
		}

		const new_data = [...data];
		new_data.push({ key: count.current, role: null, parent: null });
		setData(new_data);

		const new_parent = [...list_parent];
		new_parent.push({ data: [], loading: false });
		setListParent(new_parent);

		const new_data_main = [...data_main];
		new_data_main.push({ value: 'null', label: 'null' });
		setDataMain(new_data_main);

		count.current += 1;
	};

	const handleCustomRemove = (index) => {
		const new_data = [...data];
		new_data.splice(index, 1);
		setData(new_data);

		const new_parent = [...list_parent];
		new_parent.splice(index, 1);
		setListParent(new_parent);

		const new_data_main = [...data_main];
		new_data_main.splice(index, 1);
		setDataMain(new_data_main);

		setSelectMain(null);

		if (onChange) {
			const result = getFormatValue(new_data, select_main);
			onChange(result);
		}
	};

	const onChangeRole = (e, index) => {
		const new_data = [...data];

		// find index
		const find_index = new_data.findIndex((find) => find.role?.value === e.value);

		// if it is the same as the previously selected data, the process will not continue
		if (find_index === index) {
			return;
		}

		// if the selected data already exists, then the process will not continue
		if (find_index !== -1) {
			showNotification(
				'Information',
				`${e.label}, the role has been selected, try choosing another one`,
				'danger',
			);
			return;
		}

		// set new data
		new_data[index] = {
			...new_data.at(index),
			role: e,
		};
		setData(new_data);

		const new_main = [...data_main];
		new_main[index] = e;
		setDataMain(new_main);

		// load data parent
		fetchDataParent(e.value, index);

		if (onChange) {
			const result = getFormatValue(new_data, select_main);
			onChange(result);
		}
	};

	const onChangeParent = (e, index) => {
		const new_data = [...data];
		new_data[index] = {
			...new_data.at(index),
			parent: e,
		};
		setData(new_data);

		if (onChange) {
			const result = getFormatValue(new_data, select_main);
			onChange(result);
		}
	};

	const onCustomMain = (e) => {
		if (e.value === 'null') {
			showNotification('Information', "can't choose an empty role", 'danger');
			return;
		}
		setSelectMain(e);

		if (onChange) {
			const new_data = [...data];
			const result = getFormatValue(new_data, e);
			onChange(result);
		}
	};

	const onChangeRegional = (values, index) => {
		const new_data = [...data];

		if (values.length === 0) {
			delete new_data[index].regional;
		} else {
			new_data[index] = {
				...new_data.at(index),
				regional: values,
			};
		}
		setData(new_data);

		if (onChange) {
			const result = getFormatValue(new_data, select_main);
			onChange(result);
		}
	};

	useEffect(() => {
		if (isReset) {
			setData([]);
			setDataMain([]);
			setSelectMain(null);
			count.current = 0;

			setReset(false);
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [isReset]);

	useEffect(() => {
		if (initialValues.length !== 0 && init) {
			setInit(false);
			getFormatSelect(initialValues).then((response) => {
				const {
					value_count,
					value_data,
					value_list_main,
					value_select_main,
					value_list_parent,
				} = response;

				setListParent(value_list_parent);
				setData(value_data);
				setDataMain(value_list_main);
				setSelectMain(value_select_main);
				count.current = value_count;
			});
		}

		return <div>{/* */}</div>;
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [initialValues]);

	const getFormatSelect = (params) => {
		return new Promise((resolve, reject) => {
			try {
				let _select_main = null;
				const _list_main = [];
				const _list_parent = [];
				const _data = [];

				const _promises = [];

				params.forEach((item, index) => {
					let new_data = { key: index, role: null, parent: null };

					if (item.role_code) {
						new_data = {
							...new_data,
							role: {
								value: item.role_code,
								label: `(${item.role_code}) ${item.role_name}`,
								detail: {
									role_code: item.role_code,
									role_name: item.role_name,
									segment_code: item.segment_code,
									segment_name: item.segment_name,
								},
							},
						};

						// add to list main
						_list_main.push({
							value: item.role_code,
							label: `(${item.role_code}) ${item.role_name}`,
						});

						// set selected role
						if (item.is_default) {
							_select_main = new_data.role;
						}

						// add to list parent
						_list_parent.push({ data: [], loading: false });

						const query = `segment_code=${default_segment_code}&role_code=${item.role_code}&username=${user_value}`;
						_promises.push(
							UserModule.readSelect(query).then((response) => {
								_list_parent[index] = { ..._list_parent.at(index), data: response };
							}),
						);
					}

					if (item.parent_code) {
						new_data = {
							...new_data,
							parent: {
								value: item.parent_code,
								label: `(${item.parent_code}) ${item.parent_name}`,
								detail: {
									parent_code: item.parent_code,
									parent_name: item.parent_name,
								},
							},
						};
					}

					if (item.regional) {
						new_data = {
							...new_data,
							regional: item.regional,
						};
					}

					_data.push(new_data);
				});

				Promise.all(_promises).then(() => {
					resolve({
						value_select_main: _select_main,
						value_list_main: _list_main,
						value_data: _data,
						value_count: _data.length,
						value_list_parent: _list_parent,
					});
				});
			} catch (e) {
				reject(new Error({ error: true }));
			}
		});
	};

	const getFormatValue = (params, main) => {
		const result = params
			.map((item) => {
				let new_value = {};

				if (item.role) {
					new_value = {
						...new_value,
						...item.role?.detail,
						is_default: item.role.value === main?.value,
					};
				}

				if (item.parent) {
					new_value = {
						...new_value,
						...item.parent?.detail,
						is_default: item.role.value === main?.value,
					};
				}

				if (item.regional) {
					new_value = { ...new_value, regional: item.regional };
				}

				return new_value;
			})
			.filter((item) => Object.keys(item).length !== 0);
		return result;
	};

	const fetchDataParent = async (role, index) => {
		const new_parent = [...list_parent];
		new_parent[index] = { ...new_parent.at(index), loading: true };
		setListParent(new_parent);

		const query = `segment_code=${default_segment_code}&role_code=${role}`;
		return UserModule.readSelect(query)
			.then((response) => {
				if (Array.isArray(response)) {
					const new_parent_value = [...list_parent];
					new_parent_value[index] = {
						...new_parent_value.at(index),
						data: response,
						loading: false,
					};
					setListParent(new_parent_value);
				}
			})
			.catch(() => {
				const new_parent_value = [...list_parent];
				new_parent_value[index] = { ...new_parent_value.at(index), loading: false };
				setListParent(new_parent_value);
			})
			.finally(() => {});
	};

	return (
		<div>
			<div className='row mb-1'>
				<div className='col-md-6'>
					<InputGroup>
						<InputGroupText>Main Role</InputGroupText>
						<FormGroup className='col-md-8'>
							<CustomSelect
								options={data_main}
								onChange={onCustomMain}
								value={select_main}
								isDisable={isReadOnly}
							/>
						</FormGroup>
					</InputGroup>
				</div>
				{!isReadOnly && (
					<div className='col-md-6'>
						<Button
							icon='Add'
							color='info'
							type='button'
							className='float-end'
							isLight={darkModeStatus}
							onClick={handleCustomAdd}
							style={{ minHeight: '2.95rem' }}>
							Add Role
						</Button>
					</div>
				)}
			</div>
			<div className='row'>
				<div className='col-md-12'>
					{data.map((item, index) => (
						<div key={'div-'.concat(item.key)}>
							<div
								key={'div-border-'.concat(item.key)}
								className='col-md-12 border-bottom mb-1'
							/>
							<InputGroup key={'input-group-'.concat(item.key)} className='mb-1'>
								<Button
									key={'button-remove-'.concat(item.key)}
									icon='Clear'
									color='danger'
									type='button'
									isLight={darkModeStatus}
									style={{ minHeight: '2.95rem' }}
									onClick={() => handleCustomRemove(index)}
									isDisable={isReadOnly}
								/>
								<InputGroupText key={'igt-role-'.concat(item.key)}>
									Role {index + 1}
								</InputGroupText>
								<FormGroup key={'fg-role'.concat(item.key)} className='col-md-4'>
									<CustomSelect
										key={'cs-role'.concat(item.key)}
										options={list_role}
										onChange={(e) => onChangeRole(e, index)}
										value={item.role}
										isDisable={isReadOnly}
									/>
								</FormGroup>
								<InputGroupText key={'igt-parent'.concat(item.key)}>
									Parent
								</InputGroupText>
								<FormGroup key={'fg-parent'.concat(item.key)} className='col-md-4'>
									<CustomSelect
										key={'cs-parent'.concat(item.key)}
										options={list_parent.at(index)?.data ?? []}
										onChange={(e) => onChangeParent(e, index)}
										value={item.parent}
										isDisable={
											isReadOnly ||
											(list_parent.at(index)?.data?.length === 0 ?? [])
										}
										isSearchable={
											list_parent.at(index)?.data.length > 7 ?? false
										}
										isLoading={list_parent.at(index)?.loading ?? false}
										placeholder={
											list_parent.at(index)?.data?.length === 0 ?? false
												? 'Not found'
												: 'Select'
										}
									/>
								</FormGroup>
							</InputGroup>
							<div key={'div-regional-'.concat(item.key)} className='mb-1 ps-5'>
								<NewParameterRegional
									key={'parameter-regional-'.concat(item.key)}
									onChange={onChangeRegional}
									list_regional={list_regional}
									initialValues={item.regional}
									parameter={item.role?.value}
									index_parameter={index}
									isReadOnly={isReadOnly}
								/>
							</div>
						</div>
					))}
				</div>
			</div>
		</div>
	);
};

NewParameter.propTypes = {
	onChange: PropTypes.func,
	initialValues: PropTypes.instanceOf(Array),
	list_role: PropTypes.instanceOf(Array),
	list_regional: PropTypes.instanceOf(Array),
	isReset: PropTypes.bool,
	setReset: PropTypes.func,
	isReadOnly: PropTypes.bool,
	user_value: PropTypes.string,
	init: PropTypes.bool,
	setInit: PropTypes.func,
};
NewParameter.defaultProps = {
	onChange: null,
	initialValues: [],
	list_role: [],
	list_regional: [],
	isReset: false,
	setReset: null,
	isReadOnly: false,
	user_value: null,
	init: true,
	setInit: () => {},
};

export default NewParameter;
