import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Button from '../bootstrap/Button';
import FormGroup from '../bootstrap/forms/FormGroup';
import InputGroup, { InputGroupText } from '../bootstrap/forms/InputGroup';
import CustomSelect from './CustomSelect';
import useDarkMode from '../../hooks/useDarkMode';
import CustomParameterRegional from './CustomParameterRegional';
import { getActiveRoles } from '../../helpers/helpers';
import UserModule from '../../modules/UserModule';
import showNotification from '../extras/showNotification';

const CustomParameterRole = ({
	isReadOnly,
	indexRole,
	listRole,
	listRegional,
	handleRemove,
	valueRole,
	onChange,
	dataRole,
}) => {
	const { darkModeStatus } = useDarkMode();
	const { default_segment_code } = getActiveRoles();

	const [listParent, setListParent] = useState([]);
	const [selectParent, setSelectParent] = useState(null);
	const [selectRole, setSelectRole] = useState(valueRole);
	const [loadingParent, setLoadingParent] = useState(false);

	const onChangeRole = (e) => {
		const find_index = dataRole.findIndex((find) => find?.role?.value === e?.value);

		if (find_index === indexRole) {
			return;
		}

		if (find_index !== -1) {
			showNotification(
				'Information',
				`${e.label}, the role has been selected, try choosing another one`,
				'danger',
			);
			return;
		}

		const new_object = { role: e, parent: selectParent };
		onChange(new_object, indexRole);

		setSelectRole(e);
		fetchDataParent(e.value);
	};

	const onChangeParent = (e) => {
		if (e?.value === selectParent?.value) {
			return;
		}

		const new_object = { role: selectRole, parent: e };
		onChange(new_object, indexRole);
		setSelectParent(e);
	};

	const onChangeRegional = () => {};

	const fetchDataParent = async (role) => {
		setLoadingParent(true);
		const query = `segment_code=${default_segment_code}&role_code=${role}`;
		return UserModule.readSelect(query)
			.then((response) => {
				setListParent(response);
			})
			.catch(() => {
				setListParent([]);
			})
			.finally(() => {
				setSelectParent(null);
				setLoadingParent(false);
			});
	};

	return (
		<div className='my-1'>
			<InputGroup>
				<Button
					color='danger'
					type='button'
					icon='Clear'
					onClick={() => handleRemove(indexRole)}
					style={{ minHeight: '2.95rem' }}
					isLight={darkModeStatus}
				/>
				<InputGroupText>Role {indexRole + 1}</InputGroupText>
				<FormGroup className='col-md-4'>
					<CustomSelect
						options={listRole}
						value={selectRole}
						defaultValue={selectRole}
						onChange={onChangeRole}
					/>
				</FormGroup>
				<InputGroupText>Parent</InputGroupText>
				<FormGroup className='col-md-4'>
					<CustomSelect
						options={listParent}
						value={selectParent}
						defaultValue={selectParent}
						onChange={onChangeParent}
						isLoading={loadingParent}
						isDisable={!selectRole}
						placeholder={!selectRole ? 'User Not Found' : 'Select..'}
					/>
				</FormGroup>
			</InputGroup>
			{selectRole && (
				<div>
					<CustomParameterRegional
						isReadOnly={isReadOnly}
						listRegional={listRegional}
						onChange={onChangeRegional}
					/>
				</div>
			)}
		</div>
	);
};

CustomParameterRole.propTypes = {
	isReadOnly: PropTypes.bool,
	indexRole: PropTypes.number,
	listRole: PropTypes.instanceOf(Array),
	listRegional: PropTypes.instanceOf(Array),
	handleRemove: PropTypes.func,
	valueRole: PropTypes.instanceOf(Object),
	onChange: PropTypes.func,
	dataRole: PropTypes.instanceOf(Array),
};
CustomParameterRole.defaultProps = {
	isReadOnly: false,
	indexRole: -1,
	listRole: [],
	listRegional: [],
	handleRemove: () => {},
	valueRole: null,
	onChange: () => {},
	dataRole: [],
};

export default CustomParameterRole;
