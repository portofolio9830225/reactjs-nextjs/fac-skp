import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import useDarkMode from '../../hooks/useDarkMode';
import AreaModule from '../../modules/AreaModule';
import Button from '../bootstrap/Button';
import InputGroup, { InputGroupText } from '../bootstrap/forms/InputGroup';
import FormGroup from '../bootstrap/forms/FormGroup';
import CustomSelect from './CustomSelect';
import { getActiveRoles } from '../../helpers/helpers';
import showNotification from '../extras/showNotification';

const CustomParameterCity = ({ area, indexArea, valueCity, onChange }) => {
	const { darkModeStatus } = useDarkMode();
	const { default_segment_code } = getActiveRoles();

	const [add, setAdd] = useState(false);
	const [tempArea, setTempArea] = useState(null);
	const [listCity, setListCity] = useState([]);
	const [selectCity, setSelectCity] = useState(valueCity);

	const handleAdd = () => {
		if (listCity.length === 0) {
			showNotification('Information', "city list does not exist. can't add city", 'danger');
			return;
		}

		setAdd(true);
	};

	const handleRemove = () => {
		setAdd(false);
		onChange([], indexArea);
	};

	const onChangeCity = (e) => {
		setSelectCity(e);
		onChange(e, indexArea);
	};

	useEffect(() => {
		if (area !== tempArea) {
			fetchDataCity();
			setTempArea(area);
		}

		return <div>{/* */}</div>;
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [area]);

	const fetchDataCity = async () => {
		const query = `segment_code=${default_segment_code}&area_code=${area}`;
		return AreaModule.readSelectCity(query)
			.then((response) => {
				if (Array.isArray(response)) {
					setListCity(response);
				}
			})
			.catch(() => {
				setListCity([]);
			})
			.finally(() => {});
	};

	return (
		<div className='my-1 ps-5'>
			<div className='row'>
				<div className='col-md-5 col-sm-10'>
					{add ? (
						<InputGroup>
							<Button
								icon='Clear'
								color='danger'
								type='button'
								isLight={darkModeStatus}
								onClick={handleRemove}
								style={{ minHeight: '2.95rem' }}
							/>
							<InputGroupText>City</InputGroupText>
							<FormGroup className='col-md-10 col-sm-10'>
								<CustomSelect
									options={listCity}
									isMulti
									value={selectCity}
									defaultValue={selectCity}
									onChange={onChangeCity}
								/>
							</FormGroup>
						</InputGroup>
					) : (
						<Button
							icon='Add'
							color='info'
							type='button'
							isLight={darkModeStatus}
							onClick={handleAdd}
							style={{ minHeight: '2.95rem' }}>
							Add City
						</Button>
					)}
				</div>
			</div>
		</div>
	);
};

CustomParameterCity.propTypes = {
	area: PropTypes.string,
	indexArea: PropTypes.number,
	valueCity: PropTypes.instanceOf(Array),
	onChange: PropTypes.func,
};
CustomParameterCity.defaultProps = {
	area: null,
	indexArea: -1,
	valueCity: [],
	onChange: () => {},
};

export default CustomParameterCity;
