import React, { useRef, useState } from 'react';
import PropTypes from 'prop-types';
import useDarkMode from '../../hooks/useDarkMode';
import Button from '../bootstrap/Button';
import InputGroup, { InputGroupText } from '../bootstrap/forms/InputGroup';
import FormGroup from '../bootstrap/forms/FormGroup';
import CustomSelect from './CustomSelect';
import CustomParameterArea from './CustomParameterArea';
import showNotification from '../extras/showNotification';

const CustomParameterRegional = ({ valueData, isReadOnly, listRegional, onChange }) => {
	const { darkModeStatus } = useDarkMode();

	const [data, setData] = useState(valueData);
	const count = useRef(valueData.length);

	const handleAdd = () => {
		if (listRegional.length === 0) {
			showNotification(
				'Information',
				"region list does not exist. can't add region",
				'danger',
			);
			return;
		}

		if (data.length === listRegional.length) {
			showNotification(
				'Information',
				'has reached the maximum region limit that can be added',
				'danger',
			);
			return;
		}

		const new_data = [...data];
		new_data.push({ key: count.current });
		setData(new_data);

		const new_values = new_data;
		onChange(new_values);

		count.current += 1;
	};

	const handleRemove = (index) => {
		const new_data = [...data];
		new_data.splice(index, 1);
		setData(new_data);

		const new_values = new_data;
		onChange(new_values);
	};

	const onChangeRegional = (e, index) => {
		const new_data = [...data];

		// find index
		const find_index = new_data.findIndex((find) => find.regional?.value === e.value);

		// if it is the same as the previously selected data, the process will not continue
		if (find_index === index) {
			return;
		}

		// if the selected data already exists, then the process will not continue
		if (find_index !== -1) {
			showNotification(
				'Information',
				`${e.label}, the region has been selected, try choosing another one`,
				'danger',
			);
			return;
		}

		new_data[index] = { ...new_data[index], regional: e };
		setData(new_data);

		const new_values = new_data;
		onChange(new_values);
	};

	const onChangeArea = () => {};

	const getFormatSelectArea = () => {
		return [];
	};

	// const getFormatValueArea = () => {};

	return (
		<div className='my-1 ps-5'>
			<div className='row'>
				<div className='col-md-6'>
					<Button
						icon='Add'
						color='info'
						type='button'
						isLight={darkModeStatus}
						onClick={handleAdd}
						style={{ minHeight: '2.95rem' }}>
						Add Regional
					</Button>
				</div>
			</div>
			<div className='row'>
				<div className='col-md-12'>
					{data.map((item, index) => (
						<div key={'div-'.concat(item.key)}>
							<div
								key={'div-border-'.concat(item.key)}
								className='col-md-12 border-bottom my-1'
							/>
							<InputGroup key={'input-group-'.concat(item.key)}>
								<Button
									key={'button-remove-'.concat(item.key)}
									icon='Clear'
									color='danger'
									type='button'
									isLight={darkModeStatus}
									style={{ minHeight: '2.95rem' }}
									onClick={() => handleRemove(index)}
									isDisable={isReadOnly}
								/>
								<InputGroupText key={'igt-'.concat(item.key)}>
									Regional {index + 1}
								</InputGroupText>
								<FormGroup
									key={'fg-'.concat(item.key)}
									className='col-md-4 col-sm-8'>
									<CustomSelect
										options={listRegional}
										value={item.regional}
										defaultValue={item.regional}
										onChange={(e) => onChangeRegional(e, index)}
									/>
								</FormGroup>
							</InputGroup>
							{item?.regional && (
								<div>
									<CustomParameterArea
										key={'param-area-'.concat(item.key)}
										regional={item?.regional?.value}
										isReadOnly={isReadOnly}
										onChange={onChangeArea}
										indexRegional={index}
										valueData={getFormatSelectArea(item?.area)}
									/>
								</div>
							)}
						</div>
					))}
				</div>
			</div>
		</div>
	);
};

CustomParameterRegional.propTypes = {
	valueData: PropTypes.instanceOf(Array),
	isReadOnly: PropTypes.bool,
	listRegional: PropTypes.instanceOf(Array),
	onChange: PropTypes.func,
};
CustomParameterRegional.defaultProps = {
	valueData: [],
	isReadOnly: false,
	listRegional: [],
	onChange: () => {},
};

export default CustomParameterRegional;
