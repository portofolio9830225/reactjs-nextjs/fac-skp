import React, { useRef, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import useDarkMode from '../../hooks/useDarkMode';
import Button from '../bootstrap/Button';
import FormGroup from '../bootstrap/forms/FormGroup';
import InputGroup, { InputGroupText } from '../bootstrap/forms/InputGroup';
import showNotification from '../extras/showNotification';
import CustomSelect from './CustomSelect';
import NewParameterArea from './NewParameterArea';

const NewParameterRegional = ({
	initialValues,
	list_regional,
	isReset,
	setReset,
	isReadOnly,
	onChange,
	parameter,
	index_parameter,
}) => {
	const { darkModeStatus } = useDarkMode();

	const [data, setData] = useState([]);
	const count = useRef(0);

	const [tempParameter, setTempParameter] = useState();

	const handleCustomAdd = () => {
		if (!parameter) {
			showNotification(
				'Information',
				"can't add an region if the role is not selected",
				'danger',
			);
			return;
		}

		if (list_regional.length === 0) {
			showNotification(
				'Information',
				"region list does not exist. can't add region",
				'danger',
			);
			return;
		}

		if (data.length === list_regional.length) {
			showNotification(
				'Information',
				'has reached the maximum region limit that can be added',
				'danger',
			);
			return;
		}

		const new_data = [...data];
		new_data.push({ key: count.current, regional: null });
		setData(new_data);

		count.current += 1;
	};

	const handleCustomRemove = (index) => {
		const new_data = [...data];
		new_data.splice(index, 1);
		setData(new_data);

		if (onChange) {
			const result = getFormatValue(new_data);
			onChange(result, index_parameter);
		}
	};

	const onChangeRegional = (e, index) => {
		const new_data = [...data];

		// find index
		const find_index = new_data.findIndex((find) => find.regional?.value === e.value);

		// if it is the same as the previously selected data, the process will not continue
		if (find_index === index) {
			return;
		}

		// if the selected data already exists, then the process will not continue
		if (find_index !== -1) {
			showNotification(
				'Information',
				`${e.label}, the region has been selected, try choosing another one`,
				'danger',
			);
			return;
		}

		// set new data
		new_data[index] = {
			...new_data.at(index),
			regional: e,
		};

		setData(new_data);

		if (onChange) {
			const result = getFormatValue(new_data);
			onChange(result, index_parameter);
		}
	};

	const onChangeArea = (values, index) => {
		const new_data = [...data];

		if (values.length === 0) {
			delete new_data[index].area;
		} else {
			new_data[index] = {
				...new_data.at(index),
				area: values,
			};
		}
		setData(new_data);

		if (onChange) {
			const result = getFormatValue(new_data);
			onChange(result, index_parameter);
		}
	};

	useEffect(() => {
		if (parameter !== tempParameter) {
			setTempParameter(parameter);
			setData([]);
			count.current = 0;
		}

		return <div>{/* */}</div>;
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [parameter]);

	useEffect(() => {
		if (isReset) {
			setData([]);
			count.current = 0;

			setReset(false);
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [isReset]);

	useEffect(() => {
		if (initialValues.length !== 0 && data.length === 0) {
			const { value_data, value_count } = getFormatSelect(initialValues);

			setData(value_data);
			count.current = value_count;
		}

		return <div>{/* */}</div>;
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [initialValues]);

	const getFormatSelect = (params) => {
		const _data = [];

		params.forEach((item, index) => {
			let new_data = { key: index, regional: null };

			if (item.regional_code) {
				new_data = {
					...new_data,
					regional: {
						value: item.regional_code,
						label: `(${item.regional_code}) ${item.regional_name}`,
						detail: {
							regional_code: item.regional_code,
							regional_name: item.regional_name,
						},
					},
				};
			}

			if (item.area) {
				new_data = {
					...new_data,
					area: item.area,
				};
			}

			_data.push(new_data);
		});

		return {
			value_data: _data,
			value_count: _data.length,
		};
	};

	const getFormatValue = (params) => {
		const result = params
			.map((item) => {
				let new_value = {};

				if (item.regional) {
					new_value = { ...new_value, ...item.regional?.detail };
				}

				if (item.area) {
					new_value = { ...new_value, area: item.area };
				}

				return new_value;
			})
			.filter((item) => Object.keys(item).length !== 0);
		return result;
	};

	return (
		<div>
			{!isReadOnly && parameter && (
				<div className='row mb-1'>
					<div className='col-md-6'>
						<Button
							icon='Add'
							color='info'
							type='button'
							isLight={darkModeStatus}
							onClick={handleCustomAdd}
							style={{ minHeight: '2.95rem' }}>
							Add Regional
						</Button>
					</div>
				</div>
			)}
			<div className='row'>
				<div className='col-md-12'>
					{data.map((item, index) => (
						<div key={'div-'.concat(item.key)}>
							<div
								key={'div-border-'.concat(item.key)}
								className='col-md-12 border-bottom mb-1'
							/>
							<InputGroup key={'input-group-'.concat(item.key)} className='mb-1'>
								<Button
									key={'button-remove-'.concat(item.key)}
									icon='Clear'
									color='danger'
									type='button'
									isLight={darkModeStatus}
									style={{ minHeight: '2.95rem' }}
									onClick={() => handleCustomRemove(index)}
									isDisable={isReadOnly}
								/>
								<InputGroupText key={'igt-'.concat(item.key)}>
									Regional {index + 1}
								</InputGroupText>
								<FormGroup key={'fg'.concat(item.key)} className='col-md-6'>
									<CustomSelect
										key={'cs'.concat(item.key)}
										options={list_regional}
										onChange={(e) => onChangeRegional(e, index)}
										value={item.regional}
										isDisable={isReadOnly}
									/>
								</FormGroup>
							</InputGroup>
							<div key={'div-area-'.concat(item.key)} className='mb-1 ps-5'>
								<NewParameterArea
									key={'parameter-area-'.concat(item.key)}
									onChange={onChangeArea}
									initialValues={item.area}
									regional={item.regional?.value}
									index_regional={index}
									isReadOnly={isReadOnly}
								/>
							</div>
						</div>
					))}
				</div>
			</div>
		</div>
	);
};

NewParameterRegional.propTypes = {
	onChange: PropTypes.func,
	initialValues: PropTypes.instanceOf(Array),
	list_regional: PropTypes.instanceOf(Array),
	isReset: PropTypes.bool,
	setReset: PropTypes.func,
	isReadOnly: PropTypes.bool,
	parameter: PropTypes.string,
	index_parameter: PropTypes.number,
};
NewParameterRegional.defaultProps = {
	onChange: null,
	initialValues: [],
	list_regional: [],
	isReset: false,
	setReset: null,
	isReadOnly: false,
	parameter: null,
	index_parameter: 0,
};

export default NewParameterRegional;
