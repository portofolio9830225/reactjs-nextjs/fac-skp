import React, { useRef, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import useDarkMode from '../../hooks/useDarkMode';
import Button from '../bootstrap/Button';
import FormGroup from '../bootstrap/forms/FormGroup';
import InputGroup, { InputGroupText } from '../bootstrap/forms/InputGroup';
import showNotification from '../extras/showNotification';
import CustomSelect from './CustomSelect';
import NewParameterCity from './NewParameterCity';
import AreaModule from '../../modules/AreaModule';
import { getaActiveRoles } from '../../helpers/helpers';

const NewParameterArea = ({
	initialValues,
	isReset,
	setReset,
	isReadOnly,
	onChange,
	regional,
	index_regional,
}) => {
	const { darkModeStatus } = useDarkMode();
	const { default_segment_code } = getaActiveRoles();

	const [data, setData] = useState([]);
	const count = useRef(0);

	const [list_area, setListArea] = useState([]);
	const [tempRegional, setTempRegional] = useState();

	const handleCustomAdd = () => {
		if (!regional) {
			showNotification(
				'Information',
				"can't add an area if the region is not selected",
				'danger',
			);
			return;
		}

		if (list_area.length === 0) {
			showNotification('Information', "area list does not exist. can't add area", 'danger');
			return;
		}

		if (data.length === list_area.length) {
			showNotification(
				'Information',
				'has reached the maximum area limit that can be added',
				'danger',
			);
			return;
		}

		const new_data = [...data];
		new_data.push({ key: count.current, area: null });
		setData(new_data);

		count.current += 1;
	};

	const handleCustomRemove = (index) => {
		const new_data = [...data];
		new_data.splice(index, 1);
		setData(new_data);

		if (onChange) {
			const result = getFormatValue(new_data);
			onChange(result, index_regional);
		}
	};

	const onChangeArea = (e, index) => {
		const new_data = [...data];

		// find index
		const find_index = new_data.findIndex((find) => find.area?.value === e.value);

		// if it is the same as the previously selected data, the process will not continue
		if (find_index === index) {
			return;
		}

		// if the selected data already exists, then the process will not continue
		if (find_index !== -1) {
			showNotification(
				'Information',
				`${e.label}, the area has been selected, try choosing another one`,
				'danger',
			);
			return;
		}

		// set new data
		new_data[index] = {
			...new_data.at(index),
			area: e,
		};
		setData(new_data);

		if (onChange) {
			const result = getFormatValue(new_data);
			onChange(result, index_regional);
		}
	};

	const onChangeCity = (values, index) => {
		const new_data = [...data];

		if (values.length === 0) {
			delete new_data[index].city;
		} else {
			new_data[index] = {
				...new_data.at(index),
				city: values,
			};
		}
		setData(new_data);

		if (onChange) {
			const result = getFormatValue(new_data);
			onChange(result, index_regional);
		}
	};

	useEffect(() => {
		if (regional !== tempRegional) {
			fetchDataArea();
			setTempRegional(regional);

			setData([]);
			count.current = 0;
		}

		return <div>{/* */}</div>;
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [regional]);

	const fetchDataArea = async () => {
		const query = `segment_code=${default_segment_code}&regional_code=${regional}`;
		return AreaModule.readSelect(query)
			.then((response) => {
				if (Array.isArray(response)) {
					setListArea(response);
				}
			})
			.catch(() => {})
			.finally(() => {});
	};

	useEffect(() => {
		if (isReset) {
			setData([]);
			count.current = 0;

			setReset(false);
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [isReset]);

	useEffect(() => {
		if (initialValues.length !== 0 && data.length === 0) {
			const { value_data, value_count } = getFormatSelect(initialValues);

			setData(value_data);
			count.current = value_count;
		}

		return <div>{/* */}</div>;
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [initialValues]);

	const getFormatSelect = (params) => {
		const _data = [];

		params.forEach((item, index) => {
			let new_data = { key: index, area: null };

			if (item.area_code) {
				new_data = {
					...new_data,
					area: {
						value: item.area_code,
						label: `(${item.area_code}) ${item.area_name}`,
						detail: {
							area_code: item.area_code,
							area_name: item.area_name,
						},
					},
				};
			}

			if (item.city) {
				new_data = {
					...new_data,
					city: item.city,
				};
			}

			_data.push(new_data);
		});

		return {
			value_data: _data,
			value_count: _data.length,
		};
	};

	const getFormatValue = (params) => {
		const result = params
			.map((item) => {
				let new_value = {};

				if (item.area) {
					new_value = { ...new_value, ...item.area?.detail };
				}

				if (item.city) {
					new_value = { ...new_value, city: item.city };
				}

				return new_value;
			})
			.filter((item) => Object.keys(item).length !== 0);
		return result;
	};

	return (
		<div>
			{!isReadOnly && regional && (
				<div className='row mb-1'>
					<div className='col-md-6'>
						<Button
							icon='Add'
							color='info'
							type='button'
							isLight={darkModeStatus}
							onClick={handleCustomAdd}
							style={{ minHeight: '2.95rem' }}>
							Add Area
						</Button>
					</div>
				</div>
			)}
			<div className='row'>
				<div className='col-md-12'>
					{data.map((item, index) => (
						<div key={'div-'.concat(item.key)}>
							<div
								key={'div-border-'.concat(item.key)}
								className='col-md-12 border-bottom mb-1'
							/>
							<InputGroup key={'input-group-'.concat(item.key)} className='mb-1'>
								<Button
									key={'button-remove-'.concat(item.key)}
									icon='Clear'
									color='danger'
									type='button'
									isLight={darkModeStatus}
									style={{ minHeight: '2.95rem' }}
									onClick={() => handleCustomRemove(index)}
									isDisable={isReadOnly}
								/>
								<InputGroupText key={'igt-'.concat(item.key)}>
									Area {index + 1}
								</InputGroupText>
								<FormGroup key={'fg'.concat(item.key)} className='col-md-6'>
									<CustomSelect
										key={'cs'.concat(item.key)}
										options={list_area}
										onChange={(e) => onChangeArea(e, index)}
										value={item.area}
										isDisable={isReadOnly}
										isSearchable={list_area.length > 7}
									/>
								</FormGroup>
							</InputGroup>
							<div key={'div-city-'.concat(item.key)} className='mb-1 ps-5'>
								<NewParameterCity
									key={'parameter-city-'.concat(item.key)}
									onChange={onChangeCity}
									initialValues={item.city}
									area={item.area?.value}
									index_area={index}
									isReadOnly={isReadOnly}
								/>
							</div>
						</div>
					))}
				</div>
			</div>
		</div>
	);
};

NewParameterArea.propTypes = {
	onChange: PropTypes.func,
	initialValues: PropTypes.instanceOf(Array),
	isReset: PropTypes.bool,
	setReset: PropTypes.func,
	isReadOnly: PropTypes.bool,
	regional: PropTypes.string,
	index_regional: PropTypes.number,
};
NewParameterArea.defaultProps = {
	onChange: null,
	initialValues: [],
	isReset: false,
	setReset: null,
	isReadOnly: false,
	regional: null,
	index_regional: 0,
};

export default NewParameterArea;
