import React, { useEffect, useState } from 'react';
// import PropTypes from 'prop-types';

import { useFormik } from 'formik';
import FileDownload from 'js-file-download';
import * as XLSX from 'xlsx';

import Page from '../../../layout/Page/Page';
import PageWrapper from '../../../layout/PageWrapper/PageWrapper';
import PageLayoutHeader from '../../../pages/common/Headers/PageLayoutHeader';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardTitle,
} from '../../../components/bootstrap/Card';
import Select from '../../../components/bootstrap/forms/Select';
import Button from '../../../components/bootstrap/Button';
import showNotification from '../../../components/extras/showNotification';
import Input from '../../../components/bootstrap/forms/Input';
import Spinner from '../../../components/bootstrap/Spinner';
import Label from '../../../components/bootstrap/forms/Label';
import DarkDataTable from '../../../components/DarkDataTable';

import useDarkMode from '../../../hooks/useDarkMode';

import summaryAccountModule from '../../../modules/sodiq/summary_accountModule';
import claimsModule from '../../../modules/azhar/claimsModule';
import { isEmptyObject } from '../../../helpers/helpers';

const FormSearch = () => {
	const [listYear, setListYear] = useState([]);

	const formik = useFormik({
		initialValues: {
			type: {},
			year: {},
		},
		validate: (values) => {
			if (isEmptyObject(values.type)) {
				showNotification('Warning!', 'Please Entries Type', 'danger');
			}

			if (isEmptyObject(values.year)) {
				showNotification('Warning!', 'Please Entries Year', 'danger');
			}
		},
		onSubmit: (values) => {
			if (!isEmptyObject(values.type) && !isEmptyObject(values.year)) {
				const payload = `type=${values.type}&year=${values.year}`;

				claimsModule
					.downloadExcel(payload)
					.then((res) => {
						if (values.type == 'SKP') {
							FileDownload(res, 'Template Import SKP.xlsx');
						} else if (values.type == 'TT') {
							FileDownload(res, 'Template Import TT.xlsx');
						}
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					});
			}
		},
	});

	const listType = [
		{
			value: 'SKP',
			text: 'SKP',
			label: 'SKP',
		},
		{
			value: 'TT',
			text: 'Trading Terms',
			label: 'Trading Terms',
		},
	];

	const fetchYear = async () => {
		return summaryAccountModule.year().then((res) => {
			setListYear(res.foundData);
		});
	};

	useEffect(() => {
		fetchYear();
	}, []);

	return (
		<form onSubmit={formik.handleSubmit}>
			<div className='row'>
				<div className='col-md-2'>
					<Select
						id='type'
						placeholder='Type'
						onChange={formik.handleChange}
						value={formik.values.type}
						list={listType}
					/>
				</div>
				<div className='col-md-2'>
					<Select
						id='year'
						placeholder='Year'
						onChange={formik.handleChange}
						value={formik.values.year}
						list={listYear}
					/>
				</div>
				<div className='col-md-2'>
					<Button icon='download' color='success' type='submit'>
						Download
					</Button>
				</div>
			</div>
		</form>
	);
};

const FormImport = () => {
	const [label, setLabel] = useState('');
	const [loading, setLoading] = useState(false);
	const [labelLoading, setLabelLoading] = useState('Processing Saved Data...');

	const [preview, setPreview] = useState(false);
	const [dataPreview, setDataPreview] = useState([]);
	const [columnPreview, setColumnPreview] = useState([]);

	const formik = useFormik({
		initialValues: {
			type: {},
			file: '',
		},
		onSubmit: (values) => {
			claimsModule
				.importExcel(values.type, dataPreview)
				.then((res) => {
					if (res.status == 'success') {
						formik.resetForm({
							values: {
								type: {},
								file: '',
							},
						});

						setPreview(false);
						setLoading(false);
						setLabel('');
						showNotification('Success!', 'Success Create', 'success');
					} else {
						showNotification('Warning!', 'Failed Create', 'danger');
					}
				})
				.catch((err) => {
					showNotification('Warning!', err, 'danger');
				});
		},
	});

	const listType = [
		{
			value: 'SKP',
			text: 'SKP',
			label: 'SKP',
		},
		{
			value: 'TT',
			text: 'Trading Terms',
			label: 'Trading Terms',
		},
	];

	const handleUploadFile = (e) => {
		e.preventDefault();

		if (!isEmptyObject(formik.values.type)) {
			if (e.target.files) {
				const f = e.target.files[0];
				if (f.name.split('.').pop() === 'xlsx') {
					setLabel(f.filename);
					setLabelLoading('Processing Reading Data...');
					setLoading(true);

					// Read File
					const reader = new FileReader();
					reader.onload = (ev) => {
						/* Parse data */
						const data = ev.target.result;
						const readedData = XLSX.read(data, { type: 'binary' });

						/* Get first worksheet */
						const wsname = readedData.SheetNames[0];
						const ws = readedData.Sheets[wsname];

						/* Convert array of arrays */
						const dataParse = XLSX.utils.sheet_to_json(ws, {
							header: 1,
							blankrows: false,
						});

						if (formik.values.type == 'SKP') {
							const header = [
								'Nomor SKP',
								'Periode Claim (YYYY/MM/DD)',
								'Promo type',
								'Amount',
								'Distributor',
								'Branch',
								'Invoice number',
								'remark',
							];

							let errHeader = false;
							let message = '';
							const imprData = dataParse[0];

							/* eslint-disable-next-line no-plusplus */
							for (let i = 0; i < header.length; i++) {
								if (imprData[i] != header[i]) {
									errHeader = true;
									message = header[i];
								}
							}

							if (errHeader) {
								setLabel('');
								setLoading(false);
								setLabelLoading('');
								showNotification(
									'Warning!',
									`Please Check Row Data ${message} in ${f.name}`,
									'danger',
								);
							} else {
								const valueData = [];
								dataParse.forEach((item, index) => {
									if (index != 0) {
										valueData.push(item);
									}
								});

								// Validation
								const fieldWajib = [0, 1, 2, 3, 7];

								let checkAll = false;

								valueData.forEach((el, idx) => {
									const row = idx + 1;
									let isErr = true;

									/* eslint-disable-next-line no-plusplus */
									for (let i = 0; i < el.length; i++) {
										if (fieldWajib.includes(i) && isErr) {
											if (
												el[i] === undefined ||
												el[i] === '' ||
												el[i] === null
											) {
												isErr = false;
												checkAll = true;
												message = `${row}, ${i + 1}`;
											}
										}
									}
								});

								if (checkAll) {
									setLabel('');
									setLoading(false);
									setLabelLoading('');
									showNotification(
										'Warning!',
										`Please Check Row Data ${message} in ${f.name}`,
										'danger',
									);
								} else {
									setLoading(false);
									setLabelLoading('');
									setColumnPreview([
										{
											name: 'Nomor SKP',
											selector: (row) => row[0],
											sortable: true,
										},
										{
											name: 'Periode Claim (YYYY-MM-DD)',
											selector: (row) => row[1],
											sortable: true,
										},
										{
											name: 'Promo type',
											selector: (row) => row[2],
											sortable: true,
										},
										{
											name: 'Amount',
											selector: (row) => row[3],
											sortable: true,
										},
										{
											name: 'Distributor',
											selector: (row) => row[4],
											sortable: true,
										},
										{
											name: 'Branch',
											selector: (row) => row[5],
											sortable: true,
										},
										{
											name: 'Invoice number',
											selector: (row) => row[6],
											sortable: true,
										},
										{
											name: 'remark',
											selector: (row) => row[7],
											sortable: true,
										},
									]);
									setDataPreview(valueData);
									setPreview(true);
								}
							}
						}

						if (formik.values.type == 'TT') {
							const header = [
								'Trading Term Point',
								'Periode Claim (YYYY/MM/DD)',
								'Amount',
								'Distributor',
								'Branch',
								'Invoice number',
								'remark',
							];

							let errHeader = false;
							let message = '';
							const imprData = dataParse[0];

							/* eslint-disable-next-line no-plusplus */
							for (let i = 0; i < header.length; i++) {
								if (imprData[i] != header[i]) {
									errHeader = true;
									message = header[i];
								}
							}

							if (errHeader) {
								setLabel('');
								setLoading(false);
								setLabelLoading('');
								showNotification(
									'Warning!',
									`Please Check Row Data ${message} in ${f.name}`,
									'danger',
								);
							} else {
								const valueData = [];
								dataParse.forEach((item, index) => {
									if (index != 0) {
										valueData.push(item);
									}
								});

								// Validation
								const fieldWajib = [0, 1, 2, 6];

								let checkAll = false;

								valueData.forEach((el, idx) => {
									const row = idx + 1;
									let isErr = true;

									/* eslint-disable-next-line no-plusplus */
									for (let i = 0; i < el.length; i++) {
										if (fieldWajib.includes(i) && isErr) {
											if (
												el[i] === undefined ||
												el[i] === '' ||
												el[i] === null
											) {
												isErr = false;
												checkAll = true;
												message = `${row}, ${i + 1}`;
											}
										}
									}
								});

								if (checkAll) {
									setLabel('');
									setLoading(false);
									setLabelLoading('');
									showNotification(
										'Warning!',
										`Please Check Row Data ${message} in ${f.name}`,
										'danger',
									);
								} else {
									setLoading(false);
									setLabelLoading('');
									setColumnPreview([
										{
											name: 'Trading Term Point',
											selector: (row) => row[0],
											sortable: true,
										},
										{
											name: 'Periode Claim (YYYY-MM-DD)',
											selector: (row) => row[1],
											sortable: true,
										},
										{
											name: 'Amount',
											selector: (row) => row[2],
											sortable: true,
										},
										{
											name: 'Distributor',
											selector: (row) => row[3],
											sortable: true,
										},
										{
											name: 'Branch',
											selector: (row) => row[4],
											sortable: true,
										},
										{
											name: 'Invoice number',
											selector: (row) => row[5],
											sortable: true,
										},
										{
											name: 'remark',
											selector: (row) => row[6],
											sortable: true,
										},
									]);
									setDataPreview(valueData);
									setPreview(true);
								}
							}
						}
					};
					reader.readAsBinaryString(f);
				} else {
					showNotification(
						'Warning!',
						`Please Entries File ${f.name} Extension .xlsx`,
						'danger',
					);
				}
			}
		} else {
			showNotification('Warning!', 'Please Entries Type', 'danger');
		}
	};

	const handleReset = () => {
		formik.resetForm({
			values: {
				type: {},
				file: '',
			},
		});

		setPreview(false);
		setLoading(false);
		setLabel('');
	};

	return (
		<form onSubmit={formik.handleSubmit}>
			<div className='col-12 mb-3 g-4'>
				<div className='row'>
					<div className='col-md-2'>
						<Select
							id='type'
							placeholder='Type'
							onChange={formik.handleChange}
							value={formik.values.type}
							list={listType}
						/>
					</div>
					<div className='col-md-4'>
						<Input
							id='file'
							accept='.xlsx'
							type='file'
							onChange={(e) => handleUploadFile(e)}
							onBlur={formik.handleBlur}
							isValid={formik.isValid}
							isTouched={formik.touched.file}
							value={label}
							invalidFeedback={formik.errors.file}
							autoComplete='off'
						/>
					</div>
					<div className='col-md-2'>
						<Button
							style={{ marginLeft: '5px' }}
							icon='refresh'
							type='reset'
							onClick={handleReset}
							color='warning'>
							Reset
						</Button>
					</div>
					<div className='col-md-4' style={{ display: loading ? 'block' : 'none' }}>
						<Spinner className='float-start' color='success' role='status' size='md' />
						<Label
							style={{ marginLeft: '5px', marginTop: '5px' }}
							className='float-start'>
							{labelLoading}
						</Label>
					</div>
				</div>
			</div>
			<div className='mt-1 row' style={{ display: preview ? 'block' : 'none' }}>
				<div className='col-12 mt-4'>
					<TablePreview dataPreviews={dataPreview} columns={columnPreview} />
				</div>
			</div>
			{preview && (
				<div className='col-md-12 '>
					<Button
						icon='Save'
						isOutline
						type='submit'
						color='success'
						className='float-end'>
						Import
					</Button>
				</div>
			)}
		</form>
	);
};

const TablePreview = (dt) => {
	const { dataPreviews, columns } = dt;
	const { darkModeStatus } = useDarkMode();
	const [data, setData] = useState([]);
	const [loading, setLoading] = useState(false);

	useEffect(() => {
		fetchData(dataPreviews, 1);
	}, [dataPreviews]);

	const fetchData = (dataPreview, page, perPage = 10) => {
		setLoading(false);
		const sumRows = perPage * page;
		const start = sumRows - perPage;
		const end = dataPreview.length > sumRows ? sumRows : dataPreview.length;
		const showData = dataPreview.slice(start, end);
		setData(showData);
	};

	const handlePageChange = (page) => {
		fetchData(dataPreviews, page);
	};

	const handlePerRowsChange = (newPerPage, page) => {
		setLoading(true);
		fetchData(dataPreviews, page, newPerPage);
	};

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			progressPending={loading}
			pagination
			paginationServer
			paginationTotalRows={dataPreviews.length}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};

const ImportClaim = () => {
	return (
		<PageWrapper title='Import Claim'>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card className='col-md-12'>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>Download Template</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<div className='col-12 mb-3 g-4'>
							<FormSearch />
						</div>
					</CardBody>
				</Card>
				<Card className='col-md-12'>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>Import Claim</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<FormImport />
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

export default ImportClaim;
