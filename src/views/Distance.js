import React, { useState, useEffect, useMemo, useRef } from 'react';
import PropTypes from 'prop-types';
import { Formik, Form } from 'formik';
import * as yup from 'yup';
import Swal from 'sweetalert2';
import { useTranslation } from 'react-i18next';
import Select from 'react-select';
import moment from 'moment';
import PageWrapper from '../layout/PageWrapper/PageWrapper';
import Page from '../layout/Page/Page';
import PageLayoutHeader from '../pages/common/Headers/PageLayoutHeader';
import Card, { CardBody, CardHeader, CardLabel, CardTitle } from '../components/bootstrap/Card';
import FormGroup from '../components/bootstrap/forms/FormGroup';
import Input from '../components/bootstrap/forms/Input';
import Button from '../components/bootstrap/Button';
import DistanceModule from '../modules/DistanceModule';
import RolesModule from '../modules/RolesModule';
import TypePlanModule from '../modules/TypePlanModule';
import StoreCategoryModule from '../modules/StoreCategoryModule';

import showNotification from '../components/extras/showNotification';
import DarkDataTable from '../components/DarkDataTable';
import useDarkMode from '../hooks/useDarkMode';
import COLORS from '../common/data/enumColors';
import Modal, {
	ModalBody,
	ModalHeader,
	ModalTitle,
	ModalFooter,
} from '../components/bootstrap/Modal';
import { getRequester } from '../helpers/helpers';
import CustomSelect from '../components/CustomSelect';

const react_select_styles = (formikField, darkModeStatus, key) => {
	const select_invalid_styles = {
		control: (base, state) => ({
			...base,
			// state.isFocused can display different borderColor if you need it
			borderColor:
				(state.isFocused && COLORS.SECONDARY.code) ||
				(!state.isFocused && formikField.submitCount > 0 && COLORS.DANGER.code) ||
				(!state.isFocused && formikField.submitCount === 0 && COLORS.LIGHT.code),
			// overwrittes hover style
			'&:hover': {
				borderColor:
					(state.isFocused && COLORS.SECONDARY.code) ||
					(!state.isFocused && formikField.submitCount > 0 && COLORS.DANGER.code),
			},
		}),
		singleValue: (base) => ({
			...base,
			color: darkModeStatus ? COLORS.LIGHT.code : COLORS.DARK.code,
		}),
		input: (base) => ({
			...base,
			color: darkModeStatus ? COLORS.LIGHT.code : COLORS.DARK.code,
		}),
	};
	const select_valid_styles = {
		control: (base, state) => ({
			...base,
			// state.isFocused can display different borderColor if you need it
			borderColor:
				(state.isFocused && COLORS.SECONDARY.code) ||
				(!state.isFocused && formikField.submitCount > 0 && COLORS.SUCCESS.code) ||
				(!state.isFocused && formikField.submitCount === 0 && COLORS.LIGHT.code),

			// overwrittes hover style
			'&:hover': {
				borderColor:
					(state.isFocused && COLORS.SECONDARY.code) ||
					(!state.isFocused && formikField.submitCount > 0 && COLORS.SUCCESS.code),
			},
		}),
		singleValue: (base) => ({
			...base,
			color: darkModeStatus ? COLORS.LIGHT.code : COLORS.DARK.code,
		}),
		input: (base) => ({
			...base,
			color: darkModeStatus ? COLORS.LIGHT.code : COLORS.DARK.code,
		}),
	};
	return typeof formikField.errors[key] === 'undefined'
		? select_valid_styles
		: select_invalid_styles;
};

const react_select_themes = (theme, darkModeStatus) => {
	return {
		...theme,
		colors: darkModeStatus
			? {
					...theme.colors,
					neutral0: COLORS.DARK.code,
					neutral190: COLORS.LIGHT.code,
					primary: COLORS.SUCCESS.code,
					primary25: COLORS.PRIMARY.code,
			  }
			: theme.colors,
	};
};

const handleSubmit = (values, handleReloadData) => {
	DistanceModule.create(values)
		.then((res) => {
			showNotification('Success!', res.status, 'success');
			handleReloadData();
		})
		.catch((err) => {
			showNotification('Warning!', err, 'danger');
			Swal.fire({
				title: err,
				icon: 'error',
				showCancelButton: true,
				showConfirmButton: false,
				cancelButtonText: 'Close',
			});
		});
	return values;
};
const handleSubmitEdit = (values, handleReloadData) => {
	DistanceModule.edit(values)
		.then((res) => {
			showNotification('Success!', res.status, 'success');
			handleReloadData();
		})
		.catch((err) => {
			showNotification('Warning!', err, 'danger');
			Swal.fire({
				title: err,
				icon: 'error',
				showCancelButton: true,
				showConfirmButton: false,
				cancelButtonText: 'Close',
			});
		});
	return values;
};
const handleDelete = (val, handleReloadData) => {
	DistanceModule.delete_(val)
		.then((res) => {
			showNotification('Success!', res.status, 'success');
			handleReloadData();
		})
		.catch((err) => {
			showNotification('Warning!', err, 'danger');
			Swal.fire({
				title: err,
				icon: 'error',
				showCancelButton: true,
				showConfirmButton: false,
				cancelButtonText: 'Close',
			});
		});
	return val;
};

const FormExample = (dt) => {
	const { darkModeStatus } = useDarkMode();
	const { initialValues, handleReloadData, listTypePlan, listStoreCategory, listRoles } = dt;
	let refTypePlan = useRef(0);
	let refRole = useRef(0);
	let refStoreCategory = useRef(0);

	const submitForm = (values, resetForm) => {
		if (
			values.type_plan &&
			values.role &&
			values.store_category &&
			values.checkin_distance &&
			values.checkout_distance &&
			values.read_distance
		) {
			const { username } = getRequester();
			const val = {};
			val.requester = username;
			val.type_code = values.type_plan.value;
			val.type_name = values.type_plan.label;
			val.roles = values.role.label;
			val.roles_code = values.role.value;
			val.store_category_code = values.store_category.value;
			val.store_category_name = values.store_category.label;
			val.checkin_distance = values.checkin_distance;
			val.checkout_distance = values.checkout_distance;
			val.read_distance = values.read_distance;

			return Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'info',
				showCancelButton: true,
				confirmButtonText: 'Yes',
			}).then((result) => {
				if (result.value) {
					handleSubmit(val, handleReloadData);
					const refs = [];
					refs.push(refRole);
					refs.push(refTypePlan);
					refs.push(refStoreCategory);
					resetForm(initialValues);
					resetSelect(refs);
				} else if (result.dismiss === Swal.DismissReason.cancel) {
				}
			});
		}
		return values;
	};

	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		try {
			submitForm(values, resetForm);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	const validationSchema = yup.object({
		type_plan: yup.object({
			value: yup.string().required('Field is required'),
			label: yup.string().required('Field is required'),
		}),
		role: yup.object({
			value: yup.string().required('Field is required'),
			label: yup.string().required('Field is required'),
		}),
		store_category: yup.object({
			value: yup.string().required('Field is required'),
			label: yup.string().required('Field is required'),
		}),
		checkin_distance: yup
			.number()
			.required('Field is required')
			.min(0, 'Please input positive number'),
		checkout_distance: yup
			.number()
			.required('Field is required')
			.min(0, 'Please input positive number'),
		read_distance: yup
			.number()
			.required('Field is required')
			.min(0, 'Please input positive number'),
	});
	return (
		<Formik
			enableReinitialize
			initialValues={{ ...initialValues }}
			onSubmit={onSubmit}
			validationSchema={validationSchema}>
			{(formikField) => {
				return (
					<Form>
						<div className='row mb-4'>
							<div className='col-md-6'>
								<FormGroup id='type_plan' label='Type Plan'>
									<Select
										styles={react_select_styles(
											formikField,
											darkModeStatus,
											'type_plan',
										)}
										theme={(theme) =>
											react_select_themes(theme, darkModeStatus)
										}
										defaultValue={formikField.values.type_plan}
										onBlur={formikField.handleBlur}
										options={listTypePlan}
										onChange={(value) => {
											formikField.setFieldValue('type_plan', value);
										}}
										ref={(ref) => {
											refTypePlan = ref;
											return refTypePlan;
										}}
									/>
								</FormGroup>
								{typeof formikField.errors.type_plan !== 'undefined' &&
									formikField.submitCount > 0 && (
										<div style={{ color: COLORS.DANGER.code }}>
											Field is required
										</div>
									)}
							</div>

							<FormGroup
								id='checkin_distance'
								label='Check-In Distance'
								className='col-md-6'>
								<Input
									onChange={formikField.handleChange}
									onBlur={formikField.handleBlur}
									value={formikField.values.checkin_distance}
									isValid={formikField.isValid}
									isTouched={formikField.touched.checkin_distance}
									invalidFeedback={formikField.errors.checkin_distance}
									autoComplete='off'
									type='number'
								/>
							</FormGroup>
						</div>
						<div className='row mb-4'>
							<div className='col-md-6 mb-4'>
								<FormGroup id='role' label='Role'>
									<Select
										styles={react_select_styles(
											formikField,
											darkModeStatus,
											'role',
										)}
										theme={(theme) =>
											react_select_themes(theme, darkModeStatus)
										}
										defaultValue={formikField.values.role}
										onBlur={formikField.handleBlur}
										options={listRoles}
										onChange={(value) => {
											formikField.setFieldValue('role', value);
										}}
										ref={(ref) => {
											refRole = ref;
											return refRole;
										}}
									/>
								</FormGroup>
								{typeof formikField.errors.role !== 'undefined' &&
									formikField.submitCount > 0 && (
										<div style={{ color: COLORS.DANGER.code }}>
											Field is required
										</div>
									)}
							</div>

							<FormGroup
								id='checkout_distance'
								label='Check-Out Distance'
								className='col-md-6'>
								<Input
									onChange={formikField.handleChange}
									onBlur={formikField.handleBlur}
									value={formikField.values.checkout_distance}
									isValid={formikField.isValid}
									isTouched={formikField.touched.checkout_distance}
									invalidFeedback={formikField.errors.checkout_distance}
									autoComplete='off'
									type='number'
								/>
							</FormGroup>
						</div>
						<div className='row mb-4'>
							<div className='col-md-6 mb-4'>
								<FormGroup id='store_category' label='Store Category'>
									<Select
										styles={react_select_styles(
											formikField,
											darkModeStatus,
											'store_category',
										)}
										theme={(theme) =>
											react_select_themes(theme, darkModeStatus)
										}
										defaultValue={formikField.values.store_category}
										onBlur={formikField.handleBlur}
										options={listStoreCategory}
										onChange={(value) => {
											formikField.setFieldValue('store_category', value);
										}}
										ref={(ref) => {
											refStoreCategory = ref;
											return refStoreCategory;
										}}
									/>
								</FormGroup>
								{typeof formikField.errors.role !== 'undefined' &&
									formikField.submitCount > 0 && (
										<div style={{ color: COLORS.DANGER.code }}>
											Field is required
										</div>
									)}
							</div>
							<FormGroup
								id='read_distance'
								label='Read Distance'
								className='col-md-6'>
								<Input
									onChange={formikField.handleChange}
									onBlur={formikField.handleBlur}
									value={formikField.values.read_distance}
									isValid={formikField.isValid}
									isTouched={formikField.touched.read_distance}
									invalidFeedback={formikField.errors.read_distance}
									autoComplete='off'
									type='number'
								/>
							</FormGroup>
						</div>
						<br />

						<div className='col-md-12 '>
							<Button
								icon='Save'
								isOutline
								type='submit'
								color='success'
								className='float-end'
								isDisable={!formikField.isValid && !!formikField.submitCount}>
								Submit
							</Button>
						</div>
					</Form>
				);
			}}
		</Formik>
	);
};

const resetSelect = (refs) => {
	refs.forEach((ref) => {
		ref.setValue({});
	});
};

const FormExampleEdit = (dt) => {
	const { darkModeStatus } = useDarkMode();
	const { initialValues, listRoles, listStoreCategory, listTypePlan, handleReloadData } = dt;

	const [isOpen, setIsOpen] = useState(false);
	const [isReadOnly, setIsReadOnly] = useState(false);
	const [isAlwaysReadOnly] = useState(true);

	let refTypePlan = useRef(0);
	let refRole = useRef(0);
	let refStoreCategory = useRef(0);

	const submitForm = (values, resetForm) => {
		const {
			type_plan,
			role,
			store_category,
			checkin_distance,
			checkout_distance,
			read_distance,
			id,
		} = values;
		if (
			type_plan &&
			role &&
			store_category &&
			checkin_distance &&
			checkout_distance &&
			read_distance &&
			id
		) {
			const { username } = getRequester();
			const val = {};
			val.requester = username;
			val.type_code = values.type_plan.value;
			val.type_name = values.type_plan.label;
			val.roles = values.role.label;
			val.roles_code = values.role.value;
			val.store_category_code = values.store_category.value;
			val.store_category_name = values.store_category.label;
			val.checkin_distance = values.checkin_distance;
			val.checkout_distance = values.checkout_distance;
			val.read_distance = values.read_distance;
			val._id = id;

			return Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'info',
				showCancelButton: true,
				confirmButtonText: 'Yes',
			}).then((result) => {
				if (result.value) {
					handleSubmitEdit(val, handleReloadData);
					const refs = [];
					refs.push(refRole);
					refs.push(refTypePlan);
					refs.push(refStoreCategory);
					resetForm(initialValues);
				} else if (result.dismiss === Swal.DismissReason.cancel) {
				}
			});
		}
		return values;
	};

	const deleteForm = (id) => {
		if (id) {
			const { username } = getRequester();
			const val = {};
			val._id = id;
			val.requester = username;
			return Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'info',
				showCancelButton: true,
				confirmButtonText: 'Yes',
			}).then((result) => {
				if (result.value) {
					handleDelete(val, handleReloadData);
				} else if (result.dismiss === Swal.DismissReason.cancel) {
				}
			});
		}
		return id;
	};

	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		try {
			delete values.loading;
			submitForm(values, resetForm);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	const validate = (values) => {
		const errors = {};
		if (!values.checkin_distance) {
			errors.checkin_distance = 'Field is required';
		}
		if (values.checkin_distance < 0) {
			errors.checkin_distance = 'Please input positive number';
		}
		if (!values.checkout_distance) {
			errors.checkout_distance = 'Field is required';
		}
		if (values.checkout_distance < 0) {
			errors.checkout_distance = 'Please input positive number';
		}
		if (!values.read_distance) {
			errors.read_distance = 'Field is required';
		}
		if (values.read_distance < 0) {
			errors.read_distance = 'Please input positive number';
		}

		return errors;
	};

	return (
		<>
			<Button
				className='me-2'
				icon='GridOn'
				type='button'
				color='primary'
				onClick={() => {
					setIsOpen(true);
					setIsReadOnly(true);
				}}
			/>

			<Button
				className='me-2'
				icon='Edit'
				type='button'
				color='success'
				onClick={() => {
					setIsOpen(true);
					setIsReadOnly(false);
				}}
			/>

			<Button
				className='me-2'
				icon='Delete'
				type='button'
				color='danger'
				onClick={() => deleteForm(initialValues.id)}
			/>

			<Modal isOpen={isOpen} setIsOpen={setIsOpen} size='lg' titleId='modal-edit-menu-crud'>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-edit-menu-crud'>
						{isReadOnly ? 'View Data' : 'Update Data'}
					</ModalTitle>
				</ModalHeader>
				<Formik
					enableReinitialize
					initialValues={{ ...initialValues }}
					onSubmit={onSubmit}
					validate={validate}>
					{(formikField) => {
						return (
							<Form>
								<ModalBody className='px-4'>
									<div className='col-md-12'>
										<div className='row mb-4'>
											<div className='col-md-6'>
												<FormGroup id='type_plan' label='Type Plan'>
													<CustomSelect
														isDisabled={isAlwaysReadOnly}
														options={listTypePlan}
														defaultValue={formikField.values.type_plan}
														value={formikField.values.type_plan}
														darkTheme={darkModeStatus}
														onChange={(value) => {
															formikField.setFieldValue(
																'type_plan',
																value,
															);
														}}
														isValid={
															typeof formikField.errors.type_plan ===
															'undefined'
														}
														ref={(ref) => {
															refTypePlan = ref;
															return refTypePlan;
														}}
													/>
												</FormGroup>
											</div>

											<FormGroup
												id='checkin_distance'
												label='Check-In Distance'
												className='col-md-6'>
												<Input
													min={0}
													readOnly={isReadOnly}
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.checkin_distance}
													isValid={formikField.isValid}
													isTouched={formikField.touched.checkin_distance}
													invalidFeedback={
														formikField.errors.checkin_distance
													}
													autoComplete='off'
													type='number'
												/>
											</FormGroup>
										</div>
										<div className='row mb-4'>
											<div className='col-md-6 mb-4'>
												<FormGroup id='role' label='Role'>
													<CustomSelect
														isDisabled={isAlwaysReadOnly}
														options={listRoles}
														defaultValue={formikField.values.role}
														value={formikField.values.role}
														darkTheme={darkModeStatus}
														onChange={(value) => {
															formikField.setFieldValue(
																'role',
																value,
															);
														}}
														isValid={
															typeof formikField.errors.role ===
															'undefined'
														}
														ref={(ref) => {
															refRole = ref;
															return refRole;
														}}
													/>
												</FormGroup>
											</div>

											<FormGroup
												id='checkout_distance'
												label='Check-Out Distance'
												className='col-md-6'>
												<Input
													min={0}
													readOnly={isReadOnly}
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.checkout_distance}
													isValid={formikField.isValid}
													isTouched={
														formikField.touched.checkout_distance
													}
													invalidFeedback={
														formikField.errors.checkout_distance
													}
													autoComplete='off'
													type='number'
												/>
											</FormGroup>
										</div>
										<div className='row mb-4'>
											<div className='col-md-6 mb-4'>
												<FormGroup
													id='store_category'
													label='Store Category'>
													<CustomSelect
														isDisabled={isAlwaysReadOnly}
														options={listStoreCategory}
														defaultValue={
															formikField.values.store_category
														}
														value={formikField.values.store_category}
														darkTheme={darkModeStatus}
														onChange={(value) => {
															formikField.setFieldValue(
																'store_category',
																value,
															);
														}}
														isValid={
															typeof formikField.errors
																.store_category === 'undefined'
														}
														ref={(ref) => {
															refStoreCategory = ref;
															return refStoreCategory;
														}}
													/>
												</FormGroup>
											</div>
											<FormGroup
												id='read_distance'
												label='Read Distance'
												className='col-md-6'>
												<Input
													min={0}
													readOnly={isReadOnly}
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.read_distance}
													isValid={formikField.isValid}
													isTouched={formikField.touched.read_distance}
													invalidFeedback={
														formikField.errors.read_distance
													}
													autoComplete='off'
													type='number'
												/>
											</FormGroup>
										</div>
									</div>
								</ModalBody>
								{!isReadOnly && (
									<ModalFooter className='px-4 pb-4'>
										<div className='col-md-12 '>
											<Button
												icon='Save'
												type='submit'
												color='success'
												className='float-end'
												isDisable={
													!formikField.isValid &&
													!!formikField.submitCount
												}>
												Update
											</Button>
										</div>
									</ModalFooter>
								)}
							</Form>
						);
					}}
				</Formik>
			</Modal>
		</>
	);
};

const CustomButton = (dt) => {
	const { row, listStoreCategory, listTypePlan, listRoles, handleReloadData } = dt;

	// restructure the obj
	const initialValues = {};

	const type_plan = {};
	type_plan.label = `${row.type_name}`;
	type_plan.value = `${row.type_code}`;

	const role = {};
	role.label = `${row.roles}`;
	role.value = `${row.roles_code}`;

	const store_category = {};
	store_category.label = `${row.store_category_name}`;
	store_category.value = `${row.store_category_code}`;

	initialValues.id = row._id;
	initialValues.store_category = store_category;
	initialValues.type_plan = type_plan;
	initialValues.role = role;
	initialValues.checkin_distance = row.checkin_distance;
	initialValues.checkout_distance = row.checkout_distance;
	initialValues.read_distance = row.read_distance;

	return (
		<FormExampleEdit
			listStoreCategory={listStoreCategory}
			listTypePlan={listTypePlan}
			listRoles={listRoles}
			initialValues={initialValues}
			handleReloadData={handleReloadData}
			row={row}
		/>
	);
};

const CustomDataTable = ({
	data,
	loading,
	totalRows,
	handlePageChange,
	handlePerRowsChange,
	handleReloadData,
	listRoles,
	listStoreCategory,
	listTypePlan,
}) => {
	const { darkModeStatus } = useDarkMode();

	const columns = useMemo(
		() => [
			{
				name: 'Created Date',
				selector: (row) => moment(row.created_at).format('DD-MM-YYYY'),
				sortable: true,
			},
			{
				name: 'Type',
				selector: (row) => row.type_name,
				sortable: true,
			},
			{
				name: 'Role',
				selector: (row) => row.roles,
				sortable: true,
			},
			{
				name: 'Store Category',
				selector: (row) => row.store_category_name,
				sortable: true,
			},
			{
				name: 'Action',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (dt) => {
					return (
						<CustomButton
							row={dt}
							listStoreCategory={listStoreCategory}
							listTypePlan={listTypePlan}
							listRoles={listRoles}
							handleReloadData={handleReloadData}
						/>
					);
				},
			},
		],
		[listStoreCategory, listRoles, listTypePlan, handleReloadData],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			progressPending={loading}
			pagination
			paginationServer
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};
const Distance = () => {
	const [data, setData] = useState([]);
	const [listRoles, setRoles] = useState([]);
	const [listTypePlan, setTypePlan] = useState([]);
	const [listStoreCategory, setStoreCategory] = useState([]);

	const [loading, setLoading] = useState(false);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [page, setPage] = useState(1);
	const [reloadData, setReloadData] = useState(false);

	const fetchDistance = async () => {
		setLoading(true);
		return DistanceModule.read(`page=${page}&sizePerPage=${perPage}`).then((res) => {
			setData(res.foundData);
			setTotalRows(res.countData);
			setLoading(false);
		});
	};

	const fetchRoles = async () => {
		return RolesModule.readSelect().then((res) => {
			setRoles(res);
		});
	};

	const fetchTypePlan = async () => {
		return TypePlanModule.readSelect().then((res) => {
			setTypePlan(res);
		});
	};

	const fetchStoreCategory = async () => {
		return StoreCategoryModule.readSelect().then((res) => {
			setStoreCategory(res);
		});
	};

	const handleReloadData = () => {
		setReloadData(!reloadData);
	};
	const initialValues = {
		loading: false,
		type_plan: {},
		role: {},
		store_category: {},
		checkin_distance: '',
		checkout_distance: '',
		read_distance: '',
	};

	useEffect(() => {
		fetchDistance();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [page, perPage, reloadData]);

	useEffect(() => {
		fetchDistance();
		fetchRoles();
		fetchTypePlan();
		fetchStoreCategory();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const { t } = useTranslation('regional');
	const [title] = useState({ title: 'Regional' });
	return (
		<PageWrapper title={title.title}>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card className='col-md-4'>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('Distance')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<div className='col-12 mb-3 g-4'>
							<FormExample
								initialValues={initialValues}
								listTypePlan={listTypePlan}
								listStoreCategory={listStoreCategory}
								listRoles={listRoles}
								handleReloadData={handleReloadData}
							/>
						</div>
					</CardBody>
				</Card>
				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('Historical Data')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<CustomDataTable
							data={data}
							loading={loading}
							totalRows={totalRows}
							listRoles={listRoles}
							listStoreCategory={listStoreCategory}
							listTypePlan={listTypePlan}
							handlePageChange={setPage}
							handlePerRowsChange={setPerPage}
							handleReloadData={handleReloadData}
						/>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

CustomDataTable.propTypes = {
	listRoles: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	listStoreCategory: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	listTypePlan: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	data: PropTypes.arrayOf(PropTypes.instanceOf(Object)),
	loading: PropTypes.bool,
	totalRows: PropTypes.number,
	handlePageChange: PropTypes.func,
	handlePerRowsChange: PropTypes.func,
	handleReloadData: PropTypes.func,
};
CustomDataTable.defaultProps = {
	listRoles: {},
	listStoreCategory: {},
	listTypePlan: {},
	data: [],
	loading: false,
	totalRows: 0,
	handlePageChange: null,
	handlePerRowsChange: null,
	handleReloadData: null,
};

export default Distance;
