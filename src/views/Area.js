import React, { useState, useEffect, useMemo, useRef } from 'react';
import PropTypes from 'prop-types';
import { Formik, Form } from 'formik';
import * as yup from 'yup';
import Swal from 'sweetalert2';
import { useTranslation } from 'react-i18next';
import Select from 'react-select';
import PageWrapper from '../layout/PageWrapper/PageWrapper';
import Page from '../layout/Page/Page';
import PageLayoutHeader from '../pages/common/Headers/PageLayoutHeader';
import Card, { CardBody, CardHeader, CardLabel, CardTitle } from '../components/bootstrap/Card';
import FormGroup from '../components/bootstrap/forms/FormGroup';
import Input from '../components/bootstrap/forms/Input';
import Button from '../components/bootstrap/Button';
import RegionalModule from '../modules/RegionalModule';
import AreaModule from '../modules/AreaModule';
import CityModule from '../modules/CityModule';

import showNotification from '../components/extras/showNotification';
import DarkDataTable from '../components/DarkDataTable';
import useDarkMode from '../hooks/useDarkMode';
import CustomSelect from '../components/CustomSelect';
import COLORS from '../common/data/enumColors';
import Modal, {
	ModalBody,
	ModalHeader,
	ModalTitle,
	ModalFooter,
} from '../components/bootstrap/Modal';
import { getRequester, getaActiveRoles } from '../helpers/helpers';

const react_select_styles = (formikField, darkModeStatus) => {
	const select_invalid_styles = {
		control: (base, state) => ({
			...base,
			// state.isFocused can display different borderColor if you need it
			borderColor:
				(state.isFocused && COLORS.SECONDARY.code) ||
				(!state.isFocused && formikField.submitCount > 0 && COLORS.DANGER.code) ||
				(!state.isFocused && formikField.submitCount === 0 && COLORS.LIGHT.code),
			// overwrittes hover style
			'&:hover': {
				borderColor:
					(state.isFocused && COLORS.SECONDARY.code) ||
					(!state.isFocused && formikField.submitCount > 0 && COLORS.DANGER.code),
			},
		}),
		singleValue: (base) => ({
			...base,
			color: darkModeStatus ? COLORS.LIGHT.code : COLORS.DARK.code,
		}),
		input: (base) => ({
			...base,
			color: darkModeStatus ? COLORS.LIGHT.code : COLORS.DARK.code,
		}),
	};
	const select_valid_styles = {
		control: (base, state) => ({
			...base,
			// state.isFocused can display different borderColor if you need it
			borderColor:
				(state.isFocused && COLORS.SECONDARY.code) ||
				(!state.isFocused && formikField.submitCount > 0 && COLORS.SUCCESS.code) ||
				(!state.isFocused && formikField.submitCount === 0 && COLORS.LIGHT.code),

			// overwrittes hover style
			'&:hover': {
				borderColor:
					(state.isFocused && COLORS.SECONDARY.code) ||
					(!state.isFocused && formikField.submitCount > 0 && COLORS.SUCCESS.code),
			},
		}),
		singleValue: (base) => ({
			...base,
			color: darkModeStatus ? COLORS.LIGHT.code : COLORS.DARK.code,
		}),
		input: (base) => ({
			...base,
			color: darkModeStatus ? COLORS.LIGHT.code : COLORS.DARK.code,
		}),
	};
	return typeof formikField.errors.template_selection === 'undefined'
		? select_valid_styles
		: select_invalid_styles;
};

const react_select_themes = (theme, darkModeStatus) => {
	return {
		...theme,
		colors: darkModeStatus
			? {
					...theme.colors,
					neutral0: COLORS.DARK.code,
					neutral190: COLORS.LIGHT.code,
					primary: COLORS.SUCCESS.code,
					primary25: COLORS.PRIMARY.code,
			  }
			: theme.colors,
	};
};

const handleSubmit = (values, handleReloadData) => {
	AreaModule.create(values)
		.then((res) => {
			showNotification('Success!', res.status, 'success');
			handleReloadData();
		})
		.catch((err) => {
			showNotification('Warning!', err, 'danger');
		});
	return values;
};
const handleSubmitEdit = (values, handleReloadData) => {
	AreaModule.edit(values)
		.then((res) => {
			showNotification('Success!', res.status, 'success');
			handleReloadData();
		})
		.catch((err) => {
			showNotification('Warning!', err, 'danger');
		});
	return values;
};
const handleDelete = (val, handleReloadData) => {
	AreaModule.delete_(val)
		.then((res) => {
			showNotification('Success!', res.status, 'success');
			handleReloadData();
		})
		.catch((err) => {
			showNotification('Warning!', err, 'danger');
		});
	return val;
};

const FormExample = (dt) => {
	const { darkModeStatus } = useDarkMode();
	const { initialValues, listRegional, listCity, handleReloadData } = dt;
	const [isMulti] = useState(true);
	let refRegional = useRef(0);
	let refCity = useRef(0);

	const submitForm = (values, resetForm) => {
		if (values.regional_code && values.area_name) {
			const { username } = getRequester();
			const val = {};
			val.requester = username;
			val.regional_code = values.regional_code.value;
			val.area_name = values.area_name;
			val.city = values.city;

			return Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'info',
				showCancelButton: true,
				confirmButtonText: 'Yes',
			}).then((result) => {
				if (result.value) {
					handleSubmit(val, handleReloadData);
					const refs = [];
					refs.push(refRegional);
					refs.push(refCity);
					resetForm({ loading: false, regional_code: {}, area_name: '', city: null });
					resetSelect(refs);
				} else if (result.dismiss === Swal.DismissReason.cancel) {
				}
			});
		}
		return values;
	};

	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		try {
			submitForm(values, resetForm);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	const validationSchema = yup.object({
		regional_code: yup.object({
			value: yup.string().required('Field is required'),
			label: yup.string().required('Field is required'),
		}),
		area_name: yup.string().required('Field is required'),
	});
	return (
		<Formik
			enableReinitialize
			initialValues={{ ...initialValues }}
			onSubmit={onSubmit}
			validationSchema={validationSchema}>
			{(formikField) => {
				return (
					<Form>
						<FormGroup id='regional_code' label='Select Regional' className='mb-4'>
							<Select
								styles={react_select_styles(formikField, darkModeStatus)}
								theme={(theme) => react_select_themes(theme, darkModeStatus)}
								defaultValue={formikField.values.regional_code}
								onBlur={formikField.handleBlur}
								options={listRegional}
								onChange={(value) => {
									formikField.setFieldValue('regional_code', value);
								}}
								ref={(ref) => {
									refRegional = ref;
									return refRegional;
								}}
							/>
						</FormGroup>
						{typeof formikField.errors.regional_code !== 'undefined' &&
							formikField.submitCount > 0 && (
								<div style={{ color: COLORS.DANGER.code }}>Field is required</div>
							)}

						<FormGroup id='area_name' label='Area Name' className='col-md-12'>
							<Input
								onChange={formikField.handleChange}
								onBlur={formikField.handleBlur}
								value={formikField.values.area_name}
								isValid={formikField.isValid}
								isTouched={formikField.touched.area_name}
								invalidFeedback={formikField.errors.area_name}
								autoComplete='off'
							/>
						</FormGroup>
						<br />
						<FormGroup id='city' label='Select City' className='mb-4'>
							<Select
								styles={react_select_styles(formikField, darkModeStatus)}
								theme={(theme) => react_select_themes(theme, darkModeStatus)}
								defaultValue={formikField.values.city}
								onBlur={formikField.handleBlur}
								options={listCity}
								onChange={(value) => {
									formikField.setFieldValue('city', value);
								}}
								isMulti={isMulti}
								ref={(ref) => {
									refCity = ref;
									return refCity;
								}}
							/>
						</FormGroup>
						<br />

						<div className='col-md-12 '>
							<Button
								icon='Save'
								isOutline
								type='submit'
								color='success'
								className='float-end'
								isDisable={!formikField.isValid && !!formikField.submitCount}>
								Submit
							</Button>
						</div>
					</Form>
				);
			}}
		</Formik>
	);
};

const resetSelect = (refs) => {
	refs.forEach((ref) => {
		ref.setValue(null);
	});
};

const FormExampleEdit = (dt) => {
	const { darkModeStatus } = useDarkMode();
	const { initialValues, listRegional, listCity, handleReloadData } = dt;

	const [isOpen, setIsOpen] = useState(false);
	const [isReadOnly, setIsReadOnly] = useState(false);
	const [isAlwaysReadOnly] = useState(true);
	const [isMulti] = useState(true);
	const [isSearchable] = useState(true);

	let refRegional = useRef(0);
	let refCity = useRef(0);

	const submitForm = (values, resetForm) => {
		const { id, area_name, city } = values;
		if (area_name && city && id) {
			const { username } = getRequester();
			const val = {};
			val._id = values.id;
			val.requester = username;
			val.area_name = area_name;
			val.city = city;

			return Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'info',
				showCancelButton: true,
				confirmButtonText: 'Yes',
			}).then((result) => {
				if (result.value) {
					handleSubmitEdit(val, handleReloadData);
					const refs = [];
					refs.push(refRegional);
					refs.push(refCity);
					resetForm(initialValues);
				} else if (result.dismiss === Swal.DismissReason.cancel) {
				}
			});
		}
		return values;
	};

	const deleteForm = (id) => {
		if (id) {
			const { username } = getRequester();
			const val = {};
			val._id = id;
			val.requester = username;
			return Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'info',
				showCancelButton: true,
				confirmButtonText: 'Yes',
			}).then((result) => {
				if (result.value) {
					handleDelete(val, handleReloadData);
				} else if (result.dismiss === Swal.DismissReason.cancel) {
				}
			});
		}
		return id;
	};

	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		try {
			delete values.loading;
			submitForm(values, resetForm);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	const validate = (values) => {
		const errors = {};
		if (!values.area_name) {
			errors.area_name = 'Field is required';
		}

		return errors;
	};

	return (
		<>
			<Button
				className='me-2'
				icon='GridOn'
				type='button'
				color='primary'
				onClick={() => {
					setIsOpen(true);
					setIsReadOnly(true);
				}}
			/>

			<Button
				className='me-2'
				icon='Edit'
				type='button'
				color='success'
				onClick={() => {
					setIsOpen(true);
					setIsReadOnly(false);
				}}
			/>

			<Button
				className='me-2'
				icon='Delete'
				type='button'
				color='danger'
				onClick={() => deleteForm(initialValues.id)}
			/>

			<Modal isOpen={isOpen} setIsOpen={setIsOpen} size='lg' titleId='modal-edit-menu-crud'>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-edit-menu-crud'>
						{isReadOnly ? 'View Data' : 'Update Data'}
					</ModalTitle>
				</ModalHeader>
				<Formik
					enableReinitialize
					initialValues={{ ...initialValues }}
					onSubmit={onSubmit}
					validate={validate}>
					{(formikField) => {
						return (
							<Form>
								<ModalBody className='px-4'>
									<div className='col-md-12'>
										<FormGroup
											id='regional_code'
											label='Regional'
											className='mb-4'>
											<CustomSelect
												isDisabled={isAlwaysReadOnly}
												options={listRegional}
												defaultValue={formikField.values.regional_code}
												value={formikField.values.regional_code}
												darkTheme={darkModeStatus}
												onChange={(value) => {
													formikField.setFieldValue(
														'regional_code',
														value,
													);
												}}
												isValid={
													typeof formikField.errors.regional_code ===
													'undefined'
												}
												ref={(ref) => {
													refRegional = ref;
													return refRegional;
												}}
											/>
										</FormGroup>
										{typeof formikField.errors.regional_code !== 'undefined' &&
											formikField.submitCount > 0 && (
												<div style={{ color: COLORS.DANGER.code }}>
													Field is required
												</div>
											)}

										<FormGroup
											id='area_name'
											label='Area Name'
											className='col-md-12'>
											<Input
												disabled={isReadOnly}
												onChange={formikField.handleChange}
												onBlur={formikField.handleBlur}
												value={formikField.values.area_name}
												isValid={formikField.isValid}
												isTouched={formikField.touched.area_name}
												invalidFeedback={formikField.errors.area_name}
												autoComplete='off'
											/>
										</FormGroup>
										<br />
										<FormGroup id='city' label='City' className='mb-4'>
											<CustomSelect
												isDisabled={isReadOnly}
												options={listCity}
												defaultValue={formikField.values.city}
												value={formikField.values.city}
												darkTheme={darkModeStatus}
												onChange={(value) => {
													formikField.setFieldValue('city', value);
												}}
												isSearchable={isSearchable}
												isMulti={isMulti}
												isValid={
													typeof formikField.errors.city === 'undefined'
												}
												ref={(ref) => {
													refCity = ref;
													return refCity;
												}}
											/>
										</FormGroup>
										{typeof formikField.errors.city !== 'undefined' &&
											formikField.submitCount > 0 && (
												<div style={{ color: COLORS.DANGER.code }}>
													Field is required
												</div>
											)}
									</div>
								</ModalBody>
								{!isReadOnly && (
									<ModalFooter className='px-4 pb-4'>
										<div className='col-md-12 '>
											<Button
												icon='Save'
												type='submit'
												color='success'
												className='float-end'
												isDisable={
													!formikField.isValid &&
													!!formikField.submitCount
												}>
												Update
											</Button>
										</div>
									</ModalFooter>
								)}
							</Form>
						);
					}}
				</Formik>
			</Modal>
		</>
	);
};

const CustomButton = (dt) => {
	const { row, listRegional, listCity, handleReloadData } = dt;

	// restructure the obj
	const initialValues = {};
	const regional = {};
	regional.label = `(${row.regional_code}) ${row.regional_name}`;
	regional.value = row.regional_code;

	const res_city = row.city.map((x) => {
		return {
			value: x.city_code,
			label: `(${x.city_code}) ${x.city_name}`,
		};
	});
	initialValues.id = row._id;
	initialValues.regional_code = regional;
	initialValues.area_name = row.area_name;
	initialValues.city = res_city;

	return (
		<FormExampleEdit
			initialValues={initialValues}
			listRegional={listRegional}
			listCity={listCity}
			handleReloadData={handleReloadData}
			row={row}
		/>
	);
};

const CustomDataTable = ({
	listRegional,
	listCity,
	data,
	loading,
	totalRows,
	handlePageChange,
	handlePerRowsChange,
	handleReloadData,
}) => {
	const { darkModeStatus } = useDarkMode();

	const columns = useMemo(
		() => [
			{
				name: 'Area Code',
				selector: (row) => row.area_code,
				sortable: true,
			},
			{
				name: 'Area Name',
				selector: (row) => row.area_name,
				sortable: true,
			},

			{
				name: 'Regional Code',
				selector: (row) => row.regional_code,
				sortable: true,
			},
			{
				name: 'Regional Name',
				selector: (row) => row.regional_name,
				sortable: true,
			},
			{
				name: 'Segment Code',
				selector: (row) => row.segment_code,
				sortable: true,
			},
			{
				name: 'Segment Name',
				selector: (row) => row.segment_name,
				sortable: true,
			},
			{
				name: 'Action',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (dt) => {
					return (
						<CustomButton
							row={dt}
							listRegional={listRegional}
							listCity={listCity}
							handleReloadData={handleReloadData}
						/>
					);
				},
			},
		],
		[listRegional, listCity, handleReloadData],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			progressPending={loading}
			pagination
			paginationServer
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};
const Area = () => {
	const [data, setData] = useState([]);
	const [listRegional, setRegional] = useState([]);
	const [listCity, setCity] = useState([]);

	const [loading, setLoading] = useState(false);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [page, setPage] = useState(1);
	const [reloadData, setReloadData] = useState(false);
	const { default_segment_code } = getaActiveRoles();

	const fetchArea = async () => {
		setLoading(true);
		return AreaModule.read(
			`page=${page}&sizePerPage=${perPage}&segment_code=${default_segment_code}`,
		).then((res) => {
			setData(res.foundData);
			setTotalRows(res.countData);
			setLoading(false);
		});
	};

	const fetchListRegional = async () => {
		return RegionalModule.readSelect(`segment_code=${default_segment_code}`).then((res) => {
			setRegional(res);
		});
	};

	const fetchListCity = async () => {
		return CityModule.readSelect().then((res) => {
			setCity(res);
		});
	};

	const handleReloadData = () => {
		setReloadData(!reloadData);
	};
	const initialValues = {
		loading: false,
		regional_code: {},
		area_name: '',
		city: '',
	};

	useEffect(() => {
		fetchArea();
		fetchListRegional();
		fetchListCity();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [page, perPage, reloadData]);

	useEffect(() => {
		fetchArea();
		fetchListRegional();
		fetchListCity();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const { t } = useTranslation('area');
	const [title] = useState({ title: 'Area' });
	return (
		<PageWrapper title={title.title}>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card className='col-md-4'>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('Area')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<div className='col-12 mb-3 g-4'>
							<FormExample
								initialValues={initialValues}
								listRegional={listRegional}
								listCity={listCity}
								handleReloadData={handleReloadData}
							/>
						</div>
					</CardBody>
				</Card>
				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('Historical Data')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<CustomDataTable
							data={data}
							loading={loading}
							totalRows={totalRows}
							listRegional={listRegional}
							listCity={listCity}
							handlePageChange={setPage}
							handlePerRowsChange={setPerPage}
							handleReloadData={handleReloadData}
						/>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

CustomDataTable.propTypes = {
	listRegional: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	listCity: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	data: PropTypes.arrayOf(PropTypes.instanceOf(Object)),
	loading: PropTypes.bool,
	totalRows: PropTypes.number,
	handlePageChange: PropTypes.func,
	handlePerRowsChange: PropTypes.func,
	handleReloadData: PropTypes.func,
};
CustomDataTable.defaultProps = {
	listRegional: {},
	listCity: {},
	data: [],
	loading: false,
	totalRows: 0,
	handlePageChange: null,
	handlePerRowsChange: null,
	handleReloadData: null,
};

export default Area;
