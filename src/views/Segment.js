import React, { useState, useEffect, useMemo, useRef } from 'react';
import PropTypes from 'prop-types';
import { Formik, Form } from 'formik';
import * as yup from 'yup';
import Swal from 'sweetalert2';
import { useTranslation } from 'react-i18next';
import Select from 'react-select';
import PageWrapper from '../layout/PageWrapper/PageWrapper';
import Page from '../layout/Page/Page';
import PageLayoutHeader from '../pages/common/Headers/PageLayoutHeader';
import Card, { CardBody, CardHeader, CardLabel, CardTitle } from '../components/bootstrap/Card';
import FormGroup from '../components/bootstrap/forms/FormGroup';
import Input from '../components/bootstrap/forms/Input';
import Button from '../components/bootstrap/Button';
import SegmentModule from '../modules/SegmentModule';
import CompanyModule from '../modules/CompanyModule';
import CountryModule from '../modules/CountryModule';

import showNotification from '../components/extras/showNotification';
import DarkDataTable from '../components/DarkDataTable';
import useDarkMode from '../hooks/useDarkMode';
import CustomSelect from '../components/CustomSelect';
import COLORS from '../common/data/enumColors';
import Modal, {
	ModalBody,
	ModalHeader,
	ModalTitle,
	ModalFooter,
} from '../components/bootstrap/Modal';
import { getRequester } from '../helpers/helpers';

const react_select_styles = (formikField, darkModeStatus) => {
	const select_invalid_styles = {
		control: (base, state) => ({
			...base,
			// state.isFocused can display different borderColor if you need it
			borderColor:
				(state.isFocused && COLORS.SECONDARY.code) ||
				(!state.isFocused && formikField.submitCount > 0 && COLORS.DANGER.code) ||
				(!state.isFocused && formikField.submitCount === 0 && COLORS.LIGHT.code),
			// overwrittes hover style
			'&:hover': {
				borderColor:
					(state.isFocused && COLORS.SECONDARY.code) ||
					(!state.isFocused && formikField.submitCount > 0 && COLORS.DANGER.code),
			},
		}),
		singleValue: (base) => ({
			...base,
			color: darkModeStatus ? COLORS.LIGHT.code : COLORS.DARK.code,
		}),
		input: (base) => ({
			...base,
			color: darkModeStatus ? COLORS.LIGHT.code : COLORS.DARK.code,
		}),
	};
	const select_valid_styles = {
		control: (base, state) => ({
			...base,
			// state.isFocused can display different borderColor if you need it
			borderColor:
				(state.isFocused && COLORS.SECONDARY.code) ||
				(!state.isFocused && formikField.submitCount > 0 && COLORS.SUCCESS.code) ||
				(!state.isFocused && formikField.submitCount === 0 && COLORS.LIGHT.code),

			// overwrittes hover style
			'&:hover': {
				borderColor:
					(state.isFocused && COLORS.SECONDARY.code) ||
					(!state.isFocused && formikField.submitCount > 0 && COLORS.SUCCESS.code),
			},
		}),
		singleValue: (base) => ({
			...base,
			color: darkModeStatus ? COLORS.LIGHT.code : COLORS.DARK.code,
		}),
		input: (base) => ({
			...base,
			color: darkModeStatus ? COLORS.LIGHT.code : COLORS.DARK.code,
		}),
	};
	return typeof formikField.errors.template_selection === 'undefined'
		? select_valid_styles
		: select_invalid_styles;
};

const react_select_themes = (theme, darkModeStatus) => {
	return {
		...theme,
		colors: darkModeStatus
			? {
					...theme.colors,
					neutral0: COLORS.DARK.code,
					neutral190: COLORS.LIGHT.code,
					primary: COLORS.SUCCESS.code,
					primary25: COLORS.PRIMARY.code,
			  }
			: theme.colors,
	};
};

const handleSubmit = (values, handleReloadData) => {
	SegmentModule.create(values)
		.then((res) => {
			showNotification('Success!', res.status, 'success');
			handleReloadData();
		})
		.catch((err) => {
			showNotification('Warning!', err, 'danger');
		});
	return values;
};
const handleSubmitEdit = (values, handleReloadData) => {
	SegmentModule.edit(values)
		.then((res) => {
			showNotification('Success!', res.status, 'success');
			handleReloadData();
		})
		.catch((err) => {
			showNotification('Warning!', err, 'danger');
		});
	return values;
};
const handleDelete = (val, handleReloadData) => {
	SegmentModule.delete_(val)
		.then((res) => {
			showNotification('Success!', res.status, 'success');
			handleReloadData();
		})
		.catch((err) => {
			showNotification('Warning!', err, 'danger');
		});
	return val;
};

const FormExample = (dt) => {
	const { darkModeStatus } = useDarkMode();
	const { initialValues, listCompany, listCountry, handleReloadData } = dt;
	let refCompany = useRef(0);
	let refCountry = useRef(0);

	const submitForm = (values, resetForm) => {
		if (values.company_code && values.country_code && values.segment_name) {
			const { username } = getRequester();
			const val = {};
			val.requester = username;
			val.company_code = values.company_code.value;
			val.country_code = values.country_code.value;
			val.segment_name = values.segment_name;
			console.log(val);

			return Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'info',
				showCancelButton: true,
				confirmButtonText: 'Yes',
			}).then((result) => {
				if (result.value) {
					handleSubmit(val, handleReloadData);
					const refs = [];
					refs.push(refCompany);
					refs.push(refCountry);
					resetForm(initialValues);
					resetSelect(refs);
				} else if (result.dismiss === Swal.DismissReason.cancel) {
				}
			});
		}
		return values;
	};

	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		try {
			submitForm(values, resetForm);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	const validationSchema = yup.object({
		company_code: yup.object({
			value: yup.string().required('Field is required'),
			label: yup.string().required('Field is required'),
		}),
		country_code: yup.object({
			value: yup.string().required('Field is required'),
			label: yup.string().required('Field is required'),
		}),
		segment_name: yup.string().required('Field is required'),
	});
	return (
		<Formik
			enableReinitialize
			initialValues={{ ...initialValues }}
			onSubmit={onSubmit}
			validationSchema={validationSchema}>
			{(formikField) => {
				return (
					<Form>
						<FormGroup id='company_code' label='Select Company' className='mb-4'>
							<Select
								styles={react_select_styles(formikField, darkModeStatus)}
								theme={(theme) => react_select_themes(theme, darkModeStatus)}
								defaultValue={formikField.values.company_code}
								onBlur={formikField.handleBlur}
								options={listCompany}
								onChange={(value) => {
									formikField.setFieldValue('company_code', value);
								}}
								ref={(ref) => {
									refCompany = ref;
									return refCompany;
								}}
							/>
						</FormGroup>
						{typeof formikField.errors.company_code !== 'undefined' &&
							formikField.submitCount > 0 && (
								<div style={{ color: COLORS.DANGER.code }}>Field is required</div>
							)}

						<FormGroup id='country_code' label='Select Country' className='mb-4'>
							<Select
								styles={react_select_styles(formikField, darkModeStatus)}
								theme={(theme) => react_select_themes(theme, darkModeStatus)}
								defaultValue={formikField.values.country_code}
								onBlur={formikField.handleBlur}
								options={listCountry}
								onChange={(value) => {
									formikField.setFieldValue('country_code', value);
								}}
								ref={(ref) => {
									refCountry = ref;
									return refCountry;
								}}
							/>
						</FormGroup>
						{typeof formikField.errors.country_code !== 'undefined' &&
							formikField.submitCount > 0 && (
								<div style={{ color: COLORS.DANGER.code }}>Field is required</div>
							)}
						<FormGroup id='segment_name' label='Segment Name' className='col-md-12'>
							<Input
								onChange={formikField.handleChange}
								onBlur={formikField.handleBlur}
								value={formikField.values.segment_name}
								isValid={formikField.isValid}
								isTouched={formikField.touched.segment_name}
								invalidFeedback={formikField.errors.segment_name}
								autoComplete='off'
							/>
						</FormGroup>
						<br />

						<div className='col-md-12 '>
							<Button
								icon='Save'
								isOutline
								type='submit'
								color='success'
								className='float-end'
								isDisable={!formikField.isValid && !!formikField.submitCount}>
								Submit
							</Button>
						</div>
					</Form>
				);
			}}
		</Formik>
	);
};

const resetSelect = (refs) => {
	refs.forEach((ref) => {
		ref.setValue({});
	});
};

const FormExampleEdit = (dt) => {
	const { darkModeStatus } = useDarkMode();
	const { initialValues, listCompany, listCountry, handleReloadData } = dt;

	const [isOpen, setIsOpen] = useState(false);
	const [isReadOnly, setIsReadOnly] = useState(false);
	const [isAlwaysReadOnly] = useState(true);

	let refCompany = useRef(0);
	let refCountry = useRef(0);

	const submitForm = (values, resetForm) => {
		const { id, segment_name } = values;
		if (segment_name && id) {
			const { username } = getRequester();
			const val = {};
			val._id = values.id;
			val.requester = username;
			val.segment_name = segment_name;

			return Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'info',
				showCancelButton: true,
				confirmButtonText: 'Yes',
			}).then((result) => {
				if (result.value) {
					handleSubmitEdit(val, handleReloadData);
					const refs = [];
					refs.push(refCompany);
					refs.push(refCountry);
					resetForm(initialValues);
				} else if (result.dismiss === Swal.DismissReason.cancel) {
				}
			});
		}
		return values;
	};

	const deleteForm = (id) => {
		if (id) {
			const { username } = getRequester();
			const val = {};
			val._id = id;
			val.requester = username;
			return Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'info',
				showCancelButton: true,
				confirmButtonText: 'Yes',
			}).then((result) => {
				if (result.value) {
					handleDelete(val, handleReloadData);
				} else if (result.dismiss === Swal.DismissReason.cancel) {
				}
			});
		}
		return id;
	};

	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		try {
			delete values.loading;
			submitForm(values, resetForm);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	const validate = (values) => {
		const errors = {};
		if (!values.segment_name) {
			errors.segment_name = 'Field is required';
		}

		return errors;
	};

	return (
		<>
			<Button
				className='me-2'
				icon='GridOn'
				type='button'
				color='primary'
				onClick={() => {
					setIsOpen(true);
					setIsReadOnly(true);
				}}
			/>

			<Button
				className='me-2'
				icon='Edit'
				type='button'
				color='success'
				onClick={() => {
					setIsOpen(true);
					setIsReadOnly(false);
				}}
			/>

			<Button
				className='me-2'
				icon='Delete'
				type='button'
				color='danger'
				onClick={() => deleteForm(initialValues.id)}
			/>

			<Modal isOpen={isOpen} setIsOpen={setIsOpen} size='lg' titleId='modal-edit-menu-crud'>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-edit-menu-crud'>
						{isReadOnly ? 'View Data' : 'Update Data'}
					</ModalTitle>
				</ModalHeader>
				<Formik
					enableReinitialize
					initialValues={{ ...initialValues }}
					onSubmit={onSubmit}
					validate={validate}>
					{(formikField) => {
						return (
							<Form>
								<ModalBody className='px-4'>
									<div className='col-md-12'>
										<FormGroup
											id='company_code'
											label='Company'
											className='mb-4'>
											<CustomSelect
												isDisabled={isAlwaysReadOnly}
												options={listCompany}
												defaultValue={formikField.values.company_code}
												value={formikField.values.company_code}
												darkTheme={darkModeStatus}
												onChange={(value) => {
													formikField.setFieldValue(
														'company_code',
														value,
													);
												}}
												isValid={
													typeof formikField.errors.company_code ===
													'undefined'
												}
												ref={(ref) => {
													refCompany = ref;
													return refCompany;
												}}
											/>
										</FormGroup>
										{typeof formikField.errors.company_code !== 'undefined' &&
											formikField.submitCount > 0 && (
												<div style={{ color: COLORS.DANGER.code }}>
													Field is required
												</div>
											)}

										<FormGroup
											id='country_code'
											label='Country'
											className='mb-4'>
											<CustomSelect
												isDisabled={isAlwaysReadOnly}
												options={listCountry}
												defaultValue={formikField.values.country_code}
												value={formikField.values.country_code}
												darkTheme={darkModeStatus}
												onChange={(value) => {
													formikField.setFieldValue(
														'country_code',
														value,
													);
												}}
												isValid={
													typeof formikField.errors.country_code ===
													'undefined'
												}
												ref={(ref) => {
													refCountry = ref;
													return refCountry;
												}}
											/>
										</FormGroup>
										{typeof formikField.errors.country_code !== 'undefined' &&
											formikField.submitCount > 0 && (
												<div style={{ color: COLORS.DANGER.code }}>
													Field is required
												</div>
											)}

										<FormGroup
											id='segment_name'
											label='Segment Name'
											className='col-md-12'>
											<Input
												disabled={isReadOnly}
												onChange={formikField.handleChange}
												onBlur={formikField.handleBlur}
												value={formikField.values.segment_name}
												isValid={formikField.isValid}
												isTouched={formikField.touched.segment_name}
												invalidFeedback={formikField.errors.segment_name}
												autoComplete='off'
											/>
										</FormGroup>
									</div>
								</ModalBody>
								{!isReadOnly && (
									<ModalFooter className='px-4 pb-4'>
										<div className='col-md-12 '>
											<Button
												icon='Save'
												type='submit'
												color='success'
												className='float-end'
												isDisable={
													!formikField.isValid &&
													!!formikField.submitCount
												}>
												Update
											</Button>
										</div>
									</ModalFooter>
								)}
							</Form>
						);
					}}
				</Formik>
			</Modal>
		</>
	);
};

const CustomButton = (dt) => {
	const { row, listCompany, listCountry, handleReloadData } = dt;

	// restructure the obj
	const initialValues = {};
	const country = {};
	const company = {};
	company.label = row.company_name;
	company.value = row.company_code;
	country.label = `(${row.country_code}) ${row.country_name}`;
	country.value = row.country_name;

	initialValues.id = row._id;
	initialValues.company_code = company;
	initialValues.country_code = country;
	initialValues.segment_name = row.segment_name;

	return (
		<FormExampleEdit
			initialValues={initialValues}
			listCompany={listCompany}
			listCountry={listCountry}
			handleReloadData={handleReloadData}
			row={row}
		/>
	);
};

const CustomDataTable = ({
	listCompany,
	listCountry,
	data,
	loading,
	totalRows,
	handlePageChange,
	handlePerRowsChange,
	handleReloadData,
}) => {
	const { darkModeStatus } = useDarkMode();

	const columns = useMemo(
		() => [
			{
				name: 'Code',
				selector: (row) => row.segment_code,
				sortable: true,
			},
			{
				name: 'Segment Name',
				selector: (row) => row.segment_name,
				sortable: true,
			},
			{
				name: 'Company',
				selector: (row) => row.company_name,
				sortable: true,
			},
			{
				name: 'Country Code',
				selector: (row) => row.country_code,
				sortable: true,
			},
			{
				name: 'Country',
				selector: (row) => row.country_name,
				sortable: true,
			},
			{
				name: 'Action',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (dt) => {
					return (
						<CustomButton
							row={dt}
							listCompany={listCompany}
							listCountry={listCountry}
							handleReloadData={handleReloadData}
						/>
					);
				},
			},
		],
		[listCompany, listCountry, handleReloadData],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			progressPending={loading}
			pagination
			paginationServer
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};
const Segment = () => {
	const [data, setData] = useState([]);
	const [listCompany, setCompany] = useState([]);
	const [listCountry, setCountry] = useState([]);

	const [loading, setLoading] = useState(false);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [page, setPage] = useState(1);
	const [reloadData, setReloadData] = useState(false);

	const fetchSegment = async () => {
		setLoading(true);
		return SegmentModule.read(`page=${page}&sizePerPage=${perPage}`).then((res) => {
			setData(res.foundData);
			setTotalRows(res.countData);
			setLoading(false);
		});
	};

	const fetchCompany = async () => {
		return CompanyModule.readSelect().then((res) => {
			setCompany(res);
		});
	};
	const fetchCountry = async () => {
		return CountryModule.readSelect().then((res) => {
			setCountry(res);
		});
	};

	const handleReloadData = () => {
		setReloadData(!reloadData);
	};
	const initialValues = {
		loading: false,
		company_code: {},
		country_code: {},
		segment_name: '',
	};

	useEffect(() => {
		fetchSegment();
		fetchCompany();
		fetchCountry();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [page, perPage, reloadData]);

	useEffect(() => {
		fetchSegment();
		fetchCompany();
		fetchCountry();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const { t } = useTranslation('template-selection');
	const [title] = useState({ title: 'Segment' });
	return (
		<PageWrapper title={title.title}>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card className='col-md-4'>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('Segment')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<div className='col-12 mb-3 g-4'>
							<FormExample
								initialValues={initialValues}
								listCompany={listCompany}
								listCountry={listCountry}
								handleReloadData={handleReloadData}
							/>
						</div>
					</CardBody>
				</Card>
				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('Historical Data')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<CustomDataTable
							data={data}
							loading={loading}
							totalRows={totalRows}
							listCompany={listCompany}
							listCountry={listCountry}
							handlePageChange={setPage}
							handlePerRowsChange={setPerPage}
							handleReloadData={handleReloadData}
						/>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

CustomDataTable.propTypes = {
	listCompany: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	listCountry: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	data: PropTypes.arrayOf(PropTypes.instanceOf(Object)),
	loading: PropTypes.bool,
	totalRows: PropTypes.number,
	handlePageChange: PropTypes.func,
	handlePerRowsChange: PropTypes.func,
	handleReloadData: PropTypes.func,
};
CustomDataTable.defaultProps = {
	listCompany: {},
	listCountry: {},
	data: [],
	loading: false,
	totalRows: 0,
	handlePageChange: null,
	handlePerRowsChange: null,
	handleReloadData: null,
};

export default Segment;
