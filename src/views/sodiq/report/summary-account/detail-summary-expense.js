// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import CustomTable from '../../../../components/custom/rizky/Table/Table';

// rizky
const TableDetail = (dt) => {
	const { year, data } = dt;
	const [column, setColumn] = useState({
		expense_skp: [],
		expense_tt: [],
	});

	const handleColumn = () => {
		const  yr  = '2022';
		const month = [
			'Jan',
			'Feb',
			'Mar',
			'Apr',
			'Mei',
			'Jun',
			'Jul',
			'Aug',
			'Sep',
			'Oct',
			'Nov',
			'Des',
		];
		const resultExpenseTT = [{ heading: 'TRADING TERM', value: 'name' }];
		const resultExpenseSKP = [{ heading: 'ON TOP BUDGET / SKP', value: 'promo_type' }];
		month.forEach((e, i) => {
			resultExpenseTT.push({
				heading: `${e}-${yr.substr(2)}`,
				value: 'expense',
				index: `${i}`,
			});
			resultExpenseSKP.push({
				heading: `${e}-${yr.substr(2)}`,
				value: 'expense',
				index: `${i}`,
			});
		});
		setColumn({
			...column,
			expense_tt: [
				...resultExpenseTT,
				{ heading: `Total`, value: 'expense', sum: true },
			],
			expense_skp: [
				...resultExpenseSKP,
				{ heading: `Total`, value: 'expense', sum: true },
			],
		});
	};

	useEffect(() => {
		handleColumn();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [year]);

	if (data) {
		return (
			<>
				<CustomTable
					data={data.tt.point}
					column={column.expense_tt}
					title='DETAIL SUMMARY EXPENSE'
					type='nominal'
				/>
				<CustomTable
					data={data.skp}
					column={column.expense_skp}
					title=''
					type='nominal'
				/>
				</>
		);
	}
	return <div />;
};

export default TableDetail;
