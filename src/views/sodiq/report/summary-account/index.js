// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { useTranslation } from 'react-i18next';
import { Form, Formik } from 'formik';
import PropTypes from 'prop-types';
import PageWrapper from '../../../../layout/PageWrapper/PageWrapper';
import Page from '../../../../layout/Page/Page';
import PageLayoutHeader from '../../../../pages/common/Headers/PageLayoutHeader';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardTitle,
} from '../../../../components/bootstrap/Card';
import showNotification from '../../../../components/extras/showNotification';
import summaryAccountModule from '../../../../modules/sodiq/summary_accountModule';
import reportModule from '../../../../modules/rizky/reportModule';
import masterNkaModule from '../../../../modules/rizky/master-nkaModule';
import Select from '../../../../components/bootstrap/forms/Select';
import Button from '../../../../components/bootstrap/Button';
import Label from '../../../../components/bootstrap/forms/Label';
import TableDetail from './detail-summary-expense';

const FormSearch = (dt) => {
	const { initialValues, handleSubmit } = dt;
	const [listAccount, setListAccount] = useState([]);
	const [listYear, setListYear] = useState([]);

	const onSubmit = (values, { setSubmitting, setStatus, setErrors }) => {
		if (values.store_code !== undefined && values.date !== undefined) {
			try {
				handleSubmit(values);
				setStatus({ success: true });
			} catch (error) {
				setStatus({ success: false });
				setSubmitting(false);
				setErrors({ submit: error.message });
			}
		} else {
			if (values.store_code === undefined) {
				showNotification('Warning!', 'Please Entries Account Name', 'danger');
			}
			if (values.date === undefined) {
				showNotification('Warning!', 'Please Entries Year', 'danger');
			}
		}
	};

	const fetchAccount = async () => {
		return masterNkaModule.list().then((res) => {
			setListAccount(res.foundData);
		});
	};

	const fetchYear = async () => {
		return summaryAccountModule.year().then((res) => {
			setListYear(res.foundData);
		});
	};

	useEffect(() => {
		fetchAccount();
		fetchYear();
	}, []);

	return (
		<Formik enableReinitialize initialValues={{ ...initialValues }} onSubmit={onSubmit}>
			{(formikField) => {
				return (
					<Form>
						<div className='row'>
							<div className='col-md-2'>
								<Select
									id='store_code'
									placeholder='Select Account'
									onChange={formikField.handleChange}
									value={formikField.values.store_code}
									list={listAccount}
								/>
							</div>
							<div className='col-md-2'>
								<Select
									id='date'
									placeholder='Select Year'
									onChange={formikField.handleChange}
									value={formikField.values.date}
									list={listYear}
								/>
							</div>
							<div className='col-md-2'>
								<Button icon='search' type='submit' color='success'>
									Filter
								</Button>
							</div>
						</div>
					</Form>
				);
			}}
		</Formik>
	);
};

const Table = ({ report, types, setAccount }) => {
	let YY = '';
	let title = '';
	let label1 = '';
	let label2 = '';
	let label3 = '';
	let padding = 'col-12';
	let content1 = [];
	let content2 = [];
	let content3 = [];

	if (types === 'sell_in') {
		title = 'Sell In';
		label1 = 'Target Sell In';
		label2 = 'Actual Sell In';
		label3 = 'Achievement';
	}
	if (types === 'on_top_budget') {
		title = 'On Top Budget';
		label1 = 'Budget OTB';
		label2 = 'Actual Claim OTB';
		label3 = 'Cost Ratio OTB';
		padding = 'col-12 mt-5';
	}
	if (types === 'trading_terms') {
		title = 'Trading Term';
		label1 = 'Budget Trading Term';
		label2 = 'Actual Claim Trading Term';
		label3 = 'Cost Ratio Trading Term';
		padding = 'col-12 mt-5';
	}

	if (types === 'detail_summary_expense') {
		title = 'DETAIL SUMMARY EXPENSE';
		label1 = 'Budget Trading Term';
		label2 = 'Actual Claim Trading Term';
		label3 = 'Cost Ratio Trading Term';
		padding = 'col-12 mt-5';
	}

	if (report) {
		setAccount(report.store_name);
		YY =
			report !== null
				? -report.year.substring(report.year.length - 2, report.year.length)
				: '';
		content1 = report.target;
		content2 = report.actual;

		if (types === 'sell_in') {
			console.log(report.achievement);
			content3 = report.achievement;
		}
		if (types === 'on_top_budget') {
			content3 = report.cost_ratio_otb;
		}
		if (types === 'trading_terms') {
			content3 = report.cost_ratio_tt;
		}
	}

	const formatRupiah = (money) => {
		return new Intl.NumberFormat('id-ID', {
			style: 'currency',
			currency: 'IDR',
			minimumFractionDigits: 0,
		}).format(money);
	};

	return (
		<div className={padding}>
			<Label>
				<b>{title}</b>
			</Label>
			<div className='table-responsive'>
				<table className='table table-bordered'>
					<thead>
						<tr style={{ backgroundColor: '#e9ecef' }}>
							<th width='18%'>Keterangan</th>
							<th className='text-end'>Jan{YY}</th>
							<th className='text-end'>Feb{YY}</th>
							<th className='text-end'>Mar{YY}</th>
							<th className='text-end'>Apr{YY}</th>
							<th className='text-end'>May{YY}</th>
							<th className='text-end'>Jun{YY}</th>
							<th className='text-end'>Jul{YY}</th>
							<th className='text-end'>Aug{YY}</th>
							<th className='text-end'>Sep{YY}</th>
							<th className='text-end'>Oct{YY}</th>
							<th className='text-end'>Nov{YY}</th>
							<th className='text-end'>Dec{YY}</th>
							<th className='text-end'>Total</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td width='18%'>
								<b>{label1}</b>
							</td>
							{content1.length > 0
								? content1.map((item1) => (
										<td className='text-end'>
											{formatRupiah(item1).replace('Rp', '')}
										</td>
								  ))
								: ''}
							{content1.length > 0 ? (
								<td className='text-end'>
									{formatRupiah(
										content1.reduce((total, item) => {
											total += item;
											return total;
										}),
									).replace('Rp', '')}
								</td>
							) : (
								''
							)}
						</tr>
						<tr>
							<td width='18%'>
								<b>{label2}</b>
							</td>
							{content2.length > 0
								? content2.map((item2) => (
										<td className='text-end'>
											{formatRupiah(item2).replace('Rp', '')}
										</td>
								  ))
								: ''}
							{content2.length > 0 ? (
								<td className='text-end'>
									{formatRupiah(
										content2.reduce((total, item) => {
											total += item;
											return total;
										}),
									).replace('Rp', '')}
								</td>
							) : (
								''
							)}
						</tr>
						<tr>
							<td width='18%'>
								<b>{label3}</b>
							</td>
							{content3.length > 0
								? content3.map((item3) => (
										<td className='text-end'>
											{formatRupiah(item3.toFixed(2)).replace('Rp', '')}%
										</td>
								  ))
								: ''}
							{content3.length > 0 ? (
								<td className='text-end'>
									{formatRupiah(
										content3
											.reduce((total, item) => {
												total += item;
												return total;
											})
											.toFixed(2),
									).replace('Rp', '')}
									%
								</td>
							) : (
								''
							)}
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	);
};

const SummeryAccount = () => {
	const [listReport, setListReport] = useState([]);
	// rizky
	const [detailSummaryExpense, setDetailSummaryExpense] = useState();
	const [account, setAccount] = useState('');

	const handleSubmit = async (values) => {
		if (values) {
			summaryAccountModule
				.list(new URLSearchParams(values))
				.then((res) => {
					showNotification('Success!', 'Summary Account', 'success');
					setListReport(res);
				})
				.catch((err) => {
					showNotification('Warning!', err, 'danger');
				});
			// rizky
			reportModule
				.detailSummaryExpense(new URLSearchParams(values))
				.then((res) => {
					setDetailSummaryExpense(res);
				})
				.catch(() => {
					setDetailSummaryExpense(null);
				});
		} else {
			showNotification('Warning!', 'Please Complete Entries', 'danger');
		}
		return values;
	};

	useEffect(() => {}, [listReport]);

	const { t } = useTranslation('category');

	return (
		<PageWrapper title='Summary Account'>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card className='col-md-12'>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('Filter Summary Account')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<div className='col-12 mb-3 g-4'>
							<FormSearch handleSubmit={(e) => handleSubmit(e)} />
						</div>
					</CardBody>
				</Card>
				<Card className='col-md-12'>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>
								{t('Data Summary Account')} {account}
							</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<Table
							report={listReport.sell_in}
							types='sell_in'
							setAccount={setAccount}
						/>
						<Table
							report={listReport.otb}
							types='on_top_budget'
							setAccount={setAccount}
						/>
						<Table
							report={listReport.tt}
							types='trading_terms'
							setAccount={setAccount}
						/>
						{/* rizky */}
						<TableDetail data={detailSummaryExpense} />
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

Table.propTypes = {
	types: PropTypes.string,
	report: PropTypes.arrayOf(PropTypes.instanceOf(Object)),
	setAccount: PropTypes.func,
};

Table.defaultProps = {
	types: null,
	report: null,
	setAccount: () => {},
};

export default SummeryAccount;
