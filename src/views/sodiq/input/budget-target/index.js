// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
import React, {
	useState,
	useEffect,
	useRef,
	useImperativeHandle,
	forwardRef,
	useMemo,
} from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { useTranslation } from 'react-i18next';
import { Form, Formik } from 'formik';
import Swal from 'sweetalert2';
import FileDownload from 'js-file-download';
import * as XLSX from 'xlsx';
import PropTypes from 'prop-types';
import PageWrapper from '../../../../layout/PageWrapper/PageWrapper';
import Page from '../../../../layout/Page/Page';
import PageLayoutHeader from '../../../../pages/common/Headers/PageLayoutHeader';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardTitle,
} from '../../../../components/bootstrap/Card';
import showNotification from '../../../../components/extras/showNotification';
import Input from '../../../../components/bootstrap/forms/Input';
import useDarkMode from '../../../../hooks/useDarkMode';
import DarkDataTable from '../../../../components/DarkDataTable';
import Button from '../../../../components/bootstrap/Button';
import Select from '../../../../components/bootstrap/forms/Select';
import Spinner from '../../../../components/bootstrap/Spinner';
import Label from '../../../../components/bootstrap/forms/Label';
import budget_targetModule from '../../../../modules/sodiq/budget_targetModule';
import summaryAccountModule from '../../../../modules/sodiq/summary_accountModule';

const listType = [
	{ text: 'Target Sell In', value: 'sell_in' },
	{ text: 'Budget SKP', value: 'on_top_budget' },
	{ text: 'Budget TT', value: 'trading_terms' },
];

const formatRupiah = (money) => {
	if (money !== null && money !== '') {
		return new Intl.NumberFormat('id-ID', {
			style: 'currency',
			currency: 'IDR',
			minimumFractionDigits: 0,
		}).format(money);
	}
	return '';
};

const DataTable = ({ data, loading, totalRows, handlePageChange, handlePerRowsChange }) => {
	const { darkModeStatus } = useDarkMode();

	const columns = useMemo(
		() => [
			{
				name: <b>Account</b>,
				selector: (row) => row.name,
				sortable: true,
			},
			{
				name: <b>Type</b>,
				selector: (row) =>
					listType.map((item) => (item.value === row.type ? item.text : '')),
				sortable: true,
			},
			{
				name: <b>Year</b>,
				selector: (row) => row.year,
				sortable: true,
			},
			{
				name: <b>Jan</b>,
				selector: (row) => formatRupiah(row.jan).replace('Rp', ''),
				sortable: true,
			},
			{
				name: <b>Feb</b>,
				selector: (row) => formatRupiah(row.feb).replace('Rp', ''),
				sortable: true,
			},
			{
				name: <b>Mar</b>,
				selector: (row) => formatRupiah(row.mar).replace('Rp', ''),
				sortable: true,
			},
			{
				name: <b>Apr</b>,
				selector: (row) => formatRupiah(row.apr).replace('Rp', ''),
				sortable: true,
			},
			{
				name: <b>May</b>,
				selector: (row) => formatRupiah(row.may).replace('Rp', ''),
				sortable: true,
			},
			{
				name: <b>Jun</b>,
				selector: (row) => formatRupiah(row.jun).replace('Rp', ''),
				sortable: true,
			},
			{
				name: <b>Jul</b>,
				selector: (row) => formatRupiah(row.jul).replace('Rp', ''),
				sortable: true,
			},
			{
				name: <b>Aug</b>,
				selector: (row) => formatRupiah(row.aug).replace('Rp', ''),
				sortable: true,
			},
			{
				name: <b>Sep</b>,
				selector: (row) => formatRupiah(row.sep).replace('Rp', ''),
				sortable: true,
			},
			{
				name: <b>Oct</b>,
				selector: (row) => formatRupiah(row.oct).replace('Rp', ''),
				sortable: true,
			},
			{
				name: <b>Nov</b>,
				selector: (row) => formatRupiah(row.nov).replace('Rp', ''),
				sortable: true,
			},
			{
				name: <b>Dec</b>,
				selector: (row) => formatRupiah(row.dec).replace('Rp', ''),
				sortable: true,
			},
			{
				name: <b>Total</b>,
				selector: (row) => formatRupiah(row.total).replace('Rp', ''),
				sortable: true,
			},
		],
		[],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			progressPending={loading}
			pagination
			paginationServer
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};

const TablePreview = forwardRef((props) => {
	const { darkModeStatus } = useDarkMode();
	const [data, setData] = useState([]);
	const [loading, setLoading] = useState(false);

	useEffect(() => {
		fetchData(props.data, 1);
	}, [props.data]);

	const fetchData = (dataPreview, page, perPage = 10) => {
		setLoading(false);
		const sumRows = perPage * page;
		const start = sumRows - perPage;
		const end = dataPreview.length > sumRows ? sumRows : dataPreview.length;
		const showData = dataPreview.slice(start, end);
		setData(showData);
	};

	const handlePageChange = (page) => {
		fetchData(props.data, page);
	};

	const handlePerRowsChange = (newPerPage, page) => {
		setLoading(true);
		fetchData(props.data, page, newPerPage);
	};

	const columns = useMemo(
		() => [
			{
				name: <b>Account Name</b>,
				selector: (row) => row[0],
				sortable: true,
			},
			{
				name: <b>Account Code</b>,
				selector: (row) => row[1],
				sortable: true,
			},
			{
				name: <b>Jan</b>,
				selector: (row) => formatRupiah(row[2]).replace('Rp', ''),
				sortable: true,
			},
			{
				name: <b>Feb</b>,
				selector: (row) => formatRupiah(row[3]).replace('Rp', ''),
				sortable: true,
			},
			{
				name: <b>Mar</b>,
				selector: (row) => formatRupiah(row[4]).replace('Rp', ''),
				sortable: true,
			},
			{
				name: <b>Apr</b>,
				selector: (row) => formatRupiah(row[5]).replace('Rp', ''),
				sortable: true,
			},
			{
				name: <b>May</b>,
				selector: (row) => formatRupiah(row[6]).replace('Rp', ''),
				sortable: true,
			},
			{
				name: <b>Jun</b>,
				selector: (row) => formatRupiah(row[7]).replace('Rp', ''),
				sortable: true,
			},
			{
				name: <b>Jul</b>,
				selector: (row) => formatRupiah(row[8]).replace('Rp', ''),
				sortable: true,
			},
			{
				name: <b>Aug</b>,
				selector: (row) => formatRupiah(row[9]).replace('Rp', ''),
				sortable: true,
			},
			{
				name: <b>Sep</b>,
				selector: (row) => formatRupiah(row[10]).replace('Rp', ''),
				sortable: true,
			},
			{
				name: <b>Oct</b>,
				selector: (row) => formatRupiah(row[11]).replace('Rp', ''),
				sortable: true,
			},
			{
				name: <b>Nov</b>,
				selector: (row) => formatRupiah(row[12]).replace('Rp', ''),
				sortable: true,
			},
			{
				name: <b>Dec</b>,
				selector: (row) => formatRupiah(row[13]).replace('Rp', ''),
				sortable: true,
			},
		],
		[],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			progressPending={loading}
			pagination
			paginationServer
			paginationTotalRows={props.data.length}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
});

const handleSubmit = (
	values,
	initialValues,
	handleReloadData,
	handleReset,
	resetForm,
	setLabel,
	yearRef,
	setFilter,
	setLoading,
) => {
	if (values) {
		delete values.loading;
		return Swal.fire({
			title: 'Are you sure?',
			text: 'Please check your entries !',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				setLoading(true);
				budget_targetModule
					.create(values)
					.then(() => {
						setFilter({});
						handleReloadData();
						showNotification(
							'Information!',
							'Data has been saved successfully',
							'success',
						);
						handleReset();
						resetForm({ values: initialValues });
						setLabel('');
						setTimeout(() => {
							yearRef.current.inReloadYear();
						}, 500);
						setLoading(false);
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					});
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				Swal.fire('Cancelled', 'Your data is safe :)', 'error');
			}
		});
	}
	return values;
};

const FormSearch = forwardRef((dt, ref) => {
	const { setFilter, handleReloadData } = dt;
	const [listYear, setListYear] = useState([]);

	const onSubmit = (values) => {
		const params = {
			type: '',
			year: '',
		};
		if (values.typeSearch !== '') {
			params.type = values.typeSearch;
		}
		if (values.yearSearch !== '') {
			params.year = values.yearSearch;
		}
		if (values.typeSearch !== '' || values.yearSearch !== '') {
			setFilter(params);
		} else {
			setFilter({});
		}
		handleReloadData();
	};

	const fetchYear = async () => {
		return summaryAccountModule.year().then((res) => {
			setListYear(res.foundData);
		});
	};

	useImperativeHandle(ref, () => ({
		inReloadYear() {
			fetchYear();
		},
	}));

	useEffect(() => {
		fetchYear();
	}, []);

	const initialValues = {
		typeSearch: '',
		yearSearch: '',
	};

	return (
		<Formik enableReinitialize initialValues={{ ...initialValues }} onSubmit={onSubmit}>
			{(formikField) => {
				return (
					<Form>
						<div className='row'>
							<div className='col-md-2'>
								<Select
									id='typeSearch'
									placeholder='Select Type'
									list={listType}
									onChange={formikField.handleChange}
									value={formikField.values.typeSearch}
								/>
							</div>
							<div className='col-md-1'>
								<Select
									id='yearSearch'
									placeholder='Year'
									list={listYear}
									onChange={formikField.handleChange}
									value={formikField.values.yearSearch}
								/>
							</div>
							<div className='col-md-4'>
								<Button icon='search' type='submit' color='info'>
									Search
								</Button>
								<Button
									style={{ marginLeft: '5px' }}
									icon='refresh'
									type='reset'
									onClick={() => {
										window.location.reload(true);
									}}
									color='warning'>
									Reset
								</Button>
							</div>
						</div>
					</Form>
				);
			}}
		</Formik>
	);
});

const FormInput = (dt) => {
	const { setPreview, handleReloadData, setFilter, handlePreview, yearRef } = dt;
	const [dataImport, setDataImport] = useState([]);
	const [listYear, setListYear] = useState([]);
	const [loading, setLoading] = useState(false);
	const [label, setLabel] = useState('');
	const [labelLoading, setLabelLoading] = useState('Processing Saved Data...');

	const initialValues = {
		loading: false,
		type: '',
		year: '',
		file: '',
	};

	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		if (values.type !== '' && values.year !== '' && dataImport.length > 0) {
			const newValue = {
				...values,
				data: dataImport,
			};
			try {
				handleSubmit(
					newValue,
					initialValues,
					handleReloadData,
					handleReset,
					resetForm,
					setLabel,
					yearRef,
					setFilter,
					setLoading,
				);
				setStatus({ success: true });
			} catch (error) {
				setStatus({ success: false });
				setSubmitting(false);
				setErrors({ submit: error.message });
			}
		} else {
			if (values.type === '') {
				showNotification('Warning!', 'Please Entries Type', 'danger');
			}
			if (values.year === '') {
				showNotification('Warning!', 'Please Entries Year', 'danger');
			}
			if (dataImport.length === 0) {
				showNotification('Warning!', 'Please Entries File Import', 'danger');
			}
		}
	};

	const handleReset = () => {
		setDataImport([]);
		setPreview('N');
	};

	const handleUploadFile = async (e) => {
		e.preventDefault();
		handlePreview([]);
		setDataImport([]);
		if (e.target.files) {
			const f = e.target.files[0];
			if (f.name.split('.').pop() === 'xlsx') {
				setLabel(f.filename);
				setLabelLoading('Processing Reading Data...');
				setLoading(true);
				const reader = new FileReader();
				reader.onload = (ev) => {
					const data = ev.target.result;
					const readedData = XLSX.read(data, { type: 'binary' });
					const wsname = readedData.SheetNames[0];
					const ws = readedData.Sheets[wsname];
					const dataParse = XLSX.utils.sheet_to_json(ws, { header: 1, blankrows: false });
					if (dataParse.length > 0) {
						const imprData = dataParse.slice(1, dataParse.length);
						const valueData = [];

						imprData.forEach((item) => {
							if (item[0] !== undefined && item[0] !== '') {
								valueData.push(item);
							}
						});

						const arr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
						let checkAll = false;
						let message = '';
						valueData.forEach((items, i) => {
							const row = i + 1;
							let check = false;
							arr.forEach((item, idx) => {
								if (
									items[idx] === undefined ||
									items[idx] === '' ||
									items[idx] === null
								) {
									check = true;
								}
							});
							if (check) {
								const number = row + 1;
								if (message === '') {
									message = number;
								} else {
									message = `${message}, ${number}`;
								}
								checkAll = true;
							}
						});
						if (checkAll) {
							showNotification(
								'Warning!',
								`Please Check Row Data ${message} in ${f.name}`,
								'danger',
							);
						} else {
							handlePreview(valueData);
							setTimeout(() => {
								setDataImport(valueData);
							}, 100);
						}
					} else {
						showNotification('Warning!', `Not Found data excel`, 'danger');
						handlePreview([]);
					}
					setLabelLoading('Processing Saved Data...');
					setLoading(false);
				};
				reader.readAsBinaryString(f);
			} else {
				showNotification(
					'Warning!',
					`Please Entries File ${f.name} Extension .xlsx`,
					'danger',
				);
			}
		}
	};

	const fecthYear = () => {
		const tempYear = [];
		const lengthYear = [0, 0, 0, 0, 0, 0];
		lengthYear.forEach((item, i) => {
			const yyyy = Number(new Date().getFullYear() - 1) + i;
			const year = {
				text: String(yyyy),
				value: String(yyyy),
			};
			tempYear.push(year);
		});
		setListYear(tempYear);
	};

	useEffect(() => {
		fecthYear();
	}, []);

	return (
		<Formik enableReinitialize initialValues={{ ...initialValues }} onSubmit={onSubmit}>
			{(formikField) => {
				return (
					<Form>
						<div className='row'>
							<div className='col-md-2'>
								<Select
									id='type'
									placeholder='What to import?'
									list={listType}
									onChange={formikField.handleChange}
									value={formikField.values.type}
								/>
							</div>
							<div className='col-md-1'>
								<Select
									id='year'
									placeholder='Year'
									list={listYear}
									onChange={formikField.handleChange}
									value={formikField.values.year}
								/>
							</div>
							<div className='col-md-4'>
								<Input
									id='file'
									accept='.xlsx'
									type='file'
									onChange={(e) => handleUploadFile(e)}
									onBlur={formikField.handleBlur}
									isValid={formikField.isValid}
									isTouched={formikField.touched.file}
									value={label}
									invalidFeedback={formikField.errors.file}
									autoComplete='off'
								/>
							</div>
							<div
								className='col-md-4'
								style={{ display: loading ? 'none' : 'block' }}>
								<Button icon='check' type='submit' color='success'>
									Import
								</Button>
								<Button
									style={{ marginLeft: '5px' }}
									onClick={() => {
										formikField.resetForm({ values: initialValues });
										handlePreview([]);
										setPreview('N');
										setDataImport([]);
										setLabel('');
									}}
									icon='cancel'
									type='reset'
									color='warning'>
									Cancel
								</Button>
							</div>
							<div
								className='col-md-4'
								style={{ display: loading ? 'block' : 'none' }}>
								<Spinner
									className='float-start'
									color='success'
									role='status'
									size='md'
								/>
								<Label
									style={{ marginLeft: '5px', marginTop: '5px' }}
									className='float-start'>
									{labelLoading}
								</Label>
							</div>
						</div>
					</Form>
				);
			}}
		</Formik>
	);
};

const BudgetTarget = () => {
	const customDataTablePreview = useRef(null);
	const yearRef = useRef(null);
	const [preview, setPreview] = useState('N');
	const [dataPreview, setDataPreview] = useState([]);
	const [loading, setLoading] = useState(false);
	const [data, setData] = useState([]);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [page, setPage] = useState(1);
	const [reloadData, setReloadData] = useState(false);
	const [filter, setFilter] = useState({});

	const fetchData = async (nPage, nPerPage = 10, nFilter = {}) => {
		setLoading(true);
		const params = {
			...nFilter,
			page: nPage,
			sizePerPage: nPerPage,
		};

		return budget_targetModule.readTable(new URLSearchParams(params)).then((res) => {
			setData(res.foundData);
			setTotalRows(res.countData);
			setLoading(false);
		});
	};

	const handleReloadData = () => {
		setPage(1);
		setReloadData(!reloadData);
	};

	const handleDownload = async (type) => {
		const params = {
			typeTemplate: type,
		};
		return budget_targetModule
			.donwload(new URLSearchParams(params))
			.then((res) => {
				showNotification(
					'Information!',
					'Template has been downloded successfully',
					'success',
				);
				FileDownload(res, 'Template Budget & Target.xlsx');
			})
			.catch((err) => {
				showNotification('Warning!', err, 'danger');
			});
	};

	const handlePreview = async (e) => {
		if (e.length > 0) {
			setPreview('Y');
			setDataPreview(e);
		} else {
			setPreview('N');
		}
	};

	useEffect(() => {
		fetchData(page, perPage, filter);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [page, perPage, filter, reloadData]);

	useEffect(() => {}, [preview, dataPreview]);

	const { t } = useTranslation('category');

	return (
		<PageWrapper title='Budget & Target'>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card className='col-12'>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle> {t('Import Budget (SKP & TT) & Target Sell In')}</CardTitle>
						</CardLabel>
						<Button
							icon='download'
							type='button'
							color='info'
							onClick={() => handleDownload('')}>
							Template
						</Button>
					</CardHeader>
					<CardBody>
						<div className='col-12 mb-3 g-4'>
							<FormInput
								handlePreview={(e) => handlePreview(e)}
								setPreview={(e) => setPreview(e)}
								handleReloadData={handleReloadData}
								setFilter={setFilter}
								yearRef={yearRef}
							/>
						</div>
						<div
							className='mt-1 row'
							style={{ display: preview === 'Y' ? 'block' : 'none' }}>
							<div className='col-12 mt-4'>
								<TablePreview
									ref={customDataTablePreview}
									tab={1}
									data={dataPreview}
								/>
							</div>
						</div>
					</CardBody>
				</Card>
				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('History')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<FormSearch
							ref={yearRef}
							setFilter={setFilter}
							handleReloadData={handleReloadData}
						/>
						<div className='mt-1 row'>
							<div className='col-12 mt-4'>
								<DataTable
									data={data}
									loading={loading}
									totalRows={totalRows}
									handlePageChange={setPage}
									handlePerRowsChange={setPerPage}
								/>
							</div>
						</div>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

TablePreview.propTypes = {
	data: PropTypes.arrayOf(PropTypes.instanceOf(Array)),
};

TablePreview.defaultProps = {
	data: [],
};

DataTable.propTypes = {
	data: PropTypes.arrayOf(PropTypes.instanceOf(Object)),
	loading: PropTypes.bool,
	totalRows: PropTypes.number,
	handlePageChange: PropTypes.func,
	handlePerRowsChange: PropTypes.func,
};

DataTable.defaultProps = {
	data: [],
	loading: false,
	totalRows: 0,
	handlePageChange: null,
	handlePerRowsChange: null,
};

export default BudgetTarget;
