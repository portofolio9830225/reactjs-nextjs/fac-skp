import React, { useEffect, useMemo, useState } from 'react';
import { useFormik } from 'formik';
import { useTranslation } from 'react-i18next';
import * as yup from 'yup';
import Swal from 'sweetalert2';
import moment from 'moment';
import PropTypes from 'prop-types';
import Accordion, { AccordionItem } from '../components/bootstrap/Accordion';
import Card, { CardBody, CardHeader, CardLabel, CardTitle } from '../components/bootstrap/Card';
import Page from '../layout/Page/Page';
import PageWrapper from '../layout/PageWrapper/PageWrapper';
import PageLayoutHeader from '../pages/common/Headers/PageLayoutHeader';
import CustomWizard, { CustomWizardItem } from '../components/custom/CustomWIzard';
import useDarkMode from '../hooks/useDarkMode';
import Button from '../components/bootstrap/Button';
import Modal, { ModalBody, ModalHeader, ModalTitle } from '../components/bootstrap/Modal';
import DarkDataTable from '../components/DarkDataTable';
import { getaActiveRoles, getRequester } from '../helpers/helpers';
import showNotification from '../components/extras/showNotification';
import FormGroup from '../components/bootstrap/forms/FormGroup';
import Checks from '../components/bootstrap/forms/Checks';
import CustomSelect from '../components/custom/CustomSelect';
import Input from '../components/bootstrap/forms/Input';
import UserModule from '../modules/UserModule';
import NewParameter from '../components/custom/NewParameter';

const tmp_data = {
	foundData: [
		{
			role: [
				{
					role_code: '0009',
					role_name: 'SPREADING',
					segment_code: '0001',
					segment_name: 'Retail',
					is_default: true,
					parent_code: 'LNK000586',
					parent_name: 'RISKA SONIANGSIH',
					regional: [
						{
							regional_code: '0001-001',
							regional_name: 'West',
						},
					],
				},
				{
					role_code: '0007',
					role_name: 'SPG SELL OUT',
					segment_code: '0001',
					segment_name: 'Retail',
					is_default: false,
					parent_code: 'LNK000586',
					parent_name: 'RISKA SONIANGSIH',
					regional: [
						{
							regional_code: '0001-001',
							regional_name: 'West',
							area: [
								{
									area_code: '0001-001-011',
									area_name: 'NKA JAKARTA',
								},
							],
						},
					],
				},
			],
			_id: '633e011f12a4d7731bd3988f',
			department: 'guest',
			username: 'asd',
			password: '$2a$12$x57U4oh/1xlgMPqCreiYwe3JmO.aHjr7fEhCkGZQE0y1yTZnXrOf.',
			name: 'asd',
			email: 'asd@asd.asd',
			type: 'guest',
			detail_user: {
				phone: 123,
			},
			is_active: true,
			created_at: '2022-10-05T22:11:43.551Z',
			created_by: 'LNK000632',
			updated_at: '2022-10-06T02:52:16.455Z',
			updated_by: 'LNK000632',
		},
		{
			role: [
				{
					role_code: '0008',
					role_name: 'SPG',
					segment_code: '0001',
					segment_name: 'Retail',
					is_default: true,
					regional: [
						{
							regional_code: '0001-001',
							regional_name: 'West',
							area: [
								{
									area_code: '0001-001-012',
									area_name: 'LKA JAKARTA',
									city: [
										{
											city_code: '158',
											city_name: 'KOTA JAKARTA PUSAT',
										},
									],
								},
							],
						},
					],
				},
			],
			_id: '6319a085146c800013e7d467',
			department: 'guest',
			username: 'assdfasdf123123',
			password: '$2a$12$bpyMP02bJZ6H4WzVgbq5reK3sPqDPaDIUNWeBtvhX09nVGlWZPT16',
			name: 'sadfasdf',
			email: '123123@adfasd.com',
			type: 'guest',
			detail_user: {
				phone: 2131234,
			},
			is_active: true,
			created_at: '2022-09-08T07:57:57.697Z',
			created_by: 'LNK000727',
			updated_at: '2022-10-05T22:10:52.420Z',
			updated_by: 'LNK000632',
		},
		{
			role: [
				{
					role_code: '0008',
					role_name: 'SPG',
					segment_code: '0001',
					segment_name: 'Retail',
					is_default: true,
					parent_code: 'LNK000727',
					parent_name: 'Agus Masudi',
					regional: [
						{
							regional_code: '0001-002',
							regional_name: 'East',
							area: [
								{
									area_code: '0001-002-001',
									area_name: 'JATIM BARAT',
								},
							],
						},
					],
				},
			],
			_id: '630d5e1fb2443a0019af574a',
			department: 'guest',
			username: 'SBY0161',
			password: '$2a$12$HtBRUp9a4EcMAjK8XgkafOyT1G7rLum4tiwQvX9kutVIdfX9SdFxG',
			name: 'Kinanti Sela Dewi',
			email: 'dkinanti650@gmail.com',
			type: 'guest',
			detail_user: {
				phone: 83853422484,
			},
			is_active: true,
			created_at: '2022-08-30T00:47:27.527Z',
			created_by: 'LNK000632',
		},
		{
			role: [
				{
					role_code: '0003',
					role_name: 'ASPS',
					segment_code: '0001',
					segment_name: 'Retail',
					is_default: true,
					parent_code: 'LNK000810',
					parent_name: 'Agus Dwiraharjo',
					regional: [
						{
							regional_code: '0001-002',
							regional_name: 'East',
							area: [
								{
									area_code: '0001-002-002',
									area_name: 'JATIM TIMUR',
								},
								{
									area_code: '0001-002-001',
									area_name: 'JATIM BARAT',
								},
							],
						},
					],
				},
			],
			_id: '62e88d482d8dfe560dd9c3d2',
			department: 'guest',
			username: 'LNK000727',
			password: '$2a$12$5jj9SUkPbRYsPSEZDoZjaOu/5ELFmO2fmZ.S9Ipf8Fgg0AB1sEREm',
			name: 'Agus Masudi',
			email: 'lnk@mailnesia.com',
			type: 'guest',
			detail_user: {
				phone: 9,
			},
			is_active: true,
			created_at: '2022-08-02T02:34:48.516Z',
			created_by: 'LNK000632',
			updated_at: '2022-08-29T08:08:44.192Z',
			updated_by: 'LNK000632',
			last_login: '2022-10-26T09:09:16.301Z',
			app_version: '1.0.0',
			os: 'android',
			fcm_token:
				'fJPrqWtTRDuozMYH9o7vW7:APA91bHvWwi4neJSKigwguU9XBjDEi0WQgg3Q2nGcKobqMWzziywHu8gsMAhckg5cUoqj2LqEYNyiqNp-jyRLTPc4FQNTiX5-4B6kcP7TK0HbtWVnvtY0Qjif2YM1YjvH72YoAjlem4z',
			address: 'Address',
			phone: '123456789',
		},
		{
			role: [
				{
					role_code: '0001',
					role_name: 'NSM',
					segment_code: '0001',
					segment_name: 'Retail',
					is_default: true,
					regional: [
						{
							regional_code: '0001-001',
							regional_name: 'West',
						},
						{
							regional_code: '0001-002',
							regional_name: 'East',
						},
					],
				},
			],
			_id: '62e88c802d8dfe560dd9c3c9',
			department: 'guest',
			username: 'LNK000529',
			password: '$2a$12$jTitdcQxF8hJtKwkvS/dg.4kIfkKOTVDokDcKHVoxJJ7txgvtNYqe',
			name: 'Steven Chandra',
			email: 'lnk@mailnesia.com',
			type: 'employee',
			detail_user: {
				phone: 0,
			},
			is_active: true,
			created_at: '2022-08-02T02:31:28.545Z',
			created_by: 'LNK000632',
			updated_at: '2022-08-29T07:27:37.452Z',
			updated_by: 'LNK000632',
		},
		{
			role: [
				{
					role_code: '0008',
					role_name: 'SPG',
					segment_code: '0001',
					segment_name: 'Retail',
					is_default: true,
					parent_code: 'LNK000727',
					parent_name: 'Agus Masudi',
					regional: [
						{
							regional_code: '0001-002',
							regional_name: 'East',
							area: [
								{
									area_code: '0001-002-001',
									area_name: 'JATIM BARAT',
								},
							],
						},
					],
				},
			],
			_id: '630c5ccff479522442d47cc5',
			name: 'Fitriana',
			username: 'SBY0160',
			phone: '87762961208',
			email: 'fana1695@gmail.com',
			password: '$2y$10$6vRTGbkU0RXhvbJNpOv9H.1NZu0h/ZvUy8BQCy8OZK1zzVTQWoBry',
			type: 'guest',
			is_active: true,
			department: 'guest',
			detail_user: {
				phone: 0,
			},
			updated_at: '2022-08-29T08:40:57.431Z',
			updated_by: 'LNK000632',
		},
		{
			role: [
				{
					role_code: '0006',
					role_name: 'SALES TO',
					segment_code: '0001',
					segment_name: 'Retail',
					is_default: true,
					parent_code: 'LNK000706',
					parent_name: 'Dede Denni',
					regional: [
						{
							regional_code: '0001-001',
							regional_name: 'West',
							area: [
								{
									area_code: '0001-001-013',
									area_name: 'GT JAKARTA',
								},
							],
						},
					],
				},
			],
			_id: '630c5ccff479522442d47cc6',
			name: 'Muhammad Nur Fikri',
			username: 'JKT0467',
			phone: '85881578193',
			email: 'fikrinur273@gmail.com',
			password: '$2y$10$waaT4fKzBIb9nUWjZ5trr.jlGeh93WVuARTq9l33be27MZ7U8Iq5C',
			type: 'guest',
			is_active: true,
			department: 'guest',
			detail_user: {
				phone: 0,
			},
			updated_at: '2022-08-29T09:22:01.224Z',
			updated_by: 'LNK000632',
		},
		{
			role: [
				{
					role_code: '0006',
					role_name: 'SALES TO',
					segment_code: '0001',
					segment_name: 'Retail',
					is_default: true,
					parent_code: 'LNK000727',
					parent_name: 'Agus Masudi',
					regional: [
						{
							regional_code: '0001-002',
							regional_name: 'East',
							area: [
								{
									area_code: '0001-002-001',
									area_name: 'JATIM BARAT',
								},
							],
						},
					],
				},
			],
			_id: '62cb89319c6929f07c2c8953',
			name: 'SPG01-FAIZ',
			username: 'TES1010',
			password: '$2a$12$MDfH3wpHh3aBRh1lbF2jXOZMbvgjriY2J2LKswrehDLd8Vt5R9iIS',
			email: 'mis.stf05+2@lnk.co.id',
			type: 'guest',
			department: 'guest',
			detail_user: {
				phone: 0,
			},
			is_active: true,
			updated_at: '2022-08-29T08:19:18.650Z',
			updated_by: 'LNK000632',
			last_login: '2022-10-26T08:52:28.984Z',
			address: 'alamat jalan raya\ntest',
			phone: '0877540701818',
			app_version: '1.0.0',
			fcm_token:
				'fJPrqWtTRDuozMYH9o7vW7:APA91bHvWwi4neJSKigwguU9XBjDEi0WQgg3Q2nGcKobqMWzziywHu8gsMAhckg5cUoqj2LqEYNyiqNp-jyRLTPc4FQNTiX5-4B6kcP7TK0HbtWVnvtY0Qjif2YM1YjvH72YoAjlem4z',
			os: 'android',
		},
		{
			role: [
				{
					role_code: '0003',
					role_name: 'ASPS',
					segment_code: '0001',
					segment_name: 'Retail',
					is_default: true,
					parent_code: 'LNK000810',
					parent_name: 'Agus Dwiraharjo',
					regional: [
						{
							regional_code: '0001-002',
							regional_name: 'East',
							area: [
								{
									area_code: '0001-002-007',
									area_name: 'KALSEL, KALTENG',
								},
							],
						},
					],
				},
			],
			_id: '630c5ccff479522442d47cc7',
			name: 'Erwan Saputra',
			username: 'LNK000862',
			phone: '82152222972',
			email: 'mis.stf05+22@lnk.co.id',
			password: '$2y$10$HNh8kQoECBO4BHPlxSzISOt0Tm9NwgjXkKjqYi4uaifFhACrzN6EW',
			type: 'guest',
			is_active: true,
			department: 'guest',
			detail_user: {
				phone: 0,
			},
			updated_at: '2022-10-05T22:11:11.086Z',
			updated_by: 'LNK000632',
		},
		{
			role: [
				{
					role_code: '0009',
					role_name: 'SPREADING',
					segment_code: '0001',
					segment_name: 'Retail',
					is_default: true,
					parent_code: 'LNK000862',
					parent_name: 'Erwan Saputra',
					regional: [
						{
							regional_code: '0001-002',
							regional_name: 'East',
							area: [
								{
									area_code: '0001-002-007',
									area_name: 'KALSEL, KALTENG',
								},
							],
						},
					],
				},
			],
			_id: '630c5ccff479522442d47cc3',
			name: 'Imma Yunita',
			username: 'BNJ0039',
			phone: '81250874089',
			email: 'haikalmaulana7614@gmail.com',
			password: '$2y$10$/Nn.Z5J61ik4.YFG.Ogw3.o6EubiDCVPGvzxETXyumwyvIAuEdngy',
			type: 'guest',
			is_active: true,
			detail_user: {
				phone: 0,
			},
			department: 'guest',
			updated_at: '2022-08-29T09:56:03.460Z',
			updated_by: 'LNK000632',
		},
	],
	currentPage: 1,
	countPages: 10,
	countData: 97,
};

const handleSubmit = (values) => {
	const newResponse = new Promise((resolve, reject) => {
		try {
			resolve(
				UserModule.create(values)
					.then(() => {
						showNotification(
							'Information!',
							'Data has been saved successfully',
							'success',
						);
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					}),
			);
		} catch (e) {
			reject(new Error({ error: true }));
		}
	});
	return newResponse;
};

const handleUpdate = (values) => {
	const newResponse = new Promise((resolve, reject) => {
		try {
			resolve(
				UserModule.update(values)
					.then(() => {
						showNotification(
							'Information!',
							'Data has been update successfully',
							'success',
						);
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					}),
			);
		} catch (e) {
			reject(new Error({ error: true }));
		}
	});
	return newResponse;
};

const handleDelete = (values) => {
	const { _id } = values;
	const { username } = getRequester();
	const newValues = {
		_id,
		requester: username,
	};

	const newResponse = new Promise((resolve, reject) => {
		try {
			if (values._id) {
				Swal.fire({
					title: 'Are you sure?',
					text: 'Please check your entries !',
					icon: 'info',
					showCancelButton: true,
					confirmButtonText: 'Yes',
				}).then((result) => {
					if (result.isConfirmed) {
						resolve(
							UserModule.destroy(newValues)
								.then(() => {
									showNotification(
										'Information!',
										'Data has been deleted successfully',
										'success',
									);
								})
								.catch((err) => {
									showNotification('Warning!', err, 'danger');
								}),
						);
					} else if (result.dismiss === Swal.DismissReason.cancel) {
						Swal.fire('Cancelled', 'Your data is safe :)', 'error');
					}
				});
			}
		} catch (e) {
			reject(new Error({ error: true }));
		}
	});
	return newResponse;
};

const FormCustom = ({
	initialValues,
	isReadOnly,
	isUpdate,
	select_department,
	select_employee,
	list_department,
	list_role,
	list_regional,
	handleCustomSubmit,
	curPage,
	perPage,
	showAll,
}) => {
	const { t } = useTranslation('crud');
	const { username } = getRequester();
	const { default_segment_code } = getaActiveRoles();
	const [init, setInit] = useState(true);

	// init params detail form
	const [list_employee, setListEmployee] = useState([]);
	const [loading_employee, setLoadingEmployee] = useState(false);
	const [selectDepartment, setSelectDepartment] = useState(select_department);
	const [selectEmployee, setSelectEmployee] = useState(select_employee);

	const [isReset, setReset] = useState(false);
	const [data_role, setDataRole] = useState([]);

	const onChangeIsEmployee = (e) => {
		formik.handleChange(e);
	};

	// on change listener select department
	const onChangeDepartment = (select) => {
		setSelectDepartment(select);
		formik.setFieldValue('department', select.value);

		setSelectEmployee(null);
		formik.setFieldValue('employee', '');
		formik.setFieldValue('nik', '');
		formik.setFieldValue('name', '');
		formik.setFieldValue('email', '');
	};

	// on change listener select employee
	const onChangeEmployee = (select) => {
		setSelectEmployee(select);
		formik.setFieldValue('employee', select.value);
		formik.setFieldValue('nik', select.detail.username);
		formik.setFieldValue('name', select.detail.name);
		formik.setFieldValue('email', select.detail.email);
	};

	const validate = (values) => {
		const errors = {};

		if (values.is_employee) {
			if (!selectDepartment) errors.department = 'Required';
			if (!selectEmployee) errors.employee = 'Required';
		} else {
			if (!values.nik) errors.nik = 'Required';
			if (!values.name) errors.name = 'Required';
			if (!values.email) errors.email = 'Required';
		}

		return errors;
	};

	const formik = useFormik({
		initialValues: { ...initialValues },
		validate,
		validationSchema: yup.object({
			email: yup.string().email().required(),
			phone: yup.string().required('Required'),
		}),
		onSubmit: (values, { setSubmitting, resetForm, setErrors }) => {
			try {
				const new_values = {};

				new_values.requester = username;
				new_values.username = values.nik;
				new_values.name = values.name;
				new_values.email = values.email;
				new_values.detail_user = {
					phone: values.phone,
				};
				new_values.type = values.is_employee ? 'employee' : 'guest';
				new_values.department = values.is_employee ? values.department : 'guest';
				new_values.role = data_role.length === 0 && isUpdate ? values.role : data_role;
				new_values.segment_code = default_segment_code;

				if (isUpdate) {
					new_values._id = values._id;
				}

				const result_check = checkFormatParameter(new_values);
				if (result_check.error) {
					showNotification('Information', result_check.message, 'danger');
					return;
				}

				Swal.fire({
					title: 'Are you sure?',
					text: 'Please check your entries !',
					icon: 'info',
					showCancelButton: true,
					confirmButtonText: 'Yes',
				}).then((result) => {
					if (result.isConfirmed) {
						// submit value
						handleCustomSubmit({
							values: new_values,
							options: { curPage, perPage, showAll },
						});

						// reset field detail
						resetForm(initialValues);
						setSelectDepartment(null);
						setSelectEmployee(null);

						// reset parameter
						setReset(true);
					} else if (result.dismiss === Swal.DismissReason.cancel) {
						Swal.fire('Cancelled', 'Your data is safe :)', 'error');
					}
				});
			} catch (error) {
				setErrors({ submit: error.message });
				Swal.fire('Information ', 'Please check your entries again!', 'error');
			} finally {
				setSubmitting(false);
			}
		},
	});

	const checkFormatParameter = (params) => {
		let result = { error: false, message: 'data valid' };
		if (params.role.length === 0) {
			result = { error: true, message: 'there must be at least one role selected' };
		} else {
			let found_default = false;
			params.role.forEach((item) => {
				if (result.error) return;

				if (!item.role_code) {
					result = {
						error: true,
						message: 'complete the role parameter data before continuing the process',
					};
				}

				if (item.is_default) {
					found_default = true;
				}

				if (!item.regional) {
					result = {
						error: true,
						message: 'at least one regional chosen for each role',
					};
				}
			});

			if (!found_default && !result.error) {
				result = {
					error: true,
					message: 'choose one of the roles to make the main role',
				};
			}
		}

		return result;
	};

	useEffect(() => {
		if (selectDepartment && !isReadOnly) {
			fetchDataEmployee(selectDepartment.value);
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [selectDepartment]);

	const fetchDataEmployee = async (department) => {
		setLoadingEmployee(true);

		const query = `department=${department}`;
		return UserModule.getUserByDept(query)
			.then((response) => {
				const employee = response.map((item) => {
					return {
						value: item.username,
						label: `(${item.username}) ${item.name}`,
						detail: item,
					};
				});
				setListEmployee(employee);
			})
			.catch(() => {
				setListEmployee([]);
			})
			.finally(() => {
				setLoadingEmployee(false);
			});
	};

	return (
		<Accordion id='accordion-form' activeItemId={isUpdate ? 'form-default' : false}>
			<AccordionItem id='form-default' title={t('form')} icon='AddBox'>
				<CustomWizard
					className='mb-0'
					isHeader
					color='success'
					isDisable={isReadOnly}
					noValidate
					onSubmit={formik.handleSubmit}>
					<CustomWizardItem id='wizard-detail' title='Detail'>
						<FormGroup className='mb-3 col-md-12'>
							<Checks
								type='switch'
								id='is_employee'
								label='Is Employee'
								onChange={onChangeIsEmployee}
								checked={formik.values.is_employee}
								disabled={isUpdate}
							/>
						</FormGroup>
						{formik.values.is_employee && (
							<>
								<FormGroup
									className='mb-3'
									isColForLabel
									labelClassName='col-sm-2 text-capitalize'
									childWrapperClassName='col-sm-6'
									id='empl'
									label='Department'>
									<CustomSelect
										value={selectDepartment}
										options={list_department}
										onChange={onChangeDepartment}
										isSearchable={list_department.length > 7}
										isValid={
											formik.errors.department !== 'Required' ||
											selectDepartment !== null
										}
										invalidFeedback={formik.errors.department}
										isDisable={isUpdate}
									/>
								</FormGroup>
								<FormGroup
									className='mb-3'
									isColForLabel
									labelClassName='col-sm-2 text-capitalize'
									childWrapperClassName='col-sm-6'
									id='employee'
									label='Employee'>
									<CustomSelect
										value={selectEmployee}
										options={list_employee}
										isSearchable={list_employee.length > 7}
										onChange={onChangeEmployee}
										isValid={
											formik.errors.employee !== 'Required' ||
											selectEmployee !== null
										}
										invalidFeedback={formik.errors.employee}
										isDisable={list_employee.length === 0 || isUpdate}
										isLoading={loading_employee}
										placeholder={
											list_employee.length === 0 ? 'Not Found' : 'Select...'
										}
									/>
								</FormGroup>
							</>
						)}
						<FormGroup
							className='mb-3'
							isColForLabel
							labelClassName='col-sm-2 text-capitalize'
							childWrapperClassName='col-sm-6'
							id='nik'
							label='Code / NIK'>
							<Input
								type='text'
								onChange={formik.handleChange}
								onBlur={formik.handleBlur}
								value={formik.values.nik}
								isValid={formik.isValid}
								isTouched={formik.touched.nik}
								invalidFeedback={formik.errors.nik}
								disabled={formik.values.is_employee || isUpdate}
								autoComplete='off'
							/>
						</FormGroup>
						<FormGroup
							className='mb-3'
							isColForLabel
							labelClassName='col-sm-2 text-capitalize'
							childWrapperClassName='col-sm-6'
							id='name'
							label='Name'>
							<Input
								type='text'
								onChange={formik.handleChange}
								onBlur={formik.handleBlur}
								value={formik.values.name}
								isValid={formik.isValid}
								isTouched={formik.touched.name}
								invalidFeedback={formik.errors.name}
								disabled={formik.values.is_employee || isReadOnly}
								autoComplete='off'
							/>
						</FormGroup>
						<FormGroup
							className='mb-3'
							isColForLabel
							labelClassName='col-sm-2 text-capitalize'
							childWrapperClassName='col-sm-6'
							id='email'
							label='Email'>
							<Input
								type='email'
								onChange={formik.handleChange}
								onBlur={formik.handleBlur}
								value={formik.values.email}
								isValid={formik.isValid}
								isTouched={formik.touched.email}
								invalidFeedback={formik.errors.email}
								disabled={formik.values.is_employee || isReadOnly}
								autoComplete='off'
							/>
						</FormGroup>
						<FormGroup
							className='mb-3'
							isColForLabel
							labelClassName='col-sm-2 text-capitalize'
							childWrapperClassName='col-sm-6'
							id='phone'
							label='Phone'>
							<Input
								type='number'
								onChange={formik.handleChange}
								onBlur={formik.handleBlur}
								value={formik.values.phone}
								isValid={formik.isValid}
								isTouched={formik.touched.phone}
								invalidFeedback={formik.errors.phone}
								autoComplete='off'
								disabled={isReadOnly}
							/>
						</FormGroup>
					</CustomWizardItem>
					<CustomWizardItem id='wizard-parameter' title='Parameter'>
						<Card stretch shadow='none' style={{ height: '38rem' }}>
							<CardBody isScrollable>
								<NewParameter
									initialValues={
										init
											? formik.values.role.filter(
													(item) =>
														item.segment_code === default_segment_code,
											  )
											: data_role
									}
									onChange={setDataRole}
									list_role={list_role}
									list_regional={list_regional}
									isReset={isReset}
									setReset={setReset}
									isReadOnly={isReadOnly}
									user_value={formik.values.nik}
									init={init}
									setInit={setInit}
								/>
							</CardBody>
						</Card>
					</CustomWizardItem>
				</CustomWizard>
			</AccordionItem>
		</Accordion>
	);
};

FormCustom.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
	isReadOnly: PropTypes.bool,
	isUpdate: PropTypes.bool,
	select_department: PropTypes.instanceOf(Object),
	select_employee: PropTypes.instanceOf(Object),
	list_department: PropTypes.instanceOf(Array),
	list_role: PropTypes.instanceOf(Array),
	list_regional: PropTypes.instanceOf(Array),
	handleCustomSubmit: PropTypes.func,
	curPage: PropTypes.number,
	perPage: PropTypes.number,
	showAll: PropTypes.bool,
};
FormCustom.defaultProps = {
	initialValues: {
		_id: null,
		department: 'guest',
		username: '',
		name: '',
		email: '',
		type: 'guest',
		detail_user: null,
		role: [],
	},
	isReadOnly: false,
	isUpdate: false,
	select_department: null,
	select_employee: null,
	list_department: [],
	list_role: [],
	list_regional: [],
	handleCustomSubmit: null,
	curPage: 1,
	perPage: 10,
	showAll: false,
};

const FormCustomModal = ({
	initialValues,
	handleCustomUpdate,
	handleCustomDelete,
	list_department,
	list_role,
	list_regional,
	curPage,
	perPage,
	showAll,
}) => {
	const { darkModeStatus } = useDarkMode();
	const [isOpen, setOpen] = useState(false);
	const [isReadOnly, setReadOnly] = useState(false);

	const [title, setTitle] = useState('');

	const { is_employee, department, nik, name } = initialValues;

	return (
		<>
			<Button
				icon='GridOn'
				color='info'
				type='button'
				className='me-3'
				isLight={darkModeStatus}
				onClick={() => {
					setTitle('Detail');
					setReadOnly(true);
					setOpen(true);
				}}
			/>
			<Button
				icon='Edit'
				color='success'
				type='button'
				className='me-3'
				isLight={darkModeStatus}
				onClick={() => {
					setTitle('Update');
					setReadOnly(false);
					setOpen(true);
				}}
			/>
			<Button
				icon='Delete'
				color='danger'
				type='button'
				className='me-3'
				isLight={darkModeStatus}
				onClick={() =>
					handleCustomDelete({
						values: initialValues,
						options: { curPage, perPage, showAll },
					})
				}
			/>

			<Modal isOpen={isOpen} setIsOpen={setOpen} size='xl' titleId='modal-crud'>
				<ModalHeader setIsOpen={setOpen} className='p-4'>
					<ModalTitle id='modal-crud'>{title} Data</ModalTitle>
				</ModalHeader>
				<ModalBody>
					<FormCustom
						initialValues={initialValues}
						isUpdate={isOpen}
						isReadOnly={isReadOnly}
						handleCustomSubmit={handleCustomUpdate}
						select_department={
							is_employee
								? { value: department, label: department.toUpperCase() }
								: null
						}
						select_employee={
							is_employee ? { value: nik, label: `(${nik}) ${name}` } : null
						}
						list_department={list_department}
						list_role={list_role}
						list_regional={list_regional}
						curPage={curPage}
						perPage={perPage}
						showAll={showAll}
					/>
				</ModalBody>
			</Modal>
		</>
	);
};

FormCustomModal.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
	handleCustomUpdate: PropTypes.func,
	handleCustomDelete: PropTypes.func,
	list_department: PropTypes.instanceOf(Array),
	list_role: PropTypes.instanceOf(Array),
	list_regional: PropTypes.instanceOf(Array),
	curPage: PropTypes.number,
	perPage: PropTypes.number,
	showAll: PropTypes.bool,
};
FormCustomModal.defaultProps = {
	handleCustomUpdate: null,
	handleCustomDelete: null,
	initialValues: {
		_id: null,
		department: 'guest',
		username: '',
		name: '',
		email: '',
		type: 'guest',
		detail_user: null,
		role: [],
	},
	list_department: [],
	list_role: [],
	list_regional: [],
	curPage: 1,
	perPage: 10,
	showAll: false,
};

const TableCustom = ({
	data,
	totalRows,
	perPage,
	fetchData,
	handleCustomDelete,
	handleCustomUpdate,
	list_department,
	list_role,
	list_regional,
	curPage,
	showAll,
	setShowAll,
}) => {
	const { darkModeStatus } = useDarkMode();

	const customHandleUpdate = (dataRow) => {
		handleCustomUpdate(dataRow);
	};

	const customHandleDelete = (dataRow) => {
		handleCustomDelete(dataRow);
	};

	const handlePageChange = (page) => {
		fetchData(page, perPage, showAll);
	};

	const handlePerRowsChange = async (newPerPage, page) => {
		const all = totalRows === newPerPage;
		setShowAll(all);
		return fetchData(page, newPerPage, all);
	};

	const paginationComponentOptions = {
		selectAllRowsItem: true,
		selectAllRowsItemText: 'ALL',
	};

	const columns = useMemo(
		() => [
			{
				name: 'Name',
				selector: (row) => row.name,
				sortable: true,
			},
			{
				name: 'Email',
				selector: (row) => row.email,
				sortable: true,
			},
			{
				name: 'Username',
				selector: (row) => row.username,
				sortable: true,
			},
			{
				name: 'Type',
				selector: (row) => row.type,
				sortable: true,
			},
			{
				name: 'Created',
				selector: (row) => moment(row.created_at).format('YYYY-MM-DD'),
				sortable: true,
			},
			{
				name: 'Action',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (row) => {
					const { department, detail_user, email, name, role, type, username, _id } = row;
					const initialValues = {
						is_employee: type === 'employee',
						department,
						phone: detail_user?.phone ?? '',
						email,
						name,
						role,
						type,
						nik: username,
						employee: username,
						_id,
					};

					return (
						<FormCustomModal
							initialValues={initialValues}
							handleCustomDelete={customHandleDelete}
							handleCustomUpdate={customHandleUpdate}
							list_department={list_department}
							list_role={list_role}
							list_regional={list_regional}
							curPage={curPage}
							perPage={perPage}
							showAll={showAll}
						/>
					);
				},
			},
		],
		// eslint-disable-next-line react-hooks/exhaustive-deps
		[list_department, list_role, list_regional, curPage, perPage, showAll],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			pagination
			paginationServer
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			paginationComponentOptions={paginationComponentOptions}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};

TableCustom.propTypes = {
	data: PropTypes.instanceOf(Array),
	totalRows: PropTypes.number,
	curPage: PropTypes.number,
	perPage: PropTypes.number,
	showAll: PropTypes.bool,
	fetchData: PropTypes.func,
	handleCustomUpdate: PropTypes.func,
	handleCustomDelete: PropTypes.func,
	list_department: PropTypes.instanceOf(Array),
	list_role: PropTypes.instanceOf(Array),
	list_regional: PropTypes.instanceOf(Array),
	setShowAll: PropTypes.func,
};
TableCustom.defaultProps = {
	data: [],
	totalRows: 0,
	curPage: 1,
	perPage: 10,
	showAll: false,
	fetchData: null,
	handleCustomUpdate: null,
	handleCustomDelete: null,
	list_department: [],
	list_role: [],
	list_regional: [],
	setShowAll: null,
};

const DataUser = () => {
	const { t } = useTranslation('crud');
	const [title] = useState({ title: t('Data User') });

	const [data, setData] = useState([]);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [curPage, setCurPage] = useState(1);

	const [list_department, setListDepartment] = useState([]);
	const [list_role, setListRole] = useState([]);
	const [list_regional, setListRegional] = useState([]);

	const [showAll, setShowAll] = useState(false);

	const initialValues = {
		is_employee: false,
		department: 'guest',
		employee: 'guest',
		nik: '',
		name: '',
		email: '',
		phone: '',
		role: [],
	};

	const createSubmit = ({ values, options }) => {
		handleSubmit(values)
			.then(() => {
				fetchData(options.curPage, options.perPage, options.showAll);
			})
			.catch(() => {});
	};

	const updateSubmit = ({ values, options }) => {
		handleUpdate(values)
			.then(() => {
				fetchData(options.curPage, options.perPage, options.showAll);
			})
			.catch(() => {});
	};

	const deleteSubmit = ({ values, options }) => {
		handleDelete(values)
			.then(() => {
				fetchData(options.curPage, options.perPage, options.showAll);
			})
			.catch(() => {});
	};

	useEffect(() => {
		fetchData(curPage, perPage, showAll);
		fetchDataDepartment();
		fetchDataRole();
		fetchDataRegional();

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const fetchData = async (newPage, newPerPage, all) => {
		console.log(newPage, all);
		setData(tmp_data.foundData);
		setTotalRows(tmp_data.countData);
		setCurPage(tmp_data.currentPage);
		setPerPage(newPerPage);
	};

	const fetchDataDepartment = async () => {
		return UserModule.getAllDepartment()
			.then((response) => {
				const role = response.map((item) => {
					return {
						value: item.toLowerCase(),
						label: item,
					};
				});
				setListDepartment(role);
			})
			.catch(() => {
				setListDepartment([]);
				showNotification('Information', 'load department error', 'danger');
			})
			.finally(() => {});
	};

	const fetchDataRole = async () => {
		setListRole([
			{
				value: 'role1',
				label: 'Role 1',
				detail: {
					role_code: 'role1',
					role_name: 'Role 1',
					segment_code: '0001',
					segment_name: '0001',
				},
			},
		]);
	};

	const fetchDataRegional = async () => {
		setListRegional([
			{
				value: 'west',
				label: 'West',
				detail: { regional_code: '0001', regional_name: 'West' },
			},
		]);
	};

	return (
		<PageWrapper title={title.title}>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card>
					<CardBody>
						<FormCustom
							initialValues={initialValues}
							handleCustomSubmit={createSubmit}
							list_department={list_department}
							list_role={list_role}
							list_regional={list_regional}
							curPage={curPage}
							perPage={perPage}
							showAll={showAll}
						/>
					</CardBody>
				</Card>
				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel icon='History' iconColor='info'>
							<CardTitle>{t('historical_data')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<TableCustom
							data={data}
							totalRows={totalRows}
							perPage={perPage}
							curPage={curPage}
							showAll={showAll}
							setShowAll={setShowAll}
							fetchData={fetchData}
							handleCustomUpdate={updateSubmit}
							handleCustomDelete={deleteSubmit}
							list_department={list_department}
							list_role={list_role}
							list_regional={list_regional}
						/>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

DataUser.propTypes = {};
DataUser.defaultProps = {};

export default DataUser;
