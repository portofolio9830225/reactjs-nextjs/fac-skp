import { useFormik } from 'formik';
import React, { useState, useEffect, useMemo, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import moment from 'moment';
import Swal from 'sweetalert2';
import Accordion, { AccordionItem } from '../components/bootstrap/Accordion';
import Card, {
	CardActions,
	CardBody,
	CardHeader,
	CardLabel,
	CardTitle,
} from '../components/bootstrap/Card';
import FormGroup from '../components/bootstrap/forms/FormGroup';
import Input from '../components/bootstrap/forms/Input';
import Page from '../layout/Page/Page';
import PageWrapper from '../layout/PageWrapper/PageWrapper';
import PageLayoutHeader from '../pages/common/Headers/PageLayoutHeader';
import CustomSelect from '../components/custom/CustomSelect';
import Button from '../components/bootstrap/Button';
import useDarkMode from '../hooks/useDarkMode';
import ActivityModule from '../modules/ActivityModule';
import TargetModule from '../modules/TargetModule';
import { getaActiveRoles, getRequester } from '../helpers/helpers';
import showNotification from '../components/extras/showNotification';
import Modal, { ModalBody, ModalHeader, ModalTitle } from '../components/bootstrap/Modal';
import DarkDataTable from '../components/DarkDataTable';
import InputGroup, { InputGroupText } from '../components/bootstrap/forms/InputGroup';
import RolesModule from '../modules/RolesModule';
import Notifications from '../components/Notifications';

const _LIST_TYPE = [
	{ value: 'function', label: 'Function' },
	{ value: 'menu', label: 'Menu' },
];

const _TYPE = {
	MENU: 'menu',
	FUNCTION: 'function',
};

const handleSubmit = (values) => {
	const newResponse = new Promise((resolve, reject) => {
		try {
			resolve(
				TargetModule.create(values)
					.then(() => {
						Notifications.showNotif({
							header: 'Information',
							message: 'Data has been saved successfully',
							type: 'success',
						});
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					}),
			);
		} catch (e) {
			reject(new Error({ error: true, message: e.message }));
		}
	});
	return newResponse;
};

const handleUpdate = (values) => {
	const newResponse = new Promise((resolve, reject) => {
		try {
			resolve(
				TargetModule.update(values)
					.then(() => {
						Notifications.showNotif({
							header: 'Information',
							message: 'Data has been update successfully',
							type: 'success',
						});
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					}),
			);
		} catch (e) {
			reject(new Error({ error: true, message: e.message }));
		}
	});
	return newResponse;
};

const handleDelete = (values) => {
	const { username } = getRequester();
	const newValues = {
		_id: values._id,
		requester: username,
	};

	const newResponse = new Promise((resolve, reject) => {
		try {
			if (values._id) {
				Swal.fire({
					title: 'Are you sure?',
					text: 'Please check your entries !',
					icon: 'question',
					showCancelButton: true,
					confirmButtonText: 'Yes',
				}).then((result) => {
					if (result.isConfirmed) {
						resolve(
							TargetModule.destroy(newValues)
								.then(() => {
									Notifications.showNotif({
										header: 'Information',
										message: 'Data has been deleted successfully',
										type: 'success',
									});
								})
								.catch((err) => {
									showNotification('Warning!', err, 'danger');
								}),
						);
					} else if (result.dismiss === Swal.DismissReason.cancel) {
						Swal.fire('Cancelled', 'Your data is safe :)', 'error');
					}
				});
			}
		} catch (e) {
			reject(new Error({ error: true, message: e.message }));
		}
	});
	return newResponse;
};

const FormCustom = ({
	initialValues,
	isUpdate,
	isReadOnly,
	listMenu,
	listRole,
	handleCustomSubmit,
	defaultValueMenu,
	defaultValueAccessTarget,
}) => {
	const { t } = useTranslation('crud');
	const { darkModeStatus } = useDarkMode();
	const { username } = getRequester();
	const { default_segment_code, default_segment_name } = getaActiveRoles();

	const [selectType, setSelectType] = useState(
		_LIST_TYPE.find((item) => item.value === initialValues.type),
	);
	const [selectMenu, setSelectMenu] = useState(defaultValueMenu);

	const [dataTarget, setDataTarget] = useState(defaultValueAccessTarget);
	const countDataTarget = useRef(defaultValueAccessTarget.length);

	const validate = (values) => {
		const errors = {};

		if (!values.target_name) errors.target_name = 'Required';
		if (values.type === _TYPE.MENU) {
			if (!values.menu_code && !selectMenu) errors.menu_code = 'Required';
		}

		return errors;
	};

	const formik = useFormik({
		initialValues: { ...initialValues },
		validate,
		onSubmit: (values, { setSubmitting, resetForm, setErrors }) => {
			try {
				Swal.fire({
					title: 'Are you sure?',
					text: 'Please check your entries !',
					icon: 'question',
					showCancelButton: true,
					confirmButtonText: 'Yes',
				}).then((result) => {
					if (result.isConfirmed) {
						const new_values = {};
						if (isUpdate) {
							new_values._id = values._id;
						}
						new_values.requester = username;
						new_values.access_target = values.access_target;
						new_values.segment_code = default_segment_code;
						new_values.segment_name = default_segment_name;
						new_values.target_code = values.target_code;
						new_values.target_name = values.target_name;
						new_values.type = values.type;
						new_values.menu_code = values.menu_code;
						new_values.menu_name = values.menu_name;

						const result_check = checkFormatValue(values.access_target);
						if (result_check.error) {
							Swal.fire('Information ', `${result_check.message}!`, 'error');
							showNotification('Information', `${result_check.message}!`, 'danger');
							return;
						}

						if (new_values.access_target.length === 0) {
							Notifications.showNotif({
								header: 'Information',
								message: 'please define access target before saving data!',
								type: 'danger',
							});
							return;
						}

						if (new_values.type === _TYPE.FUNCTION) {
							delete new_values.menu_code;
							delete new_values.menu_name;
						}

						handleCustomSubmit(new_values, {
							resetForm,
							setSelectType,
							setDataTarget,
						});
					} else if (result.dismiss === Swal.DismissReason.cancel) {
						Swal.fire('Cancelled', 'Your data is safe :)', 'error');
					}
				});
			} catch (error) {
				setErrors({ submit: error.message });
				Swal.fire('Information ', 'Please check your entries again!', 'error');
			} finally {
				setSubmitting(false);
			}
		},
	});

	const onCustomReset = (e) => {
		formik.handleReset(e);

		setSelectType(_LIST_TYPE.find((item) => item.value === _TYPE.FUNCTION));
		setDataTarget([]);
		countDataTarget.current = 0;
	};

	const onChangeType = (e) => {
		setSelectType(e);

		if (e.value === formik.values.type) {
			return;
		}

		formik.setFieldValue('type', e.value);
	};

	const onChangeMenu = (e) => {
		setSelectMenu(e);

		if (e.value === formik.values.menu_code) {
			return;
		}

		formik.setFieldValue('menu_name', e.label);
		formik.setFieldValue('menu_code', e.value);
	};

	const handleCustomAdd = () => {
		if (listRole.length === 0) {
			showNotification('Information', 'role list does not exist, cannot add roles', 'danger');
			return;
		}

		if (dataTarget.length === listRole.length) {
			showNotification(
				'Information',
				'has reached the maximum role limit that can be added',
				'danger',
			);
			return;
		}

		const new_data = [...dataTarget];
		new_data.push({ key: countDataTarget.current, role: null, target: 0 });
		setDataTarget(new_data);

		const new_access_target = getFormatValue(new_data);
		formik.setFieldValue('access_target', new_access_target);

		countDataTarget.current += 1;
	};

	const handleCustomRemove = (index) => {
		const new_data = [...dataTarget];
		new_data.splice(index, 1);
		setDataTarget(new_data);

		const new_access_target = getFormatValue(new_data);
		formik.setFieldValue('access_target', new_access_target);
	};

	const onChangeRole = (e, index) => {
		const new_data = [...dataTarget];

		const find_index = new_data.findIndex((find) => find.role?.value === e.value);

		if (find_index === index) {
			return;
		}

		if (find_index !== -1) {
			showNotification(
				'Information',
				`${e.label}, the role has been selected, try choosing another one`,
				'danger',
			);
			return;
		}

		new_data[index] = { ...new_data[index], role: e };
		setDataTarget(new_data);

		const new_access_target = getFormatValue(new_data);
		formik.setFieldValue('access_target', new_access_target);
	};

	const onChangeTarget = (e, index) => {
		const new_data = [...dataTarget];
		new_data[index] = { ...new_data[index], target: Math.abs(e.target.value) };
		setDataTarget(new_data);

		const new_access_target = getFormatValue(new_data);
		formik.setFieldValue('access_target', new_access_target);
	};

	const getFormatValue = (params) => {
		const result = params.map((item) => {
			let new_value = {};

			if (item.role) {
				new_value = {
					...new_value,
					role_code: item.role.detail.role_code,
					role_name: item.role.detail.role_name,
				};
			}

			if (item.target !== null) {
				new_value = {
					...new_value,
					default_target: item.target,
				};
			}

			return new_value;
		});

		return result;
	};

	const checkFormatValue = (params) => {
		let result = { error: false, message: 'OK' };

		params.forEach((item) => {
			if (result.error) return;

			if (!item.role_code || item.default_target === null) {
				result = { error: true, message: 'please complete your access target' };
			} else if (item.default_target < 0) {
				result = {
					error: true,
					message: 'please default target value must be positive number',
				};
			}
		});

		return result;
	};

	return (
		<div tag='form' noValidate className='p-3'>
			<div className='row'>
				<div className='col-md-4'>
					<Card shadow='sm'>
						<CardBody>
							<FormGroup id='target_name' label='Target Name' className='mb-3'>
								<Input
									autoComplete='off'
									onChange={(e) => {
										formik.handleChange(e);
										formik.setFieldValue(
											'target_code',
											e.target.value.replace(/\s+/g, '_').toLowerCase(),
										);
									}}
									onBlur={formik.handleBlur}
									value={formik.values.target_name}
									isValid={formik.isValid}
									isTouched={formik.touched.target_name}
									invalidFeedback={formik.errors.target_name}
									disabled={isUpdate}
								/>
							</FormGroup>
							<FormGroup id='target_code' label='Target Code' className='mb-3'>
								<Input
									autoComplete='off'
									value={formik.values.target_code}
									disabled
								/>
							</FormGroup>
							<FormGroup id='type' label='Type' className='mb-3'>
								<CustomSelect
									value={selectType}
									defaultValue={selectType}
									options={_LIST_TYPE}
									onChange={onChangeType}
									isDisable={isReadOnly}
								/>
							</FormGroup>
							{formik.values.type === _TYPE.MENU && (
								<FormGroup id='menu_code' label='Menu' className='mb-3'>
									<CustomSelect
										options={listMenu}
										value={selectMenu}
										defaultValue={selectMenu}
										onChange={onChangeMenu}
										isSearchable={listMenu.length > 7}
										isDisable={isReadOnly}
										isValid={
											typeof formik.errors.menu_code === 'undefined' &&
											selectMenu !== null
										}
										invalidFeedback={formik.errors.menu_code}
									/>
								</FormGroup>
							)}
						</CardBody>
					</Card>
				</div>
				<div className='col-md-8'>
					<Card shadow='sm' stretch>
						<CardHeader size='sm' borderSize={1}>
							<CardLabel iconColor='info'>
								<CardTitle>{t('access_target')}</CardTitle>
							</CardLabel>
							{!isReadOnly && (
								<CardActions>
									<Button
										icon='Add'
										color='success'
										type='button'
										isLight={darkModeStatus}
										onClick={handleCustomAdd}>
										Add
									</Button>
								</CardActions>
							)}
						</CardHeader>
						<CardBody>
							{dataTarget.map((item, index) => (
								<div key={'div-'.concat(item.key)}>
									<InputGroup key={'ig-'.concat(item.key)} className='mb-1'>
										<Button
											key={'br-'.concat(item.key)}
											icon='Clear'
											color='danger'
											type='button'
											isLight={darkModeStatus}
											style={{ minHeight: '2.95rem' }}
											onClick={() => handleCustomRemove(index)}
											disabled={isReadOnly}
										/>
										<InputGroupText key={'ig-role-'.concat(item.key)}>
											Role
										</InputGroupText>
										<FormGroup
											key={'fg-role-'.concat(item.key)}
											className='col-md-6'>
											<CustomSelect
												key={'cs-'.concat(item.key)}
												options={listRole}
												onChange={(e) => onChangeRole(e, index)}
												value={item.role}
												defaultValue={item.role}
												isDisable={isReadOnly}
												isSearchable={listRole.length > 7}
											/>
										</FormGroup>
										<InputGroupText key={'ig-target-'.concat(item.key)}>
											Target
										</InputGroupText>
										<FormGroup
											key={'fg-target-'.concat(item.key)}
											className='col-md-3'>
											<Input
												key={'input-'.concat(item.key)}
												style={{
													borderRadius: '4px',
													minHeight: '2.95rem',
												}}
												type='number'
												pattern='\d+'
												min={0}
												value={item.target}
												onChange={(e) => onChangeTarget(e, index)}
												disabled={isReadOnly}
											/>
										</FormGroup>
									</InputGroup>
								</div>
							))}
						</CardBody>
					</Card>
				</div>
			</div>
			{!isReadOnly && (
				<div className='row'>
					<div className='col'>
						<Button
							icon='Save'
							color='success'
							type='submit'
							className='mx-2 mt-1 float-end'
							onClick={formik.handleSubmit}
							isLight={darkModeStatus}>
							Submit
						</Button>
						{!isUpdate && (
							<Button
								icon='Clear'
								color='danger'
								type='reset'
								className='mx-2 mt-1 float-end'
								onClick={onCustomReset}
								isLight={darkModeStatus}>
								Clear
							</Button>
						)}
					</div>
				</div>
			)}
		</div>
	);
};

FormCustom.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
	isUpdate: PropTypes.bool,
	isReadOnly: PropTypes.bool,
	listMenu: PropTypes.instanceOf(Array),
	listRole: PropTypes.instanceOf(Array),
	handleCustomSubmit: PropTypes.func,
	defaultValueMenu: PropTypes.instanceOf(Object),
	defaultValueAccessTarget: PropTypes.instanceOf(Array),
};
FormCustom.defaultProps = {
	initialValues: {
		target_name: null,
		target_code: null,
		type: null,
		menu_name: null,
		menu_code: null,
		access_target: [],
	},
	isUpdate: false,
	isReadOnly: false,
	listMenu: [],
	listRole: [],
	handleCustomSubmit: () => {},
	defaultValueMenu: null,
	defaultValueAccessTarget: [],
};

const FormCustomModal = ({
	initialValues,
	listMenu,
	listRole,
	handleCustomUpdate,
	handleCustomDelete,
	defaultValueMenu,
	defaultValueAccessTarget,
}) => {
	const { darkModeStatus } = useDarkMode();
	const [isOpen, setIsOpen] = useState(false);
	const [isReadOnly, setIsReadOnly] = useState(false);

	const [title, setTitle] = useState('');

	return (
		<>
			<Button
				icon='GridOn'
				color='info'
				type='button'
				className='me-3'
				isLight={darkModeStatus}
				onClick={() => {
					setTitle('Detail Data Target');
					setIsReadOnly(true);
					setIsOpen(true);
				}}
			/>
			<Button
				icon='Edit'
				color='success'
				type='button'
				className='me-3'
				isLight={darkModeStatus}
				onClick={() => {
					setTitle('Update Data Target');
					setIsReadOnly(false);
					setIsOpen(true);
				}}
			/>
			<Button
				icon='Delete'
				color='danger'
				type='button'
				className='me-3'
				isLight={darkModeStatus}
				onClick={() => handleCustomDelete(initialValues)}
			/>

			<Modal isOpen={isOpen} setIsOpen={setIsOpen} size='xl' titleId='modal-crud'>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-crud'>{title}</ModalTitle>
				</ModalHeader>
				<ModalBody>
					<FormCustom
						initialValues={initialValues}
						isReadOnly={isReadOnly}
						isUpdate={isOpen}
						listMenu={listMenu}
						listRole={listRole}
						handleCustomSubmit={handleCustomUpdate}
						defaultValueMenu={defaultValueMenu}
						defaultValueAccessTarget={defaultValueAccessTarget}
					/>
				</ModalBody>
			</Modal>
		</>
	);
};

FormCustomModal.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
	listMenu: PropTypes.instanceOf(Array),
	listRole: PropTypes.instanceOf(Array),
	handleCustomUpdate: PropTypes.func,
	handleCustomDelete: PropTypes.func,
	defaultValueMenu: PropTypes.instanceOf(Object),
	defaultValueAccessTarget: PropTypes.instanceOf(Array),
};
FormCustomModal.defaultProps = {
	initialValues: null,
	listMenu: [],
	listRole: [],
	handleCustomUpdate: null,
	handleCustomDelete: null,
	defaultValueMenu: null,
	defaultValueAccessTarget: [],
};

const TableCustom = ({
	data,
	listMenu,
	listRole,
	totalRows,
	perPage,
	fetchData,
	handleCustomDelete,
	handleCustomUpdate,
	loading,
}) => {
	const { darkModeStatus } = useDarkMode();

	const handlePageChange = (page) => {
		fetchData(page, perPage);
	};

	const handlePerRowsChange = async (newPerPage, page) => {
		return fetchData(page, newPerPage);
	};

	const columns = useMemo(
		() => [
			{ name: 'Code', selector: (row) => row.target_code, sortable: true },
			{ name: 'Name', selector: (row) => row.target_name, sortable: true },
			{ name: 'Type', selector: (row) => row.type, sortable: true },
			{
				name: 'Created',
				selector: (row) => moment(row).format('YYYY-MM-DD HH:MM:SS'),
				sortable: true,
			},
			{
				name: 'Action',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (row) => {
					let defaultValueMenu = null;
					if (row.type === _TYPE.MENU) {
						defaultValueMenu = { value: row.menu_code, label: row.menu_name };
					}

					const defaultValueAccessTarget = row.access_target.map((item, index) => {
						const new_value = {
							key: index,
							role: {
								value: item.role_code,
								label: `(${item.role_code}) ${item.role_name}`,
								detail: { role_code: item.role_code, role_name: item.role_name },
							},
							target: item.default_target,
						};

						return new_value;
					});

					return (
						<FormCustomModal
							initialValues={row}
							listMenu={listMenu}
							listRole={listRole}
							handleCustomUpdate={handleCustomUpdate}
							handleCustomDelete={handleCustomDelete}
							defaultValueMenu={defaultValueMenu}
							defaultValueAccessTarget={defaultValueAccessTarget}
						/>
					);
				},
			},
		],
		// eslint-disable-next-line react-hooks/exhaustive-deps
		[listMenu, listRole],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			pagination
			progressPending={loading}
			paginationServer
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};

TableCustom.propTypes = {
	data: PropTypes.instanceOf(Array),
	listMenu: PropTypes.instanceOf(Array),
	listRole: PropTypes.instanceOf(Array),
	totalRows: PropTypes.number,
	perPage: PropTypes.number,
	fetchData: PropTypes.func,
	handleCustomUpdate: PropTypes.func,
	handleCustomDelete: PropTypes.func,
	loading: PropTypes.bool,
};
TableCustom.defaultProps = {
	data: [],
	listMenu: [],
	listRole: [],
	totalRows: 0,
	perPage: 10,
	fetchData: null,
	handleCustomUpdate: null,
	handleCustomDelete: null,
	loading: false,
};

const Target = () => {
	const { t } = useTranslation('crud');
	const [title] = useState({ title: t('Target') });

	const { default_segment_code } = getaActiveRoles();

	const [data, setData] = useState([]);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [curPage, setCurPage] = useState(1);

	const [listMenu, setListMenu] = useState([]);
	const [listRole, setListRole] = useState([]);

	const [loading, setLoading] = useState(false);

	const initialValues = {
		target_name: '',
		target_code: '',
		type: _TYPE.FUNCTION,
		menu_name: '',
		menu_code: '',
		access_target: [],
	};

	const createSubmit = (value, { resetForm, setSelectType, setDataTarget }) => {
		handleSubmit(value)
			.then(() => {
				fetchData(curPage, perPage);
			})
			.catch(() => {})
			.finally(() => {
				resetForm(initialValues);
				setSelectType(_LIST_TYPE.find((find) => find.value === initialValues.type));
				setDataTarget([]);
			});
	};
	const updateSubmit = (value) => {
		handleUpdate(value)
			.then(() => {
				fetchData(curPage, perPage);
			})
			.catch(() => {})
			.finally(() => {});
	};
	const deleteSubmit = (value) => {
		handleDelete(value)
			.then(() => {
				fetchData(curPage, perPage);
			})
			.catch(() => {})
			.finally(() => {});
	};

	useEffect(() => {
		fetchData(1, 10);
		fetchDataMenu();
		fetchDataRole();

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const fetchData = async (newPage, newPerPage) => {
		setLoading(true);
		const query = `page=${newPage}&sizePerPage=${newPerPage}&segment_code=${default_segment_code}&sortOrder=asc&sortBy=target_code`;
		return TargetModule.read(query)
			.then((response) => {
				setData(response.foundData);
				setTotalRows(response.countData);
				setCurPage(response.currentPage);
				setPerPage(newPerPage);
			})
			.catch(() => {
				setData([]);
				setTotalRows(0);
				setCurPage(1);
				setPerPage(10);
			})
			.finally(() => {
				setLoading(false);
			});
	};

	const fetchDataMenu = async () => {
		const query = `segment_code=${default_segment_code}`;
		return ActivityModule.readSelect(query)
			.then((response) => {
				const new_response = response
					// get format select
					.map((item) => {
						return { value: item.detail.menu_code, label: item.detail.menu_name };
					})
					// filter distinct
					.filter((item, index, self) => {
						return self.findIndex((find) => find.value === item.value) === index;
					})
					// sort value
					.sort((a, b) => {
						if (a.value < b.value) return -1;
						if (a.value > b.value) return 1;
						return 0;
					});

				setListMenu(new_response);
			})
			.catch(() => {
				setListMenu([]);
				showNotification('Warning', 'Can not load data menu, network error', 'danger');
			})
			.finally(() => {});
	};

	const fetchDataRole = async () => {
		const query = `segment_code=${default_segment_code}`;
		return RolesModule.readSelect(query)
			.then((response) => {
				setListRole(response);
			})
			.catch(() => {
				setListRole([]);
				showNotification('Warning', 'Can not load data role, network error', 'danger');
			})
			.finally(() => {});
	};

	return (
		<PageWrapper title={title.title}>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card>
					<CardBody>
						<Accordion id='accordion-form' activeItemId={false} color='info'>
							<AccordionItem id='form-create' title={t('form')} icon='AddBox'>
								<FormCustom
									initialValues={initialValues}
									handleCustomSubmit={createSubmit}
									listMenu={listMenu}
									listRole={listRole}
								/>
							</AccordionItem>
						</Accordion>
					</CardBody>
				</Card>
				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel icon='History' iconColor='info'>
							<CardTitle>{t('historical_data')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<TableCustom
							data={data}
							totalRows={totalRows}
							perPage={perPage}
							curPage={curPage}
							fetchData={fetchData}
							listMenu={listMenu}
							listRole={listRole}
							loading={loading}
							handleCustomUpdate={updateSubmit}
							handleCustomDelete={deleteSubmit}
						/>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

Target.propTypes = {};
Target.defaultProps = {};

export default Target;
