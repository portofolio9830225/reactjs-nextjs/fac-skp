import React, { useState, useEffect, useMemo, useRef } from 'react';
import PropTypes from 'prop-types';
import { useFormik } from 'formik';
// import * as yup from 'yup';
import Swal from 'sweetalert2';
import { useTranslation } from 'react-i18next';

import { Calendar, momentLocalizer } from 'react-big-calendar';
import withDragAndDrop from 'react-big-calendar/lib/addons/dragAndDrop';
import moment from 'moment';

import PageWrapper from '../layout/PageWrapper/PageWrapper';
import Page from '../layout/Page/Page';
import PageLayoutHeader from '../pages/common/Headers/PageLayoutHeader';
import Card, { CardBody, CardHeader, CardLabel, CardTitle } from '../components/bootstrap/Card';
import FormGroup from '../components/bootstrap/forms/FormGroup';
import Input from '../components/bootstrap/forms/Input';
import Button from '../components/bootstrap/Button';
import UserModule from '../modules/UserModule';
import AreaModule from '../modules/AreaModule';
import CityModule from '../modules/CityModule';
import JourneyPlanModule from '../modules/JourneyPlanModule';
import TypePlanModule from '../modules/TypePlanModule';
import StoreModule from '../modules/StoreModule';

import showNotification from '../components/extras/showNotification';
import DarkDataTable from '../components/DarkDataTable';
import useDarkMode from '../hooks/useDarkMode';
import COLORS from '../common/data/enumColors';
import Modal, {
	ModalBody,
	ModalHeader,
	ModalTitle,
	ModalFooter,
} from '../components/bootstrap/Modal';
import { getRequester, getaActiveRoles } from '../helpers/helpers';
import { isEmptyObject } from '../helpers/helpers';
import CustomSelect from '../components/CustomSelect';

const DragAndDropCalendar = withDragAndDrop(Calendar);

const localizer = momentLocalizer(moment);

const FormExampleEdit = (dt) => {
	const { handleShowTable, row } = dt;
	return (
		<Button
			className='me-2'
			icon='Settings'
			type='button'
			color='success'
			onClick={() => {
				handleShowTable(true, row);
			}}
		/>
	);
};

const CustomButton = (dt) => {
	const { row, handleReloadData, handleShowTable } = dt;
	return (
		<FormExampleEdit
			handleShowTable={handleShowTable}
			handleReloadData={handleReloadData}
			row={row}
		/>
	);
};

const CustomDataTable = ({
	data,
	loading,
	totalRows,
	handlePageChange,
	handlePerRowsChange,
	handleReloadData,
	handleShowTable,
}) => {
	const { darkModeStatus } = useDarkMode();

	const columns = useMemo(
		() => [
			{
				name: 'Code',
				selector: (row) => row.username,
				sortable: true,
			},
			{
				name: 'Name',
				selector: (row) => row.name,
				sortable: true,
			},

			{
				name: 'Email',
				selector: (row) => row.email,
				sortable: true,
			},
			{
				name: 'Action',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (dt) => {
					return (
						<CustomButton
							row={dt}
							handleReloadData={handleReloadData}
							handleShowTable={handleShowTable}
						/>
					);
				},
			},
		],
		[handleReloadData, handleShowTable],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			progressPending={loading}
			pagination
			paginationServer
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};
const SettingPjp = () => {
	const [data, setData] = useState([]);
	const { darkModeStatus } = useDarkMode();

	let refCategory = useRef(0);
	let refArea = useRef(0);
	let refCity = useRef(0);
	let refStore = useRef(0);
	let refRole = useRef(0);

	const [loading, setLoading] = useState(false);
	const [listCategory, setCategory] = useState([]);
	const [listArea, setArea] = useState([]);
	const [listCity, setCity] = useState([]);
	const [listStore, setStore] = useState([]);
	const [listRole, setRole] = useState([]);

	const [isMulti] = useState(true);
	const [isHidden, setIsHidden] = useState(false);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [page, setPage] = useState(1);
	const [reloadData, setReloadData] = useState(false);
	const { defaultDate } = new Date();
	const [isCalendarShow, setCalendarShow] = useState(false);
	const [showAllEvents] = useState(true);
	const { draggableAccessor } = useState(true);
	const { calendarPopup } = useState(true);
	const [usernameSelected, setUsernameSelected] = useState(null);
	const [nameSelected, setNameSelected] = useState(null);
	const [areaCodeSelect, setAreaCodeSelected] = useState(null);
	const [dateSelected, setDateSelected] = useState(null);
	const [eventDetailSelected, setEventDetailSelected] = useState(null);
	const [event, setEvent] = useState(null);
	const [isReadOnly, setReadOnly] = useState(false);
	const [isOpen, setIsOpen] = useState(false);
	const [viewIsOpen, setViewIsOpen] = useState(false);
	const [hideDeleteButton, setHideDeleteButton] = useState(true);
	const [isSearchable] = useState(true);
	const { default_segment_code, default_segment_name } = getaActiveRoles();
	const { username, person_name } = getRequester();

	const initialValues = {
		loading: false,
		category: {},
		city: {},
		area: {},
		store: [],
	};

	const handleReloadData = () => {
		setReloadData(!reloadData);
	};

	const handleShowTable = (satu, row) => {
		const { username: username_selected, name } = row;
		setUsernameSelected(username_selected);
		setNameSelected(name);
		setCalendarShow(!isCalendarShow);
		formik.setFieldValue('category', null);
		formik.setFieldValue('role', null);
		formik.setFieldValue('area', null);
		formik.setFieldValue('city', null);
		formik.setFieldValue('store', null);
		formik.setFieldValue('remark', null);
	};

	const fetchCategory = async () => {
		return TypePlanModule.readSelect(`username=${username}`).then((res) => {
			setCategory(res);
		});
	};

	const categorySelected = async (val) => {
		setIsHidden(val.is_store_required);
		formik.setFieldValue('store', null);
	};

	const areaSelected = async (val) => {
		formik.setFieldValue('city', null);
		formik.setFieldValue('store', null);
		setAreaCodeSelected(val.value);
		return CityModule.readSelectByUser(
			`requester=${username}&username=${usernameSelected}&segment_code=${default_segment_code}&area_code=${val.value}`,
		).then((res) => {
			setCity(res);
		});
	};

	const citySelected = async (val) => {
		formik.setFieldValue('store', null);
		return StoreModule.readSelect(
			`username=${username}&city_code=${val.value}&area_code=${areaCodeSelect}&segment_code=${default_segment_code}`,
		).then((res) => {
			setStore(res);
		});
	};

	const fetchPjp = async () => {
		return JourneyPlanModule.read(
			`username=${usernameSelected}&segment_code=${default_segment_code}`,
		).then((res) => {
			setEvent(res);
		});
	};

	const fetchArea = async () => {
		return AreaModule.readSelectByUser(
			`requester=${username}&username=${usernameSelected}&segment_code=${default_segment_code}`,
		).then((res) => {
			setArea(res);
		});
	};

	const fetchRole = async () => {
		return UserModule.getRolebyParent(
			`requester=${username}&username=${usernameSelected}&segment_code=${default_segment_code}`,
		).then((res) => {
			setRole(res);
		});
	};

	const fetchDownline = async () => {
		setLoading(true);
		return UserModule.getDownline(
			`page=${page}&sizePerPage=${perPage}&requester=${username}&segment_code=${default_segment_code}`,
		).then((res) => {
			setData(res.foundData);
			setTotalRows(res.countData);
			setLoading(false);
		});
	};

	useEffect(() => {
		fetchDownline();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [page, perPage, username, reloadData]);

	useEffect(() => {
		fetchPjp();
		fetchRole();
		fetchArea();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [usernameSelected, username, isCalendarShow]);

	useEffect(() => {
		fetchCategory();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	// create
	const eventSelected = (e) => {
		setEventDetailSelected(e);

		if (moment(defaultDate).format('YYYY-MM-DD') == moment(e.start).format('YYYY-MM-DD')) {
			setHideDeleteButton(false);
		} else {
			setHideDeleteButton(true);
		}
		setReadOnly(true);
		setViewIsOpen(true);
	};

	const SelectSlot = (e) => {
		setReadOnly(false);
		if (moment(e.slots[0]).format('YYYY-MM-DD') >= moment(defaultDate).format('YYYY-MM-DD')) {
			setDateSelected(moment(e.slots[0]).format('YYYY-MM-DD'));
			setIsOpen(true);
		} else {
			showNotification('Warning!', "can't select the past date", 'danger');
		}
		formik.setFieldValue('category', null);
		formik.setFieldValue('role', null);
		formik.setFieldValue('area', null);
		formik.setFieldValue('city', null);
		formik.setFieldValue('store', null);
		formik.setFieldValue('remark', null);
	};

	const validate = (values) => {
		const errors = {};
		if (isEmptyObject(values.category)) {
			errors.category = 'Field is required';
		}
		if (isEmptyObject(values.role)) {
			errors.role = 'Field is required';
		}
		if (isEmptyObject(values.area)) {
			errors.area = 'Field is required';
		}
		if (isEmptyObject(values.city)) {
			errors.city = 'Field is required';
		}
		if (isHidden) {
			if (isEmptyObject(values.store)) {
				errors.store = 'Fields store is required';
			}
		}

		return errors;
	};

	const formik = useFormik({
		initialValues: { ...initialValues },
		validate,
		onSubmit: (values, { setSubmitting, resetForm, setErrors }) => {
			const newValues = {};
			newValues.date = dateSelected;
			newValues.username = usernameSelected;
			newValues.type_code = values.category.value;
			newValues.type_name = values.category.label;
			newValues.is_store_required = values.category.is_store_required;
			newValues.requester_name = person_name;
			newValues.requester = username;
			newValues.remark = values.remark ? values.remark : '';
			newValues.store = values.store ? values.store : [];
			newValues.city = values.city;
			newValues.role = values.role;
			newValues.area = values.area;
			newValues.segment_name = default_segment_name;
			newValues.segment_code = default_segment_code;

			try {
				Swal.fire({
					title: 'Are you sure?',
					text: 'Please check your entries !',
					icon: 'info',
					showCancelButton: true,
					confirmButtonText: 'Yes',
				}).then((result) => {
					if (result.isConfirmed) {
						handleSubmit(newValues, resetForm);
					}
				});
			} catch (error) {
				setErrors({ submit: error.message });
				Swal.fire('Information ', 'Please check your entries again!', 'error');
			} finally {
				setSubmitting(false);
			}
		},
	});

	const handleSubmit = (values, resetForm) => {
		const newResponse = new Promise((resolve, reject) => {
			try {
				resolve(
					JourneyPlanModule.createMultiple(values)
						.then((res) => {
							showNotification('Success!', res.status, 'success');
							fetchPjp();
							resetForm(initialValues);
							setIsOpen(false);
						})
						.catch((err) => {
							showNotification('Warning!', err, 'danger');
						}),
				);
			} catch (e) {
				reject(new Error({ error: true }));
			}
		});
		return newResponse;
	};

	const deleteForm = (id) => {
		if (id) {
			const val = {};
			val._id = id;
			val.requester = username;
			return Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'info',
				showCancelButton: true,
				confirmButtonText: 'Yes',
			}).then((result) => {
				if (result.value) {
					handleDelete(val);
				} else if (result.dismiss === Swal.DismissReason.cancel) {
				}
			});
		}
		return id;
	};

	const handleDelete = (values) => {
		const newResponse = new Promise((resolve, reject) => {
			try {
				resolve(
					JourneyPlanModule.delete_(values)
						.then((res) => {
							showNotification('Success!', res.status, 'success');
							fetchPjp();
							setViewIsOpen(false);
						})
						.catch((err) => {
							showNotification('Warning!', err, 'danger');
						}),
				);
			} catch (e) {
				reject(new Error({ error: true }));
			}
		});
		return newResponse;
	};

	const { t } = useTranslation('area');
	const [title] = useState({ title: 'Setting PJP' });
	return (
		<PageWrapper title={title.title}>
			<PageLayoutHeader />
			<Page container='fluid'>
				{!isCalendarShow && (
					<Card stretch>
						<CardHeader borderSize={1}>
							<CardLabel>
								<CardTitle>{t('Setting PJP')}</CardTitle>
							</CardLabel>
						</CardHeader>
						<CardBody>
							<CustomDataTable
								data={data}
								loading={loading}
								totalRows={totalRows}
								handlePageChange={setPage}
								handlePerRowsChange={setPerPage}
								handleReloadData={handleReloadData}
								handleShowTable={handleShowTable}
							/>
						</CardBody>
					</Card>
				)}
				{isCalendarShow && (
					<Card className='col-md-12'>
						<CardHeader borderSize={1}>
							<CardLabel>
								<CardTitle>
									{t('Setting PJP')} {nameSelected} - {usernameSelected}
								</CardTitle>
							</CardLabel>
							<Button
								className='me-2'
								icon='Close'
								type='button'
								color='danger'
								onClick={() => {
									handleShowTable(true, { username: '', name: '' });
								}}
							/>
						</CardHeader>
						<CardBody>
							<DragAndDropCalendar
								selectable
								events={event}
								localizer={localizer}
								defaultDate={defaultDate}
								date={defaultDate}
								onNavigate={() => {}}
								showAllEvents={showAllEvents}
								dayLayoutAlgorithm='no-overlap'
								draggableAccessor={draggableAccessor}
								views={['month', 'day', 'agenda']}
								startAccessor='start'
								endAccessor='end'
								style={{ height: 780 }}
								timeslots={1}
								popup={calendarPopup}
								onSelectEvent={eventSelected}
								onSelectSlot={SelectSlot}
								step={30}
							/>
						</CardBody>
					</Card>
				)}
				;
			</Page>

			<Modal
				isOpen={isOpen}
				setIsOpen={setIsOpen}
				size='lg'
				titleId='modal-edit-menu-crud'
				isStaticBackdrop>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-edit-menu-crud'>
						{isReadOnly ? `View Data ` : `Create PJP ${dateSelected}`}
					</ModalTitle>
				</ModalHeader>

				<Card tag='form' noValidate onSubmit={formik.handleSubmit}>
					<ModalBody className='px-4'>
						<div className='col-md-12'>
							<FormGroup id='role' label='Role' className='mb-4'>
								<CustomSelect
									isDisabled={isReadOnly}
									options={listRole}
									defaultValue={formik.values.role}
									value={formik.values.role}
									darkTheme={darkModeStatus}
									onChange={(value) => {
										formik.setFieldValue('role', value);
										categorySelected(value);
									}}
									isValid={typeof formik.errors.role === 'undefined'}
									ref={(ref) => {
										refRole = ref;
										return refRole;
									}}
								/>
							</FormGroup>
							{typeof formik.errors.role !== 'undefined' &&
								formik.submitCount > 0 && (
									<div style={{ color: COLORS.DANGER.code }}>
										Field is required
									</div>
								)}

							<FormGroup id='category' label='Category' className='mb-4'>
								<CustomSelect
									isDisabled={isReadOnly}
									options={listCategory}
									defaultValue={formik.values.category}
									value={formik.values.category}
									darkTheme={darkModeStatus}
									onChange={(value) => {
										formik.setFieldValue('category', value);
										categorySelected(value);
									}}
									isValid={typeof formik.errors.category === 'undefined'}
									ref={(ref) => {
										refCategory = ref;
										return refCategory;
									}}
								/>
							</FormGroup>
							{typeof formik.errors.category !== 'undefined' &&
								formik.submitCount > 0 && (
									<div style={{ color: COLORS.DANGER.code }}>
										Field is required
									</div>
								)}

							<FormGroup id='area' label='Area' className='mb-4'>
								<CustomSelect
									isDisabled={isReadOnly}
									options={listArea}
									defaultValue={formik.values.area}
									value={formik.values.area}
									darkTheme={darkModeStatus}
									onChange={(value) => {
										formik.setFieldValue('area', value);
										areaSelected(value);
									}}
									isValid={typeof formik.errors.area === 'undefined'}
									ref={(ref) => {
										refArea = ref;
										return refArea;
									}}
								/>
							</FormGroup>
							{typeof formik.errors.area !== 'undefined' &&
								formik.submitCount > 0 && (
									<div style={{ color: COLORS.DANGER.code }}>
										Field is required
									</div>
								)}

							<FormGroup id='city' label='City' className='mb-4'>
								<CustomSelect
									isDisabled={isReadOnly}
									options={listCity}
									defaultValue={formik.values.city}
									value={formik.values.city}
									darkTheme={darkModeStatus}
									onChange={(value) => {
										formik.setFieldValue('city', value);
										citySelected(value);
									}}
									isValid={typeof formik.errors.city === 'undefined'}
									ref={(ref) => {
										refCity = ref;
										return refCity;
									}}
								/>
							</FormGroup>
							{typeof formik.errors.city !== 'undefined' &&
								formik.submitCount > 0 && (
									<div style={{ color: COLORS.DANGER.code }}>
										Field is required
									</div>
								)}

							{isHidden && (
								<FormGroup id='store' label='Store' className='mb-4'>
									<CustomSelect
										isDisabled={isReadOnly}
										options={listStore}
										defaultValue={formik.values.store}
										value={formik.values.store}
										darkTheme={darkModeStatus}
										onChange={(value) => {
											formik.setFieldValue('store', value);
										}}
										isSearchable={isSearchable}
										isValid={typeof formik.errors.store === 'undefined'}
										ref={(ref) => {
											refStore = ref;
											return refStore;
										}}
										isMulti={isMulti}
									/>
								</FormGroup>
							)}
							{typeof formik.errors.store !== 'undefined' &&
								formik.submitCount > 0 &&
								isHidden && (
									<div style={{ color: COLORS.DANGER.code }}>
										Field is required
									</div>
								)}
							<FormGroup id='remark' label='Remark' className='col-md-12'>
								<Input
									isDisabled={isReadOnly}
									onChange={formik.handleChange}
									onBlur={formik.handleBlur}
									value={formik.values.remark}
									isValid={formik.isValid}
									isTouched={formik.touched.remark}
									invalidFeedback={formik.errors.remark}
									autoComplete='off'
								/>
							</FormGroup>
							<br />
						</div>
					</ModalBody>
					{!isReadOnly && (
						<ModalFooter className='px-4 pb-4'>
							<div className='col-md-12 '>
								<Button
									icon='Save'
									type='submit'
									color='success'
									className='float-end'
									isDisable={!formik.isValid && !!formik.submitCount}>
									Submit
								</Button>
							</div>
						</ModalFooter>
					)}
				</Card>
			</Modal>

			<Modal
				isOpen={viewIsOpen}
				setIsOpen={setViewIsOpen}
				size='lg'
				titleId='modal-edit-menu-crud'
				isStaticBackdrop>
				<ModalHeader setIsOpen={setViewIsOpen} className='p-4'>
					<ModalTitle id='modal-edit-menu-crud'>View Data</ModalTitle>
				</ModalHeader>

				<ModalBody className='px-4'>
					<div className='col-md-12'>
						<FormGroup id='role' label='Role' className='col-md-12'>
							<Input
								disabled={isReadOnly}
								defaultValue={eventDetailSelected ? eventDetailSelected.roles : ''}
								autoComplete='off'
								onChange={formik.handleChange}
							/>
						</FormGroup>
						<br />

						<FormGroup id='categoty' label='Category' className='col-md-12'>
							<Input
								disabled={isReadOnly}
								defaultValue={
									eventDetailSelected ? eventDetailSelected.category : ''
								}
								autoComplete='off'
							/>
						</FormGroup>
						<br />

						<FormGroup id='area' label='Area' className='col-md-12'>
							<Input
								disabled={isReadOnly}
								defaultValue={eventDetailSelected ? eventDetailSelected.area : ''}
								autoComplete='off'
							/>
						</FormGroup>
						<br />

						<FormGroup id='city' label='City' className='col-md-12'>
							<Input
								disabled={isReadOnly}
								defaultValue={eventDetailSelected ? eventDetailSelected.city : ''}
								autoComplete='off'
							/>
						</FormGroup>
						<br />

						{eventDetailSelected && !isEmptyObject(eventDetailSelected.store) && (
							<FormGroup id='store' label='Store' className='col-md-12'>
								<Input
									disabled={isReadOnly}
									defaultValue={
										eventDetailSelected ? eventDetailSelected.store_name : ''
									}
									autoComplete='off'
								/>
							</FormGroup>
						)}
						<br />

						<FormGroup id='remark' label='Remark' className='col-md-12'>
							<Input
								disabled={isReadOnly}
								defaultValue={eventDetailSelected ? eventDetailSelected.remark : ''}
								autoComplete='off'
							/>
						</FormGroup>

						<br />
					</div>
				</ModalBody>
				{isReadOnly && hideDeleteButton && (
					<ModalFooter className='px-4 pb-4'>
						<div className='col-md-12 '>
							<Button
								icon='Delete'
								color='danger'
								className='float-end'
								onClick={() => {
									deleteForm(eventDetailSelected._id);
								}}>
								Delete
							</Button>
						</div>
					</ModalFooter>
				)}
			</Modal>
		</PageWrapper>
	);
};

CustomDataTable.propTypes = {
	data: PropTypes.arrayOf(PropTypes.instanceOf(Object)),
	loading: PropTypes.bool,
	totalRows: PropTypes.number,
	handlePageChange: PropTypes.func,
	handlePerRowsChange: PropTypes.func,
	handleReloadData: PropTypes.func,
	handleShowTable: PropTypes.func,
};
CustomDataTable.defaultProps = {
	data: [],
	loading: false,
	totalRows: 0,
	handlePageChange: null,
	handlePerRowsChange: null,
	handleReloadData: null,
	handleShowTable: null,
};

export default SettingPjp;
