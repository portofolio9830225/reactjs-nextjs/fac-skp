// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
import React, {
	useState,
	useEffect,
	useRef,
	useImperativeHandle,
	forwardRef,
	useMemo,
} from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { useTranslation } from 'react-i18next';
import { Form, Formik, useFormik } from 'formik';
import Swal from 'sweetalert2';
import moment from 'moment';
import PageWrapper from '../../../../layout/PageWrapper/PageWrapper';
import Page from '../../../../layout/Page/Page';
import PageLayoutHeader from '../../../../pages/common/Headers/PageLayoutHeader';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardTitle,
} from '../../../../components/bootstrap/Card';
import FormGroup from '../../../../components/bootstrap/forms/FormGroup';
import InputGroup, { InputGroupText } from '../../../../components/bootstrap/forms/InputGroup';
import showNotification from '../../../../components/extras/showNotification';
import productModule from '../../../../modules/rizky/productModule';
import skpModule from '../../../../modules/rizky/skpModule';
import Input from '../../../../components/bootstrap/forms/Input';
import Textarea from '../../../../components/bootstrap/forms/Textarea';
import useDarkMode from '../../../../hooks/useDarkMode';
import DarkDataTable from '../../../../components/DarkDataTable';
import Button from '../../../../components/bootstrap/Button';
import CustomSelect from '../../../../components/CustomSelect';
import Modal, {
	ModalBody,
	ModalHeader,
	ModalTitle,
	ModalFooter,
} from '../../../../components/bootstrap/Modal';
import masterNkaModule from '../../../../modules/rizky/master-nkaModule';
import trading_term_pointModule from '../../../../modules/rizky/trading_term_pointModule';
import Select from '../../../../components/bootstrap/forms/Select';

const formatRupiah = (money) => {
	if (money !== null && money !== '') {
		return new Intl.NumberFormat('id-ID', {
			style: 'currency',
			currency: 'IDR',
			minimumFractionDigits: 0,
		}).format(money);
	}
	return '';
};

const FormSearch = (dt) => {
	const { handleSearch } = dt;
	const [listAccount, setListAccount] = useState([]);
	const [listYear, setListYear] = useState([]);

	const onSubmit = (values, { setSubmitting, setStatus, setErrors }) => {
		try {
			handleSearch(values);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	const initialValues = {
		datee: '',
		store_code: '',
		skp_number: '',
	};
	const fetchAccount = async () => {
		return masterNkaModule.list().then((res) => {
			setListAccount(res.foundData);
		});
	};

	const fetchYear = async () => {
		trading_term_pointModule.listYear().then((res) => {
			setListYear(res.foundData);
		});
	};

	useEffect(() => {
		fetchAccount();
		fetchYear();
	}, []);

	return (
		<Formik enableReinitialize initialValues={{ ...initialValues }} onSubmit={onSubmit}>
			{(formikField) => {
				return (
					<Form className='mb-4'>
						<div className='row'>
							<div className='col-md-2'>
								<Select
									id='store_code'
									placeholder='Select Account'
									onChange={formikField.handleChange}
									value={formikField.values.store_code}
									list={listAccount}
								/>
							</div>
							<div className='col-md-2'>
								<Select
									id='datee'
									placeholder='Select Year'
									onChange={formikField.handleChange}
									value={formikField.values.datee}
									list={listYear}
								/>
							</div>
							<div className='col-md-2'>
								<Input
									id='skp_number'
									placeholder='No. SKP'
									onChange={formikField.handleChange}
									value={formikField.values.skp_number}
								/>
							</div>
							<div className='col-md-2'>
								<div className='row'>
									<Button
										className='col-md-5'
										icon='search'
										type='submit'
										color='success'>
										Filter
									</Button>
									<div className='col-md-6'>
										{formikField.values.datee ||
										formikField.values.store_code ||
										formikField.values.skp_number ? (
											<Button
												type='reset'
												icon='close'
												onClick={() => {
													formikField.resetForm({
														values: initialValues,
													});
												}}
												color='primary'>
												Clear
											</Button>
										) : (
											<div />
										)}
									</div>
								</div>
							</div>
						</div>
					</Form>
				);
			}}
		</Formik>
	);
};

const Table = forwardRef((props, ref) => {
	// eslint-disable-next-line react/prop-types
	const [customers, setCustomers] = useState(props);
	const { darkModeStatus } = useDarkMode();
	const [data, setData] = useState([]);
	const [filter, setFilter] = useState();
	const [loading, setLoading] = useState(false);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);

	const handleSearch = async (values) => {
		fetchData(1, values);
		setFilter(values);
	};

	const fetchData = async (page, params = {}) => {
		setLoading(true);
		setCustomers(props);
		params = {
			...params,
			page,
			sizePerPage: perPage,
		};

		return skpModule.readTable(new URLSearchParams(params)).then((res) => {
			setData(res.foundData);
			setTotalRows(res.countData);
			setLoading(false);
		});
	};

	const handlePageChange = (page) => {
		fetchData(page, filter);
	};

	const handlePerRowsChange = async (newPerPage, page) => {
		setLoading(true);

		const params = {
			page,
			sizePerPage: newPerPage,
		};

		return skpModule.readTable(new URLSearchParams(params)).then((res) => {
			setData(res.foundData);
			setPerPage(newPerPage);
			setLoading(false);
		});
	};

	useEffect(() => {
		fetchData(1); // fetch page 1 of data

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	// eslint-disable-next-line react-hooks/exhaustive-deps
	const reloadTable = () => {
		fetchData(1);
	};

	useImperativeHandle(ref, () => ({
		inReloadTable(params) {
			fetchData(1, params);
		},
	}));

	const columns = useMemo(
		() => [
			{
				name: 'No. SKP',
				selector: (row) => row.skp_number,
				sortable: true,
			},
			{
				name: 'Account',
				selector: (row) => row.account.label,
				sortable: true,
			},
			{
				name: 'Name',
				selector: (row) => row.name,
				sortable: true,
			},
			{
				name: 'Mechanism',
				selector: (row) => row.mechanism,
				sortable: true,
			},
			{
				name: 'Date Start',
				selector: (row) => moment(row.program_start).format('DD MMM YYYY'),
				sortable: true,
			},
			{
				name: 'Date End',
				selector: (row) => moment(row.program_end).format('DD MMM YYYY'),
				sortable: true,
			},
			{
				name: 'Amount',
				selector: (row) => formatRupiah(row.amount).replace('Rp', ''),
				sortable: true,
			},
			{
				name: 'Action',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (dt) => {
					return (
						<CustomButton
							customers={customers.customers}
							row={dt}
							reloadTable={() => reloadTable()}
						/>
					);
				},
			},
		],
		[reloadTable, customers.customers],
	);

	return (
		<>
			<FormSearch handleSearch={(e) => handleSearch(e, 1)} />
			<DarkDataTable
				columns={columns}
				data={data}
				progressPending={loading}
				pagination
				paginationServer
				paginationTotalRows={totalRows}
				onChangeRowsPerPage={handlePerRowsChange}
				onChangePage={handlePageChange}
				theme={darkModeStatus ? 'custom_dark' : 'light'}
			/>
		</>
	);
});

const handleSubmit = (values, initialValues, reloadTable, handleReset, resetForm) => {
	if (values) {
		delete values.loading;
		return Swal.fire({
			title: 'Are you sure?',
			text: 'Please check your entries !',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				skpModule
					.create(values)
					.then((res) => {
						reloadTable();
						showNotification('Success!', res.status, 'success');
						handleReset();
						resetForm(initialValues);
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					});
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				Swal.fire('Cancelled', 'Your data is safe :)', 'error');
			}
		});
	}
	return values;
};

const handleSubmitEdit = (values, reloadTable) => {
	if (values) {
		return Swal.fire({
			title: 'Are you sure?',
			text: 'Please check your entries !',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				skpModule
					.update(values)
					.then((res) => {
						reloadTable();
						showNotification('Success!', res.status, 'success');
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					});
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				Swal.fire('Cancelled', 'Your data is safe :)', 'error');
			}
		});
	}
	return values;
};

const FormEdit = (dt) => {
	const { initialValues, reloadTable } = dt;
	const [isOpen, setIsOpen] = useState(false);
	const [listProduct, setListProduct] = useState();
	const [listStore, setListStore] = useState();
	const [storeSelected, setStoreSelected] = useState(initialValues.account);
	const [growth, setGrowth] = useState(initialValues.growth);
	const [average_prev_year, setAveragePrevYear] = useState(initialValues.average_prev_year);
	// const [budgetDisc, setBudgetDisc] = useState(initialValues.budget_disc);
	const [budgetPromo, setBudgetPromo] = useState(initialValues.budget_promo);
	const [estimatedSales, setEstimatedSales] = useState(initialValues.estimated_sales);
	const [listingCost, setListeningCost] = useState(initialValues.listing_cost);
	const [rentCost, setRentCost] = useState(initialValues.rent_cost);
	const [productSelected, setProductSelected] = useState(initialValues.product);
	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		try {
			const newValues = {
				...values,
			};
			const newOptional = {};
			newValues.product = productSelected;
			if (values.area) {
				newOptional.area_program = values.area;
			}
			if (average_prev_year) {
				newOptional.average_prev_year = average_prev_year;
			}
			// if (budgetDisc) {
			// 	newOptional.budget_disc = budgetDisc;
			// }
			if (budgetPromo) {
				newOptional.budget_promo = budgetPromo;
			}
			if (values.channel) {
				newOptional.channel = values.channel;
			}
			if (values.ca) {
				newOptional.claim_attachment = values.ca;
			}
			if (newValues.periode_claim !== '') {
				newOptional.claim_period = `${newValues.periode_claim}-01`;
			}
			if (estimatedSales) {
				newOptional.estimated_sales = estimatedSales;
			}
			if (growth) {
				newOptional.growth = growth;
			}
			if (listingCost) {
				newOptional.listing_cost = listingCost;
			}
			if (rentCost) {
				newOptional.rent_cost = rentCost;
			}
			if (values.rl) {
				newOptional.rent_location = values.rl;
			}
			if (storeSelected) {
				newValues.account_name = storeSelected.value;
				newValues.account_code = storeSelected.text;
			}

			if (!values.program_name) {
				showNotification(
					'name empty',
					'Store name does not exist, cannot create SPK',
					'danger',
				);
			}
			if (!values.number_skp) {
				showNotification(
					'Number SKP empty',
					'Number SKP does not exist, cannot create SPK',
					'danger',
				);
			}
			if (!values.mechanism) {
				showNotification(
					'Mechanism empty',
					'Mechanism does not exist, cannot create SPK',
					'danger',
				);
			}
			if (!storeSelected) {
				showNotification(
					'Account empty',
					'Account does not exist, cannot create SPK',
					'danger',
				);
			}
			if (!productSelected) {
				showNotification(
					'Product empty',
					'Product does not exist, cannot create SPK',
					'danger',
				);
			}
			if (
				values.program_name &&
				values.number_skp &&
				values.mechanism &&
				storeSelected &&
				productSelected
			) {
				newValues.optional_parameter = newOptional;
				handleSubmitEdit(newValues, reloadTable, resetForm);
				setStatus({ success: true });
			}
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	const fetchStore = async () => {
		return masterNkaModule.list().then((res) => {
			setListStore(res.foundData);
		});
	};

	const fetchProducts = async () => {
		return productModule.listProduct().then((res) => {
			setListProduct(res.foundData);
		});
	};

	useEffect(() => {
		fetchStore();
		fetchProducts();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const handleChangeGrowth = (e) => {
		const { target } = e;
		const { value } = target;
		return setGrowth(value);
	};

	const handleAveragePrevYear = (e) => {
		const { target } = e;
		const { value } = target;
		return setAveragePrevYear(value);
	};

	const handleBudgetPromo = (e) => {
		const { target } = e;
		const { value } = target;
		return setBudgetPromo(value);
	};

	const handleEstimatedSales = (e) => {
		const { target } = e;
		const { value } = target;
		return setEstimatedSales(value);
	};

	const handleListeningCost = (e) => {
		const { target } = e;
		const { value } = target;
		return setListeningCost(value);
	};

	const handleRentCost = (e) => {
		const { target } = e;
		const { value } = target;
		return setRentCost(value);
	};

	const validate = (values) => {
		const errors = {};
		if (!values.program_name) {
			errors.program_name = 'Required';
		}

		if (!values.mechanism) {
			errors.mechanism = 'Required';
		}
		if (!values.amount) {
			errors.amount = 'Required';
		}

		if (values.date_start > values.date_end) {
			errors.date_end = 'end date must be greater than start date';
		}

		return errors;
	};
	return (
		<>
			<Button
				icon='Edit'
				isOutline
				type='button'
				color='primary'
				onClick={() => setIsOpen(true)}>
				Edit
			</Button>
			<Modal
				isStaticBackdrop
				isOpen={isOpen}
				setIsOpen={setIsOpen}
				size='xl'
				titleId='modal-edit-menu-crud'>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-edit-menu-crud'>Update Data</ModalTitle>
				</ModalHeader>
				<Formik
					enableReinitialize
					initialValues={{ ...initialValues }}
					onSubmit={onSubmit}
					validate={validate}>
					{(formikField) => {
						return (
							<Form>
								<ModalBody className='px-4'>
									<div className='row'>
										<div className='col-md-6'>
											<h5>Mandatory Parameter</h5>
											<FormGroup
												id='number_skp'
												label='No. SKP'
												className='col-md-12 mb-3'>
												<Input
													readOnly
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.number_skp}
													isValid={formikField.isValid}
													isTouched={formikField.touched.number_skp}
													invalidFeedback={formikField.errors.number_skp}
													validFeedback='Good!'
													autoComplete='off'
												/>
											</FormGroup>
											<FormGroup
												id='program_name'
												label='Name'
												className='col-md-12 mb-3'>
												<Input
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.program_name}
													isValid={formikField.isValid}
													isTouched={formikField.touched.program_name}
													invalidFeedback={
														formikField.errors.program_name
													}
													autoComplete='off'
												/>
											</FormGroup>
											<FormGroup
												id='mechanism'
												label='Mechanism'
												className='col-md-12 mb-3'>
												<Textarea
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.mechanism}
													isValid={formikField.isValid}
													isTouched={formikField.touched.mechanism}
													invalidFeedback={formikField.errors.mechanism}
													autoComplete='off'
												/>
											</FormGroup>
											<div className='row'>
												<div className='col-md-6'>
													<FormGroup
														id='date_start'
														label='Date Start'
														className='col-md-12 mb-3'>
														<Input
															type='date'
															onChange={formikField.handleChange}
															onBlur={formikField.handleBlur}
															value={formikField.values.date_start}
															isValid={formikField.isValid}
															isTouched={
																formikField.touched.date_start
															}
															invalidFeedback={
																formikField.errors.date_start
															}
															autoComplete='off'
														/>
													</FormGroup>
												</div>
												<div className='col-md-6'>
													<FormGroup
														id='date_end'
														label='Date End'
														className='col-md-12 mb-3'>
														<Input
															type='date'
															onChange={formikField.handleChange}
															onBlur={formikField.handleBlur}
															value={formikField.values.date_end}
															isValid={formikField.isValid}
															isTouched={formikField.touched.date_end}
															invalidFeedback={
																formikField.errors.date_end
															}
															autoComplete='off'
														/>
													</FormGroup>
												</div>
											</div>
											<hr />

											<FormGroup
												id='product'
												label='Product'
												className='col-md-12 mb-3'>
												<CustomSelect
													isMulti='true'
													placeholder='Select Product'
													onChange={(e) => setProductSelected(e)}
													value={productSelected}
													options={listProduct}
												/>
											</FormGroup>
											<FormGroup
												id='amount'
												label='Amount (IDR)'
												className='col-md-12 mb-3'>
												<Input
													type='number'
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.amount}
													isValid={formikField.isValid}
													isTouched={formikField.touched.amount}
													invalidFeedback={formikField.errors.amount}
													autoComplete='off'
												/>
											</FormGroup>
											<hr />
											<FormGroup
												id='account'
												label='Account'
												className='col-md-12 mb-3'>
												<CustomSelect
													isDisabled='true'
													placeholder='Select Account'
													onChange={(e) => setStoreSelected(e)}
													value={storeSelected}
													options={listStore}
												/>
											</FormGroup>
										</div>
										<div className='col-md-6'>
											<h5>Optional Parameter</h5>
											<FormGroup
												id='area'
												label='Area'
												className='col-md-12 mb-3'>
												<Input
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.area}
													isValid={formikField.isValid}
													isTouched={formikField.touched.area}
													invalidFeedback={formikField.errors.area}
													autoComplete='off'
												/>
											</FormGroup>
											<FormGroup
												id='apy'
												label='Average Sell Out last 3 month'
												className='col-md-12 mb-3'>
												<InputGroup>
													<InputGroupText id='inputGroup-sizing'>
														IDR
													</InputGroupText>
													<Input
														type='number'
														ariaLabel='Sizing example input'
														onChange={(e) => handleAveragePrevYear(e)}
														onBlur={formikField.handleBlur}
														value={average_prev_year}
														isValid={formikField.isValid}
														isTouched={formikField.touched.apy}
														invalidFeedback={formikField.errors.apy}
													/>
												</InputGroup>
											</FormGroup>
											<FormGroup
												id='bp'
												label='Budget Promo'
												className='col-md-12 mb-3'>
												<InputGroup>
													<InputGroupText id='inputGroup-sizing'>
														IDR
													</InputGroupText>
													<Input
														type='number'
														ariaLabel='Sizing example input'
														onChange={(e) => handleBudgetPromo(e)}
														onBlur={formikField.handleBlur}
														value={budgetPromo}
														isValid={formikField.isValid}
														isTouched={formikField.touched.bp}
														invalidFeedback={formikField.errors.bp}
													/>
												</InputGroup>
											</FormGroup>
											<FormGroup
												id='channel'
												label='Channel'
												className='col-md-12 mb-3'>
												<Input
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.channel}
													isValid={formikField.isValid}
													isTouched={formikField.touched.channel}
													invalidFeedback={formikField.errors.channel}
													autoComplete='off'
												/>
											</FormGroup>
											<FormGroup
												id='ca'
												label='Claim Attachment'
												className='col-md-12 mb-3'>
												<Input
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.ca}
													isValid={formikField.isValid}
													isTouched={formikField.touched.ca}
													invalidFeedback={formikField.errors.ca}
													autoComplete='off'
												/>
											</FormGroup>
											<FormGroup
												id='periode_claim'
												label='Periode Claim (Month)'
												className='col-md-12 mb-3'>
												<Input
													type='Month'
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.periode_claim}
													isValid={formikField.isValid}
													isTouched={formikField.touched.periode_claim}
													invalidFeedback={
														formikField.errors.periode_claim
													}
													autoComplete='off'
												/>
											</FormGroup>
											<FormGroup
												id='es'
												label='Estimated Sales'
												className='col-md-12 mb-3'>
												<InputGroup>
													<InputGroupText id='inputGroup-sizing'>
														IDR
													</InputGroupText>
													<Input
														type='number'
														ariaLabel='Sizing example input'
														onChange={(e) => handleEstimatedSales(e)}
														onBlur={formikField.handleBlur}
														value={estimatedSales}
														isValid={formikField.isValid}
														isTouched={formikField.touched.es}
														invalidFeedback={formikField.errors.es}
													/>
												</InputGroup>
											</FormGroup>
											<FormGroup
												id='growth'
												label='Growth'
												className='col-md-12 mb-3'>
												<InputGroup>
													<input
														name='growth'
														type='number'
														onChange={(e) => handleChangeGrowth(e)}
														className='form-control'
														value={growth}
														aria-label='test'
													/>
													<InputGroupText id='inputGroup-sizing'>
														%
													</InputGroupText>
												</InputGroup>
											</FormGroup>
											<FormGroup
												id='lc'
												label='Listing Cost'
												className='col-md-12 mb-3'>
												<InputGroup>
													<InputGroupText id='inputGroup-sizing'>
														IDR
													</InputGroupText>
													<Input
														type='number'
														ariaLabel='Sizing example input'
														onChange={(e) => handleListeningCost(e)}
														onBlur={formikField.handleBlur}
														value={listingCost}
														isValid={formikField.isValid}
														isTouched={formikField.touched.lc}
														invalidFeedback={formikField.errors.lc}
													/>
												</InputGroup>
											</FormGroup>
											<FormGroup
												id='rc'
												label='Rent Cost'
												className='col-md-12 mb-3'>
												<InputGroup>
													<InputGroupText id='inputGroup-sizing'>
														IDR
													</InputGroupText>
													<Input
														type='number'
														ariaLabel='Sizing example input'
														onChange={(e) => handleRentCost(e)}
														onBlur={formikField.handleBlur}
														value={rentCost}
														isValid={formikField.isValid}
														isTouched={formikField.touched.rc}
														invalidFeedback={formikField.errors.rc}
													/>
												</InputGroup>
											</FormGroup>
											<FormGroup
												id='rl'
												label='Rent Location'
												className='col-md-12 mb-3'>
												<Input
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.rl}
													isValid={formikField.isValid}
													isTouched={formikField.touched.rl}
													invalidFeedback={formikField.errors.rl}
													autoComplete='off'
												/>
											</FormGroup>
										</div>
									</div>
								</ModalBody>
								<ModalFooter className='px-4 pb-4'>
									<div className='col-md-12 '>
										<Button
											icon='Save'
											isOutline
											type='submit'
											color='success'
											className='float-end'>
											Save
										</Button>
									</div>
								</ModalFooter>
							</Form>
						);
					}}
				</Formik>
			</Modal>
		</>
	);
};

const FormDelete = (dt) => {
	const { initialValues, reloadTable } = dt;
	const onDelete = () => {
		Swal.fire({
			title: 'You want to deleting this row?',
			text: 'click yes to continue!',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				if (result.value) {
					skpModule
						.destroy({
							_id: initialValues._id,
						})
						.then((res) => {
							reloadTable();
							showNotification('Success!', res.status, 'success');
						})
						.catch((err) => {
							showNotification('Warning!', err, 'danger');
						});
				} else if (result.dismiss === Swal.DismissReason.cancel) {
					Swal.fire('Cancelled', 'Your data is safe :)', 'error');
				}
			}
		});
	};

	return (
		<div style={{ paddingLeft: '10px' }}>
			<Button icon='Delete' isOutline type='button' color='danger' onClick={() => onDelete()}>
				Delete
			</Button>
		</div>
	);
};

const CustomButton = (dt) => {
	const { row, reloadTable } = dt;
	let split = [];
	if (row.claim_period) {
		split = row.claim_period.split('-');
	}
	const initialValues = {
		loading: false,
		number_skp: row.skp_number,
		program_name: row.name,
		mechanism: row.mechanism,
		amount: row.amount,
		date_start: moment(row.program_start).format('YYYY-MM-DD'),
		date_end: moment(row.program_end).format('YYYY-MM-DD'),
		product: row.product,
		account: row.account,
		area: row.area,
		average_prev_year: row.average_prev_year,
		budget_disc: row.budget_disc,
		budget_promo: row.budget_promo,
		channel: row.channel,
		ca: row.claim_attachment,
		periode_claim: split.length === 0 ? null : `${split[0]}-${split[1]}`,
		estimated_sales: row.estimated_sales,
		growth: row.growth,
		listing_cost: row.listing_cost,
		rent_cost: row.rent_cost,
		rl: row.rent_location,
		_id: row._id,
	};
	if (row.isDisable) {
		return <div />;
	}
	return (
		<>
			<FormEdit initialValues={initialValues} reloadTable={() => reloadTable()} />
			<FormDelete initialValues={initialValues} reloadTable={() => reloadTable()} />
		</>
	);
};

const FormInput = (dt) => {
	const { initialValues, reloadTable } = dt;
	const [checkNumberSKP, setCheckNumberSKP] = useState(true);
	const [listProduct, setListProduct] = useState();
	const [listStore, setListStore] = useState();
	const [storeSelected, setStoreSelected] = useState(null);
	const [growth, setGrowth] = useState();
	const [average_prev_year, setAveragePrevYear] = useState();
	const [budgetDisc, setBudgetDisc] = useState();
	const [budgetPromo, setBudgetPromo] = useState();
	const [estimatedSales, setEstimatedSales] = useState();
	const [listeningCost, setListeningCost] = useState();
	const [rentCost, setRentCost] = useState();
	const [productSelected, setProductSelected] = useState(null);
	const [errAcc, setErrAcc] = useState(true);
	const [errPdc, setErrPdc] = useState(true);
	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		try {
			const newValues = {
				...values,
			};
			const newOptional = {};
			newValues.product = productSelected;
			if (values.area) {
				newOptional.area_program = values.area;
			}
			if (average_prev_year) {
				newOptional.average_prev_year = average_prev_year;
			}
			if (budgetDisc) {
				newOptional.budget_disc = budgetDisc;
			}
			if (budgetPromo) {
				newOptional.budget_promo = budgetPromo;
			}
			if (values.channel) {
				newOptional.channel = values.channel;
			}
			if (values.ca) {
				newOptional.claim_attachment = values.ca;
			}
			if (estimatedSales) {
				newOptional.estimated_sales = estimatedSales;
			}
			if (newValues.periode_claim != '') {
				newOptional.claim_period = `${newValues.periode_claim}-01`;
				delete newValues.periode_claim;
			}
			if (growth) {
				newOptional.growth = growth;
			}
			if (listeningCost) {
				newOptional.listing_cost = listeningCost;
			}
			if (rentCost) {
				newOptional.rent_cost = rentCost;
			}
			if (values.rl) {
				newOptional.rent_location = values.rl;
			}
			if (storeSelected) {
				newValues.account_name = storeSelected.value;
				newValues.account_code = storeSelected.text;
			}

			if (!values.program_name) {
				showNotification(
					'name empty',
					'Store name does not exist, cannot create SPK',
					'danger',
				);
			}
			if (!values.number_skp) {
				showNotification(
					'Number SKP empty',
					'Number SKP does not exist, cannot create SPK',
					'danger',
				);
			}
			if (!values.mechanism) {
				showNotification(
					'Mechanism empty',
					'Mechanism does not exist, cannot create SPK',
					'danger',
				);
			}
			if (!storeSelected) {
				showNotification(
					'Account empty',
					'Account does not exist, cannot create SPK',
					'danger',
				);
			}
			if (!productSelected) {
				showNotification(
					'Product empty',
					'Product does not exist, cannot create SPK',
					'danger',
				);
			}
			if (values.program_name && values.number_skp && values.mechanism && storeSelected) {
				newValues.optional_parameter = newOptional;
				handleSubmit(newValues, initialValues, reloadTable, handleReset, resetForm);
				setStatus({ success: true });
			}
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	const handleReset = () => {
		initialValues.number_skp = '';
		setProductSelected(null);
		setStoreSelected(null);
		initialValues.area = '';
		setAveragePrevYear('');
		setBudgetDisc('');
		setBudgetPromo('');
		initialValues.ca = '';
		initialValues.channel = '';
		setEstimatedSales('');
		setGrowth('');
		setListeningCost('');
		setRentCost('');
		initialValues.rl = '';
	};

	const fetchStore = async () => {
		return masterNkaModule.list().then((res) => {
			setListStore(res.foundData);
		});
	};

	const fetchProducts = async () => {
		return productModule.listProduct().then((res) => {
			setListProduct(res.foundData);
		});
	};

	useEffect(() => {
		fetchStore();
		fetchProducts();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const handleChangeGrowth = (e) => {
		const { target } = e;
		const { value } = target;
		return setGrowth(value);
	};

	const handleAveragePrevYear = (e) => {
		const { target } = e;
		const { value } = target;
		return setAveragePrevYear(value);
	};

	const handleBudgetPromo = (e) => {
		const { target } = e;
		const { value } = target;
		return setBudgetPromo(value);
	};

	const handleEstimatedSales = (e) => {
		const { target } = e;
		const { value } = target;
		return setEstimatedSales(value);
	};

	const handleListeningCost = (e) => {
		const { target } = e;
		const { value } = target;
		return setListeningCost(value);
	};

	const handleRentCost = (e) => {
		const { target } = e;
		const { value } = target;
		return setRentCost(value);
	};

	const validate = (values) => {
		const errors = {};
		if (!values.number_skp) {
			errors.number_skp = 'Required';
		} else {
			skpModule.checkSKPNumber({ skp_number: values.number_skp }).then((res) => {
				setCheckNumberSKP(res.data);
			});
			if (checkNumberSKP) {
				errors.number_skp = 'Number SKP already exists!';
			}
		}
		if (!values.program_name) {
			errors.program_name = 'Required';
		}

		if (!values.mechanism) {
			errors.mechanism = 'Required';
		}
		if (!values.amount) {
			errors.amount = 'Required';
		}

		if (storeSelected == null) {
			setErrAcc(false);
		}

		if (productSelected == null) {
			setErrPdc(false);
		}

		if (!values.date_start) {
			errors.date_start = 'Required';
		}

		if (!values.date_start) {
			errors.date_end = 'Required';
		}

		if (values.date_start > values.date_end) {
			errors.date_end = 'end date must be greater than start date';
		}

		return errors;
	};

	return (
		<Formik
			enableReinitialize
			initialValues={{ ...initialValues }}
			onSubmit={onSubmit}
			validate={validate}>
			{(formikField) => {
				return (
					<Form>
						<div className='row'>
							<div className='col-md-6'>
								<h5>Mandatory Parameter</h5>
								<FormGroup
									id='number_skp'
									label='No. SKP'
									className='col-md-12 mb-3'>
									<Input
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.number_skp}
										isValid={formikField.isValid}
										isTouched={formikField.touched.number_skp}
										invalidFeedback={formikField.errors.number_skp}
										validFeedback='Good!'
										autoComplete='off'
									/>
								</FormGroup>
								<FormGroup
									id='program_name'
									label='Name'
									className='col-md-12 mb-3'>
									<Input
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.program_name}
										isValid={formikField.isValid}
										isTouched={formikField.touched.program_name}
										invalidFeedback={formikField.errors.program_name}
										autoComplete='off'
									/>
								</FormGroup>
								<FormGroup
									id='mechanism'
									label='Mechanism'
									className='col-md-12 mb-3'>
									<Textarea
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.mechanism}
										isValid={formikField.isValid}
										isTouched={formikField.touched.mechanism}
										invalidFeedback={formikField.errors.mechanism}
										autoComplete='off'
									/>
								</FormGroup>
								<div className='row'>
									<div className='col-md-6'>
										<FormGroup
											id='date_start'
											label='Date Start'
											className='col-md-12 mb-3'>
											<Input
												type='date'
												onChange={formikField.handleChange}
												onBlur={formikField.handleBlur}
												value={formikField.values.date_start}
												isValid={formikField.isValid}
												isTouched={formikField.touched.date_start}
												invalidFeedback={formikField.errors.date_start}
												autoComplete='off'
											/>
										</FormGroup>
									</div>
									<div className='col-md-6'>
										<FormGroup
											id='date_end'
											label='Date End'
											className='col-md-12 mb-3'>
											<Input
												type='date'
												onChange={formikField.handleChange}
												onBlur={formikField.handleBlur}
												value={formikField.values.date_end}
												isValid={formikField.isValid}
												isTouched={formikField.touched.date_end}
												invalidFeedback={formikField.errors.date_end}
												autoComplete='off'
											/>
										</FormGroup>
									</div>
								</div>
								<hr />
								<FormGroup id='product' label='Product' className='col-md-12 mb-3'>
									<CustomSelect
										isMulti='true'
										placeholder='Select Product'
										onChange={(e) => {
											setProductSelected(e);
											setErrPdc(true);
										}}
										value={productSelected}
										options={listProduct}
										isValid={errPdc}
										invalidFeedback='Required'
									/>
								</FormGroup>

								<FormGroup
									id='amount'
									label='Amount (IDR)'
									className='col-md-12 mb-3'>
									<Input
										type='number'
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.amount}
										isValid={formikField.isValid}
										isTouched={formikField.touched.amount}
										invalidFeedback={formikField.errors.amount}
										autoComplete='off'
									/>
								</FormGroup>
								<hr />
								<FormGroup id='account' label='Account' className='col-md-12 mb-3'>
									<CustomSelect
										placeholder='Select Account'
										onChange={(e) => {
											setStoreSelected(e);
											setErrAcc(true);
										}}
										value={storeSelected}
										options={listStore}
										isValid={errAcc}
										invalidFeedback='Required'
									/>
								</FormGroup>
							</div>
							<div className='col-md-6'>
								<h5>Optional Parameter</h5>
								<FormGroup id='area' label='Area' className='col-md-12 mb-3'>
									<Input
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.area}
										isValid={formikField.isValid}
										isTouched={formikField.touched.area}
										invalidFeedback={formikField.errors.area}
										autoComplete='off'
									/>
								</FormGroup>
								<FormGroup
									id='apy'
									label='Average Sell Out last 3 month'
									className='col-md-12 mb-3'>
									<InputGroup>
										<InputGroupText id='inputGroup-sizing'>IDR</InputGroupText>
										<Input
											type='number'
											ariaLabel='Sizing example input'
											onChange={(e) => handleAveragePrevYear(e)}
											onBlur={formikField.handleBlur}
											value={average_prev_year}
											isTouched={formikField.touched.apy}
											invalidFeedback={formikField.errors.apy}
										/>
									</InputGroup>
								</FormGroup>
								<FormGroup id='bp' label='Budget Promo' className='col-md-12 mb-3'>
									<InputGroup>
										<InputGroupText id='inputGroup-sizing'>IDR</InputGroupText>
										<Input
											type='number'
											ariaLabel='Sizing example input'
											onChange={(e) => handleBudgetPromo(e)}
											onBlur={formikField.handleBlur}
											value={budgetPromo}
											isValid={formikField.isValid}
											isTouched={formikField.touched.bp}
											invalidFeedback={formikField.errors.bp}
										/>
									</InputGroup>
								</FormGroup>
								<FormGroup id='channel' label='Channel' className='col-md-12 mb-3'>
									<Input
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.channel}
										isValid={formikField.isValid}
										isTouched={formikField.touched.channel}
										invalidFeedback={formikField.errors.channel}
										autoComplete='off'
									/>
								</FormGroup>
								<FormGroup
									id='ca'
									label='Claim Attachment'
									className='col-md-12 mb-3'>
									<Input
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.ca}
										isValid={formikField.isValid}
										isTouched={formikField.touched.ca}
										invalidFeedback={formikField.errors.ca}
										autoComplete='off'
									/>
								</FormGroup>
								<FormGroup
									id='periode_claim'
									label='Periode Claim (Month)'
									className='col-md-12 mb-3'>
									<Input
										type='Month'
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.periode_claim}
										isValid={formikField.isValid}
										isTouched={formikField.touched.periode_claim}
										invalidFeedback={formikField.errors.periode_claim}
										autoComplete='off'
									/>
								</FormGroup>
								<FormGroup
									id='es'
									label='Estimated Sales'
									className='col-md-12 mb-3'>
									<InputGroup>
										<InputGroupText id='inputGroup-sizing'>IDR</InputGroupText>
										<Input
											type='number'
											ariaLabel='Sizing example input'
											onChange={(e) => handleEstimatedSales(e)}
											onBlur={formikField.handleBlur}
											value={estimatedSales}
											isValid={formikField.isValid}
											isTouched={formikField.touched.es}
											invalidFeedback={formikField.errors.es}
										/>
									</InputGroup>
								</FormGroup>
								<FormGroup id='growth' label='Growth' className='col-md-12 mb-3'>
									<InputGroup>
										<Input
											name='growth'
											type='number'
											onChange={(e) => handleChangeGrowth(e)}
											className='form-control'
											value={growth}
											aria-label='test'
											onBlur={formikField.handleBlur}
											isValid={formikField.isValid}
											isTouched={formikField.touched.growth}
										/>
										<InputGroupText id='inputGroup-sizing'>%</InputGroupText>
									</InputGroup>
								</FormGroup>
								<FormGroup id='lc' label='Listing Cost' className='col-md-12 mb-3'>
									<InputGroup>
										<InputGroupText id='inputGroup-sizing'>IDR</InputGroupText>
										<Input
											type='number'
											ariaLabel='Sizing example input'
											onChange={(e) => handleListeningCost(e)}
											onBlur={formikField.handleBlur}
											value={listeningCost}
											isValid={formikField.isValid}
											isTouched={formikField.touched.lc}
											invalidFeedback={formikField.errors.lc}
										/>
									</InputGroup>
								</FormGroup>
								<FormGroup id='rc' label='Rent Cost' className='col-md-12 mb-3'>
									<InputGroup>
										<InputGroupText id='inputGroup-sizing'>IDR</InputGroupText>
										<Input
											type='number'
											ariaLabel='Sizing example input'
											onChange={(e) => handleRentCost(e)}
											onBlur={formikField.handleBlur}
											value={rentCost}
											isValid={formikField.isValid}
											isTouched={formikField.touched.rc}
											invalidFeedback={formikField.errors.rc}
										/>
									</InputGroup>
								</FormGroup>
								<FormGroup id='rl' label='Rent Location' className='col-md-12 mb-3'>
									<Input
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.rl}
										isValid={formikField.isValid}
										isTouched={formikField.touched.rl}
										invalidFeedback={formikField.errors.rl}
										autoComplete='off'
									/>
								</FormGroup>
							</div>
						</div>
						<div className='col-md-12 '>
							<Button
								icon='Save'
								isOutline
								type='submit'
								color='success'
								className='float-end'>
								Submit
							</Button>
						</div>
					</Form>
				);
			}}
		</Formik>
	);
};

const Skp = () => {
	const customDataTableRef = useRef(null);
	const reloadTable = () => {
		customDataTableRef.current.inReloadTable();
	};

	// eslint-disable-next-line react/prop-types
	const searchRef = useRef(null);
	const formik = useFormik({
		initialValues: {
			search: '',
		},
	});

	useEffect(() => {
		searchRef.current.focus();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const initialValues = {
		loading: false,
		number_skp: '',
		program_name: '',
		mechanism: '',
		date_start: '',
		date_end: '',
		amount: '',
		periode_claim: '',
		optional_parameter: null,
		rl: '',
		area: '',
		apy: '',
		bp: '',
		channel: '',
		ca: '',
		es: '',
		lc: '',
		rc: '',
		growth: '',
		product: '',
		account: '',
	};

	const { t } = useTranslation('category');

	return (
		<PageWrapper title='Dashboard'>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card className='col-md-12'>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('Input SKP')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<div className='col-12 mb-3 g-4'>
							<FormInput
								reloadTable={() => reloadTable()}
								initialValues={initialValues}
							/>
						</div>
					</CardBody>
				</Card>
				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('Data')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						{/* <HistoryTable ref={customDataTableRef} /> */}
						<div className='mt-5 row'>
							{/* eslint-disable-next-line react/destructuring-assignment,react/prop-types */}
							<FormGroup
								label='Invoices Number'
								formText='Use either keyboard or scanner'
								className='col-12 col-md-5 col-lg-3 col-xl-2 d-none'>
								<InputGroup size='sm'>
									<Input
										id='search'
										value={formik.values.search}
										onChange={formik.handleChange}
										ref={searchRef}
									/>
								</InputGroup>
							</FormGroup>
							<div className='col-12'>
								<Table ref={customDataTableRef} tab={1} />
							</div>
						</div>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

export default Skp;
