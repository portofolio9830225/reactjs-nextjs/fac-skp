// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
import React, {
	useState,
	useEffect,
	useRef,
	useImperativeHandle,
	forwardRef,
	useMemo,
} from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { useTranslation } from 'react-i18next';
import { Form, Formik, useFormik } from 'formik';
import Swal from 'sweetalert2';
import moment from 'moment';
import PageWrapper from '../../../../layout/PageWrapper/PageWrapper';
import Page from '../../../../layout/Page/Page';
import PageLayoutHeader from '../../../../pages/common/Headers/PageLayoutHeader';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardTitle,
} from '../../../../components/bootstrap/Card';
import FormGroup from '../../../../components/bootstrap/forms/FormGroup';
import InputGroup from '../../../../components/bootstrap/forms/InputGroup';
import showNotification from '../../../../components/extras/showNotification';
import trading_term_pointModule from '../../../../modules/rizky/trading_term_pointModule';
import Input from '../../../../components/bootstrap/forms/Input';
import useDarkMode from '../../../../hooks/useDarkMode';
import DarkDataTable from '../../../../components/DarkDataTable';
import Button from '../../../../components/bootstrap/Button';
import Modal, {
	ModalBody,
	ModalHeader,
	ModalTitle,
	ModalFooter,
} from '../../../../components/bootstrap/Modal';
import CustomSelect from '../../../../components/CustomSelect';
import masterNkaModule from '../../../../modules/rizky/master-nkaModule';
import Select from '../../../../components/bootstrap/forms/Select';

const Table = forwardRef((props, ref) => {
	const [customers, setCustomers] = useState(props);
	const { darkModeStatus } = useDarkMode();
	const [data, setData] = useState();
	const [filter, setFilter] = useState();
	const [loading, setLoading] = useState(false);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);

	const handleSearch = async (values) => {
		fetchData(1, values);
		setFilter(values);
	};

	const fetchData = async (page, params = {}) => {
		setLoading(true);
		setCustomers(props);
		params = {
			...params,
			page,
			sizePerPage: perPage,
		};

		return trading_term_pointModule.readTable(new URLSearchParams(params)).then((res) => {
			setData(res.foundData);
			setTotalRows(res.countData);
			setLoading(false);
		});
	};

	const handlePageChange = (page) => {
		fetchData(page, filter);
	};

	const handlePerRowsChange = async (newPerPage, page) => {
		setLoading(true);

		const params = {
			page,
			sizePerPage: newPerPage,
		};
		return trading_term_pointModule.readTable(new URLSearchParams(params)).then((res) => {
			setData(res.foundData);
			setPerPage(newPerPage);
			setLoading(false);
		});
	};

	useEffect(() => {
		fetchData(1); // fetch page 1 of data

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	// eslint-disable-next-line react-hooks/exhaustive-deps
	const reloadTable = () => {
		fetchData(1);
	};

	useImperativeHandle(ref, () => ({
		inReloadTable(params) {
			fetchData(1, params);
		},
	}));

	const columns = useMemo(
		() => [
			{
				name: 'Poin Name',
				selector: (row) => row.name,
				sortable: true,
			},
			{
				name: 'Year',
				selector: (row) => row.year,
				sortable: true,
			},
			{
				name: 'Action',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (dt) => {
					return (
						<CustomButton
							customers={customers.customers}
							row={dt}
							reloadTable={() => reloadTable()}
						/>
					);
				},
			},
		],
		[reloadTable, customers.customers],
	);

	return (
		<>
			<FormSearch handleSearch={(e) => handleSearch(e, 1)} />
			<DarkDataTable
				columns={columns}
				data={data}
				progressPending={loading}
				pagination
				paginationServer
				paginationTotalRows={totalRows}
				onChangeRowsPerPage={handlePerRowsChange}
				onChangePage={handlePageChange}
				theme={darkModeStatus ? 'custom_dark' : 'light'}
			/>
		</>
	);
});

const handleSubmit = (values, initialValues, reloadTable, handleReset, resetForm, readySubmit) => {
	if (values && readySubmit) {
		delete values.loading;
		return Swal.fire({
			title: 'Are you sure?',
			text: 'Please check your entries !',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				trading_term_pointModule
					.create(values)
					.then((res) => {
						reloadTable();
						showNotification('Success!', res.status, 'success');
						handleReset();
						resetForm(initialValues);
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					});
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				Swal.fire('Cancelled', 'Your data is safe :)', 'error');
			}
		});
	}
	return values;
};

const handleSubmitEdit = (values, reloadTable) => {
	if (values) {
		return Swal.fire({
			title: 'Are you sure?',
			text: 'Please check your entries !',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				trading_term_pointModule
					.update(values)
					.then((res) => {
						reloadTable();
						showNotification('Success!', res.status, 'success');
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					});
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				Swal.fire('Cancelled', 'Your data is safe :)', 'error');
			}
		});
	}
	return values;
};

const FormEdit = (dt) => {
	const { initialValues, reloadTable } = dt;
	const [isOpen, setIsOpen] = useState(false);
	const [readySubmit, setReadySubmit] = useState(true);
	const [listStore, setListStore] = useState();
	const [storeSelected, setStoreSelected] = useState({
		label: initialValues.name,
		text: initialValues.name,
		value: initialValues.name,
	});
	const [listTTPoints, setListTTPoints] = useState();
	const [point, setPoint] = useState(initialValues.point);
	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		try {
			point.forEach(async (e) => {
				if (!e.name) {
					showNotification(
						'Trading Terms Poin empty',
						'Trading Terms Poin does not exist, cannot create trading terms poin',
						'danger',
					);
				}
				if (!e.value) {
					showNotification(
						'Nominal Point empty',
						'Nominal Point does not exist, cannot create trading terms poin',
						'danger',
					);
				}
				if (!e.type) {
					showNotification(
						'Type Point empty',
						'Type Point does not exist, cannot create trading terms poin',
						'danger',
					);
				}
			});

			const newValue = {
				...values,
			};
			newValue.account = storeSelected.value;
			newValue.point = point;
			if (storeSelected && readySubmit) {
				handleSubmitEdit(newValue, reloadTable, resetForm);
				setStatus({ success: true });
			}
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	const fetchStore = async () => {
		return masterNkaModule.list().then((res) => {
			setListStore(res.foundData);
		});
	};

	const fetchTtPoints = async () => {
		return masterNkaModule.listTtpoints().then((res) => {
			setListTTPoints(res.foundData);
		});
	};

	const handleSelectStore = (e) => {
		trading_term_pointModule.checkStore({ code: e.value }).then((res) => {
			if (res.data) {
				setStoreSelected(e);
			} else {
				setStoreSelected(null);
				Swal.fire(
					'Data already exists',
					'data already exists, you cannot add a new one',
					'error',
				);
			}
		});
	};

	const handleChangeNominal = (e, index) => {
		const { target } = e;
		const { value } = target;
		let newSampel = [];
		const obj = point[index];
		obj.value = value;
		newSampel = [...point.slice(0, index), obj, ...point.slice(index + 1)];
		point.forEach((el) => {
			if (!el.name) {
				setReadySubmit(false);
			} else {
				setReadySubmit(true);
			}
			if (!el.value) {
				setReadySubmit(false);
			} else {
				setReadySubmit(true);
			}
			if (!el.type) {
				setReadySubmit(false);
			} else {
				setReadySubmit(true);
			}
		});
		return setPoint(newSampel);
	};

	const handleSelectTTP = (e, index) => {
		const obj = point[index];
		obj.name = e.value;
		const newSampel = [...point.slice(0, index), obj, ...point.slice(index + 1)];
		let isFound = false;

		point.reduce((acc, cur) => {
			const existingItem = acc.find((item) => cur.name === item.name);
			if (existingItem) {
				isFound = true;
				showNotification(
					'Trading Terms Poin already exists',
					'Trading Terms Poin already exists, you cannot add a new one',
					'danger',
				);
			} else {
				isFound = false;
				acc.push({ ...cur });
			}
			return acc;
		}, []);

		if (!isFound) {
			point.forEach((el) => {
				if (!el.name) {
					setReadySubmit(false);
				} else {
					setReadySubmit(true);
				}
				if (!el.value) {
					setReadySubmit(false);
				} else {
					setReadySubmit(true);
				}
				if (!el.type) {
					setReadySubmit(false);
				} else {
					setReadySubmit(true);
				}
			});
			setPoint(newSampel);
		}
	};

	const handleSelectType = (e, index) => {
		const obj = point[index];
		obj.type = e.value;
		const newSampel = [...point.slice(0, index), obj, ...point.slice(index + 1)];
		point.forEach((el) => {
			if (!el.name) {
				setReadySubmit(false);
			} else {
				setReadySubmit(true);
			}
			if (!el.value) {
				setReadySubmit(false);
			} else {
				setReadySubmit(true);
			}
			if (!el.type) {
				setReadySubmit(false);
			} else {
				setReadySubmit(true);
			}
		});
		return setPoint(newSampel);
	};

	const handleaddclick = () => {
		setReadySubmit(false);
		return setPoint([...point, { name: '', value: '', type: 'percentage' }]);
	};

	const handleremove = (index) => {
		if (point.length > 1) {
			const _point = [...point];
			_point.forEach((e, i) => {
				if (i === index) {
					_point.splice(index, 1);
				}
			});
			setPoint(_point);
		} else {
			Swal.fire('Error', 'There is at least 1 TT Points', 'error');
		}
	};
	const validate = (values) => {
		const errors = {};
		if (!values.year) {
			errors.year = 'Required';
		}

		return errors;
	};

	useEffect(() => {
		fetchStore();
		fetchTtPoints();
	}, []);

	const { t } = useTranslation('input');

	return (
		<>
			<Button
				icon='Edit'
				isOutline
				type='button'
				color='primary'
				onClick={() => setIsOpen(true)}>
				Edit
			</Button>
			<Modal
				isStaticBackdrop
				isOpen={isOpen}
				setIsOpen={setIsOpen}
				size='lg'
				titleId='modal-edit-menu-crud'>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-edit-menu-crud'>Edit TT Poin</ModalTitle>
				</ModalHeader>
				<Formik
					enableReinitialize
					initialValues={{ ...initialValues }}
					onSubmit={onSubmit}
					validate={validate}>
					{(formikField) => {
						return (
							<Form>
								<ModalBody className='px-4'>
									<div className='row'>
										<div className='col-md-12'>
											<FormGroup
												id='name'
												label='Store Name'
												className='col-md-12 mb-3'>
												<CustomSelect
													isDisabled='true'
													placeholder='Select Store Name'
													onChange={(e) => handleSelectStore(e)}
													value={storeSelected}
													options={listStore}
												/>
											</FormGroup>
										</div>
										<FormGroup
											id='year'
											label='Year'
											className='col-md-12 mb-3'>
											<Input
												readOnly
												type='number'
												onChange={formikField.handleChange}
												onBlur={formikField.handleBlur}
												value={formikField.values.year}
												isValid={formikField.isValid}
												isTouched={formikField.touched.year}
												invalidFeedback={formikField.errors.year}
												autoComplete='off'
											/>
										</FormGroup>
									</div>
									<div className='mt-3'>
										<div className='row'>
											<CardHeader borderSize={1}>
												<CardLabel>
													<CardTitle>{t('Point TT')}</CardTitle>
												</CardLabel>
											</CardHeader>
										</div>
										<table className='table table-bordered'>
											<thead>
												<tr>
													<th>Trading Terms Poin</th>
													<th>Nominal</th>
													<th>Type</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												{point &&
													point.map((el, i) => {
														return (
															<tr>
																<td>
																	<div>
																		<CustomSelect
																			placeholder='Trading Terms Poin'
																			onChange={(e) =>
																				handleSelectTTP(
																					e,
																					i,
																				)
																			}
																			value={
																				{
																					label: el.name,
																					text: el.name,
																					value: el.name,
																				} || null
																			}
																			options={listTTPoints}
																		/>
																	</div>
																</td>
																<td>
																	<div>
																		<input
																			type='number'
																			step='any'
																			className='form-control'
																			onChange={(e) =>
																				handleChangeNominal(
																					e,
																					i,
																				)
																			}
																			value={el.value || null}
																			name='nominal'
																			placeholder='Nominal'
																			aria-label='test'
																		/>
																	</div>
																</td>
																<td>
																	<div>
																		<CustomSelect
																			onChange={(e) =>
																				handleSelectType(
																					e,
																					i,
																				)
																			}
																			value={{
																				text: 'Percentage',
																				value: 'percentage',
																				label: '%',
																			}}
																			options={[
																				// {
																				// 	text: 'Nominal',
																				// 	value: 'nominal',
																				// 	label: 'IDR',
																				// },
																				{
																					text: 'Percentage',
																					value: 'percentage',
																					label: '%',
																				},
																			]}
																		/>
																	</div>
																</td>
																<td>
																	<Button
																		icon='close'
																		type='button'
																		color='danger'
																		onClick={() =>
																			handleremove(i)
																		}>
																		Remove
																	</Button>
																</td>
															</tr>
														);
													})}
											</tbody>
										</table>
										<div className='col-md-12 mt-2 mb-3'>
											<Button
												type='button'
												icon='plus'
												color='success'
												onClick={handleaddclick}
												className=''>
												Add
											</Button>
										</div>
									</div>
								</ModalBody>
								<ModalFooter className='px-4 pb-4'>
									<div className='col-md-12 '>
										<Button
											icon='Save'
											isOutline
											type='submit'
											color='success'
											className='float-end'>
											Update
										</Button>
									</div>
								</ModalFooter>
							</Form>
						);
					}}
				</Formik>
			</Modal>
		</>
	);
};

const FormDelete = (dt) => {
	const { initialValues, reloadTable } = dt;
	const onDelete = () => {
		Swal.fire({
			title: 'You want to deleting this row?',
			text: 'click yes to continue!',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				if (result.value) {
					trading_term_pointModule
						.destroy({
							_id: initialValues._id,
						})
						.then((res) => {
							reloadTable();
							showNotification('Success!', res.status, 'success');
						})
						.catch((err) => {
							showNotification('Warning!', err, 'danger');
						});
				} else if (result.dismiss === Swal.DismissReason.cancel) {
					Swal.fire('Cancelled', 'Your data is safe :)', 'error');
				}
			}
		});
	};

	return (
		<div style={{ paddingLeft: '10px' }}>
			<Button icon='Delete' isOutline type='button' color='danger' onClick={() => onDelete()}>
				Delete
			</Button>
		</div>
	);
};

const CustomButton = (dt) => {
	const { row, reloadTable } = dt;
	const initialValues = {
		loading: false,
		name: row.name,
		point: row.point,
		year: row.year,
		_id: row._id,
	};
	return (
		<>
			<FormEdit initialValues={initialValues} reloadTable={() => reloadTable()} />
			<FormDelete initialValues={initialValues} reloadTable={() => reloadTable()} />
		</>
	);
};

const FormInput = (dt) => {
	const { initialValues, reloadTable, listYear } = dt;
	const [listStore, setListStore] = useState();
	const [year, setYear] = useState({
		value: moment().format('YYYY'),
		text: moment().format('YYYY'),
		label: moment().format('YYYY'),
	});
	const [storeSelected, setStoreSelected] = useState(null);
	const [readySubmit, setReadySubmit] = useState(false);
	const [listTTPoints, setListTTPoints] = useState();
	const [point, setPoint] = useState([{ name: '', value: '', type: 'percentage' }]);
	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		if (!storeSelected) {
			showNotification(
				'Store empty',
				'Store name does not exist, cannot create trading terms poin',
				'danger',
			);
		}

		point.forEach(async (e) => {
			if (!e.name) {
				showNotification(
					'Trading Terms Poin empty',
					'Trading Terms Poin does not exist, cannot create trading terms poin',
					'danger',
				);
			}
			if (!e.value) {
				showNotification(
					'Nominal Point empty',
					'Nominal Point does not exist, cannot create trading terms poin',
					'danger',
				);
			}
			if (!e.type) {
				showNotification(
					'Type Point empty',
					'Type Point does not exist, cannot create trading terms poin',
					'danger',
				);
			}
		});

		if (!year) {
			showNotification(
				'Year empty',
				'Year name does not exist, cannot create trading terms poin',
				'danger',
			);
		}

		const newValue = {
			...values,
		};
		newValue.account = storeSelected.value;
		newValue.yr = year.value;
		newValue.point = point;
		if (storeSelected && readySubmit) {
			try {
				handleSubmit(
					newValue,
					initialValues,
					reloadTable,
					handleReset,
					resetForm,
					readySubmit,
				);
				setStatus({ success: true });
			} catch (error) {
				setStatus({ success: false });
				setSubmitting(false);
				setErrors({ submit: error.message });
			}
		}
	};

	const handleReset = () => {
		setPoint([]);
		setStoreSelected(null);
		setPoint([{ name: null, value: null, type: 'percentage' }]);
	};

	const fetchStore = async () => {
		return masterNkaModule.list().then((res) => {
			setListStore(res.foundData);
		});
	};

	const fetchTtPoints = async () => {
		return masterNkaModule.listTtpoints().then((res) => {
			setListTTPoints(res.foundData);
		});
	};

	const handleSelectStore = (data) => {
		const { store, yr } = data;
		setYear(yr);
		data.yr = year;
		trading_term_pointModule.checkStore(data).then((res) => {
			if (res.data) {
				setStoreSelected(store);
			} else {
				setStoreSelected(null);
				Swal.fire(
					'Data already exists',
					'data already exists, you cannot add a new one',
					'error',
				);
			}
		});
	};

	const handleSelectYear = (data) => {
		const { yr } = data;
		setYear(yr);
		trading_term_pointModule.checkStore(data).then((res) => {
			if (res.data) {
				setYear(yr);
			} else {
				setYear(null);
				Swal.fire(
					'Data already exists',
					'data already exists, you cannot add a new one',
					'error',
				);
			}
		});
	};

	const handleChangeNominal = (e, index) => {
		const { target } = e;
		const { value } = target;
		let newSampel = [];
		const obj = point[index];
		obj.value = value;
		newSampel = [...point.slice(0, index), obj, ...point.slice(index + 1)];
		point.forEach((el) => {
			if (!el.name) {
				setReadySubmit(false);
			} else {
				setReadySubmit(true);
			}
			if (!el.value) {
				setReadySubmit(false);
			} else {
				setReadySubmit(true);
			}
			if (!el.type) {
				setReadySubmit(false);
			} else {
				setReadySubmit(true);
			}
		});
		return setPoint(newSampel);
	};

	const handleSelectTTP = (e, index) => {
		const obj = point[index];
		obj.name = e.value;
		const newSampel = [...point.slice(0, index), obj, ...point.slice(index + 1)];
		let isFound = false;

		point.reduce((acc, cur) => {
			const existingItem = acc.find((item) => cur.name === item.name);
			if (existingItem) {
				isFound = true;
				Swal.fire(
					'Trading Terms Poin already exists',
					'Trading Terms Poin already exists, you cannot add a new one',
					'error',
				);
			} else {
				isFound = false;
				acc.push({ ...cur });
			}
			return acc;
		}, []);

		if (!isFound) {
			setPoint(newSampel);
			point.forEach((el) => {
				if (!el.name) {
					setReadySubmit(false);
				} else {
					setReadySubmit(true);
				}
				if (!el.value) {
					setReadySubmit(false);
				} else {
					setReadySubmit(true);
				}
				if (!el.type) {
					setReadySubmit(false);
				} else {
					setReadySubmit(true);
				}
			});
		}
	};

	const handleSelectType = (e, index) => {
		const obj = point[index];
		obj.type = e.value;
		const newSampel = [...point.slice(0, index), obj, ...point.slice(index + 1)];
		point.forEach((el) => {
			if (!el.name) {
				setReadySubmit(false);
			} else {
				setReadySubmit(true);
			}
			if (!el.value) {
				setReadySubmit(false);
			} else {
				setReadySubmit(true);
			}
			if (!el.type) {
				setReadySubmit(false);
			} else {
				setReadySubmit(true);
			}
		});
		return setPoint(newSampel);
	};

	const handleaddclick = () => {
		setReadySubmit(false);
		return setPoint([...point, { name: '', value: '', type: 'percentage' }]);
	};

	const handleremove = (index) => {
		if (point.length > 1) {
			const _point = [...point];
			_point.forEach((e, i) => {
				if (i === index) {
					_point.splice(index, 1);
				}
			});
			setPoint(_point);
		} else {
			Swal.fire('Error', 'There is at least 1 TT Points', 'error');
		}
	};

	useEffect(() => {
		fetchStore();
		fetchTtPoints();
	}, []);

	const { t } = useTranslation('input');

	return (
		<Formik enableReinitialize initialValues={{ ...initialValues }} onSubmit={onSubmit}>
			{() => {
				return (
					<Form>
						<div className='row'>
							<div className='col-md-12'>
								<FormGroup id='name' label='Store Name' className='col-md-12 mb-3'>
									<CustomSelect
										placeholder='Select Store Name'
										onChange={(e) =>
											handleSelectStore({
												store: e,
												yr: year,
											})
										}
										value={storeSelected}
										options={listStore}
										isSearchable
									/>
								</FormGroup>
							</div>
							<div className='col-md-12'>
								<FormGroup id='year' label='Year' className='col-md-12 mb-3'>
									<CustomSelect
										placeholder='Select Year'
										onChange={(e) =>
											handleSelectYear({ store: storeSelected, yr: e })
										}
										value={year}
										options={listYear}
										isSearchable
									/>
								</FormGroup>
							</div>
						</div>
						<div className='mt-3'>
							<div className='row'>
								<CardHeader borderSize={1}>
									<CardLabel>
										<CardTitle>{t('Point TT')}</CardTitle>
									</CardLabel>
								</CardHeader>
							</div>
							<table className='table table-bordered'>
								<thead>
									<tr>
										<th>Trading Terms Poin</th>
										<th>Nominal</th>
										<th>Type</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									{point &&
										point.map((el, i) => {
											return (
												<tr>
													<td>
														<div>
															<CustomSelect
																placeholder='Trading Terms Poin'
																onChange={(e) =>
																	handleSelectTTP(e, i)
																}
																value={
																	{
																		label: el.name,
																		text: el.name,
																		value: el.name,
																	} || null
																}
																options={listTTPoints}
															/>
														</div>
													</td>
													<td>
														<div>
															<input
																type='number'
																step='any'
																className='form-control'
																onChange={(e) =>
																	handleChangeNominal(e, i)
																}
																value={el.value || null}
																name='nominal'
																placeholder='Nominal'
																aria-label='test'
															/>
														</div>
													</td>
													<td>
														<div>
															<CustomSelect
																// placeholder='%'
																onChange={(e) =>
																	handleSelectType(e, i)
																}
																value={{
																	text: 'Percentage',
																	value: 'percentage',
																	label: '%',
																}}
																options={[
																	// {
																	// 	text: 'Nominal',
																	// 	value: 'nominal',
																	// 	label: 'IDR',
																	// },
																	{
																		text: 'Percentage',
																		value: 'percentage',
																		label: '%',
																	},
																]}
															/>
														</div>
													</td>
													<td>
														<Button
															icon='close'
															type='button'
															color='danger'
															onClick={() => handleremove(i)}>
															Remove
														</Button>
													</td>
												</tr>
											);
										})}
								</tbody>
							</table>
							<div className='col-md-12 mt-2 mb-3'>
								<Button
									type='button'
									icon='plus'
									color='success'
									onClick={handleaddclick}
									className=''>
									Add
								</Button>
							</div>
						</div>
						<div className='col-md-12 '>
							<Button
								icon='Save'
								isOutline
								type='submit'
								color='success'
								className='float-end'>
								Save
							</Button>
						</div>
					</Form>
				);
			}}
		</Formik>
	);
};

const FormSearch = (dt) => {
	const { handleSearch } = dt;
	const [listAccount, setListAccount] = useState([]);
	const [listYear, setListYear] = useState([]);

	const onSubmit = (values, { setSubmitting, setStatus, setErrors }) => {
		try {
			handleSearch(values);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	const initialValues = {
		datee: '',
		store_code: '',
	};
	const fetchAccount = async () => {
		return masterNkaModule.list().then((res) => {
			setListAccount(res.foundData);
		});
	};

	const fetchYear = async () => {
		trading_term_pointModule.listYear().then((res) => {
			setListYear(res.foundData);
		});
	};

	useEffect(() => {
		fetchAccount();
		fetchYear();
	}, []);

	return (
		<Formik enableReinitialize initialValues={{ ...initialValues }} onSubmit={onSubmit}>
			{(formikField) => {
				return (
					<Form className='mb-4'>
						<div className='row'>
							<div className='col-md-2'>
								<Select
									id='store_code'
									placeholder='Select Account'
									onChange={formikField.handleChange}
									value={formikField.values.store_code}
									list={listAccount}
								/>
							</div>
							<div className='col-md-2'>
								<Select
									id='datee'
									placeholder='Select Year'
									onChange={formikField.handleChange}
									value={formikField.values.datee}
									list={listYear}
								/>
							</div>
							<div className='col-md-2'>
								<div className='row'>
									<Button
										className='col-md-5'
										icon='search'
										type='submit'
										color='success'>
										Filter
									</Button>
									<div className='col-md-6'>
										{formikField.values.date ||
										formikField.values.store_code ? (
											<Button
												type='reset'
												icon='close'
												onClick={() => {
													formikField.resetForm({
														values: initialValues,
													});
												}}
												color='primary'>
												Clear
											</Button>
										) : (
											<div />
										)}
									</div>
								</div>
							</div>
						</div>
					</Form>
				);
			}}
		</Formik>
	);
};

const InputTTPoints = () => {
	const customDataTableRef = useRef(null);
	const reloadTable = () => {
		customDataTableRef.current.inReloadTable();
	};

	const nowYear = moment().format('YYYY');
	const oldYear = Number(nowYear - 1);
	const listYear = [];
	// eslint-disable-next-line no-plusplus
	for (let i = 0; i < 3; i++) {
		listYear.push({
			value: oldYear + i,
			label: oldYear + i,
			text: oldYear + i,
		});
	}
	// eslint-disable-next-line react/prop-types
	const searchRef = useRef(null);
	const formik = useFormik({
		initialValues: {
			search: '',
		},
	});

	useEffect(() => {
		searchRef.current.focus();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const initialValues = {
		loading: false,
		store: '',
	};

	const { t } = useTranslation('category');

	return (
		<PageWrapper title='Dashboard'>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card className='col-md-6'>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('Input TT Poin')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<div className='col-12 mb-3 g-4'>
							<FormInput
								listYear={listYear}
								reloadTable={() => reloadTable()}
								initialValues={initialValues}
							/>
						</div>
					</CardBody>
				</Card>
				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('History')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<div className='mt-1 row'>
							{/* eslint-disable-next-line react/destructuring-assignment,react/prop-types */}
							<FormGroup
								label='Invoices Number'
								formText='Use either keyboard or scanner'
								className='col-12 col-md-5 col-lg-3 col-xl-2 d-none'>
								<InputGroup size='sm'>
									<Input
										id='search'
										value={formik.values.search}
										onChange={formik.handleChange}
										ref={searchRef}
									/>
								</InputGroup>
							</FormGroup>
							<div className='col-12'>
								<Table ref={customDataTableRef} tab={1} />
							</div>
						</div>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

export default InputTTPoints;
