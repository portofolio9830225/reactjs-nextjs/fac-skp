// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
import React, {
	useState,
	useEffect,
	useRef,
	useImperativeHandle,
	forwardRef,
	useMemo,
} from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { useTranslation } from 'react-i18next';
import { Form, Formik, useFormik } from 'formik';
import Swal from 'sweetalert2';
import moment from 'moment';
import PageWrapper from '../../../../layout/PageWrapper/PageWrapper';
import Page from '../../../../layout/Page/Page';
import PageLayoutHeader from '../../../../pages/common/Headers/PageLayoutHeader';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardTitle,
} from '../../../../components/bootstrap/Card';
import FormGroup from '../../../../components/bootstrap/forms/FormGroup';
import InputGroup, { InputGroupText } from '../../../../components/bootstrap/forms/InputGroup';
import showNotification from '../../../../components/extras/showNotification';
import Input from '../../../../components/bootstrap/forms/Input';
import useDarkMode from '../../../../hooks/useDarkMode';
import DarkDataTable from '../../../../components/DarkDataTable';
import Button from '../../../../components/bootstrap/Button';
import Modal, {
	ModalBody,
	ModalHeader,
	ModalTitle,
	ModalFooter,
} from '../../../../components/bootstrap/Modal';
import CustomSelect from '../../../../components/CustomSelect';
import claimModule from '../../../../modules/rizky/claimsModule';
import masterNkaModule from '../../../../modules/rizky/master-nkaModule';
import ttpModule from '../../../../modules/rizky/trading_term_pointModule';
import Textarea from '../../../../components/bootstrap/forms/Textarea';
import trading_term_pointModule from '../../../../modules/rizky/trading_term_pointModule';
import Select from '../../../../components/bootstrap/forms/Select';

const FormSearch = (dt) => {
	const { handleSearch } = dt;
	const [listAccount, setListAccount] = useState([]);
	const [listYear, setListYear] = useState([]);

	const onSubmit = (values, { setSubmitting, setStatus, setErrors }) => {
		try {
			handleSearch(values);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	const initialValues = {
		datee: '',
		store_code: '',
	};
	const fetchAccount = async () => {
		return masterNkaModule.list().then((res) => {
			setListAccount(res.foundData);
		});
	};

	const fetchYear = async () => {
		trading_term_pointModule.listYear().then((res) => {
			setListYear(res.foundData);
		});
	};

	useEffect(() => {
		fetchAccount();
		fetchYear();
	}, []);

	return (
		<Formik enableReinitialize initialValues={{ ...initialValues }} onSubmit={onSubmit}>
			{(formikField) => {
				return (
					<Form className='mb-4'>
						<div className='row'>
							<div className='col-md-2'>
								<Select
									id='store_code'
									placeholder='Select Account'
									onChange={formikField.handleChange}
									value={formikField.values.store_code}
									list={listAccount}
								/>
							</div>
							<div className='col-md-2'>
								<Select
									id='datee'
									placeholder='Select Year'
									onChange={formikField.handleChange}
									value={formikField.values.datee}
									list={listYear}
								/>
							</div>
							<div className='col-md-2'>
								<div className='row'>
									<Button
										className='col-md-5'
										icon='search'
										type='submit'
										color='success'>
										Filter
									</Button>
									<div className='col-md-6'>
										{formikField.values.datee ||
										formikField.values.store_code ? (
											<Button
												type='reset'
												icon='close'
												onClick={() => {
													formikField.resetForm({
														values: initialValues,
													});
												}}
												color='primary'>
												Clear
											</Button>
										) : (
											<div />
										)}
									</div>
								</div>
							</div>
						</div>
					</Form>
				);
			}}
		</Formik>
	);
};

const Table = forwardRef((props, ref) => {
	// eslint-disable-next-line react/prop-types
	const [customers, setCustomers] = useState(props);
	const { darkModeStatus } = useDarkMode();
	const [data, setData] = useState([]);
	const [filter, setFilter] = useState();
	const [loading, setLoading] = useState(false);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);

	const handleSearch = async (values) => {
		fetchData(1, values);
		setFilter(values);
	};

	const fetchData = async (page, params = {}) => {
		setLoading(true);
		setCustomers(props);
		params = {
			...params,
			page,
			sizePerPage: perPage,
		};

		return claimModule.readTableTTP(new URLSearchParams(params)).then((res) => {
			setData(res.foundData);
			setTotalRows(res.countData);
			setLoading(false);
		});
	};

	const handlePageChange = (page) => {
		fetchData(page, filter);
	};

	const handlePerRowsChange = async (newPerPage, page) => {
		setLoading(true);

		const params = {
			page,
			sizePerPage: newPerPage,
		};

		return claimModule.readTableTTP(new URLSearchParams(params)).then((res) => {
			setData(res.foundData);
			setPerPage(newPerPage);
			setLoading(false);
		});
	};

	useEffect(() => {
		fetchData(1); // fetch page 1 of data

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	// eslint-disable-next-line react-hooks/exhaustive-deps
	const reloadTable = () => {
		fetchData(1);
	};

	useImperativeHandle(ref, () => ({
		inReloadTable(params) {
			fetchData(1, params);
		},
	}));

	const columns = useMemo(
		() => [
			{
				name: 'Account',
				selector: (row) => row.account,
				sortable: true,
			},
			{
				name: 'Trading Terms Point',
				selector: (row) => row.trading_term_points,
				sortable: true,
			},
			{
				name: 'Periode Claim',
				selector: (row) => moment(row.periode_claim).format('MMMM YYYY'),
				sortable: true,
			},
			{
				name: 'Amount',
				selector: (row) =>
					new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(
						row.amount,
					),
				sortable: true,
			},
			{
				name: 'Remark',
				selector: (row) => row.remark,
				sortable: true,
			},
			{
				name: 'Action',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (dt) => {
					return (
						<CustomButton
							customers={customers.customers}
							row={dt}
							reloadTable={() => reloadTable()}
						/>
					);
				},
			},
		],
		[reloadTable, customers.customers],
	);

	return (
		<>
			<FormSearch handleSearch={(e) => handleSearch(e, 1)} />
			<DarkDataTable
				columns={columns}
				data={data}
				progressPending={loading}
				pagination
				paginationServer
				paginationTotalRows={totalRows}
				onChangeRowsPerPage={handlePerRowsChange}
				onChangePage={handlePageChange}
				theme={darkModeStatus ? 'custom_dark' : 'light'}
			/>
		</>
	);
});

const handleSubmit = (values, initialValues, reloadTable, handleReset, resetForm) => {
	if (values) {
		delete values.loading;
		return Swal.fire({
			title: 'Are you sure?',
			text: 'Please check your entries !',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				claimModule
					.create(values)
					.then((res) => {
						if (res.status == 'amount') {
							showNotification('Warning!', res.data, 'warning');
						} else {
							reloadTable();
							showNotification('Success!', res.status, 'success');
							handleReset();
							resetForm(initialValues);
						}
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					});
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				Swal.fire('Cancelled', 'Your data is safe :)', 'error');
			}
		});
	}
	return values;
};

const handleSubmitEdit = (values, reloadTable) => {
	if (values) {
		delete values.loading;
		return Swal.fire({
			title: 'Are you sure?',
			text: 'Please check your entries !',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				claimModule
					.update(values)
					.then((res) => {
						reloadTable();
						showNotification('Success!', res.status, 'success');
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					});
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				Swal.fire('Cancelled', 'Your data is safe :)', 'error');
			}
		});
	}
	return values;
};

const FormEdit = (dt) => {
	const { initialValues, reloadTable } = dt;
	const [isOpen, setIsOpen] = useState(false);
	const [listTTP, setListTTP] = useState();
	const [listAccount, setListAccount] = useState([]);
	const [ttpSelected, setTTPSelected] = useState(initialValues.trading_term_points);
	const [accountSelected, setAccountSelected] = useState(initialValues.account);
	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		try {
			const newValue = {
				...values,
			};
			delete newValue.skp;
			newValue.type = 'tt';
			newValue.account_name = accountSelected.value;
			newValue.code = ttpSelected.value;

			handleSubmitEdit(newValue, reloadTable, resetForm);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	const handleSelect = async (e) => {
		setTTPSelected(null);
		const filter = {
			account_name: e.text,
		};
		await ttpModule.listSelected(filter).then((res) => {
			setListTTP(res.foundData);
		});
		setAccountSelected(e);
	};

	const fetchAccount = async () => {
		return masterNkaModule.list().then((res) => {
			setListAccount(res.foundData);
		});
	};

	useEffect(() => {
		fetchAccount();
	}, []);

	const validate = (values) => {
		const errors = {};
		if (!values.amount) {
			errors.amount = 'Required';
		}
		if (!values.remark) {
			errors.remark = 'Required';
		}
		if (!values.periode_claim) {
			errors.periode_claim = 'Required';
		}
		return errors;
	};

	return (
		<>
			<Button
				icon='Edit'
				isOutline
				type='button'
				color='primary'
				onClick={() => setIsOpen(true)}>
				Edit
			</Button>
			<Modal
				isStaticBackdrop
				isOpen={isOpen}
				setIsOpen={setIsOpen}
				size='lg'
				titleId='modal-edit-menu-crud'>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-edit-menu-crud'>Edit TT Poin</ModalTitle>
				</ModalHeader>
				<Formik
					enableReinitialize
					initialValues={{ ...initialValues }}
					onSubmit={onSubmit}
					validate={validate}>
					{(formikField) => {
						return (
							<Form>
								<ModalBody className='px-4'>
									<div className='row'>
										<div className='col-md-6'>
											<FormGroup
												id='account'
												label='Account'
												className='col-md-12 mb-3'>
												<CustomSelect
													isDisabled
													placeholder='Select Account'
													onChange={(e) => handleSelect(e)}
													value={accountSelected}
													options={listAccount}
													isTouched={formikField.touched.account}
												/>
											</FormGroup>
											<FormGroup
												id='ttp'
												label='Trading Term Poin'
												className='col-md-12 mb-3'>
												<CustomSelect
													isDisabled
													placeholder='Select Trading Term Poin'
													onChange={(e) => setTTPSelected(e)}
													value={ttpSelected}
													options={listTTP}
													isTouched={formikField.touched.ttp}
												/>
											</FormGroup>
										</div>
										<div className='col-md-6'>
											<FormGroup
												id='periode_claim'
												label='Periode Claim (Month)'
												className='col-md-12 mb-3'>
												<Input
													type='Month'
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.periode_claim}
													isValid={formikField.isValid}
													isTouched={formikField.touched.periode_claim}
													invalidFeedback={
														formikField.errors.periode_claim
													}
													autoComplete='off'
												/>
											</FormGroup>
											<FormGroup
												id='amount'
												label='Amount'
												className='col-md-12 mb-3'>
												<InputGroup>
													<InputGroupText id='inputGroup-sizing'>
														IDR
													</InputGroupText>
													<Input
														name='amount'
														type='number'
														onChange={formikField.handleChange}
														onBlur={formikField.handleBlur}
														value={formikField.values.amount}
														isValid={formikField.isValid}
														isTouched={formikField.touched.amount}
														invalidFeedback={formikField.errors.amount}
														autoComplete='off'
													/>
												</InputGroup>
											</FormGroup>
										</div>
										<div className='col-md-6'>
											<FormGroup
												id='distributor'
												label='Distributor'
												className='col-md-12 mb-3'>
												<Input
													type='text'
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.distributor}
													isValid={formikField.isValid}
													isTouched={formikField.touched.distributor}
													invalidFeedback={formikField.errors.distributor}
													autoComplete='off'
												/>
											</FormGroup>
											<FormGroup
												id='invoice_number'
												label='Invoice Number'
												className='col-md-12 mb-3'>
												<Input
													type='text'
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.invoice_number}
													isValid={formikField.isValid}
													isTouched={formikField.touched.invoice_number}
													invalidFeedback={
														formikField.errors.invoice_number
													}
													autoComplete='off'
												/>
											</FormGroup>
										</div>
										<div className='col-md-6'>
											<FormGroup
												id='branch'
												label='Branch'
												className='col-md-12 mb-3'>
												<Input
													type='text'
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.branch}
													isValid={formikField.isValid}
													isTouched={formikField.touched.branch}
													invalidFeedback={formikField.errors.branch}
													autoComplete='off'
												/>
											</FormGroup>
										</div>
										<FormGroup
											id='remark'
											label='Remark'
											className='col-md-12 mb-3'>
											<Textarea
												onChange={formikField.handleChange}
												onBlur={formikField.handleBlur}
												value={formikField.values.remark}
												isValid={formikField.isValid}
												isTouched={formikField.touched.remark}
												invalidFeedback={formikField.errors.remark}
												autoComplete='off'
											/>
										</FormGroup>
									</div>
								</ModalBody>
								<ModalFooter className='px-4 pb-4'>
									<div className='col-md-12 '>
										<Button
											icon='Save'
											isOutline
											type='submit'
											color='success'
											className='float-end'>
											Update
										</Button>
									</div>
								</ModalFooter>
							</Form>
						);
					}}
				</Formik>
			</Modal>
		</>
	);
};

const FormDelete = (dt) => {
	const { initialValues, reloadTable } = dt;
	const onDelete = () => {
		Swal.fire({
			title: 'You want to deleting this row?',
			text: 'click yes to continue!',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				if (result.value) {
					claimModule
						.destroy({
							_id: initialValues._id,
						})
						.then((res) => {
							reloadTable();
							showNotification('Success!', res.status, 'success');
						})
						.catch((err) => {
							showNotification('Warning!', err, 'danger');
						});
				} else if (result.dismiss === Swal.DismissReason.cancel) {
					Swal.fire('Cancelled', 'Your data is safe :)', 'error');
				}
			}
		});
	};

	return (
		<div style={{ paddingLeft: '10px' }}>
			<Button icon='Delete' isOutline type='button' color='danger' onClick={() => onDelete()}>
				Delete
			</Button>
		</div>
	);
};

const CustomButton = (dt) => {
	const { row, reloadTable } = dt;
	const split = row.periode_claim.split('-');
	const initialValues = {
		loading: false,
		amount: row.amount,
		account: { value: row.account_name, text: row.account_name, label: row.account_name },
		remark: row.remark,
		trading_term_points: {
			value: row.trading_term_points,
			text: row.trading_term_points,
			label: row.trading_term_points,
		},
		periode_claim: `${split[0]}-${split[1]}`,
		_id: row._id,
		distributor: row.distributor,
		invoice_number: row.invoice_number,
		branch: row.branch,
	};
	if (row.isDisable) {
		return <div />;
	}
	return (
		<>
			<FormEdit initialValues={initialValues} reloadTable={() => reloadTable()} />
			<FormDelete initialValues={initialValues} reloadTable={() => reloadTable()} />
		</>
	);
};

const FormInput = (dt) => {
	const { initialValues, reloadTable } = dt;
	const [listTTP, setListTTP] = useState();
	const [listAccount, setListAccount] = useState([]);
	const [ttpSelected, setTTPSelected] = useState(null);
	const [accountSelected, setAccountSelected] = useState(null);
	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		const newValue = {
			...values,
		};
		newValue.type = 'tt';
		newValue.account_name = accountSelected.value;
		newValue.code = ttpSelected.value;
		if (ttpSelected) {
			try {
				handleSubmit(newValue, initialValues, reloadTable, handleReset, resetForm);
				setStatus({ success: true });
			} catch (error) {
				setStatus({ success: false });
				setSubmitting(false);
				setErrors({ submit: error.message });
			}
		}
	};

	const handleReset = () => {
		setTTPSelected(null);
		setAccountSelected(null);
	};

	const handleSelect = async (e) => {
		setAccountSelected(e);
		setTTPSelected(null);
		setListTTP(e.point);
	};

	const fetchAccount = async () => {
		ttpModule.listSelected().then((res) => {
			setListAccount(res.foundData);
		});
	};

	useEffect(() => {
		fetchAccount();
	}, []);

	const validate = (values) => {
		const errors = {};
		if (!values.amount) {
			errors.amount = 'Required';
		}
		if (!values.remark) {
			errors.remark = 'Required';
		}
		if (!values.periode_claim) {
			errors.periode_claim = 'Required';
		}
		if (values.amount <= 0) {
			errors.amount = 'amount must be greater than 0';
		}
		return errors;
	};

	return (
		<Formik
			enableReinitialize
			initialValues={{ ...initialValues }}
			onSubmit={onSubmit}
			validate={validate}>
			{(formikField) => {
				return (
					<Form>
						<div className='row'>
							<div className='col-md-6'>
								<FormGroup id='account' label='Account' className='col-md-12 mb-3'>
									<CustomSelect
										placeholder='Select Account'
										onChange={(e) => handleSelect(e)}
										value={accountSelected}
										options={listAccount}
										isTouched={formikField.touched.account}
									/>
								</FormGroup>
								<FormGroup
									id='ttp'
									label='Trading Term Poin'
									className='col-md-12 mb-3'>
									<CustomSelect
										placeholder='Select Trading Term Poin'
										onChange={(e) => setTTPSelected(e)}
										value={ttpSelected}
										options={listTTP}
										isTouched={formikField.touched.ttp}
									/>
								</FormGroup>
							</div>
							<div className='col-md-6'>
								<FormGroup
									id='periode_claim'
									label='Periode Claim (Month)'
									className='col-md-12 mb-3'>
									<Input
										type='Month'
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.periode_claim}
										isValid={formikField.isValid}
										isTouched={formikField.touched.periode_claim}
										invalidFeedback={formikField.errors.periode_claim}
										autoComplete='off'
									/>
								</FormGroup>
								<FormGroup id='amount' label='Amount' className='col-md-12 mb-3'>
									<InputGroup>
										<InputGroupText id='inputGroup-sizing'>IDR</InputGroupText>
										<Input
											name='amount'
											type='number'
											onChange={formikField.handleChange}
											onBlur={formikField.handleBlur}
											value={formikField.values.amount}
											isValid={formikField.isValid}
											isTouched={formikField.touched.amount}
											invalidFeedback={formikField.errors.amount}
											autoComplete='off'
										/>
									</InputGroup>
								</FormGroup>
							</div>
							<div className='col-md-6'>
								<FormGroup
									id='distributor'
									label='Distributor'
									className='col-md-12 mb-3'>
									<Input
										type='text'
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.distributor}
										isValid={formikField.isValid}
										isTouched={formikField.touched.distributor}
										invalidFeedback={formikField.errors.distributor}
										autoComplete='off'
									/>
								</FormGroup>
								<FormGroup
									id='invoice_number'
									label='Invoice Number'
									className='col-md-12 mb-3'>
									<Input
										type='text'
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.invoice_number}
										isValid={formikField.isValid}
										isTouched={formikField.touched.invoice_number}
										invalidFeedback={formikField.errors.invoice_number}
										autoComplete='off'
									/>
								</FormGroup>
							</div>
							<div className='col-md-6'>
								<FormGroup id='branch' label='Branch' className='col-md-12 mb-3'>
									<Input
										type='text'
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.branch}
										isValid={formikField.isValid}
										isTouched={formikField.touched.branch}
										invalidFeedback={formikField.errors.branch}
										autoComplete='off'
									/>
								</FormGroup>
							</div>
							<FormGroup id='remark' label='Remark' className='col-md-12 mb-3'>
								<Textarea
									onChange={formikField.handleChange}
									onBlur={formikField.handleBlur}
									value={formikField.values.remark}
									isValid={formikField.isValid}
									isTouched={formikField.touched.remark}
									invalidFeedback={formikField.errors.remark}
									autoComplete='off'
								/>
							</FormGroup>
						</div>
						<div className='col-md-12 '>
							<Button
								icon='Save'
								isOutline
								type='submit'
								color='success'
								className='float-end'>
								Save
							</Button>
						</div>
					</Form>
				);
			}}
		</Formik>
	);
};

const ClaimTTP = () => {
	const customDataTableRef = useRef(null);
	const reloadTable = () => {
		customDataTableRef.current.inReloadTable();
	};

	// eslint-disable-next-line react/prop-types
	const searchRef = useRef(null);
	const formik = useFormik({
		initialValues: {
			search: '',
		},
	});

	useEffect(() => {
		searchRef.current.focus();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const initialValues = {
		loading: false,
		amount: '',
		remark: '',
		periode_claim: '',
		distributor: '',
		invoice_number: '',
		branch: '',
	};

	const { t } = useTranslation('category');

	return (
		<PageWrapper title='Dashboard'>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card className='col-md-6'>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('Claim Trading Term Monthly')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<div className='col-12 mb-3 g-4'>
							<FormInput
								reloadTable={() => reloadTable()}
								initialValues={initialValues}
							/>
						</div>
					</CardBody>
				</Card>
				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('History')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						{/* <HistoryTable ref={customDataTableRef} /> */}
						<div className='mt-1 row'>
							{/* eslint-disable-next-line react/destructuring-assignment,react/prop-types */}
							<FormGroup
								label='Invoices Number'
								formText='Use either keyboard or scanner'
								className='col-12 col-md-5 col-lg-3 col-xl-2 d-none'>
								<InputGroup size='sm'>
									<Input
										id='search'
										value={formik.values.search}
										onChange={formik.handleChange}
										ref={searchRef}
									/>
								</InputGroup>
							</FormGroup>
							<div className='col-12 mt-4'>
								<Table ref={customDataTableRef} tab={1} />
							</div>
						</div>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

export default ClaimTTP;
