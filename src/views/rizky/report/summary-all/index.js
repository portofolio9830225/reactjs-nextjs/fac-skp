// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable react/prop-types */
/* eslint-enable-next-line react/prop-types */
import React, { useState, useEffect } from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { useTranslation } from 'react-i18next';
import PageWrapper from '../../../../layout/PageWrapper/PageWrapper';
import Page from '../../../../layout/Page/Page';
import PageLayoutHeader from '../../../../pages/common/Headers/PageLayoutHeader';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardTitle,
} from '../../../../components/bootstrap/Card';
import CustomSelect from '../../../../components/CustomSelect';
import FormGroup from '../../../../components/bootstrap/forms/FormGroup';
import parameterModule from '../../../../modules/rizky/parametersModule';
import reportModule from '../../../../modules/rizky/reportModule';
import CustomTable from '../../../../components/custom/rizky/Table/Table';

const Table = (dt) => {
	const { year } = dt;
	const [dataTableSellIn, setDataTableSellIn] = useState([]);
	const [dataTableExpenseAll, setDataTableExpenseAll] = useState([]);
	const [dataTableExpenseSKP, setDataTableExpenseSKP] = useState([]);
	const [dataTableExpenseTT, setDataTableExpenseTT] = useState([]);
	const [column, setColumn] = useState({
		sell_in: [],
		expense_all: [],
		expense_skp: [],
		expense_tt: [],
	});
	const fetchMasterNka = async (yr) => {
		return reportModule.readTable(yr.value).then((res) => {
			const new_sell_in = [...res.sell_in];
			const new_expense_all = [...res.expense_all];
			const new_expense_skp = [...res.expense_skp];
			const new_expense_tt = [...res.expense_tt];

			const total_actual_sell_in = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
			const total_expense_expense_all = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
			const total_costRatio_expense_all = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
			const total_actual_expense_skp = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
			const total_costRatio_expense_skp = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
			const total_actual_expense_tt = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
			const total_costRatio_expense_tt = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

			new_sell_in.forEach((el) => {
				el.actual.forEach((itm, idx) => {
					total_actual_sell_in[idx] += itm;
				});
			});

			new_expense_all.forEach((el) => {
				el.expense.forEach((itm, idx) => {
					total_expense_expense_all[idx] += itm;
				});

				el.cost_ratio.forEach((itm, idx) => {
					total_costRatio_expense_all[idx] += itm;
				});
			});

			new_expense_skp.forEach((el) => {
				el.actual.forEach((itm, idx) => {
					total_actual_expense_skp[idx] += itm;
				});

				el.cost_ratio.forEach((itm, idx) => {
					total_costRatio_expense_skp[idx] += itm;
				});
			});

			new_expense_tt.forEach((el) => {
				el.actual.forEach((itm, idx) => {
					total_actual_expense_tt[idx] += itm;
				});

				el.cost_ratio.forEach((itm, idx) => {
					total_costRatio_expense_tt[idx] += Math.round(itm * 100) / 100;
				});
			});

			new_sell_in.push({
				store_code: 'TOTAL',
				store_name: 'TOTAL',
				actual: total_actual_sell_in,
			});

			new_expense_all.push({
				cost_ratio: total_costRatio_expense_all,
				expense: total_expense_expense_all,
				store_code: 'TOTAL',
				store_name: 'TOTAL',
			});

			new_expense_skp.push({
				cost_ratio: total_costRatio_expense_skp,
				actual: total_actual_expense_skp,
				store_code: 'TOTAL',
				store_name: 'TOTAL',
			});

			new_expense_tt.push({
				cost_ratio: total_costRatio_expense_tt,
				actual: total_actual_expense_tt,
				store_code: 'TOTAL',
				store_name: 'TOTAL',
			});

			handleColumn({ yr: year });
			setDataTableSellIn(new_sell_in);
			setDataTableExpenseSKP(new_expense_skp);
			setDataTableExpenseAll(new_expense_all);
			setDataTableExpenseTT(new_expense_tt);
		});
	};

	const handleColumn = (dta) => {
		const { yr } = dta;
		const month = [
			'Jan',
			'Feb',
			'Mar',
			'Apr',
			'Mei',
			'Jun',
			'Jul',
			'Aug',
			'Sep',
			'Oct',
			'Nov',
			'Des',
		];
		const resultSellIn = [{ heading: 'Nama Toko', value: 'store_name' }];
		const resultExpenseAll = [{ heading: 'Nama Toko', value: 'store_name' }];
		const resultExpenseAllRatio = [{ heading: 'Nama Toko', value: 'store_name' }];
		const resultExpenseSKP = [{ heading: 'Nama Toko', value: 'store_name' }];
		const resultExpenseTT = [{ heading: 'Nama Toko', value: 'store_name' }];
		const resultExpenseTTRatio = [{ heading: 'Nama Toko', value: 'store_name' }];
		const resultExpenseSKPRatio = [{ heading: 'Nama Toko', value: 'store_name' }];
		month.forEach((e, i) => {
			resultSellIn.push({
				heading: `${e}-${yr.value.substr(2)}`,
				value: 'actual',
				index: `${i}`,
			});
			resultExpenseAll.push({
				heading: `${e}-${yr.value.substr(2)}`,
				value: 'expense',
				index: `${i}`,
			});
			resultExpenseAllRatio.push({
				heading: `${e}-${yr.value.substr(2)}`,
				value: 'cost_ratio',
				index: `${i}`,
			});
			resultExpenseSKP.push({
				heading: `${e}-${yr.value.substr(2)}`,
				value: 'actual',
				index: `${i}`,
			});
			resultExpenseTT.push({
				heading: `${e}-${yr.value.substr(2)}`,
				value: 'actual',
				index: `${i}`,
			});
			resultExpenseTTRatio.push({
				heading: `${e}-${yr.value.substr(2)}`,
				value: 'cost_ratio',
				index: `${i}`,
			});
			resultExpenseSKPRatio.push({
				heading: `${e}-${yr.value.substr(2)}`,
				value: 'cost_ratio',
				index: `${i}`,
			});
		});
		setColumn({
			...column,
			sell_in: [...resultSellIn, { heading: `TOTAL SELL IN`, value: 'actual', sum: true }],
			expense_all: [
				...resultExpenseAll,
				{ heading: `TOTAL EXPENSE ALL`, value: 'expense', sum: true },
			],
			expense_all_ratio: [
				...resultExpenseAllRatio,
				{
					heading: `ACTUAL COST RATIO TRADING TERM & ON TOP BUDGET`,
					value: 'cost_ratio',
					sum: true,
				},
			],
			expense_skp: [
				...resultExpenseSKP,
				{ heading: `TOTAL ON TOP BUDGET`, value: 'actual', sum: true },
			],
			expense_tt: [
				...resultExpenseTT,
				{ heading: `TOTAL TRADING TERMS`, value: 'actual', sum: true },
			],
			expense_tt_ratio: [
				...resultExpenseTTRatio,
				{ heading: `ACTUAL COST RATIO TRADING TERM`, value: 'cost_ratio', sum: true },
			],
			expense_skp_ratio: [
				...resultExpenseSKPRatio,
				{ heading: `ACTUAL COST RATIO ON TOP BUDGET`, value: 'cost_ratio', sum: true },
			],
		});
	};

	useEffect(() => {
		fetchMasterNka(year);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [year]);

	if (dataTableExpenseTT.length > 0) {
		return (
			<>
				<CustomTable
					data={dataTableSellIn}
					column={column.sell_in}
					title='NKA SELL IN'
					type='nominal'
				/>
				<CustomTable
					data={dataTableExpenseAll}
					column={column.expense_all}
					title='NKA EXPENSE ALL (TRADING TERM, ON TOP BUDGET)'
					type='nominal'
				/>
				<CustomTable
					data={dataTableExpenseAll}
					column={column.expense_all_ratio}
					title='COST RATIO NKA ALL EXPENSE'
					type='precentage'
				/>
				<CustomTable
					data={dataTableExpenseSKP}
					column={column.expense_skp}
					title='NKA EXPENSE (ON TOP BUDGET)'
					type='nominal'
				/>
				<CustomTable
					data={dataTableExpenseSKP}
					column={column.expense_skp_ratio}
					title='COST RATIO (ON TOP BUDGET)'
					type='precentage'
				/>
				<CustomTable
					data={dataTableExpenseTT}
					column={column.expense_tt}
					title='NKA EXPENSE (TRADING TERMS)'
					type='nominal'
				/>
				<CustomTable
					data={dataTableExpenseTT}
					column={column.expense_tt_ratio}
					title='COST RATIO (TRADING TERM)'
					type='precentage'
				/>
			</>
		);
	}
	return <div />;
};

const SummaryAll = () => {
	const [yearSelected, setYearSelected] = useState(null);
	const [listYear, setListYear] = useState([]);
	const fetchYear = async () => {
		return parameterModule.listYear().then((res) => {
			setListYear(res.foundData);
		});
	};

	useEffect(() => {
		fetchYear();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const { t } = useTranslation('category');

	return (
		<PageWrapper title='Dashboard'>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('Report Summary All')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<div className='row'>
							<div className='col d-flex justify-content-start'>
								<FormGroup id='year' label='Filter Year' className='col-md-1 mb-3'>
									<CustomSelect
										isSearchable
										placeholder='Select Year'
										onChange={(e) => setYearSelected(e)}
										value={yearSelected}
										options={listYear}
									/>
								</FormGroup>
							</div>
							<div className='col-12 mt-4'>
								<Table year={yearSelected} />
							</div>
						</div>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

export default SummaryAll;
