// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
import React, { useState, useEffect, useRef } from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { useTranslation } from 'react-i18next';
import { Form, Formik } from 'formik';
import Swal from 'sweetalert2';
import PageWrapper from '../../../../layout/PageWrapper/PageWrapper';
import Page from '../../../../layout/Page/Page';
import PageLayoutHeader from '../../../../pages/common/Headers/PageLayoutHeader';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardTitle,
} from '../../../../components/bootstrap/Card';
import FormGroup from '../../../../components/bootstrap/forms/FormGroup';
import showNotification from '../../../../components/extras/showNotification';
import generateModule from '../../../../modules/rizky/generateModule';
import Input from '../../../../components/bootstrap/forms/Input';
import Select from '../../../../components/bootstrap/forms/Select';
import Button from '../../../../components/bootstrap/Button';
import summary_accountModule from '../../../../modules/sodiq/summary_accountModule';

const handleSubmit = (values, initialValues, handleReset, resetForm) => {
	if (values.date && values.type) {
		delete values.loading;
		return Swal.fire({
			title: 'Are you sure?',
			text: 'Please check your entries !',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				generateModule
					.skp_tt(new URLSearchParams(values))
					.then((res) => {
						showNotification('Success!', res.status, 'success');
						handleReset();
						resetForm(initialValues);
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					});
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				Swal.fire('Cancelled', 'Your data is safe :)', 'error');
			}
		});
	}
	return values;
};
const handleSubmitSellIn = (values, initialValues, handleReset, resetForm) => {
	if (values.date) {
		return Swal.fire({
			title: 'Are you sure?',
			text: 'Please check your entries !',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				generateModule
					.sell_in(new URLSearchParams(values))
					.then(() => {
						showNotification('Success!', 'Success generate', 'success');      
                        handleReset();
						resetForm(initialValues);
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					});
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				Swal.fire('Cancelled', 'Your data is safe :)', 'error');
			}
		});
	}
	return values;
};

const FormInputSellIn = (dt) => {
	const { year } = dt;
    const initialValues = {
        datee: ''
    }
	const onSubmit = (values, { setSubmitting, setStatus, resetForm, setErrors }) => {
        values.date = `${values.datee}-01-01`;
        delete values.datee;
		try {
			handleSubmitSellIn(values, initialValues, handleReset, resetForm );
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	}; 

	const handleReset = () => {
        initialValues.date = ''
    };

	useEffect(() => {}, []);

    const validate = (values) => {
		const errors = {};

		if (!values.datee) {
			errors.datee = 'Required';
		}
		return errors;
	};

	return (
		<Formik
        enableReinitialize
			initialValues={initialValues}
            validate={validate}
			onSubmit={onSubmit}>
			{(formikField) => {
				return (
					<Form>
						<div className='mt-3 row'>
							{/* eslint-disable-next-line react/destructuring-assignment,react/prop-types */}

							<div className='col-md-3'>
                            <Select
									placeholder='Select Year'
									onChange={formikField.handleChange}
									value={formikField.values.datee}
                                    name='datee'
									list={year}
									isValid={formikField.isValid}
									isTouched={formikField.touched.datee}
									invalidFeedback={formikField.errors.datee}
								/>
							</div>
							<div className='col-md-2'>
								<Button type='submit' color='success'>
									Generate
								</Button>
							</div>
						</div>
					</Form>
				);
			}}
		</Formik>
	);
};

const FormInputGenerateSKPTT = () => {
	const type = [
		{ value: 'skp', text: 'SKP' },
		{ value: 'tt', text: 'Trading Term' },
	];
    const initialValues = {
        datee: '',
        type: ''
    }
	const onSubmit = (values, { setSubmitting,resetForm, setStatus, setErrors }) => {
        values.date = `${values.datee}-01`;
        delete values.datee;
		try {
			handleSubmit(values, initialValues, handleReset, resetForm);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	const handleReset = () => {
        initialValues.datee = '';
        initialValues.type = '';
    };

	useEffect(() => {}, []);

	const validate = (values) => {
		const errors = {};

		if (!values.type) {
			errors.type = 'Required';
		}
		if (!values.datee) {
			errors.datee = 'Required';
		}
		return errors;
	};

	return (
		<Formik
			validate={validate}
			initialValues={{ ...initialValues }}
			onSubmit={onSubmit}>
			{(formikField) => {
				return (
					<Form>
						<div className='row'>
							{/* eslint-disable-next-line react/destructuring-assignment,react/prop-types */}
							<FormGroup id='datee' label='' className='col-md-3 mb-3'>
								<Input
									type='Month'
									onChange={formikField.handleChange}
									onBlur={formikField.handleBlur}
									value={formikField.values.datee}
                                    name='datee'
									isValid={formikField.isValid}
									isTouched={formikField.touched.datee}
									invalidFeedback={formikField.errors.datee}
									autoComplete='off'
								/>
							</FormGroup>
							<div className='col-md-3'>
								<Select
									id='type'
									placeholder='Select Type'
									onChange={formikField.handleChange}
									value={formikField.values.type}
									list={type}
									name='type'
									isValid={formikField.isValid}
									isTouched={formikField.touched.type}
									invalidFeedback={formikField.errors.type}
								/>
							</div>
							<div className='col-md-2'>
								<Button type='submit' color='success'>
									Generate
								</Button>
							</div>
						</div>
					</Form>
				);
			}}
		</Formik>
	);
};

const SellInClaim = () => {
	const customDataTableRef = useRef(null);
	const [listYear, setListYear] = useState([]);
	const reloadTable = () => {
		customDataTableRef.current.inReloadTable();
	};

	const fetchYear = async () => {
		return summary_accountModule.year().then((res) => {
			setListYear(res.foundData);
		});
	};

	useEffect(() => {
		fetchYear();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const { t } = useTranslation('category');

	return (
		<PageWrapper title='Dashboard'>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card className='col-md-8'><CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('Generate Sell In')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<div className='col-12 mb-3 g-4'>
							<FormInputSellIn
								year={listYear}
								reloadTable={() => reloadTable()}
							/>
						</div>
					</CardBody>
				</Card>
				<Card className='col-md-8'>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('Generate Claim SKP & TT')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<div className='col-12  g-4'>
							<FormInputGenerateSKPTT
								year={listYear}
								reloadTable={() => reloadTable()}
							/>
						</div>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

export default SellInClaim;
