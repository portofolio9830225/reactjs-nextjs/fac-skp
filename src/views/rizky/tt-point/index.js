// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
import React, {
	useState,
	useEffect,
	useRef,
	useImperativeHandle,
	forwardRef,
	useMemo,
} from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { useTranslation } from 'react-i18next';
import { Form, Formik, useFormik } from 'formik';
import Swal from 'sweetalert2';
import PageWrapper from '../../../layout/PageWrapper/PageWrapper';
import Page from '../../../layout/Page/Page';
import PageLayoutHeader from '../../../pages/common/Headers/PageLayoutHeader';
import Card, {
	CardBody,
	CardHeader,
	CardLabel,
	CardTitle,
} from '../../../components/bootstrap/Card';
import FormGroup from '../../../components/bootstrap/forms/FormGroup';
import InputGroup from '../../../components/bootstrap/forms/InputGroup';
import showNotification from '../../../components/extras/showNotification';
import ttpointsModule from '../../../modules/rizky/ttpointsModule';
import Input from '../../../components/bootstrap/forms/Input';
import useDarkMode from '../../../hooks/useDarkMode';
import DarkDataTable from '../../../components/DarkDataTable';
import Button from '../../../components/bootstrap/Button';
import Modal, {
	ModalBody,
	ModalHeader,
	ModalTitle,
	ModalFooter,
} from '../../../components/bootstrap/Modal';

const Table = forwardRef((props, ref) => {
	// eslint-disable-next-line react/prop-types
	const [customers, setCustomers] = useState(props);
	const { darkModeStatus } = useDarkMode();
	const [data, setData] = useState([]);
	const [loading, setLoading] = useState(false);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);

	const fetchData = async (page, params = {}) => {
		setLoading(true);
		setCustomers(props);
		params = {
			...params,
			page,
			sizePerPage: perPage,
		};

		return ttpointsModule.readTable(new URLSearchParams(params)).then((res) => {
			setData(res.foundData);
			setTotalRows(res.countData);
			setLoading(false);
		});
	};

	const handlePageChange = (page) => {
		fetchData(page);
	};

	const handlePerRowsChange = async (newPerPage, page) => {
		setLoading(true);

		const params = {
			page,
			sizePerPage: newPerPage,
		};

		return ttpointsModule.readTable(new URLSearchParams(params)).then((res) => {
			setData(res.foundData);
			setPerPage(newPerPage);
			setLoading(false);
		});
	};

	useEffect(() => {
		fetchData(1); // fetch page 1 of data

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	// eslint-disable-next-line react-hooks/exhaustive-deps
	const reloadTable = () => {
		fetchData(1);
	};

	useImperativeHandle(ref, () => ({
		inReloadTable(params) {
			fetchData(1, params);
		},
	}));

	const columns = useMemo(
		() => [
			{
				name: 'Poin Name',
				selector: (row) => row.name,
				sortable: true,
			},
			{
				name: 'Action',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (dt) => {
					return (
						<CustomButton
							customers={customers.customers}
							row={dt}
							reloadTable={() => reloadTable()}
						/>
					);
				},
			},
		],
		[reloadTable, customers.customers],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			progressPending={loading}
			pagination
			paginationServer
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
});

const handleSubmit = (values, initialValues, reloadTable, handleReset, resetForm) => {
	if (values.name) {
		delete values.loading;
		return Swal.fire({
			title: 'Are you sure?',
			text: 'Please check your entries !',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				ttpointsModule
					.create(values)
					.then((res) => {
						reloadTable();
						showNotification('Success!', res.status, 'success');
						resetForm(initialValues);
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					});
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				Swal.fire('Cancelled', 'Your data is safe :)', 'error');
			}
		});
	}
	return values;
};

const handleSubmitEdit = (values, reloadTable) => {
	if (values) {
		return Swal.fire({
			title: 'Are you sure?',
			text: 'Please check your entries !',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				ttpointsModule
					.update(values)
					.then((res) => {
						reloadTable();
						showNotification('Success!', res.status, 'success');
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					});
			} else if (result.dismiss === Swal.DismissReason.cancel) {
				Swal.fire('Cancelled', 'Your data is safe :)', 'error');
			}
		});
	}
	return values;
};

const FormEdit = (dt) => {
	const { initialValues, reloadTable } = dt;
	const [isOpen, setIsOpen] = useState(false);
	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		try {
			handleSubmitEdit(values, reloadTable, resetForm);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	useEffect(() => {}, []);

	return (
		<>
			<Button
				icon='Edit'
				isOutline
				type='button'
				color='primary'
				onClick={() => setIsOpen(true)}>
				Edit
			</Button>
			<Modal isOpen={isOpen} setIsOpen={setIsOpen} size='lg' titleId='modal-edit-menu-crud'>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-edit-menu-crud'> Update Data</ModalTitle>
				</ModalHeader>
				<Formik enableReinitialize initialValues={{ ...initialValues }} onSubmit={onSubmit}>
					{(formikField) => {
						return (
							<Form>
								<ModalBody className='px-4'>
									<div className='row'>
										<div className='col-md-6'>
											<FormGroup
												id='name'
												label='Poin Name'
												className='col-md-12 mb-3'>
												<Input
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.name}
													isValid={formikField.isValid}
													isTouched={formikField.touched.name}
													invalidFeedback={formikField.errors.name}
													autoComplete='off'
												/>
											</FormGroup>
										</div>
									</div>
								</ModalBody>
								<ModalFooter className='px-4 pb-4'>
									<div className='col-md-12 '>
										<Button
											icon='Save'
											isOutline
											type='submit'
											color='success'
											className='float-end'>
											Update
										</Button>
									</div>
								</ModalFooter>
							</Form>
						);
					}}
				</Formik>
			</Modal>
		</>
	);
};

const FormDelete = (dt) => {
	const { initialValues, reloadTable } = dt;
	const onDelete = () => {
		Swal.fire({
			title: 'You want to deleting this row?',
			text: 'click yes to continue!',
			icon: 'info',
			showCancelButton: true,
			confirmButtonText: 'Yes',
		}).then((result) => {
			if (result.value) {
				if (result.value) {
					ttpointsModule
						.destroy({
							_id: initialValues._id,
						})
						.then((res) => {
							reloadTable();
							showNotification('Success!', res.status, 'success');
						})
						.catch((err) => {
							showNotification('Warning!', err, 'danger');
						});
				} else if (result.dismiss === Swal.DismissReason.cancel) {
					Swal.fire('Cancelled', 'Your data is safe :)', 'error');
				}
			}
		});
	};

	return (
		<div style={{ paddingLeft: '10px' }}>
			<Button icon='Delete' isOutline type='button' color='danger' onClick={() => onDelete()}>
				Delete
			</Button>
		</div>
	);
};

const CustomButton = (dt) => {
	const { row, reloadTable } = dt;
	const initialValues = {
		loading: false,
		name: row.name,
		_id: row._id,
	};
	return (
		<>
			<FormEdit initialValues={initialValues} reloadTable={() => reloadTable()} />
			<FormDelete initialValues={initialValues} reloadTable={() => reloadTable()} />
		</>
	);
};

const FormInput = (dt) => {
	const { initialValues, reloadTable } = dt;
	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		try {
			handleSubmit(values, initialValues, reloadTable, handleReset, resetForm);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	const handleReset = () => {};

	useEffect(() => {}, []);

	return (
		<Formik enableReinitialize initialValues={{ ...initialValues }} onSubmit={onSubmit}>
			{(formikField) => {
				return (
					<Form>
						<div className='row'>
							<div className='col-md-12'>
								<FormGroup id='name' label='Poin Name' className='col-md-12 mb-3'>
									<Input
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.name}
										isValid={formikField.isValid}
										isTouched={formikField.touched.name}
										invalidFeedback={formikField.errors.name}
										autoComplete='off'
									/>
								</FormGroup>
							</div>
						</div>
						<div className='col-md-12 '>
							<Button
								icon='Save'
								isOutline
								type='submit'
								color='success'
								className='float-end'>
								Submit
							</Button>
						</div>
					</Form>
				);
			}}
		</Formik>
	);
};

const TTPoints = () => {
	const customDataTableRef = useRef(null);
	const reloadTable = () => {
		customDataTableRef.current.inReloadTable();
	};

	// eslint-disable-next-line react/prop-types
	const searchRef = useRef(null);
	const formik = useFormik({
		initialValues: {
			search: '',
		},
	});

	useEffect(() => {
		searchRef.current.focus();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const initialValues = {
		loading: false,
		name: '',
	};

	const { t } = useTranslation('category');

	return (
		<PageWrapper title='Dashboard'>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card className='col-md-6'>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('Master TT Poin')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<div className='col-12 mb-3 g-4'>
							<FormInput
								reloadTable={() => reloadTable()}
								initialValues={initialValues}
							/>
						</div>
					</CardBody>
				</Card>
				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('Data')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						{/* <HistoryTable ref={customDataTableRef} /> */}
						<div className='mt-5 row'>
							{/* eslint-disable-next-line react/destructuring-assignment,react/prop-types */}
							<FormGroup
								label='Invoices Number'
								formText='Use either keyboard or scanner'
								className='col-12 col-md-5 col-lg-3 col-xl-2 d-none'>
								<InputGroup size='sm'>
									<Input
										id='search'
										value={formik.values.search}
										onChange={formik.handleChange}
										ref={searchRef}
									/>
								</InputGroup>
							</FormGroup>
							<div className='col-12 mt-4'>
								<Table ref={customDataTableRef} tab={1} />
							</div>
						</div>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

export default TTPoints;
