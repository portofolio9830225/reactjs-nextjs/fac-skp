import React, { useState, useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import { Formik, Form } from 'formik';
import * as yup from 'yup';
import Swal from 'sweetalert2';
import { useTranslation } from 'react-i18next';
import PageWrapper from '../layout/PageWrapper/PageWrapper';
import Page from '../layout/Page/Page';
import PageLayoutHeader from '../pages/common/Headers/PageLayoutHeader';
import Card, { CardBody, CardHeader, CardLabel, CardTitle } from '../components/bootstrap/Card';
import FormGroup from '../components/bootstrap/forms/FormGroup';
import Input from '../components/bootstrap/forms/Input';
import Button from '../components/bootstrap/Button';
import SegmentModule from '../modules/SegmentModule';
import RegionalModule from '../modules/RegionalModule';

import showNotification from '../components/extras/showNotification';
import DarkDataTable from '../components/DarkDataTable';
import useDarkMode from '../hooks/useDarkMode';
import CustomSelect from '../components/CustomSelect';
import COLORS from '../common/data/enumColors';
import Modal, {
	ModalBody,
	ModalHeader,
	ModalTitle,
	ModalFooter,
} from '../components/bootstrap/Modal';
import { getaActiveRoles, getRequester } from '../helpers/helpers';

const handleSubmit = (values, handleReloadData) => {
	RegionalModule.create(values)
		.then((res) => {
			showNotification('Success!', res.status, 'success');
			handleReloadData();
		})
		.catch((err) => {
			showNotification('Warning!', err, 'danger');
		});
	return values;
};
const handleSubmitEdit = (values, handleReloadData) => {
	RegionalModule.edit(values)
		.then((res) => {
			showNotification('Success!', res.status, 'success');
			handleReloadData();
		})
		.catch((err) => {
			showNotification('Warning!', err, 'danger');
		});
	return values;
};
const handleDelete = (val, handleReloadData) => {
	RegionalModule.delete_(val)
		.then((res) => {
			showNotification('Success!', res.status, 'success');
			handleReloadData();
		})
		.catch((err) => {
			showNotification('Warning!', err, 'danger');
		});
	return val;
};

const FormExample = (dt) => {
	const { initialValues, handleReloadData } = dt;
	const { default_segment_code } = getaActiveRoles();

	const submitForm = (values, resetForm) => {
		if (values.regional_name) {
			const { username } = getRequester();
			const val = {};
			val.requester = username;
			val.segment_code = default_segment_code;
			val.regional_name = values.regional_name;

			return Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'info',
				showCancelButton: true,
				confirmButtonText: 'Yes',
			}).then((result) => {
				if (result.value) {
					handleSubmit(val, handleReloadData);
					const refs = [];
					resetForm(initialValues);
					resetSelect(refs);
				} else if (result.dismiss === Swal.DismissReason.cancel) {
				}
			});
		}
		return values;
	};

	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		try {
			submitForm(values, resetForm);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	const validationSchema = yup.object({
		regional_name: yup.string().required('Field is required'),
	});
	return (
		<Formik
			enableReinitialize
			initialValues={{ ...initialValues }}
			onSubmit={onSubmit}
			validationSchema={validationSchema}>
			{(formikField) => {
				return (
					<Form>
						<FormGroup id='regional_name' label='Regional Name' className='col-md-12'>
							<Input
								onChange={formikField.handleChange}
								onBlur={formikField.handleBlur}
								value={formikField.values.regional_name}
								isValid={formikField.isValid}
								isTouched={formikField.touched.regional_name}
								invalidFeedback={formikField.errors.regional_name}
								autoComplete='off'
							/>
						</FormGroup>
						<br />

						<div className='col-md-12 '>
							<Button
								icon='Save'
								isOutline
								type='submit'
								color='success'
								className='float-end'
								isDisable={!formikField.isValid && !!formikField.submitCount}>
								Submit
							</Button>
						</div>
					</Form>
				);
			}}
		</Formik>
	);
};

const resetSelect = (refs) => {
	refs.forEach((ref) => {
		ref.setValue({});
	});
};

const FormExampleEdit = (dt) => {
	const { darkModeStatus } = useDarkMode();
	const { initialValues, listSegment, handleReloadData } = dt;

	const [isOpen, setIsOpen] = useState(false);
	const [isReadOnly, setIsReadOnly] = useState(false);
	const [isAlwaysReadOnly] = useState(true);

	const submitForm = (values, resetForm) => {
		const { id, regional_name } = values;
		if (regional_name && id) {
			const { username } = getRequester();
			const val = {};
			val._id = values.id;
			val.requester = username;
			val.regional_name = regional_name;

			return Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'info',
				showCancelButton: true,
				confirmButtonText: 'Yes',
			}).then((result) => {
				if (result.value) {
					handleSubmitEdit(val, handleReloadData);
					resetForm(initialValues);
				} else if (result.dismiss === Swal.DismissReason.cancel) {
				}
			});
		}
		return values;
	};

	const deleteForm = (id) => {
		if (id) {
			const { username } = getRequester();
			const val = {};
			val._id = id;
			val.requester = username;
			return Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'info',
				showCancelButton: true,
				confirmButtonText: 'Yes',
			}).then((result) => {
				if (result.value) {
					handleDelete(val, handleReloadData);
				} else if (result.dismiss === Swal.DismissReason.cancel) {
				}
			});
		}
		return id;
	};

	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		try {
			delete values.loading;
			submitForm(values, resetForm);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	const validate = (values) => {
		const errors = {};
		if (!values.regional_name) {
			errors.regional_name = 'Field is required';
		}

		return errors;
	};

	return (
		<>
			<Button
				className='me-2'
				icon='GridOn'
				type='button'
				color='primary'
				onClick={() => {
					setIsOpen(true);
					setIsReadOnly(true);
				}}
			/>

			<Button
				className='me-2'
				icon='Edit'
				type='button'
				color='success'
				onClick={() => {
					setIsOpen(true);
					setIsReadOnly(false);
				}}
			/>

			<Button
				className='me-2'
				icon='Delete'
				type='button'
				color='danger'
				onClick={() => deleteForm(initialValues.id)}
			/>

			<Modal isOpen={isOpen} setIsOpen={setIsOpen} size='lg' titleId='modal-edit-menu-crud'>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-edit-menu-crud'>
						{isReadOnly ? 'View Data' : 'Update Data'}
					</ModalTitle>
				</ModalHeader>
				<Formik
					enableReinitialize
					initialValues={{ ...initialValues }}
					onSubmit={onSubmit}
					validate={validate}>
					{(formikField) => {
						return (
							<Form>
								<ModalBody className='px-4'>
									<div className='col-md-12'>
										<FormGroup
											id='segment_code'
											label='Segment'
											className='mb-4'>
											<CustomSelect
												isDisabled={isAlwaysReadOnly}
												options={listSegment}
												defaultValue={formikField.values.segment_code}
												value={formikField.values.segment_code}
												darkTheme={darkModeStatus}
												onChange={(value) => {
													formikField.setFieldValue(
														'segment_code',
														value,
													);
												}}
												isValid={
													typeof formikField.errors.segment_code ===
													'undefined'
												}
											/>
										</FormGroup>
										{typeof formikField.errors.segment_code !== 'undefined' &&
											formikField.submitCount > 0 && (
												<div style={{ color: COLORS.DANGER.code }}>
													Field is required
												</div>
											)}

										<FormGroup
											id='regional_name'
											label='Regional Name'
											className='col-md-12'>
											<Input
												disabled={isReadOnly}
												onChange={formikField.handleChange}
												onBlur={formikField.handleBlur}
												value={formikField.values.regional_name}
												isValid={formikField.isValid}
												isTouched={formikField.touched.regional_name}
												invalidFeedback={formikField.errors.regional_name}
												autoComplete='off'
											/>
										</FormGroup>
									</div>
								</ModalBody>
								{!isReadOnly && (
									<ModalFooter className='px-4 pb-4'>
										<div className='col-md-12 '>
											<Button
												icon='Save'
												type='submit'
												color='success'
												className='float-end'
												isDisable={
													!formikField.isValid &&
													!!formikField.submitCount
												}>
												Update
											</Button>
										</div>
									</ModalFooter>
								)}
							</Form>
						);
					}}
				</Formik>
			</Modal>
		</>
	);
};

const CustomButton = (dt) => {
	const { row, listSegment, handleReloadData } = dt;

	// restructure the obj
	const initialValues = {};
	const segment = {};
	segment.label = `(${row.segment_code}) ${row.segment_name}`;
	segment.value = row.segment_code;

	initialValues.id = row._id;
	initialValues.segment_code = segment;
	initialValues.regional_name = row.regional_name;

	return (
		<FormExampleEdit
			initialValues={initialValues}
			listSegment={listSegment}
			handleReloadData={handleReloadData}
			row={row}
		/>
	);
};

const CustomDataTable = ({
	listSegment,
	data,
	loading,
	totalRows,
	handlePageChange,
	handlePerRowsChange,
	handleReloadData,
}) => {
	const { darkModeStatus } = useDarkMode();

	const columns = useMemo(
		() => [
			{
				name: 'Regional Code',
				selector: (row) => row.regional_code,
				sortable: true,
			},
			{
				name: 'Regional Name',
				selector: (row) => row.regional_name,
				sortable: true,
			},
			{
				name: 'Segment Code',
				selector: (row) => row.segment_code,
				sortable: true,
			},
			{
				name: 'Segment Name',
				selector: (row) => row.segment_name,
				sortable: true,
			},
			{
				name: 'Action',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (dt) => {
					return (
						<CustomButton
							row={dt}
							listSegment={listSegment}
							handleReloadData={handleReloadData}
						/>
					);
				},
			},
		],
		[listSegment, handleReloadData],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			progressPending={loading}
			pagination
			paginationServer
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};
const Regional = () => {
	const [data, setData] = useState([]);
	const [listSegment, setSegment] = useState([]);

	const [loading, setLoading] = useState(false);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [page, setPage] = useState(1);
	const [reloadData, setReloadData] = useState(false);
	const { default_segment_code } = getaActiveRoles();
	const { username } = getRequester();

	const fetchRegional = async () => {
		setLoading(true);
		return RegionalModule.read(
			`page=${page}&sizePerPage=${perPage}&segment_code=${default_segment_code}`,
		).then((res) => {
			setData(res.foundData);
			setTotalRows(res.countData);
			setLoading(false);
		});
	};

	const fetchSegmentUser = async () => {
		return SegmentModule.readSelectSegmentUser(username).then((res) => {
			setSegment(res);
		});
	};

	const handleReloadData = () => {
		setReloadData(!reloadData);
	};
	const initialValues = {
		loading: false,
		segment_code: {},
		regional_name: '',
	};

	useEffect(() => {
		fetchRegional();
		fetchSegmentUser();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [page, perPage, reloadData]);

	useEffect(() => {
		fetchRegional();
		fetchSegmentUser();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const { t } = useTranslation('regional');
	const [title] = useState({ title: 'Regional' });
	return (
		<PageWrapper title={title.title}>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card className='col-md-4'>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('Regional')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<div className='col-12 mb-3 g-4'>
							<FormExample
								initialValues={initialValues}
								listSegment={listSegment}
								handleReloadData={handleReloadData}
							/>
						</div>
					</CardBody>
				</Card>
				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('Historical Data')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<CustomDataTable
							data={data}
							loading={loading}
							totalRows={totalRows}
							listSegment={listSegment}
							handlePageChange={setPage}
							handlePerRowsChange={setPerPage}
							handleReloadData={handleReloadData}
						/>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

CustomDataTable.propTypes = {
	listSegment: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	data: PropTypes.arrayOf(PropTypes.instanceOf(Object)),
	loading: PropTypes.bool,
	totalRows: PropTypes.number,
	handlePageChange: PropTypes.func,
	handlePerRowsChange: PropTypes.func,
	handleReloadData: PropTypes.func,
};
CustomDataTable.defaultProps = {
	listSegment: {},
	data: [],
	loading: false,
	totalRows: 0,
	handlePageChange: null,
	handlePerRowsChange: null,
	handleReloadData: null,
};

export default Regional;
