import React, { useEffect, useMemo, useState } from 'react';
import { useFormik } from 'formik';
import Swal from 'sweetalert2';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import Card, {
	CardBody,
	CardFooter,
	CardFooterLeft,
	CardFooterRight,
	CardHeader,
	CardLabel,
	CardTitle,
} from '../components/bootstrap/Card';
import Page from '../layout/Page/Page';
import PageWrapper from '../layout/PageWrapper/PageWrapper';
import PageLayoutHeader from '../pages/common/Headers/PageLayoutHeader';
import Accordion, { AccordionItem } from '../components/bootstrap/Accordion';
import DarkDataTable from '../components/DarkDataTable';
import useDarkMode from '../hooks/useDarkMode';
import Button from '../components/bootstrap/Button';
import Modal, { ModalBody, ModalHeader, ModalTitle } from '../components/bootstrap/Modal';
import { getaActiveRoles, getRequester } from '../helpers/helpers';
import FormGroup from '../components/bootstrap/forms/FormGroup';
import Input from '../components/bootstrap/forms/Input';
import CustomSelect from '../components/custom/CustomSelect';
import RolesModule from '../modules/RolesModule';
import showNotification from '../components/extras/showNotification';
import StoreSegmentModule from '../modules/StoreSegmentModule';
import Checks from '../components/bootstrap/forms/Checks';

const handleSubmit = (values) => {
	const newResponse = new Promise((resolve, reject) => {
		try {
			resolve(
				RolesModule.create(values)
					.then(() => {
						showNotification(
							'Information!',
							'Data has been saved successfully',
							'success',
						);
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					}),
			);
		} catch (e) {
			reject(new Error({ error: true }));
		}
	});
	return newResponse;
};

const handleUpdate = (values) => {
	const newResponse = new Promise((resolve, reject) => {
		try {
			resolve(
				RolesModule.update(values)
					.then(() => {
						showNotification(
							'Information!',
							'Data has been update successfully',
							'success',
						);
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					}),
			);
		} catch (e) {
			reject(new Error({ error: true }));
		}
	});
	return newResponse;
};

const handleDelete = (values) => {
	const { _id } = values;
	const { username } = getRequester();
	const newValues = {
		_id,
		requester: username,
	};

	const newResponse = new Promise((resolve, reject) => {
		try {
			if (values._id) {
				Swal.fire({
					title: 'Are you sure?',
					text: 'Please check your entries !',
					icon: 'info',
					showCancelButton: true,
					confirmButtonText: 'Yes',
				}).then((result) => {
					if (result.isConfirmed) {
						resolve(
							RolesModule.destroy(newValues)
								.then(() => {
									showNotification(
										'Information!',
										'Data has been deleted successfully',
										'success',
									);
								})
								.catch((err) => {
									showNotification('Warning!', err, 'danger');
								}),
						);
					} else if (result.dismiss === Swal.DismissReason.cancel) {
						Swal.fire('Cancelled', 'Your data is safe :)', 'error');
					}
				});
			}
		} catch (e) {
			reject(new Error({ error: true }));
		}
	});
	return newResponse;
};

const FormCustom = ({
	initialValues,
	isReadOnly,
	isUpdate,
	handleCustomSubmit,
	listRole,
	listStoreSegment,
	selectParentRoleValue,
	selectStoreSegmentValue,
	loadingRole,
	loadingStore,
}) => {
	const { t } = useTranslation('crud');
	const { darkModeStatus } = useDarkMode();

	const { username } = getRequester();

	const [selectParentRole, setSelectParentRole] = useState(selectParentRoleValue);
	const [selectStoreSegment, setSelectStoreSegment] = useState(selectStoreSegmentValue);

	const { default_segment_code, default_segment_name } = getaActiveRoles();

	// validate form field
	const validate = (values) => {
		const errors = {};

		if (!values.role_name) {
			errors.role_name = 'Required';
		}
		if (!values.parent_role_code && !selectParentRole) {
			errors.parent_role_code = 'Required';
		}
		if (values.store_segment.length === 0 && !values.all_store_segment) {
			errors.store_segment = 'Required';
		}

		return errors;
	};

	// handle all form field
	const formik = useFormik({
		initialValues: { ...initialValues },
		validate,
		onSubmit: (values, { setSubmitting, resetForm, setErrors }) => {
			try {
				Swal.fire({
					title: 'Are you sure?',
					text: 'Please check your entries !',
					icon: 'info',
					showCancelButton: true,
					confirmButtonText: 'Yes',
				}).then((result) => {
					if (result.isConfirmed) {
						const newValues = {};
						if (isUpdate) {
							newValues._id = values._id;
							newValues.role_code = values.role_code;
						}
						newValues.role_name = values.role_name;
						newValues.segment_code = default_segment_code;
						newValues.segment_name = default_segment_name;
						if (selectParentRole?.detail?.role_code) {
							newValues.parent_role_code = values.parent_role_code;
							newValues.parent_role_name = values.parent_role_name;
						}
						newValues.store_segment = values.all_store_segment
							? []
							: values.store_segment;

						newValues.requester = username;

						handleCustomSubmit(newValues);

						// reset field
						resetForm(initialValues);
						setSelectParentRole(null);
						setSelectStoreSegment(null);
					} else if (result.dismiss === Swal.DismissReason.cancel) {
						Swal.fire('Cancelled', 'Your data is safe :)', 'error');
					}
				});
			} catch (error) {
				setErrors({ submit: error.message });
				Swal.fire('Information ', 'Please check your entries again!', 'error');
			} finally {
				setSubmitting(false);
			}
		},
	});

	// on change parent role
	const onChangeParentRole = (select) => {
		formik.setFieldValue('parent_role_code', select?.detail?.role_code);
		formik.setFieldValue('parent_role_name', select?.detail?.role_name);
		setSelectParentRole(select);

		delete formik.errors.parent_role_code;
	};

	// on change store segment
	const onChangeStoreSegment = (select) => {
		const result = select.map((item) => item?.detail);

		formik.setFieldValue('store_segment', result);
		setSelectStoreSegment(select);

		delete formik.errors.store_segment_code;
	};

	return (
		<Accordion id='accordion-form' activeItemId={isUpdate ? 'form-default' : false}>
			<AccordionItem id='form-default' title={t('form')} icon='AddBox'>
				<div className={isReadOnly || isUpdate ? 'col-md-12' : 'col-md-6'}>
					<Card shadow='none' tag='form' noValidate onSubmit={formik.handleSubmit}>
						<CardBody>
							<FormGroup id='role_name' label='Role Name' className='mb-3'>
								<Input
									autoComplete='off'
									onChange={formik.handleChange}
									onBlur={formik.handleBlur}
									value={formik.values.role_name}
									isValid={formik.isValid}
									isTouched={formik.touched.role_name}
									invalidFeedback={formik.errors.role_name}
									disabled={isReadOnly}
								/>
							</FormGroup>
							<FormGroup id='parent_role_code' label='Parent Role' className='mb-3'>
								<CustomSelect
									value={selectParentRole}
									options={listRole}
									onChange={onChangeParentRole}
									onBlur={formik.handleBlur}
									darkTheme={darkModeStatus}
									isSearchable={listRole.length > 7}
									invalidFeedback={formik.errors.parent_role_code}
									isValid={
										typeof formik.errors.parent_role_code === 'undefined' ||
										selectParentRole !== null
									}
									isDisabled={isReadOnly}
									isLoading={loadingRole}
								/>
							</FormGroup>
							<FormGroup className='mb-3'>
								<Checks
									type='switch'
									id='all_store_segment'
									label='All Segment'
									onChange={formik.handleChange}
									checked={formik.values.all_store_segment}
									disabled={isReadOnly}
								/>
							</FormGroup>
							{!formik.values.all_store_segment && (
								<FormGroup
									id='store_segment_code'
									label='Store Segment'
									className='mb-3'>
									<CustomSelect
										value={selectStoreSegment}
										options={listStoreSegment}
										onChange={onChangeStoreSegment}
										darkTheme={darkModeStatus}
										isSearchable={listStoreSegment.length > 7}
										invalidFeedback={formik.errors.store_segment}
										isValid={typeof formik.errors.store_segment === 'undefined'}
										isDisabled={isReadOnly}
										isLoading={loadingStore}
										isMulti
										closeMenuOnSelect={false}
									/>
								</FormGroup>
							)}
						</CardBody>
						{!isReadOnly && (
							<CardFooter>
								{!isUpdate && (
									<CardFooterLeft>
										<Button
											icon='Clear'
											color='danger'
											type='reset'
											onClick={(e) => {
												formik.handleReset(e);
												setSelectParentRole(null);
												setSelectStoreSegment(null);
											}}
											isLight={darkModeStatus}>
											Reset
										</Button>
									</CardFooterLeft>
								)}
								<CardFooterRight>
									<Button
										icon='Save'
										color='success'
										type='submit'
										isLight={darkModeStatus}>
										Submit
									</Button>
								</CardFooterRight>
							</CardFooter>
						)}
					</Card>
				</div>
			</AccordionItem>
		</Accordion>
	);
};

const FormCustomModal = ({
	initialValues,
	listRole,
	listStoreSegment,
	handleCustomUpdate,
	handleCustomDelete,
}) => {
	const { darkModeStatus } = useDarkMode();
	const [isOpen, setIsOpen] = useState(false);
	const [isReadOnly, setIsReadOnly] = useState(false);

	const [title, setTitle] = useState('');

	const getFormatSelectParentRole = (data) => {
		let result = {};
		if (data.parent_role_code) {
			result = {
				value: data.parent_role_code,
				label: `(${data.parent_role_code}) ${data.parent_role_name}`,
				detail: {
					role_code: data.parent_role_code,
					role_name: data.parent_role_name,
				},
			};
		} else {
			result = { value: 'null', label: 'null' };
		}
		return result;
	};

	const getFormatSelectStoreSegment = (data) => {
		const result = data.store_segment.map((item) => {
			return {
				value: item.store_segment_code,
				label: `(${item.store_segment_code}) ${item.store_segment_name}`,
				detail: {
					role_code: item.store_segment_code,
					role_name: item.store_segment_name,
				},
			};
		});
		return result;
	};

	return (
		<>
			<Button
				icon='GridOn'
				color='info'
				type='button'
				className='me-3'
				isLight={darkModeStatus}
				onClick={() => {
					setTitle('Detail');
					setIsReadOnly(true);
					setIsOpen(true);
				}}
			/>
			<Button
				icon='Edit'
				color='success'
				type='button'
				className='me-3'
				isLight={darkModeStatus}
				onClick={() => {
					setTitle('Update');
					setIsReadOnly(false);
					setIsOpen(true);
				}}
			/>
			<Button
				icon='Delete'
				color='danger'
				type='button'
				className='me-3'
				isLight={darkModeStatus}
				onClick={() => handleCustomDelete(initialValues)}
			/>

			<Modal isOpen={isOpen} setIsOpen={setIsOpen} size='lg' titleId='modal-crud'>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-crud'>{title} Data</ModalTitle>
				</ModalHeader>
				<ModalBody>
					<FormCustom
						initialValues={initialValues}
						isReadOnly={isReadOnly}
						isUpdate={isOpen}
						listRole={listRole}
						listStoreSegment={listStoreSegment}
						selectParentRoleValue={getFormatSelectParentRole(initialValues)}
						selectStoreSegmentValue={getFormatSelectStoreSegment(initialValues)}
						handleCustomSubmit={handleCustomUpdate}
					/>
				</ModalBody>
			</Modal>
		</>
	);
};

const TableCustom = ({
	data,
	totalRows,
	perPage,
	fetchData,
	listRole,
	listStoreSegment,
	handleCustomDelete,
	handleCustomUpdate,
}) => {
	const { darkModeStatus } = useDarkMode();

	const customHandleUpdate = (dataRow) => {
		handleCustomUpdate(dataRow);
	};

	const customHandleDelete = (dataRow) => {
		handleCustomDelete(dataRow);
	};

	const handlePageChange = (page) => {
		fetchData(page, perPage);
	};

	const handlePerRowsChange = async (newPerPage, page) => {
		return fetchData(page, newPerPage);
	};

	const columns = useMemo(
		() => [
			{
				name: 'Code',
				selector: (row) => row.role_code,
				sortable: true,
			},
			{
				name: 'Role',
				selector: (row) => row.role_name,
				sortable: true,
			},
			{
				name: 'Parent Role Code',
				selector: (row) => row.parent_role_code,
				sortable: true,
			},
			{
				name: 'Parent Role Name',
				selector: (row) => row.parent_role_name,
				sortable: true,
			},
			{
				name: 'Created',
				selector: (row) => moment(row.created_at).format('YYYY-MM-DD'),
				sortable: true,
			},
			{
				name: 'Action',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (row) => {
					const initialValues = {};
					initialValues._id = row._id;
					initialValues.role_code = row.role_code;
					initialValues.role_name = row.role_name;
					initialValues.segment_code = row.segment_code;
					initialValues.segment_name = row.segment_name;
					initialValues.store_segment = row.store_segment;
					initialValues.all_store_segment = row.store_segment.length === 0;
					if (row.parent_role_code) {
						initialValues.parent_role_code = row.parent_role_code;
						initialValues.parent_role_name = row.parent_role_name;
					}

					return (
						<FormCustomModal
							initialValues={initialValues}
							listRole={listRole}
							listStoreSegment={listStoreSegment}
							handleCustomDelete={customHandleDelete}
							handleCustomUpdate={customHandleUpdate}
						/>
					);
				},
			},
		],
		// eslint-disable-next-line react-hooks/exhaustive-deps
		[listRole, listStoreSegment],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			pagination
			paginationServer
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};

const Roles = () => {
	const { t } = useTranslation('crud');
	const [title] = useState({ title: t('Roles') });

	const [data, setData] = useState([]);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [curPage, setCurPage] = useState(1);

	const [listRole, setListRole] = useState([]);
	const [listStoreSegment, setListStoreSegment] = useState([]);

	const [loadingRole, setLoadingRole] = useState(false);
	const [loadingStore, setLoadingStore] = useState(false);

	const { default_segment_code } = getaActiveRoles();

	const initialValues = {
		role_name: '',
		segment_code: '',
		segment_name: '',
		parent_role_code: '',
		parent_role_name: '',
		store_segment: [],
		all_store_segment: false,
	};

	const createSubmit = (values) => {
		handleSubmit(values)
			.then(() => {
				fetchData(curPage, perPage);
				fetchDataRole();
				fetchDataStoreSegment();
			})
			.catch(() => {});
	};

	const updateSubmit = (values) => {
		handleUpdate(values)
			.then(() => {
				fetchData(curPage, perPage);
				fetchDataRole();
				fetchDataStoreSegment();
			})
			.catch(() => {});
	};

	const deleteSubmit = (values) => {
		handleDelete(values)
			.then(() => {
				fetchData(curPage, perPage);
				fetchDataRole();
				fetchDataStoreSegment();
			})
			.catch(() => {});
	};

	// handle first time render
	useEffect(() => {
		fetchData(1, 10);
		fetchDataRole();
		fetchDataStoreSegment();

		return <div> {/*  */} </div>;
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	// reload data table
	const fetchData = async (newPage, newPerPage) => {
		const query = `page=${newPage}&sizePerPage=${newPerPage}&segment_code=${default_segment_code}`;
		return RolesModule.read(query)
			.then((response) => {
				setData(response.foundData);
				setTotalRows(response.countData);
				setCurPage(response.currentPage);
				setPerPage(newPerPage);
			})
			.catch((err) => {
				showNotification('Information', err, 'danger');
			});
	};

	// reload data role
	const fetchDataRole = async () => {
		setLoadingRole(true);
		const query = ``;
		return RolesModule.readSelect(query)
			.then((response) => {
				const new_role = [...response];
				new_role.unshift({ value: 'null', label: 'null' });
				setListRole(new_role);
			})
			.catch((err) => {
				showNotification('Information', err, 'danger');
			})
			.finally(() => {
				setLoadingRole(false);
			});
	};

	// reload data store segment
	const fetchDataStoreSegment = async () => {
		setLoadingStore(true);
		const query = ``;
		return StoreSegmentModule.readSelect(query)
			.then((response) => {
				setListStoreSegment(response);
			})
			.catch((err) => {
				showNotification('Information', err, 'danger');
			})
			.finally(() => {
				setLoadingStore(false);
			});
	};

	return (
		<PageWrapper title={title.title}>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card>
					<CardBody>
						<FormCustom
							initialValues={initialValues}
							handleCustomSubmit={createSubmit}
							listRole={listRole}
							listStoreSegment={listStoreSegment}
							loadingRole={loadingRole}
							loadingStore={loadingStore}
						/>
					</CardBody>
				</Card>
				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel icon='History' iconColor='info'>
							<CardTitle>{t('historical_data')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<TableCustom
							data={data}
							totalRows={totalRows}
							perPage={perPage}
							curPage={curPage}
							fetchData={fetchData}
							listRole={listRole}
							listStoreSegment={listStoreSegment}
							handleCustomUpdate={updateSubmit}
							handleCustomDelete={deleteSubmit}
						/>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

FormCustom.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
	listRole: PropTypes.instanceOf(Array),
	listStoreSegment: PropTypes.instanceOf(Array),
	selectParentRoleValue: PropTypes.instanceOf(Object),
	selectStoreSegmentValue: PropTypes.instanceOf(Array),
	isReadOnly: PropTypes.bool,
	isUpdate: PropTypes.bool,
	handleCustomSubmit: PropTypes.func,
	loadingRole: PropTypes.bool,
	loadingStore: PropTypes.bool,
};
FormCustom.defaultProps = {
	initialValues: null,
	listRole: [],
	listStoreSegment: [],
	selectParentRoleValue: null,
	selectStoreSegmentValue: [],
	isReadOnly: false,
	isUpdate: false,
	handleCustomSubmit: null,
	loadingRole: false,
	loadingStore: false,
};

FormCustomModal.propTypes = {
	handleCustomUpdate: PropTypes.func,
	handleCustomDelete: PropTypes.func,
	initialValues: PropTypes.instanceOf(Object),
	listRole: PropTypes.instanceOf(Array),
	listStoreSegment: PropTypes.instanceOf(Array),
};
FormCustomModal.defaultProps = {
	handleCustomUpdate: null,
	handleCustomDelete: null,
	initialValues: null,
	listRole: [],
	listStoreSegment: [],
};

TableCustom.propTypes = {
	data: PropTypes.instanceOf(Array),
	listRole: PropTypes.instanceOf(Array),
	listStoreSegment: PropTypes.instanceOf(Array),
	totalRows: PropTypes.number,
	perPage: PropTypes.number,
	fetchData: PropTypes.func,
	handleCustomUpdate: PropTypes.func,
	handleCustomDelete: PropTypes.func,
};
TableCustom.defaultProps = {
	data: [],
	listRole: [],
	listStoreSegment: [],
	totalRows: 0,
	perPage: 10,
	fetchData: null,
	handleCustomUpdate: null,
	handleCustomDelete: null,
};

Roles.propTypes = {};
Roles.defaultProps = {};

export default Roles;
