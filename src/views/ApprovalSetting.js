import React, { useMemo, useState, useEffect, useRef } from 'react';
import { useFormik } from 'formik';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import Swal from 'sweetalert2';
import Card, {
	CardActions,
	CardBody,
	CardHeader,
	CardLabel,
	CardTitle,
} from '../components/bootstrap/Card';
import InputGroup, { InputGroupText } from '../components/bootstrap/forms/InputGroup';
import DarkDataTable from '../components/DarkDataTable';
import Page from '../layout/Page/Page';
import PageWrapper from '../layout/PageWrapper/PageWrapper';
import PageLayoutHeader from '../pages/common/Headers/PageLayoutHeader';
import useDarkMode from '../hooks/useDarkMode';
import { getaActiveRoles, getRequester } from '../helpers/helpers';
import ApprovalSettingModule from '../modules/ApprovalSettingModule';
import Button from '../components/bootstrap/Button';
import Modal, { ModalBody, ModalHeader, ModalTitle } from '../components/bootstrap/Modal';
import Accordion, { AccordionItem } from '../components/bootstrap/Accordion';
import showNotification from '../components/extras/showNotification';
import FormGroup from '../components/bootstrap/forms/FormGroup';
import CustomSelect from '../components/custom/CustomSelect';
import Checks, { ChecksGroup } from '../components/bootstrap/forms/Checks';
import Input from '../components/bootstrap/forms/Input';
import RolesModule from '../modules/RolesModule';
import UserModule from '../modules/UserModule';
import Spinner from '../components/bootstrap/Spinner';

const APPROVAL_CATEGORY = {
	UPPER_PARENT: {
		id: 'upper_parant',
		value: 'Upper Parent',
		label: 'Upper Parent',
	},
	MANUAL_SETTING: {
		id: 'manual_setting',
		value: 'Manual Setting',
		label: 'Manual Setting',
	},
	name: 'approval_category',
};

const handleSubmit = (values) => {
	const newResponse = new Promise((resolve, reject) => {
		try {
			resolve(
				ApprovalSettingModule.create(values)
					.then(() => {
						showNotification(
							'Information!',
							'Data has been saved successfully',
							'success',
						);
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					}),
			);
		} catch (e) {
			reject(new Error({ error: true }));
		}
	});
	return newResponse;
};

const handleUpdate = (values) => {
	const newResponse = new Promise((resolve, reject) => {
		try {
			resolve(
				ApprovalSettingModule.update(values)
					.then(() => {
						showNotification(
							'Information!',
							'Data has been update successfully',
							'success',
						);
					})
					.catch((err) => {
						showNotification('Warning!', err, 'danger');
					}),
			);
		} catch (e) {
			reject(new Error({ error: true }));
		}
	});
	return newResponse;
};

const handleDelete = (values) => {
	const { _id } = values;
	const { username } = getRequester();
	const newValues = {
		_id,
		requester: username,
	};

	const newResponse = new Promise((resolve, reject) => {
		try {
			if (values._id) {
				Swal.fire({
					title: 'Are you sure?',
					text: 'Please check your entries !',
					icon: 'info',
					showCancelButton: true,
					confirmButtonText: 'Yes',
				}).then((result) => {
					if (result.isConfirmed) {
						resolve(
							ApprovalSettingModule.destroy(newValues)
								.then(() => {
									showNotification(
										'Information!',
										'Data has been deleted successfully',
										'success',
									);
								})
								.catch((err) => {
									showNotification('Warning!', err, 'danger');
								}),
						);
					} else if (result.dismiss === Swal.DismissReason.cancel) {
						Swal.fire('Cancelled', 'Your data is safe :)', 'error');
					}
				});
			}
		} catch (e) {
			reject(new Error({ error: true }));
		}
	});
	return newResponse;
};

const FormCustom = ({
	initialValues,
	list_role,
	isReadOnly,
	isUpdate,
	handleCustomSubmit,
	value_select_user,
	value_list_user,
	value_data_user,
	value_count_user,
}) => {
	const { darkModeStatus } = useDarkMode();
	const { t } = useTranslation('crud');
	const { default_segment_code } = getaActiveRoles();
	const { username } = getRequester();

	const [select_role, setSelectRole] = useState(value_select_user);

	const [list_user, setListUser] = useState(value_list_user);

	const [loading, setLoading] = useState(false);

	const [data_user, setDataUser] = useState(value_data_user);
	const count_user = useRef(value_count_user);

	const handleCustomAdd = () => {
		if (!select_role) {
			showNotification('Information', 'choose a role before adding a user', 'danger');
			return;
		}
		if (list_user.length === 0) {
			showNotification('Information', "users not found. can't add data", 'danger');
			return;
		}
		if (data_user.length === list_user.length) {
			showNotification('Information', 'user additions are at their limit', 'danger');
			return;
		}

		const new_user = [...data_user];
		new_user.push({
			key: count_user.current,
			data: null,
		});
		setDataUser(new_user);

		count_user.current += 1;
	};

	const handleCustomRemove = (index) => {
		const new_user = [...data_user];
		new_user.splice(index, 1);
		setDataUser(new_user);
	};

	const onChangeRole = (e) => {
		setSelectRole(e);

		if (e.value === select_role?.value) {
			return;
		}

		formik.setFieldValue('role_code', e?.detail?.role_code);
		formik.setFieldValue('role_name', e?.detail?.role_name);
		formik.setFieldValue('segment_code', e?.detail?.segment_code);
		formik.setFieldValue('segment_name', e?.detail?.segment_name);
		formik.setFieldValue('min_approval', 0);

		// load data role
		fetchDataUser(e.value);
	};

	const onChangeUser = (e, index) => {
		const new_user = [...data_user];

		// find index user
		const find_index = new_user.findIndex((find) => find.data?.value === e.value);

		// if it is the same as the previously selected data, the process will not continue
		if (find_index === index) {
			return;
		}

		// if the selected data already exists, then the process will not continue
		if (find_index !== -1) {
			showNotification(
				'Information',
				`${e.label}, the user has been selected, try choosing another one`,
				'danger',
			);
			return;
		}

		// set new data user
		const new_index_user = {
			...new_user.at(index),
			data: e,
		};
		new_user[index] = new_index_user;
		setDataUser(new_user);
	};

	const validate = (values) => {
		const errors = {};

		if (!values.role_code && !select_role) {
			errors.role_code = 'Required';
		}
		if (select_role && values.approval_category === APPROVAL_CATEGORY.MANUAL_SETTING.value) {
			if (list_user.length !== 0) {
				if (values.min_approval > list_user.length || values.min_approval <= 0) {
					errors.min_approval = `Available values are 1 to ${list_user.length}`;
				}
			}
		}

		return errors;
	};

	const formik = useFormik({
		initialValues: { ...initialValues },
		validate,
		onSubmit: (values, { setSubmitting, resetForm, setErrors }) => {
			try {
				if (
					data_user.length === 0 &&
					values.approval_category === APPROVAL_CATEGORY.MANUAL_SETTING.value
				) {
					showNotification('Information', 'user not define', 'danger');
					return;
				}

				if (
					values.min_approval > data_user.length &&
					values.approval_category === APPROVAL_CATEGORY.MANUAL_SETTING.value
				) {
					showNotification(
						'Information',
						'minimum approval does not match the number of users',
						'danger',
					);
					return;
				}

				let error = false;

				data_user.forEach((item) => {
					if (error) {
						return;
					}
					if (!item.data) {
						error = true;
					}
				});

				if (error) {
					showNotification(
						'Information',
						'Please complete user selection before saving data',
						'danger',
					);
					return;
				}

				Swal.fire({
					title: 'Are you sure?',
					text: 'Please check your entries !',
					icon: 'info',
					showCancelButton: true,
					confirmButtonText: 'Yes',
				}).then((result) => {
					if (result.isConfirmed) {
						const new_values = {};
						new_values.requester = username;
						new_values.approval_category = values.approval_category;
						new_values.min_approval = values.min_approval;

						if (values.approval_category === APPROVAL_CATEGORY.MANUAL_SETTING.value) {
							new_values.user = getFormatValue(data_user);
						}

						if (isUpdate) {
							new_values._id = values._id;
						} else {
							new_values.segment_code = values.segment_code;
							new_values.segment_name = values.segment_name;
							new_values.role_code = values.role_code;
							new_values.role_name = values.role_name;
						}

						// process
						if (handleCustomSubmit) {
							handleCustomSubmit(new_values);
						}

						// reset field
						resetForm(initialValues);

						// reset role selection
						setSelectRole(null);
					} else if (result.dismiss === Swal.DismissReason.cancel) {
						Swal.fire('Cancelled', 'Your data is safe :)', 'error');
					}
				});
			} catch (error) {
				setErrors({ submit: error.message });
				Swal.fire('Information ', 'Please check your entries again!', 'error');
			} finally {
				setSubmitting(false);
			}
		},
	});

	const getFormatValue = (params) => {
		return params.map((item) => {
			return { ...item.data?.detail_user };
		});
	};

	const fetchDataUser = async (role) => {
		setLoading(true);
		const query = `segment_code=${default_segment_code}&role_code=${role}`;
		return UserModule.readSelect(query)
			.then((response) => {
				setListUser(response);
				setDataUser([]);
			})
			.catch(() => {})
			.finally(() => {
				setLoading(false);
			});
	};

	return (
		<Accordion id='accordion-form' activeItemId={isUpdate ? 'form-default' : false}>
			<AccordionItem id='form-default' title={t('form')} icon='AddBox'>
				<Card shadow='none' tag='form' noValidate onSubmit={formik.handleSubmit}>
					<CardBody>
						<div className='row'>
							<div className='col-md-5'>
								<Card shadow='sm' style={{ minHeight: '20rem' }}>
									<CardBody>
										<FormGroup id='role_code' label='Role' className='mb-3'>
											<CustomSelect
												value={select_role}
												options={list_role}
												isSearchable={list_role > 7}
												onChange={onChangeRole}
												isValid={
													typeof formik.errors.role_code ===
														'undefined' || select_role !== null
												}
												invalidFeedback={formik.errors.role_code}
												isDisable={isUpdate}
												loading={loading}
											/>
										</FormGroup>
										<FormGroup
											id='approval_category'
											label='Approval Category'
											className='mb-3'>
											<ChecksGroup isInline>
												<Checks
													type='radio'
													id={APPROVAL_CATEGORY.UPPER_PARENT.id}
													label={APPROVAL_CATEGORY.UPPER_PARENT.label}
													name={APPROVAL_CATEGORY.name}
													value={APPROVAL_CATEGORY.UPPER_PARENT.value}
													onChange={formik.handleChange}
													checked={formik.values.approval_category}
													disabled={isReadOnly}
												/>
												<Checks
													type='radio'
													id={APPROVAL_CATEGORY.MANUAL_SETTING.id}
													label={APPROVAL_CATEGORY.MANUAL_SETTING.label}
													name={APPROVAL_CATEGORY.name}
													value={APPROVAL_CATEGORY.MANUAL_SETTING.value}
													onChange={formik.handleChange}
													checked={formik.values.approval_category}
													disabled={isReadOnly}
												/>
											</ChecksGroup>
										</FormGroup>
										<FormGroup
											id='min_approval'
											label='Min Approval'
											className='mb-3'>
											<Input
												type='number'
												onChange={formik.handleChange}
												onBlur={formik.handleBlur}
												value={formik.values.min_approval}
												isValid={formik.isValid}
												isTouched={formik.touched.min_approval}
												invalidFeedback={formik.errors.min_approval}
												disabled={isReadOnly || select_role === null}
												min={0}
												max={
													formik.values.approval_category ===
													APPROVAL_CATEGORY.MANUAL_SETTING.value
														? list_user.length
														: 100
												}
											/>
										</FormGroup>
									</CardBody>
								</Card>
							</div>
							{formik.values.approval_category ===
								APPROVAL_CATEGORY.MANUAL_SETTING.value && (
								<div className='col-md-7'>
									<Card shadow='sm' style={{ minHeight: '20rem' }}>
										{!isReadOnly && (
											<CardHeader size='sm'>
												<CardActions>
													{loading ? (
														<Button
															color='info'
															isLight={darkModeStatus}>
															<Spinner isSmall inButton />
															Loading...
														</Button>
													) : (
														<Button
															icon='Add'
															color='info'
															type='button'
															isLight={darkModeStatus}
															onClick={handleCustomAdd}>
															Add User
														</Button>
													)}
												</CardActions>
											</CardHeader>
										)}
										<CardBody isScrollable>
											{data_user.map((item, index) => (
												<div key={'div-'.concat(item.key)} className='mb-1'>
													<InputGroup>
														<InputGroupText>
															User {index + 1}
														</InputGroupText>
														<FormGroup
															key={'user-'.concat(item.key)}
															id={'user-'.concat(item.key)}
															className='col-md-9'>
															<CustomSelect
																key={'select-'.concat(item.key)}
																options={list_user}
																value={item.data}
																onChange={(e) =>
																	onChangeUser(e, index)
																}
																isSearchable={list_user.length > 7}
																isDisable={isReadOnly}
															/>
														</FormGroup>
														<Button
															icon='Clear'
															color='danger'
															type='button'
															isLight={darkModeStatus}
															style={{ minHeight: '2.95rem' }}
															onClick={() =>
																handleCustomRemove(index)
															}
															isDisable={isReadOnly}
														/>
													</InputGroup>
												</div>
											))}
										</CardBody>
									</Card>
								</div>
							)}
						</div>
					</CardBody>
					{!isReadOnly && (
						<div className='row'>
							<div className='col-md-12'>
								<Button
									icon='Save'
									color='success'
									type='submit'
									className='float-end me-3 mb-3'
									isLight={darkModeStatus}>
									Submit
								</Button>
							</div>
						</div>
					)}
				</Card>
			</AccordionItem>
		</Accordion>
	);
};

FormCustom.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
	list_role: PropTypes.instanceOf(Array),
	isReadOnly: PropTypes.bool,
	isUpdate: PropTypes.bool,
	handleCustomSubmit: PropTypes.func,
	value_select_user: PropTypes.instanceOf(Object),
	value_list_user: PropTypes.instanceOf(Array),
	value_data_user: PropTypes.instanceOf(Array),
	value_count_user: PropTypes.number,
};
FormCustom.defaultProps = {
	initialValues: null,
	list_role: [],
	isReadOnly: false,
	isUpdate: false,
	handleCustomSubmit: null,
	value_select_user: null,
	value_list_user: [],
	value_data_user: [],
	value_count_user: 0,
};

const FormCustomModal = ({ initialValues, handleCustomUpdate, handleCustomDelete, list_role }) => {
	const { darkModeStatus } = useDarkMode();
	const { default_segment_code } = getaActiveRoles();

	const [isOpen, setOpen] = useState(false);
	const [isReadOnly, setReadOnly] = useState(false);

	const [title, setTitle] = useState('');

	const [select_role, setSelectRole] = useState(null);
	const [list_user, setListUser] = useState([]);
	const [data_user, setDataUser] = useState([]);
	const [count_user, setCountUser] = useState(0);

	const getFormatSelect = (params) => {
		const newResponse = new Promise((resolve, reject) => {
			try {
				const _data_user = [];
				let _list_user = [];
				let _select_role = null;

				// return is params null value
				if (!params || !params.role_code) {
					resolve({
						value_select_role: _select_role,
						value_list_user: _list_user,
						value_data_user: _data_user,
						value_count_user: _data_user.length,
					});
				} else {
					if (params.role_code) {
						_select_role = {
							value: params.role_code,
							label: `(${params.role_code}) ${params.role_name}`,
							detail: {
								role_code: params.role_code,
								role_name: params.role_name,
								segment_code: params.segment_code,
								segment_name: params.segment_name,
							},
						};
					}

					params.user.forEach((item, index) => {
						const new_user = {
							key: index,
							data: {
								value: item.username,
								label: `(${item.username}) ${item.name}`,
								detail: { ...item },
							},
						};

						_data_user.push(new_user);
					});

					const query = `segment_code=${default_segment_code}&role_code=${params.role_code}`;
					UserModule.readSelect(query).then((response) => {
						_list_user = response;

						resolve({
							value_select_role: _select_role,
							value_list_user: _list_user,
							value_data_user: _data_user,
							value_count_user: _data_user.length,
						});
					});
				}
			} catch (e) {
				reject(new Error({ error: true }));
			}
		});
		return newResponse;
	};

	return (
		<>
			<Button
				icon='GridOn'
				color='info'
				type='button'
				className='me-3'
				isLight={darkModeStatus}
				onClick={() => {
					getFormatSelect(initialValues).then((response) => {
						setSelectRole(response.value_select_role);
						setDataUser(response.value_data_user);
						setListUser(response.value_list_user);
						setCountUser(response.value_count_user);
					});
					setTitle('Detail');
					setReadOnly(true);

					// loading
					setTimeout(() => {
						setOpen(true);
					}, 1000);
				}}
			/>
			<Button
				icon='Edit'
				color='success'
				type='button'
				className='me-3'
				isLight={darkModeStatus}
				onClick={() => {
					getFormatSelect(initialValues).then((response) => {
						setSelectRole(response.value_select_role);
						setDataUser(response.value_data_user);
						setListUser(response.value_list_user);
						setCountUser(response.value_count_user);
					});
					setTitle('Update');
					setReadOnly(false);

					// loading
					setTimeout(() => {
						setOpen(true);
					}, 1000);
				}}
			/>
			<Button
				icon='Delete'
				color='danger'
				type='button'
				className='me-3'
				isLight={darkModeStatus}
				onClick={() => handleCustomDelete(initialValues)}
			/>

			<Modal isOpen={isOpen} setIsOpen={setOpen} size='xl' titleId='modal-crud'>
				<ModalHeader setIsOpen={setOpen} className='p-4'>
					<ModalTitle id='modal-crud'>{title} Data</ModalTitle>
				</ModalHeader>
				<ModalBody>
					<FormCustom
						initialValues={initialValues}
						handleCustomSubmit={handleCustomUpdate}
						isReadOnly={isReadOnly}
						isUpdate={isOpen}
						list_role={list_role}
						value_count_user={count_user}
						value_data_user={data_user}
						value_list_user={list_user}
						value_select_user={select_role}
					/>
				</ModalBody>
			</Modal>
		</>
	);
};

FormCustomModal.propTypes = {
	initialValues: PropTypes.instanceOf(Object),
	handleCustomUpdate: PropTypes.func,
	handleCustomDelete: PropTypes.func,
	list_role: PropTypes.instanceOf(Array),
};
FormCustomModal.defaultProps = {
	initialValues: null,
	handleCustomUpdate: null,
	handleCustomDelete: null,
	list_role: [],
};

const TableCustom = ({
	data,
	totalRows,
	perPage,
	fetchData,
	handleCustomUpdate,
	handleCustomDelete,
	list_role,
}) => {
	const { darkModeStatus } = useDarkMode();
	const { username } = getRequester();

	const customHandleUpdate = (dataRow) => {
		handleCustomUpdate(dataRow);
	};

	const customHandleDelete = (dataRow) => {
		handleCustomDelete(dataRow);
	};

	const handlePageChange = (page) => {
		fetchData(page, perPage);
	};

	const handlePerRowsChange = async (newPerPage, page) => {
		return fetchData(page, newPerPage);
	};

	const columns = useMemo(
		() => [
			{
				name: 'Role Code',
				selector: (row) => row.role_code,
				sortable: true,
			},
			{
				name: 'Role Name',
				selector: (row) => row.role_name,
				sortable: true,
			},
			{
				name: 'Category',
				selector: (row) => row.approval_category,
				sortable: true,
			},
			{
				name: 'Action',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (row) => {
					const {
						_id,
						role_code,
						role_name,
						segment_code,
						segment_name,
						approval_category,
						min_approval,
						user,
					} = row;
					const initialValues = {
						_id,
						role_code,
						role_name,
						segment_code,
						segment_name,
						approval_category,
						min_approval,
						user,
						requester: username,
					};
					return (
						<FormCustomModal
							initialValues={initialValues}
							handleCustomUpdate={customHandleUpdate}
							handleCustomDelete={customHandleDelete}
							list_role={list_role}
						/>
					);
				},
			},
		],
		// eslint-disable-next-line react-hooks/exhaustive-deps
		[username, list_role],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			pagination
			paginationServer
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};

TableCustom.propTypes = {
	data: PropTypes.instanceOf(Array),
	totalRows: PropTypes.number,
	perPage: PropTypes.number,
	fetchData: PropTypes.func,
	handleCustomUpdate: PropTypes.func,
	handleCustomDelete: PropTypes.func,
	list_role: PropTypes.instanceOf(Array),
};
TableCustom.defaultProps = {
	data: [],
	totalRows: 0,
	perPage: 10,
	fetchData: null,
	handleCustomUpdate: null,
	handleCustomDelete: null,
	list_role: [],
};

const ApprovalSetting = () => {
	const { t } = useTranslation('crud');
	const [title] = useState({ title: t('Approval Setting') });

	const { default_segment_code } = getaActiveRoles();
	const { username } = getRequester();

	const [list_role, setListRole] = useState([]);

	const [data, setData] = useState([]);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [curPage, setCurPage] = useState(1);

	const initialValues = {
		requester: username,
		role_code: '',
		role_name: '',
		segment_code: '',
		segment_name: '',
		approval_category: APPROVAL_CATEGORY.UPPER_PARENT.value,
		min_approval: 0,
		user: [],
	};

	const createSubmit = (values) => {
		handleSubmit(values)
			.then(() => {
				fetchData(curPage, perPage);
			})
			.catch(() => {});
	};

	const updateSubmit = (values) => {
		handleUpdate(values)
			.then(() => {
				fetchData(curPage, perPage);
			})
			.catch(() => {});
	};

	const deleteSubmit = (values) => {
		handleDelete(values)
			.then(() => {
				fetchData(curPage, perPage);
			})
			.catch(() => {});
	};

	useEffect(() => {
		fetchData(1, 10);
		fetchDataRole();

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const fetchData = async (newPage, newPerPage) => {
		const query = `page=${newPage}&sizePerPage=${newPerPage}&segment_code=${default_segment_code}`;
		return ApprovalSettingModule.read(query)
			.then((response) => {
				setData(response.foundData);
				setTotalRows(response.countData);
				setCurPage(response.currentPage);
				setPerPage(newPerPage);
			})
			.catch(() => {})
			.finally(() => {});
	};

	const fetchDataRole = async () => {
		const query = `segment_code=${default_segment_code}`;
		return RolesModule.readSelect(query)
			.then((response) => {
				setListRole(response);
			})
			.catch(() => {})
			.finally(() => {});
	};

	return (
		<PageWrapper title={title.title}>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card>
					<CardBody>
						<FormCustom
							initialValues={initialValues}
							handleCustomSubmit={createSubmit}
							list_role={list_role}
						/>
					</CardBody>
				</Card>
				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel icon='History' iconColor='info'>
							<CardTitle>{t('historical_data')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<TableCustom
							data={data}
							totalRows={totalRows}
							perPage={perPage}
							curPage={curPage}
							fetchData={fetchData}
							handleCustomUpdate={updateSubmit}
							handleCustomDelete={deleteSubmit}
							list_role={list_role}
						/>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

ApprovalSetting.propTypes = {};
ApprovalSetting.defaultProps = {};

export default ApprovalSetting;
