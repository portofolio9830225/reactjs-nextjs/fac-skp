import React, { useState, useEffect, useMemo, useRef } from 'react';
import PropTypes from 'prop-types';
import { Formik, Form } from 'formik';
import * as yup from 'yup';
import Swal from 'sweetalert2';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import PageWrapper from '../layout/PageWrapper/PageWrapper';
import Page from '../layout/Page/Page';
import PageLayoutHeader from '../pages/common/Headers/PageLayoutHeader';
import Card, { CardBody, CardHeader, CardLabel, CardTitle } from '../components/bootstrap/Card';
import FormGroup from '../components/bootstrap/forms/FormGroup';
import Input from '../components/bootstrap/forms/Input';
import Checks from '../components/bootstrap/forms/Checks';
import Button from '../components/bootstrap/Button';
import RoleModule from '../modules/RolesModule';
import TypePlan from '../modules/TypePlanModule';
import StoreCategory from '../modules/StoreCategoryModule';
import FieldInputModule from '../modules/FieldInputModule';
import FunctionModule from '../modules/FunctionModule';
import showNotification from '../components/extras/showNotification';
import DarkDataTable from '../components/DarkDataTable';
import useDarkMode from '../hooks/useDarkMode';
import CustomSelect from '../components/CustomSelect';
import COLORS from '../common/data/enumColors';
import Modal, {
	ModalBody,
	ModalHeader,
	ModalTitle,
	ModalFooter,
} from '../components/bootstrap/Modal';
import { getRequester, getaActiveRoles } from '../helpers/helpers';
import ActivityModule from '../modules/ActivityModule';

const type = [
	{ value: 'activity', label: 'activity' },
	{
		value: 'function',
		label: 'function',
	},
];

const SUPPORTED_FORMATS = ['image/jpg', 'image/jpeg', 'image/png'];

const generateFieldCode = (field_name) => {
	return field_name.replace(/\s+/g, '_').toLowerCase();
};

const Filter = (props) => {
	const { listRole, listTypePlan, listStoreCategory, onChange } = props;
	const { darkModeStatus } = useDarkMode();

	const [role, setRole] = useState(null);
	const [typePlan, setTypePlan] = useState(null);
	const [storeCategory, setStoreCategory] = useState(null);

	let refRole = useRef(null);
	let refTypeVisit = useRef(null);
	let refStoreCategory = useRef(null);

	useEffect(() => {
		onChange({ role, typePlan, storeCategory });
	}, [role, typePlan, storeCategory, onChange]);

	return (
		<div className='row m-2 mb-'>
			<CardTitle className='ml-2'>Filter</CardTitle>
			<div className='col-md-4'>
				<CustomSelect
					options={listRole}
					defaultValue={role}
					value={role}
					isClearable
					darkTheme={darkModeStatus}
					placeholder='Role'
					onChange={(value) => {
						setRole(value);
					}}
					ref={(ref) => {
						refRole = ref;
						return refRole;
					}}
				/>
			</div>
			<div className='col-md-4'>
				<CustomSelect
					options={listTypePlan}
					defaultValue={typePlan}
					value={typePlan}
					darkTheme={darkModeStatus}
					isClearable
					placeholder='Type Plan'
					onChange={(value) => {
						setTypePlan(value);
					}}
					ref={(ref) => {
						refTypeVisit = ref;
						return refTypeVisit;
					}}
				/>
			</div>
			<div className='col-md-4'>
				<CustomSelect
					options={listStoreCategory}
					defaultValue={storeCategory}
					value={storeCategory}
					darkTheme={darkModeStatus}
					isClearable
					placeholder='Store Category'
					onChange={(value) => {
						setStoreCategory(value);
					}}
					ref={(ref) => {
						refStoreCategory = ref;
						return refStoreCategory;
					}}
				/>
			</div>
		</div>
	);
};

const FormFieldInput = (props) => {
	const { formikField, listFieldInput, valid, readOnly } = props;

	const onChangeField = (value, idx, key) => {
		if (key === 'remove') {
			handleRemove(idx);
			return;
		}
		const old_fields = [...formikField.values.field];
		if (key === 'field_code') {
			old_fields[idx].field_code = value.value;
			old_fields[idx].field_name = value.label;
		} else {
			old_fields[idx][key] = value;
		}

		formikField.setFieldValue('field', old_fields);
	};

	const handleAdd = (id) => {
		const default_field = {
			id,
			field_name: null,
			field_code: null,
			is_mandatory: false,
			ordering: 0,
		};
		const old_field = [...formikField.values.field];
		old_field.push(default_field);
		formikField.setFieldValue('field', old_field);
	};

	const handleRemove = (idx) => {
		const old_field = [...formikField.values.field];
		old_field.splice(idx, 1);
		formikField.setFieldValue('field', old_field);
	};

	return (
		<>
			<div className='row align-items-center mb-2'>
				<div className='col-md-11 align-middle'>
					<h6 className='align-middle'>Input Field</h6>
				</div>
				{!readOnly && (
					<div className='col-md-1'>
						<Button
							color='success'
							isLight
							size='sm'
							icon='Add'
							type='button'
							onClick={() => handleAdd(formikField.values.field.length + 1)}
						/>
					</div>
				)}
			</div>
			<div className='border border-light w-100 mb-2' />
			{!valid && (
				<div className='align-items- text-danger mt-2'>
					<h6>Please check your field input</h6>
				</div>
			)}
			<FieldInput
				formikField={formikField}
				field={formikField.values.field}
				listFieldInput={listFieldInput}
				onChange={onChangeField}
				readOnly={readOnly}
			/>
		</>
	);
};

const FormAccepted = (props) => {
	const { formikField, listFunction, readOnly } = props;
	const { darkModeStatus } = useDarkMode();

	return (
		<>
			<FormGroup id='accepted' label='Accepted' className='mb-4'>
				<CustomSelect
					darkTheme={darkModeStatus}
					defaultValue={formikField.values.accepted}
					value={formikField.values.accepted}
					options={listFunction}
					onChange={(value) => formikField.setFieldValue('accepted', value)}
					isDisabled={readOnly}
				/>
			</FormGroup>
			{typeof formikField.errors.accepted !== 'undefined' && formikField.submitCount > 0 && (
				<div style={{ color: COLORS.DANGER.code }}>Field is required</div>
			)}
		</>
	);
};

const FieldInput = (props) => {
	const { field, listFieldInput, onChange, readOnly } = props;
	const { darkModeStatus } = useDarkMode();

	const getMenu = (obj) => ({ value: obj.field_code, label: obj.field_name });

	return (
		<div className='p-2' style={{ height: '600px', overflowY: 'auto', overflowX: 'hidden' }}>
			{field.map((f, idx) => (
				<div
					key={`cont-${f.id}`}
					className='row p-2 align-items-center rounded border-1 mt-2'>
					<div className='col-md-4'>
						<FormGroup id={`field-${idx}`} name='Field' className='mb-4'>
							<CustomSelect
								darkTheme={darkModeStatus}
								defaultValue={getMenu(f)}
								value={getMenu(f)}
								options={listFieldInput}
								onChange={(val) => onChange(val, idx, 'field_code')}
								isDisabled={readOnly}
							/>
						</FormGroup>
					</div>
					<div className='col-md-3 align-items-center'>
						<FormGroup
							id={`is_mandatory-${idx}`}
							label='is mandatory?'
							className='align-items-center mb-4'>
							<Checks
								style={{
									justifyContent: 'center',
									marginLeft: '1px',
								}}
								onChange={(val) =>
									onChange(val.target.checked, idx, 'is_mandatory')
								}
								checked={field[idx].is_mandatory}
								autoComplete='off'
								type='file'
								isInline
								readOnly={readOnly}
							/>
						</FormGroup>
					</div>
					<div className='col-md-4'>
						<FormGroup id={`ordering-${idx}`} label='Ordering' className='mb-4'>
							<Input
								onChange={(val) => onChange(val.target.value, idx, 'ordering')}
								value={field[idx].ordering}
								autoComplete='off'
								type='number'
								readOnly={readOnly}
							/>
						</FormGroup>
					</div>
					{!readOnly && (
						<div className='col-md-1'>
							<Button
								color='danger'
								isLight
								size='sm'
								icon='Delete'
								type='button'
								isDisable={field.length < 2}
								onClick={() => onChange('', idx, 'remove')}
							/>
						</div>
					)}
				</div>
			))}
		</div>
	);
};

const FormExample = (dt) => {
	const { darkModeStatus } = useDarkMode();
	const {
		initialValues,
		handleReloadData,
		listRole,
		listTypePlan,
		listActivity,
		listStoreCategory,
		listFieldInput,
		listFunction,
	} = dt;

	const [isAlwaysReadOnly] = useState(true);
	const [validFieldInput, setValidFieldInput] = useState(true);
	const [files, setFiles] = useState(null);

	let refType = useRef(0);
	let refRole = useRef(0);
	let refTypeVisit = useRef(0);
	let refStoreCategory = useRef(0);
	let refActivity = useRef(0);

	const handleSubmit = (values, resetForm) => {
		ActivityModule.create(values)
			.then((res) => {
				showNotification('Success!', res.status, 'success');
				const refs = [];
				refs.push(refType);
				refs.push(refActivity);
				refs.push(refRole);
				refs.push(refTypeVisit);
				refs.push(refStoreCategory);
				resetForm(initialValues);
				resetSelect(refs);
				handleReloadData();
			})
			.catch((err) => {
				showNotification('Warning!', err, 'danger');
			});
		return values;
	};

	const submitForm = (values, resetForm) => {
		const valid = values.field.some((f) => {
			return f.field_code && f.ordering > 0;
		});
		setValidFieldInput(valid);

		if (
			values.menu_code &&
			values.menu_name &&
			values.role &&
			values.type_visit &&
			values.store_category &&
			values.ordering &&
			values.type &&
			valid
		) {
			const { default_segment_code, default_segment_name } = getaActiveRoles();
			const { username } = getRequester();
			const val = {};
			const formData = new FormData();

			val.requester = username;
			val.menu_name = values.menu_name;
			val.menu_code = values.menu_code;
			val.type_code = values.type_visit.value;
			val.type_name = values.type_visit.label;
			val.store_category_code = values.store_category.value;
			val.store_category_name = values.store_category.label;
			val.is_mandatory = values.is_mandatory;
			val.access_after = values.access_after;
			val.field = values.field;
			val.segment = default_segment_name;
			val.segment_code = default_segment_code;
			val.type = values.type.value;
			val.ordering = values.ordering;
			val.role = values.role.label;
			val.role_code = values.role.value;
			val.accepted = values.accepted;

			if (files) {
				if (!SUPPORTED_FORMATS.includes(files.type)) {
					return Swal.fire({
						title: 'Cancelled!',
						text: 'Please upload png, jpg, or jpeg files',
						icon: 'error',
						confirmButtonText: 'Okay',
					});
				}
				formData.append('files', files);
			}
			formData.append('data', JSON.stringify(val));

			return Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'info',
				showCancelButton: true,
				confirmButtonText: 'Yes',
			}).then((result) => {
				if (result.value) {
					handleSubmit(formData, resetForm);
				} else if (result.dismiss === Swal.DismissReason.cancel) {
				}
			});
		}
		return values;
	};

	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		try {
			submitForm(values, resetForm);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	const onHandleFile = (event, formikField) => {
		formikField.handleChange(event);
		setFiles(event.target.files[0]);
	};

	const validationSchema = yup.object({
		menu_code: yup.string().required('Field is required'),
		menu_name: yup.string().required('Field is required'),
		role: yup.object({
			value: yup.string().required('Field is required'),
			label: yup.string().required('Field is required'),
		}),
		type_visit: yup.object({
			value: yup.string().required('Field is required'),
			label: yup.string().required('Field is required'),
		}),
		store_category: yup.object({
			value: yup.string().required('Field is required'),
			label: yup.string().required('Field is required'),
		}),
		ordering: yup
			.number()
			.required('Field is required')
			.moreThan(0, 'Value should be more than 0'),
		type: yup.object({
			value: yup.string().required('Field is required'),
			label: yup.string().required('Field is required'),
		}),
	});

	return (
		<Formik
			enableReinitialize
			initialValues={{ ...initialValues }}
			onSubmit={onSubmit}
			validationSchema={validationSchema}>
			{(formikField) => {
				return (
					<Form>
						<div className='row mb-4'>
							<div className='col-md-4'>
								<FormGroup id='menu_name' label='Menu Name' className='col-md-12'>
									<Input
										onChange={(val) => {
											formikField.handleChange(val);
											formikField.setFieldValue(
												'menu_code',
												generateFieldCode(val.target.value),
											);
										}}
										onBlur={formikField.handleBlur}
										value={formikField.values.menu_name}
										isValid={formikField.isValid}
										isTouched={formikField.touched.menu_name}
										invalidFeedback={formikField.errors.menu_name}
										autoComplete='off'
									/>
								</FormGroup>
								<FormGroup id='menu_code' label='Menu Code' className='col-md-12'>
									<Input
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.menu_code}
										isValid={formikField.isValid}
										isTouched={formikField.touched.menu_code}
										invalidFeedback={formikField.errors.menu_code}
										autoComplete='off'
										readOnly={isAlwaysReadOnly}
									/>
								</FormGroup>

								<FormGroup id='menu_icon' label='Menu Icon' className='col-md-12'>
									<Input
										onChange={(event) => onHandleFile(event, formikField)}
										onBlur={formikField.handleBlur}
										value={formikField.values.menu_icon}
										isValid={formikField.isValid}
										isTouched={formikField.touched.menu_icon}
										invalidFeedback={formikField.errors.menu_icon}
										autoComplete='off'
										type='file'
									/>
								</FormGroup>
								{typeof formikField.errors.menu_icon !== 'undefined' &&
									formikField.submitCount > 0 && (
										<div style={{ color: COLORS.DANGER.code }}>
											Field is required
										</div>
									)}
								<FormGroup id='role' label='Role' className='mb-4'>
									<CustomSelect
										darkTheme={darkModeStatus}
										defaultValue={formikField.values.role}
										value={formikField.values.role}
										options={listRole}
										onChange={(value) => {
											formikField.setFieldValue('role', value);
										}}
										setRef={(ref) => {
											refRole = ref;
											return refRole;
										}}
									/>
								</FormGroup>
								<FormGroup id='type_visit' label='Type Visit' className='mb-4'>
									<CustomSelect
										darkTheme={darkModeStatus}
										defaultValue={formikField.values.type_visit}
										value={formikField.values.type_visit}
										options={listTypePlan}
										onChange={(value) => {
											formikField.setFieldValue('type_visit', value);
										}}
										setRef={(ref) => {
											refTypeVisit = ref;
											return refTypeVisit;
										}}
									/>
								</FormGroup>
								<FormGroup
									id='store_category'
									label='Store Category'
									className='mb-4'>
									<CustomSelect
										darkTheme={darkModeStatus}
										defaultValue={formikField.values.store_category}
										value={formikField.values.store_category}
										options={listStoreCategory}
										onChange={(value) => {
											formikField.setFieldValue('store_category', value);
										}}
										setRef={(ref) => {
											refStoreCategory = ref;
											return refStoreCategory;
										}}
									/>
								</FormGroup>
								<FormGroup id='is_mandatory' label='is mandatory?' className='mb-4'>
									<Checks
										style={{ justifyContent: 'center', marginLeft: '1px' }}
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										checked={formikField.values.is_mandatory}
										isValid={formikField.isValid}
										isTouched={formikField.touched.is_mandatory}
										invalidFeedback={formikField.errors.is_mandatory}
										autoComplete='off'
										type='file'
										isInline
									/>
								</FormGroup>
								<FormGroup id='access_after' label='Access After' className='mb-4'>
									<CustomSelect
										options={listActivity}
										defaultValue={formikField.values.access_after}
										value={formikField.values.access_after}
										darkTheme={darkModeStatus}
										onChange={(value) => {
											formikField.setFieldValue('access_after', value);
										}}
										isValid={
											typeof formikField.errors.access_after === 'undefined'
										}
										setRef={(ref) => {
											refActivity = ref;
											return refActivity;
										}}
										isClearable
									/>
								</FormGroup>
								<FormGroup id='ordering' label='Ordering' className='col-md-12'>
									<Input
										onChange={formikField.handleChange}
										onBlur={formikField.handleBlur}
										value={formikField.values.ordering}
										isValid={formikField.isValid}
										isTouched={formikField.touched.ordering}
										invalidFeedback={formikField.errors.ordering}
										autoComplete='off'
										type='number'
									/>
								</FormGroup>
								{typeof formikField.errors.ordering !== 'undefined' &&
									formikField.submitCount > 0 && (
										<div style={{ color: COLORS.DANGER.code }}>
											Field is required
										</div>
									)}
								<FormGroup id='type' label='Type' className='mb-4'>
									<CustomSelect
										darkTheme={darkModeStatus}
										defaultValue={formikField.values.type}
										value={formikField.values.type}
										onBlur={formikField.handleBlur}
										options={type}
										onChange={(value) => {
											formikField.setFieldValue('type', value);
										}}
										setRef={(ref) => {
											refType = ref;
											return refType;
										}}
									/>
								</FormGroup>
								{formikField.values.type &&
									formikField.values.type.value === 'function' && (
										<>
											<FormGroup
												id='accepted'
												label='Accepted'
												className='mb-4'>
												<CustomSelect
													darkTheme={darkModeStatus}
													defaultValue={formikField.values.accepted}
													value={formikField.values.accepted}
													options={listFunction}
													onChange={(value) =>
														formikField.setFieldValue('accepted', value)
													}
												/>
											</FormGroup>
											{typeof formikField.errors.accepted !== 'undefined' &&
												formikField.submitCount > 0 && (
													<div style={{ color: COLORS.DANGER.code }}>
														Field is required
													</div>
												)}
										</>
									)}
							</div>
							<div className='col-md-8'>
								<FormFieldInput
									listFieldInput={listFieldInput}
									formikField={formikField}
									valid={validFieldInput}
								/>
							</div>
						</div>

						<div className='col-md-12 '>
							<Button
								icon='Save'
								isOutline
								type='submit'
								color='success'
								className='float-end'
								isDisable={!formikField.isValid && !!formikField.submitCount}>
								Submit
							</Button>
						</div>
					</Form>
				);
			}}
		</Formik>
	);
};

const resetSelect = (refs) => {
	refs.forEach((ref) => {
		ref.setValue({});
	});
};

const FormExampleEdit = (dt) => {
	const { darkModeStatus } = useDarkMode();
	const {
		initialValues,
		handleReloadData,
		listRole,
		listTypePlan,
		listActivity,
		listFieldInput,
		listStoreCategory,
	} = dt;

	const [isAlwaysReadOnly] = useState(true);
	const [validFieldInput, setValidFieldInput] = useState(true);
	const [files, setFiles] = useState(null);
	const [isOpen, setIsOpen] = useState(false);
	const [isReadOnly, setIsReadOnly] = useState(false);

	let refType = useRef(0);
	let refRole = useRef(0);
	let refTypeVisit = useRef(0);
	let refStoreCategory = useRef(0);
	let refActivity = useRef(0);

	const handleSubmitEdit = (values, resetForm) => {
		ActivityModule.update(values)
			.then((res) => {
				showNotification('Success!', res.status, 'success');
				const refs = [];
				refs.push(refType);
				resetForm(initialValues);
				handleReloadData();
			})
			.catch((err) => {
				if (err === 'Data already queued for update. Do you wish to overwrite?') {
					Swal.fire({
						title: 'Warning!',
						text: err,
						icon: 'info',
						showCancelButton: true,
						confirmButtonText: 'Yes',
					}).then((result) => {
						values.overwrite = true;
						if (result.value) {
							handleSubmitEdit(values, handleReloadData);
						} else if (result.dismiss === Swal.DismissReason.cancel) {
						}
					});
					return;
				}
				showNotification('Warning!', err, 'danger');
			});
		return values;
	};

	const handleDelete = (val) => {
		ActivityModule.delete_(val)
			.then((res) => {
				showNotification('Success!', res.status, 'success');
				handleReloadData();
			})
			.catch((err) => {
				showNotification('Warning!', err, 'danger');
			});
		return val;
	};

	const submitForm = (values, resetForm) => {
		const valid = values.field.some((f) => {
			return f.field_code && f.ordering > 0;
		});
		setValidFieldInput(valid);
		if (
			values.menu_code &&
			values.menu_name &&
			values.role &&
			values.type_visit &&
			values.store_category &&
			values.ordering &&
			values.type &&
			values.execute_date &&
			valid
		) {
			const { default_segment_code, default_segment_name } = getaActiveRoles();
			const { username } = getRequester();
			const val = {};
			const formData = new FormData();

			val._id = values._id;
			val.requester = username;
			val.menu_name = values.menu_name;
			val.menu_code = values.menu_code;
			val.type_code = values.type_visit.value;
			val.type_name = values.type_visit.label;
			val.store_category_code = values.store_category.value;
			val.store_category_name = values.store_category.label;
			val.is_mandatory = values.is_mandatory;
			val.access_after = values.access_after;
			val.field = values.field;
			val.segment = default_segment_name;
			val.segment_code = default_segment_code;
			val.type = values.type.value;
			val.ordering = values.ordering;
			val.role = values.role.label;
			val.role_code = values.role.value;
			val.execute_date = values.execute_date;
			val.accepted = values.accepted;

			if (files) {
				if (!SUPPORTED_FORMATS.includes(files.type)) {
					return Swal.fire({
						title: 'Cancelled!',
						text: 'Please upload png, jpg, or jpeg files',
						icon: 'error',
						confirmButtonText: 'Okay',
					});
				}
				formData.append('files', files);
			}
			formData.append('data', JSON.stringify(val));
			return Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'info',
				showCancelButton: true,
				confirmButtonText: 'Yes',
			}).then((result) => {
				if (result.value) {
					handleSubmitEdit(val, resetForm);
				} else if (result.dismiss === Swal.DismissReason.cancel) {
				}
			});
		}
		return values;
	};

	const deleteForm = (id) => {
		if (id) {
			const { username } = getRequester();
			const val = {};
			val._id = id;
			val.requester = username;
			return Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'info',
				showCancelButton: true,
				confirmButtonText: 'Yes',
			}).then((result) => {
				if (result.value) {
					handleDelete(val);
				} else if (result.dismiss === Swal.DismissReason.cancel) {
				}
			});
		}
		return id;
	};

	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		try {
			delete values.loading;
			submitForm(values, resetForm);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	const onHandleFile = (event, formikField) => {
		formikField.handleChange(event);
		setFiles(event.target.files[0]);
	};

	const validationSchema = yup.object({
		menu_code: yup.string().required('Field is required'),
		menu_name: yup.string().required('Field is required'),
		role: yup.object({
			value: yup.string().required('Field is required'),
			label: yup.string().required('Field is required'),
		}),
		type_visit: yup.object({
			value: yup.string().required('Field is required'),
			label: yup.string().required('Field is required'),
		}),
		store_category: yup.object({
			value: yup.string().required('Field is required'),
			label: yup.string().required('Field is required'),
		}),
		ordering: yup
			.number()
			.required('Field is required')
			.moreThan(0, 'Value should be more than 0'),
		type: yup.object({
			value: yup.string().required('Field is required'),
			label: yup.string().required('Field is required'),
		}),
	});

	return (
		<>
			<Button
				className='me-2'
				icon='GridOn'
				type='button'
				color='primary'
				onClick={() => {
					setIsOpen(true);
					setIsReadOnly(true);
				}}
			/>

			<Button
				className='me-2'
				icon='Edit'
				type='button'
				color='success'
				onClick={() => {
					setIsOpen(true);
					setIsReadOnly(false);
				}}
			/>

			<Button
				className='me-2'
				icon='Delete'
				type='button'
				color='danger'
				onClick={() => deleteForm(initialValues._id)}
			/>

			<Modal isOpen={isOpen} setIsOpen={setIsOpen} size='lg' titleId='modal-edit-menu-crud'>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-edit-menu-crud'>
						{isReadOnly ? 'View Data' : 'Update Data'}
					</ModalTitle>
				</ModalHeader>
				<Formik
					enableReinitialize
					initialValues={{ ...initialValues }}
					onSubmit={onSubmit}
					validationSchema={validationSchema}>
					{(formikField) => {
						return (
							<Form>
								<ModalBody className='px-4'>
									<div className='row mb-4'>
										<div className='col-md-4'>
											<FormGroup
												id='menu_name'
												label='Menu Name'
												className='col-md-12'>
												<Input
													onChange={(val) => {
														formikField.handleChange(val);
														formikField.setFieldValue(
															'menu_code',
															generateFieldCode(val.target.value),
														);
													}}
													onBlur={formikField.handleBlur}
													value={formikField.values.menu_name}
													isValid={formikField.isValid}
													isTouched={formikField.touched.menu_name}
													invalidFeedback={formikField.errors.menu_name}
													autoComplete='off'
													readOnly={isAlwaysReadOnly}
												/>
											</FormGroup>
											<FormGroup
												id='menu_code'
												label='Menu Code'
												className='col-md-12'>
												<Input
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.menu_code}
													isValid={formikField.isValid}
													isTouched={formikField.touched.menu_code}
													invalidFeedback={formikField.errors.menu_code}
													autoComplete='off'
													readOnly={isAlwaysReadOnly}
												/>
											</FormGroup>

											{!isReadOnly && (
												<FormGroup
													id='menu_icon'
													label='Menu Icon'
													className='col-md-12'>
													<Input
														onChange={(event) =>
															onHandleFile(event, formikField)
														}
														onBlur={formikField.handleBlur}
														value={formikField.values.menu_icon}
														isValid={formikField.isValid}
														isTouched={formikField.touched.menu_icon}
														invalidFeedback={
															formikField.errors.menu_icon
														}
														autoComplete='off'
														type='file'
													/>
												</FormGroup>
											)}
											{typeof formikField.errors.menu_icon !== 'undefined' &&
												formikField.submitCount > 0 && (
													<div style={{ color: COLORS.DANGER.code }}>
														Field is required
													</div>
												)}
											<FormGroup id='role' label='Role' className='mb-4'>
												<CustomSelect
													darkTheme={darkModeStatus}
													defaultValue={formikField.values.role}
													value={formikField.values.role}
													options={listRole}
													onChange={(value) => {
														formikField.setFieldValue('role', value);
													}}
													setRef={(ref) => {
														refRole = ref;
														return refRole;
													}}
													isDisabled={isReadOnly}
												/>
											</FormGroup>
											<FormGroup
												id='type_visit'
												label='Type Visit'
												className='mb-4'>
												<CustomSelect
													darkTheme={darkModeStatus}
													defaultValue={formikField.values.type_visit}
													value={formikField.values.type_visit}
													options={listTypePlan}
													onChange={(value) => {
														formikField.setFieldValue(
															'type_visit',
															value,
														);
													}}
													setRef={(ref) => {
														refTypeVisit = ref;
														return refTypeVisit;
													}}
													isDisabled={isReadOnly}
												/>
											</FormGroup>
											<FormGroup
												id='store_category'
												label='Store Category'
												className='mb-4'>
												<CustomSelect
													darkTheme={darkModeStatus}
													defaultValue={formikField.values.store_category}
													value={formikField.values.store_category}
													options={listStoreCategory}
													onChange={(value) => {
														formikField.setFieldValue(
															'store_category',
															value,
														);
													}}
													isValid={
														typeof formikField.errors.store_category ===
														'undefined'
													}
													setRef={(ref) => {
														refStoreCategory = ref;
														return refStoreCategory;
													}}
													isDisabled={isReadOnly}
												/>
											</FormGroup>
											<FormGroup
												id='is_mandatory'
												label='is mandatory?'
												className='mb-4'>
												<Checks
													style={{
														justifyContent: 'center',
														marginLeft: '1px',
													}}
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													checked={formikField.values.is_mandatory}
													isValid={formikField.isValid}
													isTouched={formikField.touched.is_mandatory}
													invalidFeedback={
														formikField.errors.is_mandatory
													}
													autoComplete='off'
													type='file'
													isInline
													readOnly={isReadOnly}
												/>
											</FormGroup>
											<FormGroup
												id='access_after'
												label='Access After'
												className='mb-4'>
												<CustomSelect
													options={listActivity}
													defaultValue={formikField.values.access_after}
													value={formikField.values.access_after}
													darkTheme={darkModeStatus}
													onChange={(value) => {
														formikField.setFieldValue(
															'access_after',
															value,
														);
													}}
													isValid={
														typeof formikField.errors.access_after ===
														'undefined'
													}
													setRef={(ref) => {
														refActivity = ref;
														return refActivity;
													}}
													isDisabled={isReadOnly}
													isClearable
												/>
											</FormGroup>
											<FormGroup
												id='ordering'
												label='Ordering'
												className='col-md-12'>
												<Input
													onChange={formikField.handleChange}
													onBlur={formikField.handleBlur}
													value={formikField.values.ordering}
													isValid={formikField.isValid}
													isTouched={formikField.touched.ordering}
													invalidFeedback={formikField.errors.ordering}
													autoComplete='off'
													type='number'
													readOnly={isReadOnly}
												/>
											</FormGroup>
											{typeof formikField.errors.ordering !== 'undefined' &&
												formikField.submitCount > 0 && (
													<div style={{ color: COLORS.DANGER.code }}>
														Field is required
													</div>
												)}
											<FormGroup id='type' label='Type' className='mb-4'>
												<CustomSelect
													darkTheme={darkModeStatus}
													defaultValue={formikField.values.type}
													value={formikField.values.type}
													options={type}
													onChange={(value) => {
														formikField.setFieldValue('type', value);
													}}
													setRef={(ref) => {
														refType = ref;
														return refType;
													}}
													isDisabled={isReadOnly}
												/>
											</FormGroup>
											{!isReadOnly && (
												<FormGroup id='execute_date' label='Execute Date'>
													<Input
														type='date'
														onChange={formikField.handleChange}
														onInput={(value) => {
															formikField.setFieldValue(
																'execute_date',
																value,
															);
														}}
														onBlur={formikField.handleBlur}
														value={formikField.values.execute_date}
														isValid={formikField.isValid}
														isTouched={formikField.touched.execute_date}
														invalidFeedback={
															formikField.errors.execute_date
														}
														min={moment()
															.add(1, 'days')
															.format('YYYY-MM-DD')}
														autoComplete='off'
													/>
												</FormGroup>
											)}
										</div>
										<div className='col-md-8'>
											<FormFieldInput
												listFieldInput={listFieldInput}
												formikField={formikField}
												valid={validFieldInput}
												readOnly={isReadOnly}
											/>
										</div>
									</div>
								</ModalBody>
								{!isReadOnly && (
									<ModalFooter className='px-4 pb-4'>
										<div className='col-md-12 '>
											<Button
												icon='Save'
												type='submit'
												color='success'
												className='float-end'
												isDisable={
													!formikField.isValid &&
													!!formikField.submitCount
												}>
												Update
											</Button>
										</div>
									</ModalFooter>
								)}
							</Form>
						);
					}}
				</Formik>
			</Modal>
		</>
	);
};

const CustomButton = (dt) => {
	const {
		row,
		listRole,
		listTypePlan,
		listStoreCategory,
		listFieldInput,
		listActivity,
		handleReloadData,
	} = dt;

	// restructure the obj
	const initialValues = {};
	const role = {};
	const type_visit = {};
	const store_category = {};
	const type_o = {};
	const access_after = {};

	role.value = row.role_code;
	role.label = row.role;
	type_visit.value = row.type_code;
	type_visit.label = row.type_name;
	store_category.value = row.store_category_code;
	store_category.label = row.store_category_name;
	type_o.value = row.type;
	type_o.label = row.type;

	if (row.access_after) {
		access_after.value = row.access_after.menu_code;
		access_after.label = row.access_after.menu_name;
	}

	initialValues._id = row._id;

	initialValues.role = role;
	initialValues.type_visit = type_visit;
	initialValues.store_category = store_category;
	initialValues.type = type_o;
	initialValues.field = row.field;
	initialValues.access_after = access_after;

	initialValues.menu_code = row.menu_code;
	initialValues.menu_name = row.menu_name;
	initialValues.ordering = row.ordering;
	initialValues.is_mandatory = row.is_mandatory;
	initialValues.execute_date = moment().add(1, 'days').format('YYYY-MM-DD');

	return (
		<FormExampleEdit
			initialValues={initialValues}
			listRole={listRole}
			listTypePlan={listTypePlan}
			listStoreCategory={listStoreCategory}
			listActivity={listActivity}
			listFieldInput={listFieldInput}
			handleReloadData={handleReloadData}
			row={row}
		/>
	);
};

const CustomDataTable = ({
	listRole,
	listTypePlan,
	listStoreCategory,
	listActivity,
	listFieldInput,
	data,
	loading,
	totalRows,
	handlePageChange,
	handlePerRowsChange,
	handleReloadData,
}) => {
	const { darkModeStatus } = useDarkMode();

	const columns = useMemo(
		() => [
			{
				name: 'Created At',
				selector: (row) => moment(row.created_at).format('YYYY-MM-DD'),
				sortable: true,
			},
			{
				name: 'Activity Name',
				selector: (row) => row.menu_name,
				sortable: true,
			},
			{
				name: 'Segment',
				selector: (row) => row.segment,
				sortable: true,
			},
			{
				name: 'Type',
				selector: (row) => row.type_name,
				sortable: true,
			},
			{
				name: 'Store Category',
				selector: (row) => row.store_category_name,
				sortable: true,
			},
			{
				name: 'Action',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (dt) => {
					return (
						<CustomButton
							row={dt}
							listRole={listRole}
							listTypePlan={listTypePlan}
							listStoreCategory={listStoreCategory}
							listActivity={listActivity}
							listFieldInput={listFieldInput}
							handleReloadData={handleReloadData}
						/>
					);
				},
			},
		],
		[handleReloadData, listActivity, listFieldInput, listRole, listStoreCategory, listTypePlan],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			progressPending={loading}
			pagination
			paginationServer
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};
const Activity = () => {
	const [data, setData] = useState([]);
	const [listRole, setRole] = useState([]);
	const [listTypePlan, setTypePlan] = useState([]);
	const [listStoreCategory, setStoreCategory] = useState([]);
	const [listActivity, setActivity] = useState([]);
	const [listFieldInput, setFieldInput] = useState([]);
	const [listFunction, setFunction] = useState([]);

	const [loading, setLoading] = useState(false);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [page, setPage] = useState(1);
	const [reloadData, setReloadData] = useState(false);
	const [queryFilter, setQueryFilter] = useState('');

	const fetchFunction = async () => {
		const { default_segment_code } = getaActiveRoles();
		return FunctionModule.readSelect(`segment_code=${default_segment_code}`).then((res) => {
			setFunction(res);
		});
	};

	const fetchActivity = async () => {
		setLoading(true);
		return ActivityModule.read(`page=${page}&sizePerPage=${perPage}${queryFilter}`).then(
			(res) => {
				setData(res.foundData);
				setTotalRows(res.countData);
				setLoading(false);
			},
		);
	};

	const fetchRole = async () => {
		return RoleModule.readSelect().then((res) => {
			setRole(res);
		});
	};

	const fetchTypePlan = async () => {
		return TypePlan.readSelect().then((res) => {
			setTypePlan(res);
		});
	};

	const fetchStoreCategory = async () => {
		return StoreCategory.readSelect().then((res) => {
			setStoreCategory(res);
		});
	};

	const fetchActivitySelect = async (role_code, type_code, store_category_code) => {
		const { default_segment_code } = getaActiveRoles();
		let url = `segment_code=${default_segment_code}`;
		if (role_code) {
			url += `&role_code=${role_code}`;
		}
		if (type_code) {
			url += `&type_code=${type_code}`;
		}
		if (store_category_code) {
			url += `&store_category_code=${store_category_code}`;
		}
		return ActivityModule.readSelect(url).then((res) => {
			setActivity(res);
		});
	};

	const fetchFieldInputSelect = async () => {
		const { default_segment_code } = getaActiveRoles();

		return FieldInputModule.readSelect(`segment_code=${default_segment_code}`).then((res) => {
			setFieldInput(res);
		});
	};

	const handleReloadData = () => {
		setReloadData(!reloadData);
	};

	const onChangeFilter = ({ role, typePlan, storeCategory }) => {
		let query = '';
		if (role) {
			query += `&role=${role.value}`;
		}
		if (typePlan) {
			query += `&type_plan=${typePlan.value}`;
		}
		if (storeCategory) {
			query += `&store_category=${storeCategory.value}`;
		}
		setQueryFilter(query);
	};

	const initialValues = {
		loading: false,
		segment: null,
		menu_code: '',
		menu_name: '',
		menu_icon: null,
		role: null,
		type_visit: null,
		store_category: null,
		accepted: null,
		is_mandatory: false,
		access_after: null,
		ordering: 0,
		type: null,
		field: [
			{
				id: 0,
				field_code: null,
				field_name: null,
				is_mandatory: false,
				ordering: null,
			},
		],
	};

	useEffect(() => {
		fetchActivity();

		fetchRole();
		fetchTypePlan();
		fetchStoreCategory();
		fetchActivitySelect();
		fetchFieldInputSelect();
		fetchFunction();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [page, perPage, reloadData, queryFilter]);

	useEffect(() => {
		fetchActivity();

		fetchRole();
		fetchTypePlan();
		fetchStoreCategory();
		fetchActivitySelect();
		fetchFieldInputSelect();
		fetchFunction();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const { t } = useTranslation('field-input');
	const [title] = useState({ title: 'Activity' });
	return (
		<PageWrapper title={title.title}>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card className='col-md-6'>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('Activity')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<div className='col-12 mb-3 g-5'>
							<FormExample
								initialValues={initialValues}
								listRole={listRole}
								listTypePlan={listTypePlan}
								listStoreCategory={listStoreCategory}
								listActivity={listActivity}
								listFieldInput={listFieldInput}
								listFunction={listFunction}
								handleReloadData={handleReloadData}
							/>
						</div>
					</CardBody>
				</Card>
				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('Historical Data')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<Filter
							listRole={listRole}
							listTypePlan={listTypePlan}
							listStoreCategory={listStoreCategory}
							onChange={onChangeFilter}
						/>
						<CustomDataTable
							data={data}
							loading={loading}
							totalRows={totalRows}
							listRole={listRole}
							listTypePlan={listTypePlan}
							listStoreCategory={listStoreCategory}
							listActivity={listActivity}
							listFieldInput={listFieldInput}
							handlePageChange={setPage}
							handlePerRowsChange={setPerPage}
							handleReloadData={handleReloadData}
						/>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

CustomDataTable.propTypes = {
	listRole: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	listTypePlan: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	listStoreCategory: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	listActivity: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	listFieldInput: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	data: PropTypes.arrayOf(PropTypes.instanceOf(Object)),
	loading: PropTypes.bool,
	totalRows: PropTypes.number,
	handlePageChange: PropTypes.func,
	handlePerRowsChange: PropTypes.func,
	handleReloadData: PropTypes.func,
};
CustomDataTable.defaultProps = {
	listRole: {},
	listTypePlan: {},
	listStoreCategory: {},
	listActivity: {},
	listFieldInput: {},
	data: [],
	loading: false,
	totalRows: 0,
	handlePageChange: null,
	handlePerRowsChange: null,
	handleReloadData: null,
};

FormFieldInput.propTypes = {
	formikField: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	listFieldInput: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	valid: PropTypes.bool,
	readOnly: PropTypes.bool,
};

FormFieldInput.defaultProps = {
	formikField: null,
	listFieldInput: null,
	valid: true,
	readOnly: false,
};

FieldInput.propTypes = {
	listFieldInput: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	field: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	onChange: PropTypes.func,
	readOnly: PropTypes.bool,
};

FieldInput.defaultProps = {
	listFieldInput: null,
	field: [],
	onChange: () => {},
	readOnly: false,
};

FormAccepted.propTypes = {
	listFunction: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	formikField: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	readOnly: PropTypes.bool,
};

FormAccepted.defaultProps = {
	listFunction: null,
	formikField: null,
	readOnly: false,
};

Filter.propTypes = {
	listRole: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	listStoreCategory: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	listTypePlan: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	onChange: PropTypes.func,
};

Filter.defaultProps = {
	listRole: null,
	listStoreCategory: null,
	listTypePlan: null,
	onChange: () => {},
};

export default Activity;
