import React, { useState, useEffect, useMemo, useRef } from 'react';
import PropTypes from 'prop-types';
import { Formik, Form } from 'formik';
import * as yup from 'yup';
import Swal from 'sweetalert2';
import { useTranslation } from 'react-i18next';
import Select from 'react-select';
import moment from 'moment';
import PageWrapper from '../layout/PageWrapper/PageWrapper';
import Page from '../layout/Page/Page';
import PageLayoutHeader from '../pages/common/Headers/PageLayoutHeader';
import Card, { CardBody, CardHeader, CardLabel, CardTitle } from '../components/bootstrap/Card';
import FormGroup from '../components/bootstrap/forms/FormGroup';
import Input from '../components/bootstrap/forms/Input';
import Button from '../components/bootstrap/Button';
import DynamicSelect from '../components/DynamicSelect';
import SegmentModule from '../modules/SegmentModule';
import SelectUrlModule from '../modules/SelectUrlModule';
import FunctionModule from '../modules/FunctionModule';
import FieldInputModule from '../modules/FieldInputModule';
import KeyModule from '../modules/KeyModule';
import showNotification from '../components/extras/showNotification';
import DarkDataTable from '../components/DarkDataTable';
import useDarkMode from '../hooks/useDarkMode';
import CustomSelect from '../components/CustomSelect';
import COLORS from '../common/data/enumColors';
import Modal, {
	ModalBody,
	ModalHeader,
	ModalTitle,
	ModalFooter,
} from '../components/bootstrap/Modal';
import { getRequester, getaActiveRoles } from '../helpers/helpers';

const tipe_data = [
	{ value: 'text', label: 'text' },
	{
		value: 'number',
		label: 'number',
	},
];

const category = [
	{
		value: 'input',
		label: 'input',
	},
	{
		value: 'textarea',
		label: 'textarea',
	},
	{
		value: 'date',
		label: 'date',
	},
	{ value: 'single_select_url', label: 'single_select_url' },
	{ value: 'single_select_url_parameter', label: 'single_select_url_parameter' },

	{
		value: 'camera',
		label: 'camera',
	},
	{ value: 'button', label: 'button' },
	{ value: 'single_select', label: 'single_select' },
	{
		value: 'checkbox',
		label: 'checkbox',
	},
];
const generateFieldCode = (field_name) => {
	return field_name.replace(/\s+/g, '_').toLowerCase();
};
const react_select_styles = (formikField, darkModeStatus, key) => {
	const select_invalid_styles = {
		control: (base, state) => ({
			...base,
			// state.isFocused can display different borderColor if you need it
			borderColor:
				(state.isFocused && COLORS.SECONDARY.code) ||
				(!state.isFocused && formikField.submitCount > 0 && COLORS.DANGER.code) ||
				(!state.isFocused && formikField.submitCount === 0 && COLORS.LIGHT.code),
			// overwrittes hover style
			'&:hover': {
				borderColor:
					(state.isFocused && COLORS.SECONDARY.code) ||
					(!state.isFocused && formikField.submitCount > 0 && COLORS.DANGER.code),
			},
		}),
		singleValue: (base) => ({
			...base,
			color: darkModeStatus ? COLORS.LIGHT.code : COLORS.DARK.code,
		}),
		input: (base) => ({
			...base,
			color: darkModeStatus ? COLORS.LIGHT.code : COLORS.DARK.code,
		}),
	};
	const select_valid_styles = {
		control: (base, state) => ({
			...base,
			// state.isFocused can display different borderColor if you need it
			borderColor:
				(state.isFocused && COLORS.SECONDARY.code) ||
				(!state.isFocused && formikField.submitCount > 0 && COLORS.SUCCESS.code) ||
				(!state.isFocused && formikField.submitCount === 0 && COLORS.LIGHT.code),

			// overwrittes hover style
			'&:hover': {
				borderColor:
					(state.isFocused && COLORS.SECONDARY.code) ||
					(!state.isFocused && formikField.submitCount > 0 && COLORS.SUCCESS.code),
			},
		}),
		singleValue: (base) => ({
			...base,
			color: darkModeStatus ? COLORS.LIGHT.code : COLORS.DARK.code,
		}),
		input: (base) => ({
			...base,
			color: darkModeStatus ? COLORS.LIGHT.code : COLORS.DARK.code,
		}),
	};
	return typeof formikField.errors[key] === 'undefined'
		? select_valid_styles
		: select_invalid_styles;
};

const react_select_themes = (theme, darkModeStatus) => {
	return {
		...theme,
		colors: darkModeStatus
			? {
					...theme.colors,
					neutral0: COLORS.DARK.code,
					neutral190: COLORS.LIGHT.code,
					primary: COLORS.SUCCESS.code,
					primary25: COLORS.PRIMARY.code,
			  }
			: theme.colors,
	};
};

const handleSubmit = (values, handleReloadData) => {
	FieldInputModule.create(values)
		.then((res) => {
			showNotification('Success!', res.status, 'success');
			handleReloadData();
		})
		.catch((err) => {
			showNotification('Warning!', err, 'danger');
		});
	return values;
};
const handleSubmitEdit = (values, handleReloadData) => {
	FieldInputModule.update(values)
		.then((res) => {
			showNotification('Success!', res.status, 'success');
			handleReloadData();
		})
		.catch((err) => {
			if (err === 'Data already queued for update. Do you wish to overwrite?') {
				Swal.fire({
					title: 'Warning!',
					text: err,
					icon: 'info',
					showCancelButton: true,
					confirmButtonText: 'Yes',
				}).then((result) => {
					values.overwrite = true;
					if (result.value) {
						handleSubmitEdit(values, handleReloadData);
					} else if (result.dismiss === Swal.DismissReason.cancel) {
					}
				});
				return;
			}
			showNotification('Warning!', err, 'danger');
		});
	return values;
};
const handleDelete = (val, handleReloadData) => {
	FieldInputModule.delete_(val)
		.then((res) => {
			showNotification('Success!', res.status, 'success');
			handleReloadData();
		})
		.catch((err) => {
			showNotification('Warning!', err, 'danger');
		});
	return val;
};

const FormAccepted = (props) => {
	const { darkModeStatus } = useDarkMode();
	const { formikField, listSelectUrl, onSelectUrlParameter, listKey, isReadOnly } = props;

	const [accepted, setAccepted] = useState([]);
	const select_items_accepted = [];
	const errors_loc_accepted = {};

	const onChangeData = (newData) => {
		setAccepted(newData);
		formikField.setFieldValue('accepted', newData);
		return newData;
	};
	const getLoad = () => {
		const newRes = new Promise((resolve, reject) => {
			try {
				resolve([]);
			} catch (e) {
				reject(new Error({ error: true }));
			}
		});
		return newRes;
	};

	useEffect(() => {
		setAccepted([]);
		formikField.setFieldValue('accepted', null);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [formikField.values.category]);

	useEffect(() => {
		if (formikField.values.category) {
			if (formikField.values.category.value === 'single_select_url_parameter') {
				onSelectUrlParameter(formikField.values.accepted);
			}
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [formikField.values.accepted, formikField.values.category]);

	if (formikField.values.category === null) {
		return null;
	}

	if (formikField.values.category.value === 'single_select_url') {
		return (
			<>
				<FormGroup id='accepted' label='Accepted' className='mb-4'>
					<Select
						styles={react_select_styles(formikField, darkModeStatus, 'accepted')}
						theme={(theme) => react_select_themes(theme, darkModeStatus)}
						defaultValue={formikField.values.accepted}
						onBlur={formikField.handleBlur}
						options={listSelectUrl}
						onChange={(value) => {
							formikField.setFieldValue('accepted', value);
						}}
						isDisabled={isReadOnly}
					/>
				</FormGroup>
				{typeof formikField.errors.accepted !== 'undefined' &&
					formikField.submitCount > 0 && (
						<div style={{ color: COLORS.DANGER.code }}>Field is required</div>
					)}
			</>
		);
	}

	if (formikField.values.category.value === 'single_select_url_parameter') {
		return (
			<>
				<FormGroup id='accepted' label='Accepted' className='mb-4'>
					<Select
						styles={react_select_styles(formikField, darkModeStatus, 'accepted')}
						theme={(theme) => react_select_themes(theme, darkModeStatus)}
						defaultValue={formikField.values.accepted}
						onBlur={formikField.handleBlur}
						options={listSelectUrl.filter((item) => item.is_parameter)}
						onChange={(value) => {
							formikField.setFieldValue('accepted', value);
						}}
						isDisabled={isReadOnly}
					/>
				</FormGroup>
				{typeof formikField.errors.accepted !== 'undefined' &&
					formikField.submitCount > 0 && (
						<div style={{ color: COLORS.DANGER.code }}>Field is required</div>
					)}
				{listKey.length > 0 && (
					<>
						<FormGroup id='parameter_key' label='Parameter Key' className='mb-4'>
							<Select
								styles={react_select_styles(
									formikField,
									darkModeStatus,
									'parameter_key',
								)}
								theme={(theme) => react_select_themes(theme, darkModeStatus)}
								defaultValue={formikField.values.parameter}
								onBlur={formikField.handleBlur}
								options={listKey}
								onChange={(value) => {
									formikField.setFieldValue('parameter_key', value);
								}}
								isDisabled={isReadOnly}
							/>
						</FormGroup>
						{typeof formikField.errors.accepted !== 'undefined' &&
							formikField.submitCount > 0 && (
								<div style={{ color: COLORS.DANGER.code }}>Field is required</div>
							)}
						<FormGroup id='parameter_value' label='Parameter Value' className='mb-4'>
							<Select
								styles={react_select_styles(
									formikField,
									darkModeStatus,
									'parameter_value',
								)}
								theme={(theme) => react_select_themes(theme, darkModeStatus)}
								defaultValue={formikField.values.parameter}
								onBlur={formikField.handleBlur}
								options={listKey}
								onChange={(value) => {
									formikField.setFieldValue('parameter_value', value);
								}}
								isDisabled={isReadOnly}
							/>
						</FormGroup>
						{typeof formikField.errors.accepted !== 'undefined' &&
							formikField.submitCount > 0 && (
								<div style={{ color: COLORS.DANGER.code }}>Field is required</div>
							)}
					</>
				)}
			</>
		);
	}

	if (
		formikField.values.category.value === 'single_select' ||
		formikField.values.category.value === 'checkbox'
	) {
		return (
			<>
				<FormGroup id='accepted' label='Accepted' className='mb-4'>
					<DynamicSelect
						data={accepted}
						select_items={select_items_accepted}
						onChange={onChangeData}
						readOnly={isReadOnly}
						isEdit={isReadOnly}
						errors={errors_loc_accepted}
						defaultOptions
						loadOptions={getLoad}
						placeholder='Choose ...'
						darkTheme={darkModeStatus}
					/>
				</FormGroup>
				{typeof formikField.errors.accepted !== 'undefined' &&
					formikField.submitCount > 0 && (
						<div style={{ color: COLORS.DANGER.code }}>Field is required</div>
					)}
			</>
		);
	}

	return null;
};

const FormExample = (dt) => {
	const { darkModeStatus } = useDarkMode();
	const {
		initialValues,
		handleReloadData,
		listSegment,
		listSelectUrl,
		onSelectUrlParameter,
		listKey,
	} = dt;

	const [isAlwaysReadOnly] = useState(true);
	const [isNeedAccepted, setNeedAccepted] = useState(true);
	const [isNeedParameter, setNeedParameter] = useState(true);

	let refSegment = useRef(0);
	let refTipeData = useRef(0);
	let refCategory = useRef(0);

	useEffect(() => {
		const checkIfNeedAccepted = (cat) => {
			if (!cat) {
				setNeedAccepted(false);
				setNeedParameter(false);
				return;
			}
			if (cat.value === 'single_select_url_parameter') {
				setNeedAccepted(true);
				setNeedParameter(true);
				return;
			}
			if (
				cat.value === 'single_select' ||
				cat.value === 'checkbox' ||
				cat.value === 'single_select_url' ||
				cat.value === 'button'
			) {
				setNeedAccepted(true);
				return;
			}
			setNeedAccepted(false);
			setNeedParameter(false);
		};
		checkIfNeedAccepted(initialValues.category);
	}, [initialValues]);

	const submitForm = (values, resetForm) => {
		if (
			values.segment &&
			values.field_name &&
			values.field_code &&
			values.tipe_data &&
			values.category
		) {
			const { username } = getRequester();
			const val = {};
			val.requester = username;
			val.field_name = values.field_name;
			val.field_code = values.field_code;
			val.tipe_data = values.tipe_data.value;
			val.category = values.category.value;
			val.segment = values.segment.label;
			val.segment_code = values.segment.value;
			val.accepted = values.accepted;
			val.parameter_key = values.parameter_key;
			val.parameter_value = values.parameter_value;

			return Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'info',
				showCancelButton: true,
				confirmButtonText: 'Yes',
			}).then((result) => {
				if (result.value) {
					handleSubmit(val, handleReloadData);
					const refs = [];
					refs.push(refSegment);
					refs.push(refTipeData);
					refs.push(refCategory);
					resetForm(initialValues);
					resetSelect(refs);
				} else if (result.dismiss === Swal.DismissReason.cancel) {
				}
			});
		}
		return values;
	};

	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		try {
			submitForm(values, resetForm);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	const validationSchema = yup.object({
		segment: yup.object({
			value: yup.string().required('Field is required'),
			label: yup.string().required('Field is required'),
		}),
		tipe_data: yup.object({
			value: yup.string().required('Field is required'),
			label: yup.string().required('Field is required'),
		}),
		category: yup.object({
			value: yup.string().required('Field is required'),
			label: yup.string().required('Field is required'),
		}),
		field_name: yup.string().required('Field is required'),
		field_code: yup.string().required('Field is required'),
		accepted: isNeedAccepted
			? yup.lazy((val) =>
					Array.isArray(val)
						? yup.array().of(yup.string().required('Field is required'))
						: yup.object({
								value: yup.string().required('Field is required'),
								label: yup.string().required('Field is required'),
						  }),
			  )
			: yup.mixed(),
		parameter_key: isNeedParameter
			? yup.lazy((val) =>
					Array.isArray(val)
						? yup.array().of(yup.string().required('Field is required'))
						: yup.object({
								value: yup.string().required('Field is required'),
								label: yup.string().required('Field is required'),
						  }),
			  )
			: yup.mixed(),
		parameter_value: isNeedParameter
			? yup.lazy((val) =>
					Array.isArray(val)
						? yup.array().of(yup.string().required('Field is required'))
						: yup.object({
								value: yup.string().required('Field is required'),
								label: yup.string().required('Field is required'),
						  }),
			  )
			: yup.mixed(),
	});

	return (
		<Formik
			enableReinitialize
			initialValues={{ ...initialValues }}
			onSubmit={onSubmit}
			validationSchema={validationSchema}>
			{(formikField) => {
				return (
					<Form>
						<FormGroup id='segment' label='Segment' className='mb-4'>
							<Select
								styles={react_select_styles(formikField, darkModeStatus, 'segment')}
								theme={(theme) => react_select_themes(theme, darkModeStatus)}
								defaultValue={formikField.values.segment}
								onBlur={formikField.handleBlur}
								options={listSegment}
								onChange={(value) => {
									formikField.setFieldValue('segment', value);
								}}
								ref={(ref) => {
									refSegment = ref;
									return refSegment;
								}}
							/>
						</FormGroup>
						{typeof formikField.errors.segment !== 'undefined' &&
							formikField.submitCount > 0 && (
								<div style={{ color: COLORS.DANGER.code }}>Field is required</div>
							)}

						<FormGroup id='field_name' label='Field Name' className='col-md-12'>
							<Input
								onChange={(val) => {
									formikField.handleChange(val);
									formikField.setFieldValue(
										'field_code',
										generateFieldCode(val.target.value),
									);
								}}
								onBlur={formikField.handleBlur}
								value={formikField.values.field_name}
								isValid={formikField.isValid}
								isTouched={formikField.touched.field_name}
								invalidFeedback={formikField.errors.field_name}
								autoComplete='off'
							/>
						</FormGroup>
						<FormGroup id='field_code' label='Field Code' className='col-md-12'>
							<Input
								onChange={formikField.handleChange}
								onBlur={formikField.handleBlur}
								value={formikField.values.field_code}
								isValid={formikField.isValid}
								isTouched={formikField.touched.field_code}
								invalidFeedback={formikField.errors.field_code}
								autoComplete='off'
								readOnly={isAlwaysReadOnly}
							/>
						</FormGroup>
						<FormGroup id='tipe_data' label='Tipe Data' className='mb-4'>
							<Select
								styles={react_select_styles(
									formikField,
									darkModeStatus,
									'tipe_data',
								)}
								theme={(theme) => react_select_themes(theme, darkModeStatus)}
								defaultValue={formikField.values.tipe_data}
								onBlur={formikField.handleBlur}
								options={tipe_data}
								onChange={(value) => {
									formikField.setFieldValue('tipe_data', value);
								}}
								ref={(ref) => {
									refTipeData = ref;
									return refTipeData;
								}}
							/>
						</FormGroup>
						{typeof formikField.errors.tipe_data !== 'undefined' &&
							formikField.submitCount > 0 && (
								<div style={{ color: COLORS.DANGER.code }}>Field is required</div>
							)}
						<FormGroup id='category' label='Category' className='mb-4'>
							<Select
								styles={react_select_styles(
									formikField,
									darkModeStatus,
									'category',
								)}
								theme={(theme) => react_select_themes(theme, darkModeStatus)}
								defaultValue={formikField.values.category}
								onBlur={formikField.handleBlur}
								options={category}
								onChange={(value) => {
									formikField.setFieldValue('category', value);
								}}
								ref={(ref) => {
									refCategory = ref;
									return refCategory;
								}}
							/>
						</FormGroup>
						{typeof formikField.errors.category !== 'undefined' &&
							formikField.submitCount > 0 && (
								<div style={{ color: COLORS.DANGER.code }}>Field is required</div>
							)}
						<FormAccepted
							formikField={formikField}
							listSelectUrl={listSelectUrl}
							listKey={listKey}
							onSelectUrlParameter={onSelectUrlParameter}
						/>

						<br />

						<div className='col-md-12 '>
							<Button
								icon='Save'
								isOutline
								type='submit'
								color='success'
								className='float-end'
								isDisable={!formikField.isValid && !!formikField.submitCount}>
								Submit
							</Button>
						</div>
					</Form>
				);
			}}
		</Formik>
	);
};

const resetSelect = (refs) => {
	refs.forEach((ref) => {
		ref.setValue({});
	});
};

const FormExampleEdit = (dt) => {
	const { darkModeStatus } = useDarkMode();
	const {
		initialValues,
		listSegment,
		listSelectUrl,
		handleReloadData,
		listKey,
		onSelectUrlParameter,
	} = dt;

	const [isOpen, setIsOpen] = useState(false);
	const [isReadOnly, setIsReadOnly] = useState(false);
	const [isAlwaysReadOnly] = useState(true);
	const [isNeedAccepted, setNeedAccepted] = useState(false);
	const [isNeedParameter, setNeedParameter] = useState(false);

	let refSegment = useRef(0);
	let refTipeData = useRef(0);
	let refCategory = useRef(0);

	useEffect(() => {
		const checkIfNeedAccepted = (cat) => {
			if (!cat) {
				setNeedAccepted(false);
				setNeedParameter(false);
				return;
			}
			if (cat.value === 'single_select_url_parameter') {
				setNeedAccepted(true);
				setNeedParameter(true);
				return;
			}
			if (
				cat.value === 'single_select' ||
				cat.value === 'checkbox' ||
				cat.value === 'single_select_url' ||
				cat.value === 'button'
			) {
				setNeedAccepted(true);
				return;
			}
			setNeedAccepted(false);
			setNeedParameter(false);
		};
		checkIfNeedAccepted(initialValues.category);
	}, [initialValues]);

	const submitForm = (values, resetForm) => {
		if (
			values.segment &&
			values.field_name &&
			values.field_code &&
			values.tipe_data &&
			values.category
		) {
			const { username } = getRequester();
			const val = {};
			val.requester = username;
			val.field_name = values.field_name;
			val.field_code = values.field_code;
			val.tipe_data = values.tipe_data.value;
			val.category = values.category.value;
			val.segment = values.segment.label;
			val.segment_code = values.segment.value;
			val.accepted = values.accepted;
			val.execute_date = values.execute_date;
			val._id = initialValues._id;
			val.parameter_key = values.parameter_key;
			val.parameter_value = values.parameter_value;

			return Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'info',
				showCancelButton: true,
				confirmButtonText: 'Yes',
			}).then((result) => {
				if (result.value) {
					handleSubmitEdit(val, handleReloadData);
					const refs = [];
					refs.push(refSegment);
					refs.push(refTipeData);
					refs.push(refCategory);
					resetForm(initialValues);
				} else if (result.dismiss === Swal.DismissReason.cancel) {
				}
			});
		}
		return values;
	};

	const deleteForm = (id) => {
		if (id) {
			const { username } = getRequester();
			const val = {};
			val._id = id;
			val.requester = username;
			return Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'info',
				showCancelButton: true,
				confirmButtonText: 'Yes',
			}).then((result) => {
				if (result.value) {
					handleDelete(val, handleReloadData);
				} else if (result.dismiss === Swal.DismissReason.cancel) {
				}
			});
		}
		return id;
	};

	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		try {
			delete values.loading;
			submitForm(values, resetForm);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	const validationSchema = yup.object({
		segment: yup.object({
			value: yup.string().required('Field is required'),
			label: yup.string().required('Field is required'),
		}),
		tipe_data: yup.object({
			value: yup.string().required('Field is required'),
			label: yup.string().required('Field is required'),
		}),
		category: yup.object({
			value: yup.string().required('Field is required'),
			label: yup.string().required('Field is required'),
		}),
		field_name: yup.string().required('Field is required'),
		field_code: yup.string().required('Field is required'),
		accepted: isNeedAccepted
			? yup.lazy((val) =>
					Array.isArray(val)
						? yup.array().of(yup.string().required('Field is required'))
						: yup.object({
								value: yup.string().required('Field is required'),
								label: yup.string().required('Field is required'),
						  }),
			  )
			: yup.mixed(),
		parameter_key: isNeedParameter
			? yup.lazy((val) =>
					Array.isArray(val)
						? yup.array().of(yup.string().required('Field is required'))
						: yup.object({
								value: yup.string().required('Field is required'),
								label: yup.string().required('Field is required'),
						  }),
			  )
			: yup.mixed(),
		parameter_value: isNeedParameter
			? yup.lazy((val) =>
					Array.isArray(val)
						? yup.array().of(yup.string().required('Field is required'))
						: yup.object({
								value: yup.string().required('Field is required'),
								label: yup.string().required('Field is required'),
						  }),
			  )
			: yup.mixed(),
		execute_date: yup.date().min(moment().toDate()).required('Field is required'),
	});

	return (
		<>
			<Button
				className='me-2'
				icon='GridOn'
				type='button'
				color='primary'
				onClick={() => {
					setIsOpen(true);
					setIsReadOnly(true);
				}}
			/>

			<Button
				className='me-2'
				icon='Edit'
				type='button'
				color='success'
				onClick={() => {
					setIsOpen(true);
					setIsReadOnly(false);
				}}
			/>

			<Button
				className='me-2'
				icon='Delete'
				type='button'
				color='danger'
				onClick={() => deleteForm(initialValues._id)}
			/>

			<Modal isOpen={isOpen} setIsOpen={setIsOpen} size='lg' titleId='modal-edit-menu-crud'>
				<ModalHeader setIsOpen={setIsOpen} className='p-4'>
					<ModalTitle id='modal-edit-menu-crud'>
						{isReadOnly ? 'View Data' : 'Update Data'}
					</ModalTitle>
				</ModalHeader>
				<Formik
					enableReinitialize
					initialValues={{ ...initialValues }}
					onSubmit={onSubmit}
					validationSchema={validationSchema}>
					{(formikField) => {
						return (
							<Form>
								<ModalBody className='px-4'>
									<div className='col-md-12'>
										<FormGroup id='segment' label='Segment' className='mb-4'>
											<CustomSelect
												isDisabled={isAlwaysReadOnly}
												options={listSegment}
												defaultValue={formikField.values.segment}
												value={formikField.values.segment}
												darkTheme={darkModeStatus}
												onChange={(value) => {
													formikField.setFieldValue('segment', value);
												}}
												isValid={
													typeof formikField.errors.segment ===
													'undefined'
												}
												ref={(ref) => {
													refSegment = ref;
													return refSegment;
												}}
											/>
										</FormGroup>
										{typeof formikField.errors.segment !== 'undefined' &&
											formikField.submitCount > 0 && (
												<div style={{ color: COLORS.DANGER.code }}>
													Field is required
												</div>
											)}
										<FormGroup
											id='field_name'
											label='Field Name'
											className='col-md-12'>
											<Input
												onChange={(val) => {
													formikField.handleChange(val);
													formikField.setFieldValue(
														'field_code',
														generateFieldCode(val.target.value),
													);
												}}
												onBlur={formikField.handleBlur}
												value={formikField.values.field_name}
												isValid={formikField.isValid}
												isTouched={formikField.touched.field_name}
												invalidFeedback={formikField.errors.field_name}
												autoComplete='off'
												readOnly={isAlwaysReadOnly}
											/>
										</FormGroup>
										<FormGroup
											id='field_code'
											label='Field Code'
											className='col-md-12'>
											<Input
												onChange={formikField.handleChange}
												onBlur={formikField.handleBlur}
												value={formikField.values.field_code}
												isValid={formikField.isValid}
												isTouched={formikField.touched.field_code}
												invalidFeedback={formikField.errors.field_code}
												autoComplete='off'
												readOnly={isAlwaysReadOnly}
											/>
										</FormGroup>
										<FormGroup
											id='tipe_data'
											label='Tipe Data'
											className='mb-4'>
											<CustomSelect
												isDisabled={isReadOnly}
												options={tipe_data}
												defaultValue={formikField.values.tipe_data}
												value={formikField.values.tipe_data}
												darkTheme={darkModeStatus}
												onChange={(value) => {
													formikField.setFieldValue('tipe_data', value);
												}}
												isValid={
													typeof formikField.errors.tipe_data ===
													'undefined'
												}
												ref={(ref) => {
													refTipeData = ref;
													return refTipeData;
												}}
											/>
										</FormGroup>
										{typeof formikField.errors.tipe_data !== 'undefined' &&
											formikField.submitCount > 0 && (
												<div style={{ color: COLORS.DANGER.code }}>
													Field is required
												</div>
											)}
										<FormGroup id='category' label='Category' className='mb-4'>
											<CustomSelect
												isDisabled={isReadOnly}
												options={category}
												defaultValue={formikField.values.category}
												value={formikField.values.category}
												darkTheme={darkModeStatus}
												onChange={(value) => {
													formikField.setFieldValue('category', value);
												}}
												isValid={
													typeof formikField.errors.category ===
													'undefined'
												}
												ref={(ref) => {
													refCategory = ref;
													return refCategory;
												}}
											/>
										</FormGroup>
										{typeof formikField.errors.category !== 'undefined' &&
											formikField.submitCount > 0 && (
												<div style={{ color: COLORS.DANGER.code }}>
													Field is required
												</div>
											)}
										<FormAccepted
											formikField={formikField}
											listSelectUrl={listSelectUrl}
											listKey={listKey}
											onSelectUrlParameter={onSelectUrlParameter}
											isReadOnly={isReadOnly}
										/>
										{!isReadOnly && (
											<FormGroup id='execute_date' label='Execute Date'>
												<Input
													type='date'
													onChange={formikField.handleChange}
													onInput={(value) => {
														formikField.setFieldValue(
															'execute_date',
															value,
														);
													}}
													onBlur={formikField.handleBlur}
													value={formikField.values.execute_date}
													isValid={formikField.isValid}
													isTouched={formikField.touched.execute_date}
													invalidFeedback={
														formikField.errors.execute_date
													}
													min={moment()
														.add(1, 'days')
														.format('YYYY-MM-DD')}
													autoComplete='off'
												/>
											</FormGroup>
										)}
									</div>
								</ModalBody>
								{!isReadOnly && (
									<ModalFooter className='px-4 pb-4'>
										<div className='col-md-12 '>
											<Button
												icon='Save'
												type='submit'
												color='success'
												className='float-end'
												isDisable={
													!formikField.isValid &&
													!!formikField.submitCount
												}>
												Update
											</Button>
										</div>
									</ModalFooter>
								)}
							</Form>
						);
					}}
				</Formik>
			</Modal>
		</>
	);
};

const CustomButton = (dt) => {
	const { row, listSelectUrl, listSegment, handleReloadData, listKey, onSelectUrlParameter } = dt;

	// restructure the obj
	const initialValues = {};
	const country = {};
	const company = {};
	const segment = {};
	const tipeData = {};
	const field = {};
	const cat = {};
	let accepted = {};
	let parameter_key = {};
	let parameter_value = {};

	company.label = row.company_name;
	company.value = row.company_code;
	country.label = `(${row.country_code}) ${row.country_name}`;
	country.value = row.country_name;

	segment.value = row.segment_code;
	segment.label = row.segment;

	field.value = row.field_code;
	field.label = row.field_name;

	tipeData.value = row.tipe_data;
	tipeData.label = row.tipe_data;

	cat.value = row.category;
	cat.label = row.category;

	accepted = row.accepted;
	parameter_key = row.parameter_key;
	parameter_value = row.parameter_value;

	initialValues.id = row._id;

	initialValues.segment = segment;
	initialValues.tipe_data = tipeData;
	initialValues.category = cat;
	initialValues.field_code = row.field_code;
	initialValues.field_name = row.field_name;
	initialValues._id = row._id;
	initialValues.execute_date = moment().add(1, 'days').format('YYYY-MM-DD');
	initialValues.accepted = accepted;
	initialValues.parameter_key = parameter_key;
	initialValues.parameter_value = parameter_value;

	return (
		<FormExampleEdit
			initialValues={initialValues}
			listSegment={listSegment}
			listSelectUrl={listSelectUrl}
			handleReloadData={handleReloadData}
			onSelectUrlParameter={onSelectUrlParameter}
			listKey={listKey}
			row={row}
		/>
	);
};

const CustomDataTable = ({
	listSegment,
	listSelectUrl,
	listFunction,
	data,
	loading,
	totalRows,
	handlePageChange,
	handlePerRowsChange,
	handleReloadData,
	onSelectUrlParameter,
	listKey,
}) => {
	const { darkModeStatus } = useDarkMode();

	const columns = useMemo(
		() => [
			{
				name: 'Created At',
				selector: (row) => moment(row.created_at).format('YYYY-MM-DD'),
				sortable: true,
			},
			{
				name: 'Field Name',
				selector: (row) => row.field_name,
				sortable: true,
			},
			{
				name: 'Segment',
				selector: (row) => row.segment,
				sortable: true,
			},
			{
				name: 'Tipe Data',
				selector: (row) => row.tipe_data,
				sortable: true,
			},
			{
				name: 'Category',
				selector: (row) => row.category,
				sortable: true,
			},
			{
				name: 'Action',
				// eslint-disable-next-line react/no-unstable-nested-components
				cell: (dt) => {
					return (
						<CustomButton
							row={dt}
							listSegment={listSegment}
							listSelectUrl={listSelectUrl}
							listKey={listKey}
							listFunction={listFunction}
							handleReloadData={handleReloadData}
							onSelectUrlParameter={onSelectUrlParameter}
						/>
					);
				},
			},
		],
		[handleReloadData, listFunction, listSelectUrl, listSegment, onSelectUrlParameter, listKey],
	);

	return (
		<DarkDataTable
			columns={columns}
			data={data}
			progressPending={loading}
			pagination
			paginationServer
			paginationTotalRows={totalRows}
			onChangeRowsPerPage={handlePerRowsChange}
			onChangePage={handlePageChange}
			theme={darkModeStatus ? 'custom_dark' : 'light'}
		/>
	);
};
const Segment = () => {
	const [data, setData] = useState([]);
	const [listSegment, setSegment] = useState([]);
	const [listSelectUrl, setSelectUrl] = useState([]);
	const [listFunction, setFunction] = useState([]);
	const [listKey, setKey] = useState([]);

	const [loading, setLoading] = useState(false);
	const [totalRows, setTotalRows] = useState(0);
	const [perPage, setPerPage] = useState(10);
	const [page, setPage] = useState(1);
	const [reloadData, setReloadData] = useState(false);
	const { username } = getRequester();

	const fetchFieldInput = async () => {
		const { default_segment_code } = getaActiveRoles();
		setLoading(true);
		return FieldInputModule.read(
			`page=${page}&sizePerPage=${perPage}&segment_code=${default_segment_code}`,
		).then((res) => {
			setData(res.foundData);
			setTotalRows(res.countData);
			setLoading(false);
		});
	};

	const fetchSegmentUser = async () => {
		return SegmentModule.readSelectSegmentUser(username).then((res) => {
			setSegment(res);
		});
	};

	const fetchSelectUrl = async () => {
		return SelectUrlModule.readSelect().then((res) => {
			setSelectUrl(res);
		});
	};

	const fetchKey = async (url) => {
		return KeyModule.read(url).then((res) => {
			setKey(res);
		});
	};

	const fetchFunction = async () => {
		return FunctionModule.readSelect().then((res) => {
			setFunction(res);
		});
	};

	const handleReloadData = () => {
		setReloadData(!reloadData);
	};
	const initialValues = {
		loading: false,
		segment: null,
		field_code: '',
		field_name: '',
		tipe_data: null,
		category: null,
		accepted: null,
		parameter: null,
	};

	useEffect(() => {
		fetchFieldInput();
		fetchSegmentUser();
		fetchSelectUrl();
		fetchFunction();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [page, perPage, reloadData]);

	useEffect(() => {
		fetchFieldInput();
		fetchSegmentUser();
		fetchSelectUrl();
		fetchFunction();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const { t } = useTranslation('field-input');
	const [title] = useState({ title: 'Field Input' });
	return (
		<PageWrapper title={title.title}>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card className='col-md-4'>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('Field Input')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<div className='col-12 mb-3 g-4'>
							<FormExample
								listSegment={listSegment}
								initialValues={initialValues}
								listSelectUrl={listSelectUrl}
								listKey={listKey}
								handleReloadData={handleReloadData}
								onSelectUrlParameter={(select) => select && fetchKey(select.url)}
							/>
						</div>
					</CardBody>
				</Card>
				<Card stretch>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('Historical Data')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<CustomDataTable
							data={data}
							loading={loading}
							totalRows={totalRows}
							listFunction={listFunction}
							listSelectUrl={listSelectUrl}
							listKey={listKey}
							listSegment={listSegment}
							handlePageChange={setPage}
							handlePerRowsChange={setPerPage}
							handleReloadData={handleReloadData}
							onSelectUrlParameter={(select) => select && fetchKey(select.url)}
						/>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

CustomDataTable.propTypes = {
	listSelectUrl: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	listFunction: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	listSegment: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	listKey: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	data: PropTypes.arrayOf(PropTypes.instanceOf(Object)),
	loading: PropTypes.bool,
	totalRows: PropTypes.number,
	handlePageChange: PropTypes.func,
	handlePerRowsChange: PropTypes.func,
	handleReloadData: PropTypes.func,
	onSelectUrlParameter: PropTypes.func,
};
CustomDataTable.defaultProps = {
	listSelectUrl: {},
	listFunction: {},
	listSegment: {},
	listKey: [],
	data: [],
	loading: false,
	totalRows: 0,
	handlePageChange: null,
	handlePerRowsChange: null,
	handleReloadData: null,
	onSelectUrlParameter: () => {},
};

FormAccepted.propTypes = {
	formikField: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	listSelectUrl: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	onSelectUrlParameter: PropTypes.func,
	listKey: PropTypes.oneOfType([PropTypes.instanceOf(Object)]),
	isReadOnly: PropTypes.bool,
};

FormAccepted.defaultProps = {
	formikField: null,
	listSelectUrl: null,
	onSelectUrlParameter: () => {},
	listKey: [],
	isReadOnly: false,
};

export default Segment;
