import React, { useState, useEffect, useRef } from 'react';
import { Formik, Form } from 'formik';
import * as yup from 'yup';
import Swal from 'sweetalert2';
import { useTranslation } from 'react-i18next';
import FileDownload from 'js-file-download';
import moment from 'moment';

import PageWrapper from '../layout/PageWrapper/PageWrapper';
import Page from '../layout/Page/Page';
import PageLayoutHeader from '../pages/common/Headers/PageLayoutHeader';
import Card, { CardBody, CardHeader, CardLabel, CardTitle } from '../components/bootstrap/Card';
import FormGroup from '../components/bootstrap/forms/FormGroup';
import Button from '../components/bootstrap/Button';
import Spinner from '../components/bootstrap/Spinner';
import Input from '../components/bootstrap/forms/Input';
import ExportExcelModule from '../modules/ExportExcelModule';
import showNotification from '../components/extras/showNotification';
import useDarkMode from '../hooks/useDarkMode';
import AreaModule from '../modules/AreaModule';
import CityModule from '../modules/CityModule';
import COLORS from '../common/data/enumColors';
import CustomSelect from '../components/CustomSelect';

import { getRequester, getaActiveRoles } from '../helpers/helpers';

const FormExample = (dt) => {
	const { darkModeStatus } = useDarkMode();
	const { initialValues, handleReloadData, listArea, listCity, onChangeArea } = dt;
	const { username } = getRequester();
	const { default_segment_code } = getaActiveRoles();

	const [loading, setLoading] = useState(false);

	let refArea = useRef(0);
	let refCity = useRef(0);

	const submitForm = (values, resetForm) => {
		if (values.area && values.date_start && values.date_end) {
			const val = {};
			val.requester = username;
			val.segment_code = default_segment_code;
			val.area = values.area;
			val.city = values.city;
			val.date_start = values.date_start;
			val.date_end = values.date_end;

			return Swal.fire({
				title: 'Are you sure?',
				text: 'Please check your entries !',
				icon: 'info',
				showCancelButton: true,
				confirmButtonText: 'Yes',
			}).then((result) => {
				if (result.value) {
					setLoading(true);
					checkExportAvailable(val);
					const refs = [];
					refs.push(refArea);
					refs.push(refCity);
					resetForm(initialValues);
					resetSelect(refs);
				} else if (result.dismiss === Swal.DismissReason.cancel) {
				}
			});
		}
		return values;
	};

	const handleSubmit = (values) => {
		let query = '';
		if (values.city) {
			query = `requester=${values.requester}&segment_code=${values.segment_code}&area=${values.area.value}&city=${values.city.value}&date_start=${values.date_start}&date_end=${values.date_end}`;
		} else {
			query = `requester=${values.requester}&segment_code=${values.segment_code}&area=${values.area.value}&date_start=${values.date_start}&date_end=${values.date_end}`;
		}
		ExportExcelModule.export_excel(query)
			.then((res) => {
				const { default_segment_name } = getaActiveRoles();
				showNotification('Success!', res.status, 'success');
				FileDownload(
					res.data,
					`JourneyPlan_${default_segment_name}_${values.area.label}_${
						values.city ? values.city.label : ''
					}.xlsx`,
				);
				handleReloadData();
			})
			.catch((err) => {
				setLoading(false);
				showNotification('Warning!', err, 'danger');
				Swal.fire({
					title: err,
					icon: 'error',
					showCancelButton: true,
					showConfirmButton: false,
					cancelButtonText: 'Close',
				});
			})
			.finally(() => {
				setLoading(false);
			});
		return values;
	};

	const checkExportAvailable = (values) => {
		let query = '';
		if (values.city) {
			query = `requester=${values.requester}&segment_code=${values.segment_code}&area=${values.area.value}&city=${values.city.value}&date_start=${values.date_start}&date_end=${values.date_end}`;
		} else {
			query = `requester=${values.requester}&segment_code=${values.segment_code}&area=${values.area.value}&date_start=${values.date_start}&date_end=${values.date_end}`;
		}
		ExportExcelModule.check_export_excel(query)
			.then(() => {
				handleSubmit(values);
			})
			.catch((err) => {
				setLoading(false);
				showNotification('Warning!', err, 'danger');
				Swal.fire({
					title: err,
					icon: 'error',
					showCancelButton: true,
					showConfirmButton: false,
					cancelButtonText: 'Close',
				});
			});
		return values;
	};

	const onSubmit = (values, { setSubmitting, resetForm, setStatus, setErrors }) => {
		try {
			submitForm(values, resetForm);
			setStatus({ success: true });
		} catch (error) {
			setStatus({ success: false });
			setSubmitting(false);
			setErrors({ submit: error.message });
		}
	};

	const validationSchema = yup.object({
		area: yup.object({
			value: yup.string().required('Field is required'),
			label: yup.string().required('Field is required'),
		}),
		date_start: yup.string().required('Field is required'),
		date_end: yup.string().required('Field is required'),
	});

	return (
		<Formik
			enableReinitialize
			initialValues={{ ...initialValues }}
			onSubmit={onSubmit}
			validationSchema={validationSchema}>
			{(formikField) => {
				return (
					<Form>
						<FormGroup id='date_start' label='Date Start' className='mb-4'>
							<Input
								onChange={formikField.handleChange}
								onBlur={formikField.handleBlur}
								defaultValue={formikField.values.date_start}
								value={formikField.values.date_start}
								isValid={formikField.isValid}
								isTouched={formikField.touched.date_start}
								invalidFeedback={formikField.errors.date_start}
								autoComplete='off'
								type='date'
							/>
						</FormGroup>
						<FormGroup id='date_end' label='Date End' className='mb-4'>
							<Input
								onChange={formikField.handleChange}
								onBlur={formikField.handleBlur}
								defaultValue={formikField.values.date_end}
								value={formikField.values.date_end}
								isValid={formikField.isValid}
								isTouched={formikField.touched.date_end}
								invalidFeedback={formikField.errors.date_end}
								autoComplete='off'
								type='date'
								max={moment(formikField.values.date_start).add(31, 'days').toDate()}
							/>
						</FormGroup>
						<FormGroup id='area' label='Area' className='mb-4'>
							<CustomSelect
								options={listArea}
								defaultValue={formikField.values.area}
								value={formikField.values.area}
								isClearable
								isSearchable
								darkTheme={darkModeStatus}
								placeholder='Area'
								onChange={(value) => {
									formikField.setFieldValue('area', value);
									onChangeArea(value);
								}}
								ref={(ref) => {
									refArea = ref;
									return refArea;
								}}
							/>
						</FormGroup>
						{typeof formikField.errors.area !== 'undefined' &&
							formikField.submitCount > 0 && (
								<div style={{ color: COLORS.DANGER.code }}>Field is required</div>
							)}
						<FormGroup id='city' label='City' className='mb-4'>
							<CustomSelect
								options={listCity}
								defaultValue={formikField.values.city}
								value={formikField.values.city}
								isClearable
								isSearchable
								darkTheme={darkModeStatus}
								placeholder='City'
								onChange={(value) => {
									formikField.setFieldValue('city', value);
								}}
								ref={(ref) => {
									refCity = ref;
									return refCity;
								}}
								isDisabled={formikField.values.area === null}
							/>
						</FormGroup>
						{typeof formikField.errors.city !== 'undefined' &&
							formikField.submitCount > 0 && (
								<div style={{ color: COLORS.DANGER.code }}>Field is required</div>
							)}

						<br />

						{(formikField.values.area || loading) && (
							<div className='col-md-12 '>
								<Button
									icon={!loading && 'Download'}
									isOutline
									type='submit'
									color='success'
									className='float-end'
									isDisable={
										(!formikField.isValid && !!formikField.submitCount) ||
										loading
									}>
									{loading && <Spinner inButton isSmall />}
									Export
								</Button>
							</div>
						)}
					</Form>
				);
			}}
		</Formik>
	);
};

const resetSelect = (refs) => {
	refs.forEach((ref) => {
		if (ref) {
			ref.setValue({});
		}
	});
};

const ExportExcel = () => {
	const [listArea, setArea] = useState([]);
	const [listCity, setCity] = useState([]);
	const [pickedArea, setPickedArea] = useState(null);
	const [reloadData, setReloadData] = useState(false);
	const { username } = getRequester();
	const { default_segment_code } = getaActiveRoles();

	const fetchArea = async () => {
		const query = `requester=${username}&username=${username}&segment_code=${default_segment_code}`;
		return AreaModule.readSelectByUser(query).then((res) => {
			setArea(res);
		});
	};

	const fetchCity = async () => {
		const query = `requester=${username}&username=${username}&segment_code=${default_segment_code}&area_code=${pickedArea}`;
		return CityModule.readSelectByUser(query).then((res) => {
			setCity(res);
		});
	};
	const handleReloadData = () => {
		setReloadData(!reloadData);
	};
	const initialValues = {
		loading: false,
		area: null,
		city: null,
		date_start: moment().format('YYYY-MM-DD'),
		date_end: moment().format('YYYY-MM-DD'),
	};

	useEffect(() => {
		fetchArea();
		fetchCity();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	useEffect(() => {
		fetchArea();
		fetchCity();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [reloadData]);

	useEffect(() => {
		if (pickedArea) {
			fetchCity();
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [pickedArea]);

	const onChangeArea = (val) => {
		setPickedArea(val.value);
	};

	const { t } = useTranslation('field-input');
	const [title] = useState({ title: 'Export Excel' });
	return (
		<PageWrapper title={title.title}>
			<PageLayoutHeader />
			<Page container='fluid'>
				<Card className='col-md-4'>
					<CardHeader borderSize={1}>
						<CardLabel>
							<CardTitle>{t('Export Excel')}</CardTitle>
						</CardLabel>
					</CardHeader>
					<CardBody>
						<div className='col-12 mb-3 g-4'>
							<FormExample
								listArea={listArea}
								listCity={listCity}
								initialValues={initialValues}
								handleReloadData={handleReloadData}
								onChangeArea={onChangeArea}
							/>
						</div>
					</CardBody>
				</Card>
			</Page>
		</PageWrapper>
	);
};

export default ExportExcel;
