import axios from 'axios';
import authHeader from './auth-header';

const API_URL_DEFAULT = process.env.REACT_APP_API;

const export_excel = async (payload) => {
	return axios.get(`${API_URL_DEFAULT}export-excel?${payload}`, {
		headers: await authHeader(),
		responseType: 'blob',
	});
};

const check_export_excel = async (payload) => {
	return axios.get(`${API_URL_DEFAULT}export-excel/check/?${payload}`, {
		headers: await authHeader(),
	});
};

export default { check_export_excel, export_excel };
