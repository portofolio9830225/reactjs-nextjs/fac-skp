import axios from 'axios';
import authHeader from './auth-header';

const API_URL_DEFAULT = process.env.REACT_APP_API;

const create = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}activity`, payload, { headers: await authHeader() });
};

const read = async (query_string) => {
	return axios.get(`${API_URL_DEFAULT}activity/?${query_string}`, {
		headers: await authHeader(),
	});
};

const readSelect = async (query_string) => {
	return axios.get(`${API_URL_DEFAULT}activity/select/?${query_string}`, {
		headers: await authHeader(),
	});
};

const update = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}activity`, payload, { headers: await authHeader() });
};

const delete_ = async (payload) => {
	return axios.delete(`${API_URL_DEFAULT}activity`, {
		data: payload,
		headers: await authHeader(),
	});
};

export default { create, read, update, delete_, readSelect };
