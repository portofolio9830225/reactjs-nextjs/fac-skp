// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
import axios from 'axios';
import authHeader from '../auth-header';

// const modelName = 'report';
const API_URL_DEFAULT = process.env.REACT_APP_API;

const dataTable = async (year) => {
	return axios.get(`${API_URL_DEFAULT}report/summary_all/?date=${year}-12-01`, {
		headers: await authHeader(),
	});
};

const detailSummaryExpense = async (query_string) => {
	return axios.get(`${API_URL_DEFAULT}report-expsense/tt?${query_string}`, {
		headers: await authHeader(),
	});
};

export default {
    dataTable,
	detailSummaryExpense
}