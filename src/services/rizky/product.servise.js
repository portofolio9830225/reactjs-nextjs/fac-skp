// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
import axios from 'axios';
import authHeader from '../auth-header';

const modelName = 'products';
const API_URL_DEFAULT = process.env.REACT_APP_API;

const listProduct = async () => {
	return axios.get(`${API_URL_DEFAULT}${modelName}`, {
		headers: await authHeader(),
	});
};

export default {
	listProduct,
};