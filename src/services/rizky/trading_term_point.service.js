// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
import axios from 'axios';
import authHeader from '../auth-header';

const modelName = 'trading-term-points';
const API_URL_DEFAULT = process.env.REACT_APP_API;

const list = async (query_string) => {
	return axios.get(`${API_URL_DEFAULT}${modelName}/?${query_string}`, {
		headers: await authHeader(),
	});
};

const create = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}${modelName}`, payload, { headers: await authHeader() });
};

const listSelected = async () => {
	return axios.get(`${API_URL_DEFAULT}${modelName}/list`, { headers: await authHeader() });
};

const listYear = async () => {
	return axios.get(`${API_URL_DEFAULT}${modelName}/listyear`, {
		headers: await authHeader(),
	});
};

const checkStore = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}${modelName}/check-store`, payload, {
		headers: await authHeader(),
	});
};

const update = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}${modelName}`, payload, { headers: await authHeader() });
};

const destroy = async (payload) => {
	return axios.delete(`${API_URL_DEFAULT}${modelName}`, {
		data: payload,
		headers: await authHeader(),
	});
};

export default {
	list,
	create,
	update,
	destroy,
	checkStore,
	listSelected,
	listYear,
};
