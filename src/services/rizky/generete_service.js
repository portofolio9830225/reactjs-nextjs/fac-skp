// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
import axios from 'axios';
import authHeader from '../auth-header';

const modelName = 'realisation';
const API_URL_DEFAULT = process.env.REACT_APP_API;

const sell_in = async (query_string) => {
	return axios.get(`${API_URL_DEFAULT}${modelName}/sell_in/?${query_string}`, {
		headers: await authHeader(),
	});
}

const skp_tt = async (query_string) => {
	return axios.get(`${API_URL_DEFAULT}${modelName}/skp-tt/?${query_string}`, {
		headers: await authHeader(),
	});
}

export default { sell_in, skp_tt }
