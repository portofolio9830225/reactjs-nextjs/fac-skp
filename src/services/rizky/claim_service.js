// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
import axios from 'axios';
import authHeader from '../auth-header';

const modelName = 'claims';
const API_URL_DEFAULT = process.env.REACT_APP_API;

const tableSkp = async (query_string) => {
	return axios.get(`${API_URL_DEFAULT}${modelName}/skp/?${query_string}`, {
		headers: await authHeader(),
	});
};

const tableTTP = async (query_string) => {
	return axios.get(`${API_URL_DEFAULT}${modelName}/ttp/?${query_string}`, {
		headers: await authHeader(),
	});
};

const create = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}${modelName}`, payload, { headers: await authHeader() });
};

const update = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}${modelName}`, payload, { headers: await authHeader() });
};

const destroy = async (payload) => {
	return axios.delete(`${API_URL_DEFAULT}${modelName}`, {
		data: payload,
		headers: await authHeader(),
	});
};

export default {
	tableSkp,
	tableTTP,
    create,
	update,
	destroy,
};
