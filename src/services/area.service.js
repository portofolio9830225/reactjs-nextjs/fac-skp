import axios from 'axios';
import authHeader from './auth-header';

const API_URL_DEFAULT = process.env.REACT_APP_API;

const create = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}area`, payload, {
		headers: await authHeader(),
	});
};
const read = async (query) => {
	return axios.get(`${API_URL_DEFAULT}area/?${query}`, {
		headers: await authHeader(),
	});
};
const readSelect = async (query) => {
	return axios.get(`${API_URL_DEFAULT}area/select?${query}`, {
		headers: await authHeader(),
	});
};
const readSelectCity = async (query) => {
	return axios.get(`${API_URL_DEFAULT}area/select/city?${query}`, {
		headers: await authHeader(),
	});
};
const readSelectByUser = async (query) => {
	return axios.get(`${API_URL_DEFAULT}area/select/access?${query}`, {
		headers: await authHeader(),
	});
};
const edit = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}area`, payload, {
		headers: await authHeader(),
	});
};

const delete_ = async (payload) => {
	return axios.delete(`${API_URL_DEFAULT}area`, {
		data: payload,
		headers: await authHeader(),
	});
};

export default {
	create,
	read,
	readSelect,
	readSelectCity,
	readSelectByUser,
	edit,
	delete_,
};
