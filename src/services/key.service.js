import axios from 'axios';
import authHeader from './auth-header';

const API_URL_DEFAULT = process.env.REACT_APP_API;

const read = async (url) => {
	return axios.get(`${API_URL_DEFAULT}${url}/key`, {
		headers: await authHeader(),
	});
};

export default {
	read,
};
