import axios from 'axios';
import authHeader from './auth-header';

const API_URL_DEFAULT = process.env.REACT_APP_API;

const create = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}parameter_checklist`, payload, {
		headers: await authHeader(),
	});
};
const read = async (query_string) => {
	return axios.get(`${API_URL_DEFAULT}parameter_checklist/?${query_string}`, {
		headers: await authHeader(),
	});
};
const edit = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}parameter_checklist`, payload, {
		headers: await authHeader(),
	});
};

const deleteData = async (payload) => {
	return axios.delete(`${API_URL_DEFAULT}parameter_checklist`, {
		data: payload,
		headers: await authHeader(),
	});
};

export default {
	create,
	read,
	edit,
	deleteData,
};
