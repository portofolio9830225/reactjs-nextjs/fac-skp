import axios from 'axios';
import authHeader from '../auth-header';

const API_URL_DEFAULT = process.env.REACT_APP_API;
const modelName = 'claims';

const downloadExcel = async (payload) => {
	return axios.get(`${API_URL_DEFAULT}${modelName}/download-excel?${payload}`, {
		headers: await authHeader(),
		responseType: 'blob',
	});
};

const importExcel = async (type, payload) => {
	return axios.post(
		`${API_URL_DEFAULT}${modelName}/import-excel`,
		{
			data: {
				type,
				payload,
			},
		},
		{
			headers: await authHeader(),
		},
	);
};

export default { downloadExcel, importExcel };
