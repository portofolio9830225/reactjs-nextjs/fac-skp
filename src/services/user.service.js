import axios from 'axios';
import authHeader from './auth-header';

const API_URL_DEFAULT = process.env.REACT_APP_API;

const getAllDepartment = async () => {
	return axios.get(`${API_URL_DEFAULT}user/department`, { headers: await authHeader() });
};

const getUserByDept = async (payload) => {
	return axios.get(`${API_URL_DEFAULT}user/detail?${payload}`, { headers: await authHeader() });
};

const getDownline = async (payload) => {
	return axios.get(`${API_URL_DEFAULT}user/downline?${payload}`, { headers: await authHeader() });
};

const getDownlineSelect = async (payload) => {
	return axios.get(`${API_URL_DEFAULT}user/downline/select?${payload}`, {
		headers: await authHeader(),
	});
};

const getRolebyParent = async (payload) => {
	return axios.get(`${API_URL_DEFAULT}user/role_by_parent?${payload}`, {
		headers: await authHeader(),
	});
};

const create = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}user`, payload, { headers: await authHeader() });
};

const read = async (query_string) => {
	return axios.get(`${API_URL_DEFAULT}user?${query_string}`, {
		headers: await authHeader(),
	});
};

const readSelect = async (query_string) => {
	return axios.get(`${API_URL_DEFAULT}user/select?${query_string}`, {
		headers: await authHeader(),
	});
};

const update = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}user`, payload, { headers: await authHeader() });
};

const destroy = async (payload) => {
	return axios.delete(`${API_URL_DEFAULT}user`, {
		data: payload,
		headers: await authHeader(),
	});
};

const findUser = async (query_string) => {
	return axios.get(`${API_URL_DEFAULT}user/find?${query_string}`, {
		headers: await authHeader(),
	});
};

export default {
	getAllDepartment,
	getUserByDept,
	getDownline,
	getDownlineSelect,
	getRolebyParent,
	create,
	read,
	readSelect,
	update,
	destroy,
	findUser,
};
