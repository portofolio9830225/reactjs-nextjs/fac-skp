// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
import axios from 'axios';
import authHeader from '../auth-header';

const modelName = 'report/summary_account';
const modelNameYear = 'parameter/year';
const API_URL_DEFAULT = process.env.REACT_APP_API;

const list = async (query_string) => {
	return axios.get(`${API_URL_DEFAULT}${modelName}/?${query_string}`, {
		headers: await authHeader(),
	});
}

const year = async () => {
	return axios.get(`${API_URL_DEFAULT}${modelNameYear}`, {
		headers: await authHeader(),
	});
}

export default { list, year }
