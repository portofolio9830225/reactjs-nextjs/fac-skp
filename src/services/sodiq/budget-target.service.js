// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
import axios from 'axios';
import authHeader from '../auth-header';

const modelName = 'budget-target';
const API_URL_DEFAULT = process.env.REACT_APP_API;

const list = async (query_string) => {
	return axios.get(`${API_URL_DEFAULT}${modelName}/?${query_string}`, {
		headers: await authHeader(),
	});
};

const create = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}${modelName}`, payload, { headers: await authHeader() });
};

const donwload = async (query_string) => {
	return axios.get(`${API_URL_DEFAULT}${modelName}/download?${query_string}`, {
		headers: await authHeader(),
		responseType: 'arraybuffer'
	});
};

export default {
	list,
	create,
	donwload
};
