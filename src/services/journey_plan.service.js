import axios from 'axios';
import authHeader from './auth-header';

const API_URL_DEFAULT = process.env.REACT_APP_API;

const create = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}journey_plan`, payload, {
		headers: await authHeader(),
	});
};
const createMultiple = async (payload) => {
	return axios.post(`${API_URL_DEFAULT}journey_plan/multiple`, payload, {
		headers: await authHeader(),
	});
};
const read = async (query_string) => {
	return axios.get(`${API_URL_DEFAULT}journey_plan/?${query_string}`, {
		headers: await authHeader(),
	});
};
const edit = async (payload) => {
	return axios.put(`${API_URL_DEFAULT}journey_plan`, payload, {
		headers: await authHeader(),
	});
};

const delete_ = async (payload) => {
	return axios.delete(`${API_URL_DEFAULT}journey_plan`, {
		data: payload,
		headers: await authHeader(),
	});
};

export default {
	create,
	createMultiple,
	read,
	edit,
	delete_,
};
