import React, { lazy } from 'react';
import { layoutMenu } from '../menu';
import Login from '../pages/presentation/auth/Login';

const PAGE_LAYOUTS = {
	HEADER: lazy(() => import('../pages/presentation/page-layouts/OnlyHeader')),
};

const presentation = [
	{
		path: 'login',
		element: <Login />,
		exact: true,
	},
	{
		path: layoutMenu.pageLayout.subMenu.onlyHeader.path,
		element: <PAGE_LAYOUTS.HEADER />,
		exact: true,
	},
];
const documentation = [];
const contents = [...presentation, ...documentation];

export default contents;
