import productServise from "../../services/rizky/product.servise";

const listProduct = () => {
	return productServise.listProduct().then(
		(response) => {
			return Promise.resolve(response.data, response.data.message);
		},
		(error) => {
			const message =
				(error.response && error.response.data && error.response.data.message) ||
				error.message ||
				error.toString();
			return Promise.reject(message);
		},
	);
};

export default  { listProduct }