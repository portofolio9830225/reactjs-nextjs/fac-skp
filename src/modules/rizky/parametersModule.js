import parameters_service from "../../services/rizky/parameters_service";

const listYear = () => {
	return parameters_service.listYear().then(
		(response) => {
			return Promise.resolve(response.data, response.data.message);
		},
		(error) => {
			const message =
				(error.response && error.response.data && error.response.data.message) ||
				error.message ||
				error.toString();
			return Promise.reject(message);
		},
	);
};

export default {listYear}