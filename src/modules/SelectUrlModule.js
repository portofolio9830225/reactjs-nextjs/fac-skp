import SelectUrlService from '../services/selectUrl.service';

const readSelect = (query_string) => {
	return SelectUrlService.readSelect(query_string).then(
		(response) => {
			return Promise.resolve(response.data, response.data.message);
		},
		(error) => {
			const message =
				(error.response && error.response.data && error.response.data.message) ||
				error.message ||
				error.toString();
			return Promise.reject(message);
		},
	);
};

export default { readSelect };
