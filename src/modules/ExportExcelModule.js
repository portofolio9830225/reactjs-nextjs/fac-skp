import ExportExcelService from '../services/export_excel.service';

const export_excel = (payload) => {
	return ExportExcelService.export_excel(payload).then(
		(response) => {
			return Promise.resolve(response);
		},
		(error) => {
			const message =
				(error.response && error.response.data && error.response.data.message) ||
				error.message ||
				error.toString();
			return Promise.reject(message);
		},
	);
};

const check_export_excel = (payload) => {
	return ExportExcelService.check_export_excel(payload).then(
		(response) => {
			return Promise.resolve(response);
		},
		(error) => {
			const message =
				(error.response && error.response.data && error.response.data.message) ||
				error.message ||
				error.toString();
			return Promise.reject(message);
		},
	);
};

export default { export_excel, check_export_excel };
