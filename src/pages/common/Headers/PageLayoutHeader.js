import React from 'react';
import classNames from 'classnames';
import Header from '../../../layout/Header/Header';
import CommonHeaderRight from './CommonHeaderRight';

const PageLayoutHeader = () => {
	return (
		<Header>
			<div className={classNames('header-left', 'col-md')}> </div>
			<CommonHeaderRight />
		</Header>
	);
};

export default PageLayoutHeader;
